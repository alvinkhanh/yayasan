/*
 * This combined file was created by the DataTables downloader builder:
 *   https://datatables.net/download
 *
 * To rebuild or modify this file with the latest versions of the included
 * software please visit:
 *   https://datatables.net/download/#bs4/dt-1.12.1/e-2.0.8/r-2.3.0
 *
 * Included libraries:
 *   DataTables 1.12.1, Editor 2.0.8, Responsive 2.3.0
 */

/*! DataTables 1.12.1
 * ©2008-2022 SpryMedia Ltd - datatables.net/license
 */

/**
 * @summary     DataTables
 * @description Paginate, search and order HTML tables
 * @version     1.12.1
 * @author      SpryMedia Ltd
 * @contact     www.datatables.net
 * @copyright   SpryMedia Ltd.
 *
 * This source file is free software, available under the following license:
 *   MIT license - http://datatables.net/license
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 * For details please refer to: http://www.datatables.net
 */

/*jslint evil: true, undef: true, browser: true */
/*globals $,require,jQuery,define,_selector_run,_selector_opts,_selector_first,_selector_row_indexes,_ext,_Api,_api_register,_api_registerPlural,_re_new_lines,_re_html,_re_formatted_numeric,_re_escape_regex,_empty,_intVal,_numToDecimal,_isNumber,_isHtml,_htmlNumeric,_pluck,_pluck_order,_range,_stripHtml,_unique,_fnBuildAjax,_fnAjaxUpdate,_fnAjaxParameters,_fnAjaxUpdateDraw,_fnAjaxDataSrc,_fnAddColumn,_fnColumnOptions,_fnAdjustColumnSizing,_fnVisibleToColumnIndex,_fnColumnIndexToVisible,_fnVisbleColumns,_fnGetColumns,_fnColumnTypes,_fnApplyColumnDefs,_fnHungarianMap,_fnCamelToHungarian,_fnLanguageCompat,_fnBrowserDetect,_fnAddData,_fnAddTr,_fnNodeToDataIndex,_fnNodeToColumnIndex,_fnGetCellData,_fnSetCellData,_fnSplitObjNotation,_fnGetObjectDataFn,_fnSetObjectDataFn,_fnGetDataMaster,_fnClearTable,_fnDeleteIndex,_fnInvalidate,_fnGetRowElements,_fnCreateTr,_fnBuildHead,_fnDrawHead,_fnDraw,_fnReDraw,_fnAddOptionsHtml,_fnDetectHeader,_fnGetUniqueThs,_fnFeatureHtmlFilter,_fnFilterComplete,_fnFilterCustom,_fnFilterColumn,_fnFilter,_fnFilterCreateSearch,_fnEscapeRegex,_fnFilterData,_fnFeatureHtmlInfo,_fnUpdateInfo,_fnInfoMacros,_fnInitialise,_fnInitComplete,_fnLengthChange,_fnFeatureHtmlLength,_fnFeatureHtmlPaginate,_fnPageChange,_fnFeatureHtmlProcessing,_fnProcessingDisplay,_fnFeatureHtmlTable,_fnScrollDraw,_fnApplyToChildren,_fnCalculateColumnWidths,_fnThrottle,_fnConvertToWidth,_fnGetWidestNode,_fnGetMaxLenString,_fnStringToCss,_fnSortFlatten,_fnSort,_fnSortAria,_fnSortListener,_fnSortAttachListener,_fnSortingClasses,_fnSortData,_fnSaveState,_fnLoadState,_fnSettingsFromNode,_fnLog,_fnMap,_fnBindAction,_fnCallbackReg,_fnCallbackFire,_fnLengthOverflow,_fnRenderer,_fnDataSource,_fnRowAttributes*/

(function( factory ) {
	"use strict";

	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				// CommonJS environments without a window global must pass a
				// root. This will give an error otherwise
				root = window;
			}

			if ( ! $ ) {
				$ = typeof window !== 'undefined' ? // jQuery's factory checks for a global window
					require('jquery') :
					require('jquery')( root );
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		window.DataTable = factory( jQuery, window, document );
	}
}
(function( $, window, document, undefined ) {
	"use strict";

	
	var DataTable = function ( selector, options )
	{
		// When creating with `new`, create a new DataTable, returning the API instance
		if (this instanceof DataTable) {
			return $(selector).DataTable(options);
		}
		else {
			// Argument switching
			options = selector;
		}
	
		/**
		 * Perform a jQuery selector action on the table's TR elements (from the tbody) and
		 * return the resulting jQuery object.
		 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
		 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
		 *  @param {string} [oOpts.filter=none] Select TR elements that meet the current filter
		 *    criterion ("applied") or all TR elements (i.e. no filter).
		 *  @param {string} [oOpts.order=current] Order of the TR elements in the processed array.
		 *    Can be either 'current', whereby the current sorting of the table is used, or
		 *    'original' whereby the original order the data was read into the table is used.
		 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
		 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
		 *    'current' and filter is 'applied', regardless of what they might be given as.
		 *  @returns {object} jQuery object, filtered by the given selector.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Highlight every second row
		 *      oTable.$('tr:odd').css('backgroundColor', 'blue');
		 *    } );
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Filter to rows with 'Webkit' in them, add a background colour and then
		 *      // remove the filter, thus highlighting the 'Webkit' rows only.
		 *      oTable.fnFilter('Webkit');
		 *      oTable.$('tr', {"search": "applied"}).css('backgroundColor', 'blue');
		 *      oTable.fnFilter('');
		 *    } );
		 */
		this.$ = function ( sSelector, oOpts )
		{
			return this.api(true).$( sSelector, oOpts );
		};
		
		
		/**
		 * Almost identical to $ in operation, but in this case returns the data for the matched
		 * rows - as such, the jQuery selector used should match TR row nodes or TD/TH cell nodes
		 * rather than any descendants, so the data can be obtained for the row/cell. If matching
		 * rows are found, the data returned is the original data array/object that was used to
		 * create the row (or a generated array if from a DOM source).
		 *
		 * This method is often useful in-combination with $ where both functions are given the
		 * same parameters and the array indexes will match identically.
		 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
		 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
		 *  @param {string} [oOpts.filter=none] Select elements that meet the current filter
		 *    criterion ("applied") or all elements (i.e. no filter).
		 *  @param {string} [oOpts.order=current] Order of the data in the processed array.
		 *    Can be either 'current', whereby the current sorting of the table is used, or
		 *    'original' whereby the original order the data was read into the table is used.
		 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
		 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
		 *    'current' and filter is 'applied', regardless of what they might be given as.
		 *  @returns {array} Data for the matched elements. If any elements, as a result of the
		 *    selector, were not TR, TD or TH elements in the DataTable, they will have a null
		 *    entry in the array.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Get the data from the first row in the table
		 *      var data = oTable._('tr:first');
		 *
		 *      // Do something useful with the data
		 *      alert( "First cell is: "+data[0] );
		 *    } );
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Filter to 'Webkit' and get all data for
		 *      oTable.fnFilter('Webkit');
		 *      var data = oTable._('tr', {"search": "applied"});
		 *
		 *      // Do something with the data
		 *      alert( data.length+" rows matched the search" );
		 *    } );
		 */
		this._ = function ( sSelector, oOpts )
		{
			return this.api(true).rows( sSelector, oOpts ).data();
		};
		
		
		/**
		 * Create a DataTables Api instance, with the currently selected tables for
		 * the Api's context.
		 * @param {boolean} [traditional=false] Set the API instance's context to be
		 *   only the table referred to by the `DataTable.ext.iApiIndex` option, as was
		 *   used in the API presented by DataTables 1.9- (i.e. the traditional mode),
		 *   or if all tables captured in the jQuery object should be used.
		 * @return {DataTables.Api}
		 */
		this.api = function ( traditional )
		{
			return traditional ?
				new _Api(
					_fnSettingsFromNode( this[ _ext.iApiIndex ] )
				) :
				new _Api( this );
		};
		
		
		/**
		 * Add a single new row or multiple rows of data to the table. Please note
		 * that this is suitable for client-side processing only - if you are using
		 * server-side processing (i.e. "bServerSide": true), then to add data, you
		 * must add it to the data source, i.e. the server-side, through an Ajax call.
		 *  @param {array|object} data The data to be added to the table. This can be:
		 *    <ul>
		 *      <li>1D array of data - add a single row with the data provided</li>
		 *      <li>2D array of arrays - add multiple rows in a single call</li>
		 *      <li>object - data object when using <i>mData</i></li>
		 *      <li>array of objects - multiple data objects when using <i>mData</i></li>
		 *    </ul>
		 *  @param {bool} [redraw=true] redraw the table or not
		 *  @returns {array} An array of integers, representing the list of indexes in
		 *    <i>aoData</i> ({@link DataTable.models.oSettings}) that have been added to
		 *    the table.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    // Global var for counter
		 *    var giCount = 2;
		 *
		 *    $(document).ready(function() {
		 *      $('#example').dataTable();
		 *    } );
		 *
		 *    function fnClickAddRow() {
		 *      $('#example').dataTable().fnAddData( [
		 *        giCount+".1",
		 *        giCount+".2",
		 *        giCount+".3",
		 *        giCount+".4" ]
		 *      );
		 *
		 *      giCount++;
		 *    }
		 */
		this.fnAddData = function( data, redraw )
		{
			var api = this.api( true );
		
			/* Check if we want to add multiple rows or not */
			var rows = Array.isArray(data) && ( Array.isArray(data[0]) || $.isPlainObject(data[0]) ) ?
				api.rows.add( data ) :
				api.row.add( data );
		
			if ( redraw === undefined || redraw ) {
				api.draw();
			}
		
			return rows.flatten().toArray();
		};
		
		
		/**
		 * This function will make DataTables recalculate the column sizes, based on the data
		 * contained in the table and the sizes applied to the columns (in the DOM, CSS or
		 * through the sWidth parameter). This can be useful when the width of the table's
		 * parent element changes (for example a window resize).
		 *  @param {boolean} [bRedraw=true] Redraw the table or not, you will typically want to
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable( {
		 *        "sScrollY": "200px",
		 *        "bPaginate": false
		 *      } );
		 *
		 *      $(window).on('resize', function () {
		 *        oTable.fnAdjustColumnSizing();
		 *      } );
		 *    } );
		 */
		this.fnAdjustColumnSizing = function ( bRedraw )
		{
			var api = this.api( true ).columns.adjust();
			var settings = api.settings()[0];
			var scroll = settings.oScroll;
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw( false );
			}
			else if ( scroll.sX !== "" || scroll.sY !== "" ) {
				/* If not redrawing, but scrolling, we want to apply the new column sizes anyway */
				_fnScrollDraw( settings );
			}
		};
		
		
		/**
		 * Quickly and simply clear a table
		 *  @param {bool} [bRedraw=true] redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Immediately 'nuke' the current rows (perhaps waiting for an Ajax callback...)
		 *      oTable.fnClearTable();
		 *    } );
		 */
		this.fnClearTable = function( bRedraw )
		{
			var api = this.api( true ).clear();
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw();
			}
		};
		
		
		/**
		 * The exact opposite of 'opening' a row, this function will close any rows which
		 * are currently 'open'.
		 *  @param {node} nTr the table row to 'close'
		 *  @returns {int} 0 on success, or 1 if failed (can't find the row)
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnClose = function( nTr )
		{
			this.api( true ).row( nTr ).child.hide();
		};
		
		
		/**
		 * Remove a row for the table
		 *  @param {mixed} target The index of the row from aoData to be deleted, or
		 *    the TR element you want to delete
		 *  @param {function|null} [callBack] Callback function
		 *  @param {bool} [redraw=true] Redraw the table or not
		 *  @returns {array} The row that was deleted
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Immediately remove the first row
		 *      oTable.fnDeleteRow( 0 );
		 *    } );
		 */
		this.fnDeleteRow = function( target, callback, redraw )
		{
			var api = this.api( true );
			var rows = api.rows( target );
			var settings = rows.settings()[0];
			var data = settings.aoData[ rows[0][0] ];
		
			rows.remove();
		
			if ( callback ) {
				callback.call( this, settings, data );
			}
		
			if ( redraw === undefined || redraw ) {
				api.draw();
			}
		
			return data;
		};
		
		
		/**
		 * Restore the table to it's original state in the DOM by removing all of DataTables
		 * enhancements, alterations to the DOM structure of the table and event listeners.
		 *  @param {boolean} [remove=false] Completely remove the table from the DOM
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      // This example is fairly pointless in reality, but shows how fnDestroy can be used
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnDestroy();
		 *    } );
		 */
		this.fnDestroy = function ( remove )
		{
			this.api( true ).destroy( remove );
		};
		
		
		/**
		 * Redraw the table
		 *  @param {bool} [complete=true] Re-filter and resort (if enabled) the table before the draw.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Re-draw the table - you wouldn't want to do it here, but it's an example :-)
		 *      oTable.fnDraw();
		 *    } );
		 */
		this.fnDraw = function( complete )
		{
			// Note that this isn't an exact match to the old call to _fnDraw - it takes
			// into account the new data, but can hold position.
			this.api( true ).draw( complete );
		};
		
		
		/**
		 * Filter the input based on data
		 *  @param {string} sInput String to filter the table on
		 *  @param {int|null} [iColumn] Column to limit filtering to
		 *  @param {bool} [bRegex=false] Treat as regular expression or not
		 *  @param {bool} [bSmart=true] Perform smart filtering or not
		 *  @param {bool} [bShowGlobal=true] Show the input global filter in it's input box(es)
		 *  @param {bool} [bCaseInsensitive=true] Do case-insensitive matching (true) or not (false)
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sometime later - filter...
		 *      oTable.fnFilter( 'test string' );
		 *    } );
		 */
		this.fnFilter = function( sInput, iColumn, bRegex, bSmart, bShowGlobal, bCaseInsensitive )
		{
			var api = this.api( true );
		
			if ( iColumn === null || iColumn === undefined ) {
				api.search( sInput, bRegex, bSmart, bCaseInsensitive );
			}
			else {
				api.column( iColumn ).search( sInput, bRegex, bSmart, bCaseInsensitive );
			}
		
			api.draw();
		};
		
		
		/**
		 * Get the data for the whole table, an individual row or an individual cell based on the
		 * provided parameters.
		 *  @param {int|node} [src] A TR row node, TD/TH cell node or an integer. If given as
		 *    a TR node then the data source for the whole row will be returned. If given as a
		 *    TD/TH cell node then iCol will be automatically calculated and the data for the
		 *    cell returned. If given as an integer, then this is treated as the aoData internal
		 *    data index for the row (see fnGetPosition) and the data for that row used.
		 *  @param {int} [col] Optional column index that you want the data of.
		 *  @returns {array|object|string} If mRow is undefined, then the data for all rows is
		 *    returned. If mRow is defined, just data for that row, and is iCol is
		 *    defined, only data for the designated cell is returned.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    // Row data
		 *    $(document).ready(function() {
		 *      oTable = $('#example').dataTable();
		 *
		 *      oTable.$('tr').click( function () {
		 *        var data = oTable.fnGetData( this );
		 *        // ... do something with the array / object of data for the row
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Individual cell data
		 *    $(document).ready(function() {
		 *      oTable = $('#example').dataTable();
		 *
		 *      oTable.$('td').click( function () {
		 *        var sData = oTable.fnGetData( this );
		 *        alert( 'The cell clicked on had the value of '+sData );
		 *      } );
		 *    } );
		 */
		this.fnGetData = function( src, col )
		{
			var api = this.api( true );
		
			if ( src !== undefined ) {
				var type = src.nodeName ? src.nodeName.toLowerCase() : '';
		
				return col !== undefined || type == 'td' || type == 'th' ?
					api.cell( src, col ).data() :
					api.row( src ).data() || null;
			}
		
			return api.data().toArray();
		};
		
		
		/**
		 * Get an array of the TR nodes that are used in the table's body. Note that you will
		 * typically want to use the '$' API method in preference to this as it is more
		 * flexible.
		 *  @param {int} [iRow] Optional row index for the TR element you want
		 *  @returns {array|node} If iRow is undefined, returns an array of all TR elements
		 *    in the table's body, or iRow is defined, just the TR element requested.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Get the nodes from the table
		 *      var nNodes = oTable.fnGetNodes( );
		 *    } );
		 */
		this.fnGetNodes = function( iRow )
		{
			var api = this.api( true );
		
			return iRow !== undefined ?
				api.row( iRow ).node() :
				api.rows().nodes().flatten().toArray();
		};
		
		
		/**
		 * Get the array indexes of a particular cell from it's DOM element
		 * and column index including hidden columns
		 *  @param {node} node this can either be a TR, TD or TH in the table's body
		 *  @returns {int} If nNode is given as a TR, then a single index is returned, or
		 *    if given as a cell, an array of [row index, column index (visible),
		 *    column index (all)] is given.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      $('#example tbody td').click( function () {
		 *        // Get the position of the current data from the node
		 *        var aPos = oTable.fnGetPosition( this );
		 *
		 *        // Get the data array for this row
		 *        var aData = oTable.fnGetData( aPos[0] );
		 *
		 *        // Update the data array and return the value
		 *        aData[ aPos[1] ] = 'clicked';
		 *        this.innerHTML = 'clicked';
		 *      } );
		 *
		 *      // Init DataTables
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnGetPosition = function( node )
		{
			var api = this.api( true );
			var nodeName = node.nodeName.toUpperCase();
		
			if ( nodeName == 'TR' ) {
				return api.row( node ).index();
			}
			else if ( nodeName == 'TD' || nodeName == 'TH' ) {
				var cell = api.cell( node ).index();
		
				return [
					cell.row,
					cell.columnVisible,
					cell.column
				];
			}
			return null;
		};
		
		
		/**
		 * Check to see if a row is 'open' or not.
		 *  @param {node} nTr the table row to check
		 *  @returns {boolean} true if the row is currently open, false otherwise
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnIsOpen = function( nTr )
		{
			return this.api( true ).row( nTr ).child.isShown();
		};
		
		
		/**
		 * This function will place a new row directly after a row which is currently
		 * on display on the page, with the HTML contents that is passed into the
		 * function. This can be used, for example, to ask for confirmation that a
		 * particular record should be deleted.
		 *  @param {node} nTr The table row to 'open'
		 *  @param {string|node|jQuery} mHtml The HTML to put into the row
		 *  @param {string} sClass Class to give the new TD cell
		 *  @returns {node} The row opened. Note that if the table row passed in as the
		 *    first parameter, is not found in the table, this method will silently
		 *    return.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnOpen = function( nTr, mHtml, sClass )
		{
			return this.api( true )
				.row( nTr )
				.child( mHtml, sClass )
				.show()
				.child()[0];
		};
		
		
		/**
		 * Change the pagination - provides the internal logic for pagination in a simple API
		 * function. With this function you can have a DataTables table go to the next,
		 * previous, first or last pages.
		 *  @param {string|int} mAction Paging action to take: "first", "previous", "next" or "last"
		 *    or page number to jump to (integer), note that page 0 is the first page.
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnPageChange( 'next' );
		 *    } );
		 */
		this.fnPageChange = function ( mAction, bRedraw )
		{
			var api = this.api( true ).page( mAction );
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw(false);
			}
		};
		
		
		/**
		 * Show a particular column
		 *  @param {int} iCol The column whose display should be changed
		 *  @param {bool} bShow Show (true) or hide (false) the column
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Hide the second column after initialisation
		 *      oTable.fnSetColumnVis( 1, false );
		 *    } );
		 */
		this.fnSetColumnVis = function ( iCol, bShow, bRedraw )
		{
			var api = this.api( true ).column( iCol ).visible( bShow );
		
			if ( bRedraw === undefined || bRedraw ) {
				api.columns.adjust().draw();
			}
		};
		
		
		/**
		 * Get the settings for a particular table for external manipulation
		 *  @returns {object} DataTables settings object. See
		 *    {@link DataTable.models.oSettings}
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      var oSettings = oTable.fnSettings();
		 *
		 *      // Show an example parameter from the settings
		 *      alert( oSettings._iDisplayStart );
		 *    } );
		 */
		this.fnSettings = function()
		{
			return _fnSettingsFromNode( this[_ext.iApiIndex] );
		};
		
		
		/**
		 * Sort the table by a particular column
		 *  @param {int} iCol the data index to sort on. Note that this will not match the
		 *    'display index' if you have hidden data entries
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sort immediately with columns 0 and 1
		 *      oTable.fnSort( [ [0,'asc'], [1,'asc'] ] );
		 *    } );
		 */
		this.fnSort = function( aaSort )
		{
			this.api( true ).order( aaSort ).draw();
		};
		
		
		/**
		 * Attach a sort listener to an element for a given column
		 *  @param {node} nNode the element to attach the sort listener to
		 *  @param {int} iColumn the column that a click on this node will sort on
		 *  @param {function} [fnCallback] callback function when sort is run
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sort on column 1, when 'sorter' is clicked on
		 *      oTable.fnSortListener( document.getElementById('sorter'), 1 );
		 *    } );
		 */
		this.fnSortListener = function( nNode, iColumn, fnCallback )
		{
			this.api( true ).order.listener( nNode, iColumn, fnCallback );
		};
		
		
		/**
		 * Update a table cell or row - this method will accept either a single value to
		 * update the cell with, an array of values with one element for each column or
		 * an object in the same format as the original data source. The function is
		 * self-referencing in order to make the multi column updates easier.
		 *  @param {object|array|string} mData Data to update the cell/row with
		 *  @param {node|int} mRow TR element you want to update or the aoData index
		 *  @param {int} [iColumn] The column to update, give as null or undefined to
		 *    update a whole row.
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @param {bool} [bAction=true] Perform pre-draw actions or not
		 *  @returns {int} 0 on success, 1 on error
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnUpdate( 'Example update', 0, 0 ); // Single cell
		 *      oTable.fnUpdate( ['a', 'b', 'c', 'd', 'e'], $('tbody tr')[0] ); // Row
		 *    } );
		 */
		this.fnUpdate = function( mData, mRow, iColumn, bRedraw, bAction )
		{
			var api = this.api( true );
		
			if ( iColumn === undefined || iColumn === null ) {
				api.row( mRow ).data( mData );
			}
			else {
				api.cell( mRow, iColumn ).data( mData );
			}
		
			if ( bAction === undefined || bAction ) {
				api.columns.adjust();
			}
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw();
			}
			return 0;
		};
		
		
		/**
		 * Provide a common method for plug-ins to check the version of DataTables being used, in order
		 * to ensure compatibility.
		 *  @param {string} sVersion Version string to check for, in the format "X.Y.Z". Note that the
		 *    formats "X" and "X.Y" are also acceptable.
		 *  @returns {boolean} true if this version of DataTables is greater or equal to the required
		 *    version, or false if this version of DataTales is not suitable
		 *  @method
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      alert( oTable.fnVersionCheck( '1.9.0' ) );
		 *    } );
		 */
		this.fnVersionCheck = _ext.fnVersionCheck;
		
	
		var _that = this;
		var emptyInit = options === undefined;
		var len = this.length;
	
		if ( emptyInit ) {
			options = {};
		}
	
		this.oApi = this.internal = _ext.internal;
	
		// Extend with old style plug-in API methods
		for ( var fn in DataTable.ext.internal ) {
			if ( fn ) {
				this[fn] = _fnExternApiFunc(fn);
			}
		}
	
		this.each(function() {
			// For each initialisation we want to give it a clean initialisation
			// object that can be bashed around
			var o = {};
			var oInit = len > 1 ? // optimisation for single table case
				_fnExtend( o, options, true ) :
				options;
	
			/*global oInit,_that,emptyInit*/
			var i=0, iLen, j, jLen, k, kLen;
			var sId = this.getAttribute( 'id' );
			var bInitHandedOff = false;
			var defaults = DataTable.defaults;
			var $this = $(this);
			
			
			/* Sanity check */
			if ( this.nodeName.toLowerCase() != 'table' )
			{
				_fnLog( null, 0, 'Non-table node initialisation ('+this.nodeName+')', 2 );
				return;
			}
			
			/* Backwards compatibility for the defaults */
			_fnCompatOpts( defaults );
			_fnCompatCols( defaults.column );
			
			/* Convert the camel-case defaults to Hungarian */
			_fnCamelToHungarian( defaults, defaults, true );
			_fnCamelToHungarian( defaults.column, defaults.column, true );
			
			/* Setting up the initialisation object */
			_fnCamelToHungarian( defaults, $.extend( oInit, $this.data() ), true );
			
			
			
			/* Check to see if we are re-initialising a table */
			var allSettings = DataTable.settings;
			for ( i=0, iLen=allSettings.length ; i<iLen ; i++ )
			{
				var s = allSettings[i];
			
				/* Base check on table node */
				if (
					s.nTable == this ||
					(s.nTHead && s.nTHead.parentNode == this) ||
					(s.nTFoot && s.nTFoot.parentNode == this)
				) {
					var bRetrieve = oInit.bRetrieve !== undefined ? oInit.bRetrieve : defaults.bRetrieve;
					var bDestroy = oInit.bDestroy !== undefined ? oInit.bDestroy : defaults.bDestroy;
			
					if ( emptyInit || bRetrieve )
					{
						return s.oInstance;
					}
					else if ( bDestroy )
					{
						s.oInstance.fnDestroy();
						break;
					}
					else
					{
						_fnLog( s, 0, 'Cannot reinitialise DataTable', 3 );
						return;
					}
				}
			
				/* If the element we are initialising has the same ID as a table which was previously
				 * initialised, but the table nodes don't match (from before) then we destroy the old
				 * instance by simply deleting it. This is under the assumption that the table has been
				 * destroyed by other methods. Anyone using non-id selectors will need to do this manually
				 */
				if ( s.sTableId == this.id )
				{
					allSettings.splice( i, 1 );
					break;
				}
			}
			
			/* Ensure the table has an ID - required for accessibility */
			if ( sId === null || sId === "" )
			{
				sId = "DataTables_Table_"+(DataTable.ext._unique++);
				this.id = sId;
			}
			
			/* Create the settings object for this table and set some of the default parameters */
			var oSettings = $.extend( true, {}, DataTable.models.oSettings, {
				"sDestroyWidth": $this[0].style.width,
				"sInstance":     sId,
				"sTableId":      sId
			} );
			oSettings.nTable = this;
			oSettings.oApi   = _that.internal;
			oSettings.oInit  = oInit;
			
			allSettings.push( oSettings );
			
			// Need to add the instance after the instance after the settings object has been added
			// to the settings array, so we can self reference the table instance if more than one
			oSettings.oInstance = (_that.length===1) ? _that : $this.dataTable();
			
			// Backwards compatibility, before we apply all the defaults
			_fnCompatOpts( oInit );
			_fnLanguageCompat( oInit.oLanguage );
			
			// If the length menu is given, but the init display length is not, use the length menu
			if ( oInit.aLengthMenu && ! oInit.iDisplayLength )
			{
				oInit.iDisplayLength = Array.isArray( oInit.aLengthMenu[0] ) ?
					oInit.aLengthMenu[0][0] : oInit.aLengthMenu[0];
			}
			
			// Apply the defaults and init options to make a single init object will all
			// options defined from defaults and instance options.
			oInit = _fnExtend( $.extend( true, {}, defaults ), oInit );
			
			
			// Map the initialisation options onto the settings object
			_fnMap( oSettings.oFeatures, oInit, [
				"bPaginate",
				"bLengthChange",
				"bFilter",
				"bSort",
				"bSortMulti",
				"bInfo",
				"bProcessing",
				"bAutoWidth",
				"bSortClasses",
				"bServerSide",
				"bDeferRender"
			] );
			_fnMap( oSettings, oInit, [
				"asStripeClasses",
				"ajax",
				"fnServerData",
				"fnFormatNumber",
				"sServerMethod",
				"aaSorting",
				"aaSortingFixed",
				"aLengthMenu",
				"sPaginationType",
				"sAjaxSource",
				"sAjaxDataProp",
				"iStateDuration",
				"sDom",
				"bSortCellsTop",
				"iTabIndex",
				"fnStateLoadCallback",
				"fnStateSaveCallback",
				"renderer",
				"searchDelay",
				"rowId",
				[ "iCookieDuration", "iStateDuration" ], // backwards compat
				[ "oSearch", "oPreviousSearch" ],
				[ "aoSearchCols", "aoPreSearchCols" ],
				[ "iDisplayLength", "_iDisplayLength" ]
			] );
			_fnMap( oSettings.oScroll, oInit, [
				[ "sScrollX", "sX" ],
				[ "sScrollXInner", "sXInner" ],
				[ "sScrollY", "sY" ],
				[ "bScrollCollapse", "bCollapse" ]
			] );
			_fnMap( oSettings.oLanguage, oInit, "fnInfoCallback" );
			
			/* Callback functions which are array driven */
			_fnCallbackReg( oSettings, 'aoDrawCallback',       oInit.fnDrawCallback,      'user' );
			_fnCallbackReg( oSettings, 'aoServerParams',       oInit.fnServerParams,      'user' );
			_fnCallbackReg( oSettings, 'aoStateSaveParams',    oInit.fnStateSaveParams,   'user' );
			_fnCallbackReg( oSettings, 'aoStateLoadParams',    oInit.fnStateLoadParams,   'user' );
			_fnCallbackReg( oSettings, 'aoStateLoaded',        oInit.fnStateLoaded,       'user' );
			_fnCallbackReg( oSettings, 'aoRowCallback',        oInit.fnRowCallback,       'user' );
			_fnCallbackReg( oSettings, 'aoRowCreatedCallback', oInit.fnCreatedRow,        'user' );
			_fnCallbackReg( oSettings, 'aoHeaderCallback',     oInit.fnHeaderCallback,    'user' );
			_fnCallbackReg( oSettings, 'aoFooterCallback',     oInit.fnFooterCallback,    'user' );
			_fnCallbackReg( oSettings, 'aoInitComplete',       oInit.fnInitComplete,      'user' );
			_fnCallbackReg( oSettings, 'aoPreDrawCallback',    oInit.fnPreDrawCallback,   'user' );
			
			oSettings.rowIdFn = _fnGetObjectDataFn( oInit.rowId );
			
			/* Browser support detection */
			_fnBrowserDetect( oSettings );
			
			var oClasses = oSettings.oClasses;
			
			$.extend( oClasses, DataTable.ext.classes, oInit.oClasses );
			$this.addClass( oClasses.sTable );
			
			
			if ( oSettings.iInitDisplayStart === undefined )
			{
				/* Display start point, taking into account the save saving */
				oSettings.iInitDisplayStart = oInit.iDisplayStart;
				oSettings._iDisplayStart = oInit.iDisplayStart;
			}
			
			if ( oInit.iDeferLoading !== null )
			{
				oSettings.bDeferLoading = true;
				var tmp = Array.isArray( oInit.iDeferLoading );
				oSettings._iRecordsDisplay = tmp ? oInit.iDeferLoading[0] : oInit.iDeferLoading;
				oSettings._iRecordsTotal = tmp ? oInit.iDeferLoading[1] : oInit.iDeferLoading;
			}
			
			/* Language definitions */
			var oLanguage = oSettings.oLanguage;
			$.extend( true, oLanguage, oInit.oLanguage );
			
			if ( oLanguage.sUrl )
			{
				/* Get the language definitions from a file - because this Ajax call makes the language
				 * get async to the remainder of this function we use bInitHandedOff to indicate that
				 * _fnInitialise will be fired by the returned Ajax handler, rather than the constructor
				 */
				$.ajax( {
					dataType: 'json',
					url: oLanguage.sUrl,
					success: function ( json ) {
						_fnCamelToHungarian( defaults.oLanguage, json );
						_fnLanguageCompat( json );
						$.extend( true, oLanguage, json, oSettings.oInit.oLanguage );
			
						_fnCallbackFire( oSettings, null, 'i18n', [oSettings]);
						_fnInitialise( oSettings );
					},
					error: function () {
						// Error occurred loading language file, continue on as best we can
						_fnInitialise( oSettings );
					}
				} );
				bInitHandedOff = true;
			}
			else {
				_fnCallbackFire( oSettings, null, 'i18n', [oSettings]);
			}
			
			/*
			 * Stripes
			 */
			if ( oInit.asStripeClasses === null )
			{
				oSettings.asStripeClasses =[
					oClasses.sStripeOdd,
					oClasses.sStripeEven
				];
			}
			
			/* Remove row stripe classes if they are already on the table row */
			var stripeClasses = oSettings.asStripeClasses;
			var rowOne = $this.children('tbody').find('tr').eq(0);
			if ( $.inArray( true, $.map( stripeClasses, function(el, i) {
				return rowOne.hasClass(el);
			} ) ) !== -1 ) {
				$('tbody tr', this).removeClass( stripeClasses.join(' ') );
				oSettings.asDestroyStripes = stripeClasses.slice();
			}
			
			/*
			 * Columns
			 * See if we should load columns automatically or use defined ones
			 */
			var anThs = [];
			var aoColumnsInit;
			var nThead = this.getElementsByTagName('thead');
			if ( nThead.length !== 0 )
			{
				_fnDetectHeader( oSettings.aoHeader, nThead[0] );
				anThs = _fnGetUniqueThs( oSettings );
			}
			
			/* If not given a column array, generate one with nulls */
			if ( oInit.aoColumns === null )
			{
				aoColumnsInit = [];
				for ( i=0, iLen=anThs.length ; i<iLen ; i++ )
				{
					aoColumnsInit.push( null );
				}
			}
			else
			{
				aoColumnsInit = oInit.aoColumns;
			}
			
			/* Add the columns */
			for ( i=0, iLen=aoColumnsInit.length ; i<iLen ; i++ )
			{
				_fnAddColumn( oSettings, anThs ? anThs[i] : null );
			}
			
			/* Apply the column definitions */
			_fnApplyColumnDefs( oSettings, oInit.aoColumnDefs, aoColumnsInit, function (iCol, oDef) {
				_fnColumnOptions( oSettings, iCol, oDef );
			} );
			
			/* HTML5 attribute detection - build an mData object automatically if the
			 * attributes are found
			 */
			if ( rowOne.length ) {
				var a = function ( cell, name ) {
					return cell.getAttribute( 'data-'+name ) !== null ? name : null;
				};
			
				$( rowOne[0] ).children('th, td').each( function (i, cell) {
					var col = oSettings.aoColumns[i];
			
					if ( col.mData === i ) {
						var sort = a( cell, 'sort' ) || a( cell, 'order' );
						var filter = a( cell, 'filter' ) || a( cell, 'search' );
			
						if ( sort !== null || filter !== null ) {
							col.mData = {
								_:      i+'.display',
								sort:   sort !== null   ? i+'.@data-'+sort   : undefined,
								type:   sort !== null   ? i+'.@data-'+sort   : undefined,
								filter: filter !== null ? i+'.@data-'+filter : undefined
							};
			
							_fnColumnOptions( oSettings, i );
						}
					}
				} );
			}
			
			var features = oSettings.oFeatures;
			var loadedInit = function () {
				/*
				 * Sorting
				 * @todo For modularisation (1.11) this needs to do into a sort start up handler
				 */
			
				// If aaSorting is not defined, then we use the first indicator in asSorting
				// in case that has been altered, so the default sort reflects that option
				if ( oInit.aaSorting === undefined ) {
					var sorting = oSettings.aaSorting;
					for ( i=0, iLen=sorting.length ; i<iLen ; i++ ) {
						sorting[i][1] = oSettings.aoColumns[ i ].asSorting[0];
					}
				}
			
				/* Do a first pass on the sorting classes (allows any size changes to be taken into
				 * account, and also will apply sorting disabled classes if disabled
				 */
				_fnSortingClasses( oSettings );
			
				if ( features.bSort ) {
					_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
						if ( oSettings.bSorted ) {
							var aSort = _fnSortFlatten( oSettings );
							var sortedColumns = {};
			
							$.each( aSort, function (i, val) {
								sortedColumns[ val.src ] = val.dir;
							} );
			
							_fnCallbackFire( oSettings, null, 'order', [oSettings, aSort, sortedColumns] );
							_fnSortAria( oSettings );
						}
					} );
				}
			
				_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
					if ( oSettings.bSorted || _fnDataSource( oSettings ) === 'ssp' || features.bDeferRender ) {
						_fnSortingClasses( oSettings );
					}
				}, 'sc' );
			
			
				/*
				 * Final init
				 * Cache the header, body and footer as required, creating them if needed
				 */
			
				// Work around for Webkit bug 83867 - store the caption-side before removing from doc
				var captions = $this.children('caption').each( function () {
					this._captionSide = $(this).css('caption-side');
				} );
			
				var thead = $this.children('thead');
				if ( thead.length === 0 ) {
					thead = $('<thead/>').appendTo($this);
				}
				oSettings.nTHead = thead[0];
			
				var tbody = $this.children('tbody');
				if ( tbody.length === 0 ) {
					tbody = $('<tbody/>').insertAfter(thead);
				}
				oSettings.nTBody = tbody[0];
			
				var tfoot = $this.children('tfoot');
				if ( tfoot.length === 0 && captions.length > 0 && (oSettings.oScroll.sX !== "" || oSettings.oScroll.sY !== "") ) {
					// If we are a scrolling table, and no footer has been given, then we need to create
					// a tfoot element for the caption element to be appended to
					tfoot = $('<tfoot/>').appendTo($this);
				}
			
				if ( tfoot.length === 0 || tfoot.children().length === 0 ) {
					$this.addClass( oClasses.sNoFooter );
				}
				else if ( tfoot.length > 0 ) {
					oSettings.nTFoot = tfoot[0];
					_fnDetectHeader( oSettings.aoFooter, oSettings.nTFoot );
				}
			
				/* Check if there is data passing into the constructor */
				if ( oInit.aaData ) {
					for ( i=0 ; i<oInit.aaData.length ; i++ ) {
						_fnAddData( oSettings, oInit.aaData[ i ] );
					}
				}
				else if ( oSettings.bDeferLoading || _fnDataSource( oSettings ) == 'dom' ) {
					/* Grab the data from the page - only do this when deferred loading or no Ajax
					 * source since there is no point in reading the DOM data if we are then going
					 * to replace it with Ajax data
					 */
					_fnAddTr( oSettings, $(oSettings.nTBody).children('tr') );
				}
			
				/* Copy the data index array */
				oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
			
				/* Initialisation complete - table can be drawn */
				oSettings.bInitialised = true;
			
				/* Check if we need to initialise the table (it might not have been handed off to the
				 * language processor)
				 */
				if ( bInitHandedOff === false ) {
					_fnInitialise( oSettings );
				}
			};
			
			/* Must be done after everything which can be overridden by the state saving! */
			_fnCallbackReg( oSettings, 'aoDrawCallback', _fnSaveState, 'state_save' );
			
			if ( oInit.bStateSave )
			{
				features.bStateSave = true;
				_fnLoadState( oSettings, oInit, loadedInit );
			}
			else {
				loadedInit();
			}
			
		} );
		_that = null;
		return this;
	};
	
	
	/*
	 * It is useful to have variables which are scoped locally so only the
	 * DataTables functions can access them and they don't leak into global space.
	 * At the same time these functions are often useful over multiple files in the
	 * core and API, so we list, or at least document, all variables which are used
	 * by DataTables as private variables here. This also ensures that there is no
	 * clashing of variable names and that they can easily referenced for reuse.
	 */
	
	
	// Defined else where
	//  _selector_run
	//  _selector_opts
	//  _selector_first
	//  _selector_row_indexes
	
	var _ext; // DataTable.ext
	var _Api; // DataTable.Api
	var _api_register; // DataTable.Api.register
	var _api_registerPlural; // DataTable.Api.registerPlural
	
	var _re_dic = {};
	var _re_new_lines = /[\r\n\u2028]/g;
	var _re_html = /<.*?>/g;
	
	// This is not strict ISO8601 - Date.parse() is quite lax, although
	// implementations differ between browsers.
	var _re_date = /^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/;
	
	// Escape regular expression special characters
	var _re_escape_regex = new RegExp( '(\\' + [ '/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\', '$', '^', '-' ].join('|\\') + ')', 'g' );
	
	// http://en.wikipedia.org/wiki/Foreign_exchange_market
	// - \u20BD - Russian ruble.
	// - \u20a9 - South Korean Won
	// - \u20BA - Turkish Lira
	// - \u20B9 - Indian Rupee
	// - R - Brazil (R$) and South Africa
	// - fr - Swiss Franc
	// - kr - Swedish krona, Norwegian krone and Danish krone
	// - \u2009 is thin space and \u202F is narrow no-break space, both used in many
	// - Ƀ - Bitcoin
	// - Ξ - Ethereum
	//   standards as thousands separators.
	var _re_formatted_numeric = /['\u00A0,$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfkɃΞ]/gi;
	
	
	var _empty = function ( d ) {
		return !d || d === true || d === '-' ? true : false;
	};
	
	
	var _intVal = function ( s ) {
		var integer = parseInt( s, 10 );
		return !isNaN(integer) && isFinite(s) ? integer : null;
	};
	
	// Convert from a formatted number with characters other than `.` as the
	// decimal place, to a Javascript number
	var _numToDecimal = function ( num, decimalPoint ) {
		// Cache created regular expressions for speed as this function is called often
		if ( ! _re_dic[ decimalPoint ] ) {
			_re_dic[ decimalPoint ] = new RegExp( _fnEscapeRegex( decimalPoint ), 'g' );
		}
		return typeof num === 'string' && decimalPoint !== '.' ?
			num.replace( /\./g, '' ).replace( _re_dic[ decimalPoint ], '.' ) :
			num;
	};
	
	
	var _isNumber = function ( d, decimalPoint, formatted ) {
		var strType = typeof d === 'string';
	
		// If empty return immediately so there must be a number if it is a
		// formatted string (this stops the string "k", or "kr", etc being detected
		// as a formatted number for currency
		if ( _empty( d ) ) {
			return true;
		}
	
		if ( decimalPoint && strType ) {
			d = _numToDecimal( d, decimalPoint );
		}
	
		if ( formatted && strType ) {
			d = d.replace( _re_formatted_numeric, '' );
		}
	
		return !isNaN( parseFloat(d) ) && isFinite( d );
	};
	
	
	// A string without HTML in it can be considered to be HTML still
	var _isHtml = function ( d ) {
		return _empty( d ) || typeof d === 'string';
	};
	
	
	var _htmlNumeric = function ( d, decimalPoint, formatted ) {
		if ( _empty( d ) ) {
			return true;
		}
	
		var html = _isHtml( d );
		return ! html ?
			null :
			_isNumber( _stripHtml( d ), decimalPoint, formatted ) ?
				true :
				null;
	};
	
	
	var _pluck = function ( a, prop, prop2 ) {
		var out = [];
		var i=0, ien=a.length;
	
		// Could have the test in the loop for slightly smaller code, but speed
		// is essential here
		if ( prop2 !== undefined ) {
			for ( ; i<ien ; i++ ) {
				if ( a[i] && a[i][ prop ] ) {
					out.push( a[i][ prop ][ prop2 ] );
				}
			}
		}
		else {
			for ( ; i<ien ; i++ ) {
				if ( a[i] ) {
					out.push( a[i][ prop ] );
				}
			}
		}
	
		return out;
	};
	
	
	// Basically the same as _pluck, but rather than looping over `a` we use `order`
	// as the indexes to pick from `a`
	var _pluck_order = function ( a, order, prop, prop2 )
	{
		var out = [];
		var i=0, ien=order.length;
	
		// Could have the test in the loop for slightly smaller code, but speed
		// is essential here
		if ( prop2 !== undefined ) {
			for ( ; i<ien ; i++ ) {
				if ( a[ order[i] ][ prop ] ) {
					out.push( a[ order[i] ][ prop ][ prop2 ] );
				}
			}
		}
		else {
			for ( ; i<ien ; i++ ) {
				out.push( a[ order[i] ][ prop ] );
			}
		}
	
		return out;
	};
	
	
	var _range = function ( len, start )
	{
		var out = [];
		var end;
	
		if ( start === undefined ) {
			start = 0;
			end = len;
		}
		else {
			end = start;
			start = len;
		}
	
		for ( var i=start ; i<end ; i++ ) {
			out.push( i );
		}
	
		return out;
	};
	
	
	var _removeEmpty = function ( a )
	{
		var out = [];
	
		for ( var i=0, ien=a.length ; i<ien ; i++ ) {
			if ( a[i] ) { // careful - will remove all falsy values!
				out.push( a[i] );
			}
		}
	
		return out;
	};
	
	
	var _stripHtml = function ( d ) {
		return d.replace( _re_html, '' );
	};
	
	
	/**
	 * Determine if all values in the array are unique. This means we can short
	 * cut the _unique method at the cost of a single loop. A sorted array is used
	 * to easily check the values.
	 *
	 * @param  {array} src Source array
	 * @return {boolean} true if all unique, false otherwise
	 * @ignore
	 */
	var _areAllUnique = function ( src ) {
		if ( src.length < 2 ) {
			return true;
		}
	
		var sorted = src.slice().sort();
		var last = sorted[0];
	
		for ( var i=1, ien=sorted.length ; i<ien ; i++ ) {
			if ( sorted[i] === last ) {
				return false;
			}
	
			last = sorted[i];
		}
	
		return true;
	};
	
	
	/**
	 * Find the unique elements in a source array.
	 *
	 * @param  {array} src Source array
	 * @return {array} Array of unique items
	 * @ignore
	 */
	var _unique = function ( src )
	{
		if ( _areAllUnique( src ) ) {
			return src.slice();
		}
	
		// A faster unique method is to use object keys to identify used values,
		// but this doesn't work with arrays or objects, which we must also
		// consider. See jsperf.com/compare-array-unique-versions/4 for more
		// information.
		var
			out = [],
			val,
			i, ien=src.length,
			j, k=0;
	
		again: for ( i=0 ; i<ien ; i++ ) {
			val = src[i];
	
			for ( j=0 ; j<k ; j++ ) {
				if ( out[j] === val ) {
					continue again;
				}
			}
	
			out.push( val );
			k++;
		}
	
		return out;
	};
	
	// Surprisingly this is faster than [].concat.apply
	// https://jsperf.com/flatten-an-array-loop-vs-reduce/2
	var _flatten = function (out, val) {
		if (Array.isArray(val)) {
			for (var i=0 ; i<val.length ; i++) {
				_flatten(out, val[i]);
			}
		}
		else {
			out.push(val);
		}
	  
		return out;
	}
	
	var _includes = function (search, start) {
		if (start === undefined) {
			start = 0;
		}
	
		return this.indexOf(search, start) !== -1;	
	};
	
	// Array.isArray polyfill.
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
	if (! Array.isArray) {
	    Array.isArray = function(arg) {
	        return Object.prototype.toString.call(arg) === '[object Array]';
	    };
	}
	
	if (! Array.prototype.includes) {
		Array.prototype.includes = _includes;
	}
	
	// .trim() polyfill
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trim
	if (!String.prototype.trim) {
	  String.prototype.trim = function () {
	    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	  };
	}
	
	if (! String.prototype.includes) {
		String.prototype.includes = _includes;
	}
	
	/**
	 * DataTables utility methods
	 * 
	 * This namespace provides helper methods that DataTables uses internally to
	 * create a DataTable, but which are not exclusively used only for DataTables.
	 * These methods can be used by extension authors to save the duplication of
	 * code.
	 *
	 *  @namespace
	 */
	DataTable.util = {
		/**
		 * Throttle the calls to a function. Arguments and context are maintained
		 * for the throttled function.
		 *
		 * @param {function} fn Function to be called
		 * @param {integer} freq Call frequency in mS
		 * @return {function} Wrapped function
		 */
		throttle: function ( fn, freq ) {
			var
				frequency = freq !== undefined ? freq : 200,
				last,
				timer;
	
			return function () {
				var
					that = this,
					now  = +new Date(),
					args = arguments;
	
				if ( last && now < last + frequency ) {
					clearTimeout( timer );
	
					timer = setTimeout( function () {
						last = undefined;
						fn.apply( that, args );
					}, frequency );
				}
				else {
					last = now;
					fn.apply( that, args );
				}
			};
		},
	
	
		/**
		 * Escape a string such that it can be used in a regular expression
		 *
		 *  @param {string} val string to escape
		 *  @returns {string} escaped string
		 */
		escapeRegex: function ( val ) {
			return val.replace( _re_escape_regex, '\\$1' );
		},
	
		/**
		 * Create a function that will write to a nested object or array
		 * @param {*} source JSON notation string
		 * @returns Write function
		 */
		set: function ( source ) {
			if ( $.isPlainObject( source ) ) {
				/* Unlike get, only the underscore (global) option is used for for
				 * setting data since we don't know the type here. This is why an object
				 * option is not documented for `mData` (which is read/write), but it is
				 * for `mRender` which is read only.
				 */
				return DataTable.util.set( source._ );
			}
			else if ( source === null ) {
				// Nothing to do when the data source is null
				return function () {};
			}
			else if ( typeof source === 'function' ) {
				return function (data, val, meta) {
					source( data, 'set', val, meta );
				};
			}
			else if ( typeof source === 'string' && (source.indexOf('.') !== -1 ||
					  source.indexOf('[') !== -1 || source.indexOf('(') !== -1) )
			{
				// Like the get, we need to get data from a nested object
				var setData = function (data, val, src) {
					var a = _fnSplitObjNotation( src ), b;
					var aLast = a[a.length-1];
					var arrayNotation, funcNotation, o, innerSrc;
		
					for ( var i=0, iLen=a.length-1 ; i<iLen ; i++ ) {
						// Protect against prototype pollution
						if (a[i] === '__proto__' || a[i] === 'constructor') {
							throw new Error('Cannot set prototype values');
						}
		
						// Check if we are dealing with an array notation request
						arrayNotation = a[i].match(__reArray);
						funcNotation = a[i].match(__reFn);
		
						if ( arrayNotation ) {
							a[i] = a[i].replace(__reArray, '');
							data[ a[i] ] = [];
		
							// Get the remainder of the nested object to set so we can recurse
							b = a.slice();
							b.splice( 0, i+1 );
							innerSrc = b.join('.');
		
							// Traverse each entry in the array setting the properties requested
							if ( Array.isArray( val ) ) {
								for ( var j=0, jLen=val.length ; j<jLen ; j++ ) {
									o = {};
									setData( o, val[j], innerSrc );
									data[ a[i] ].push( o );
								}
							}
							else {
								// We've been asked to save data to an array, but it
								// isn't array data to be saved. Best that can be done
								// is to just save the value.
								data[ a[i] ] = val;
							}
		
							// The inner call to setData has already traversed through the remainder
							// of the source and has set the data, thus we can exit here
							return;
						}
						else if ( funcNotation ) {
							// Function call
							a[i] = a[i].replace(__reFn, '');
							data = data[ a[i] ]( val );
						}
		
						// If the nested object doesn't currently exist - since we are
						// trying to set the value - create it
						if ( data[ a[i] ] === null || data[ a[i] ] === undefined ) {
							data[ a[i] ] = {};
						}
						data = data[ a[i] ];
					}
		
					// Last item in the input - i.e, the actual set
					if ( aLast.match(__reFn ) ) {
						// Function call
						data = data[ aLast.replace(__reFn, '') ]( val );
					}
					else {
						// If array notation is used, we just want to strip it and use the property name
						// and assign the value. If it isn't used, then we get the result we want anyway
						data[ aLast.replace(__reArray, '') ] = val;
					}
				};
		
				return function (data, val) { // meta is also passed in, but not used
					return setData( data, val, source );
				};
			}
			else {
				// Array or flat object mapping
				return function (data, val) { // meta is also passed in, but not used
					data[source] = val;
				};
			}
		},
	
		/**
		 * Create a function that will read nested objects from arrays, based on JSON notation
		 * @param {*} source JSON notation string
		 * @returns Value read
		 */
		get: function ( source ) {
			if ( $.isPlainObject( source ) ) {
				// Build an object of get functions, and wrap them in a single call
				var o = {};
				$.each( source, function (key, val) {
					if ( val ) {
						o[key] = DataTable.util.get( val );
					}
				} );
		
				return function (data, type, row, meta) {
					var t = o[type] || o._;
					return t !== undefined ?
						t(data, type, row, meta) :
						data;
				};
			}
			else if ( source === null ) {
				// Give an empty string for rendering / sorting etc
				return function (data) { // type, row and meta also passed, but not used
					return data;
				};
			}
			else if ( typeof source === 'function' ) {
				return function (data, type, row, meta) {
					return source( data, type, row, meta );
				};
			}
			else if ( typeof source === 'string' && (source.indexOf('.') !== -1 ||
					  source.indexOf('[') !== -1 || source.indexOf('(') !== -1) )
			{
				/* If there is a . in the source string then the data source is in a
				 * nested object so we loop over the data for each level to get the next
				 * level down. On each loop we test for undefined, and if found immediately
				 * return. This allows entire objects to be missing and sDefaultContent to
				 * be used if defined, rather than throwing an error
				 */
				var fetchData = function (data, type, src) {
					var arrayNotation, funcNotation, out, innerSrc;
		
					if ( src !== "" ) {
						var a = _fnSplitObjNotation( src );
		
						for ( var i=0, iLen=a.length ; i<iLen ; i++ ) {
							// Check if we are dealing with special notation
							arrayNotation = a[i].match(__reArray);
							funcNotation = a[i].match(__reFn);
		
							if ( arrayNotation ) {
								// Array notation
								a[i] = a[i].replace(__reArray, '');
		
								// Condition allows simply [] to be passed in
								if ( a[i] !== "" ) {
									data = data[ a[i] ];
								}
								out = [];
		
								// Get the remainder of the nested object to get
								a.splice( 0, i+1 );
								innerSrc = a.join('.');
		
								// Traverse each entry in the array getting the properties requested
								if ( Array.isArray( data ) ) {
									for ( var j=0, jLen=data.length ; j<jLen ; j++ ) {
										out.push( fetchData( data[j], type, innerSrc ) );
									}
								}
		
								// If a string is given in between the array notation indicators, that
								// is used to join the strings together, otherwise an array is returned
								var join = arrayNotation[0].substring(1, arrayNotation[0].length-1);
								data = (join==="") ? out : out.join(join);
		
								// The inner call to fetchData has already traversed through the remainder
								// of the source requested, so we exit from the loop
								break;
							}
							else if ( funcNotation ) {
								// Function call
								a[i] = a[i].replace(__reFn, '');
								data = data[ a[i] ]();
								continue;
							}
		
							if ( data === null || data[ a[i] ] === undefined ) {
								return undefined;
							}
	
							data = data[ a[i] ];
						}
					}
		
					return data;
				};
		
				return function (data, type) { // row and meta also passed, but not used
					return fetchData( data, type, source );
				};
			}
			else {
				// Array or flat object mapping
				return function (data, type) { // row and meta also passed, but not used
					return data[source];
				};
			}
		}
	};
	
	
	
	/**
	 * Create a mapping object that allows camel case parameters to be looked up
	 * for their Hungarian counterparts. The mapping is stored in a private
	 * parameter called `_hungarianMap` which can be accessed on the source object.
	 *  @param {object} o
	 *  @memberof DataTable#oApi
	 */
	function _fnHungarianMap ( o )
	{
		var
			hungarian = 'a aa ai ao as b fn i m o s ',
			match,
			newKey,
			map = {};
	
		$.each( o, function (key, val) {
			match = key.match(/^([^A-Z]+?)([A-Z])/);
	
			if ( match && hungarian.indexOf(match[1]+' ') !== -1 )
			{
				newKey = key.replace( match[0], match[2].toLowerCase() );
				map[ newKey ] = key;
	
				if ( match[1] === 'o' )
				{
					_fnHungarianMap( o[key] );
				}
			}
		} );
	
		o._hungarianMap = map;
	}
	
	
	/**
	 * Convert from camel case parameters to Hungarian, based on a Hungarian map
	 * created by _fnHungarianMap.
	 *  @param {object} src The model object which holds all parameters that can be
	 *    mapped.
	 *  @param {object} user The object to convert from camel case to Hungarian.
	 *  @param {boolean} force When set to `true`, properties which already have a
	 *    Hungarian value in the `user` object will be overwritten. Otherwise they
	 *    won't be.
	 *  @memberof DataTable#oApi
	 */
	function _fnCamelToHungarian ( src, user, force )
	{
		if ( ! src._hungarianMap ) {
			_fnHungarianMap( src );
		}
	
		var hungarianKey;
	
		$.each( user, function (key, val) {
			hungarianKey = src._hungarianMap[ key ];
	
			if ( hungarianKey !== undefined && (force || user[hungarianKey] === undefined) )
			{
				// For objects, we need to buzz down into the object to copy parameters
				if ( hungarianKey.charAt(0) === 'o' )
				{
					// Copy the camelCase options over to the hungarian
					if ( ! user[ hungarianKey ] ) {
						user[ hungarianKey ] = {};
					}
					$.extend( true, user[hungarianKey], user[key] );
	
					_fnCamelToHungarian( src[hungarianKey], user[hungarianKey], force );
				}
				else {
					user[hungarianKey] = user[ key ];
				}
			}
		} );
	}
	
	
	/**
	 * Language compatibility - when certain options are given, and others aren't, we
	 * need to duplicate the values over, in order to provide backwards compatibility
	 * with older language files.
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnLanguageCompat( lang )
	{
		// Note the use of the Hungarian notation for the parameters in this method as
		// this is called after the mapping of camelCase to Hungarian
		var defaults = DataTable.defaults.oLanguage;
	
		// Default mapping
		var defaultDecimal = defaults.sDecimal;
		if ( defaultDecimal ) {
			_addNumericSort( defaultDecimal );
		}
	
		if ( lang ) {
			var zeroRecords = lang.sZeroRecords;
	
			// Backwards compatibility - if there is no sEmptyTable given, then use the same as
			// sZeroRecords - assuming that is given.
			if ( ! lang.sEmptyTable && zeroRecords &&
				defaults.sEmptyTable === "No data available in table" )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sEmptyTable' );
			}
	
			// Likewise with loading records
			if ( ! lang.sLoadingRecords && zeroRecords &&
				defaults.sLoadingRecords === "Loading..." )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sLoadingRecords' );
			}
	
			// Old parameter name of the thousands separator mapped onto the new
			if ( lang.sInfoThousands ) {
				lang.sThousands = lang.sInfoThousands;
			}
	
			var decimal = lang.sDecimal;
			if ( decimal && defaultDecimal !== decimal ) {
				_addNumericSort( decimal );
			}
		}
	}
	
	
	/**
	 * Map one parameter onto another
	 *  @param {object} o Object to map
	 *  @param {*} knew The new parameter name
	 *  @param {*} old The old parameter name
	 */
	var _fnCompatMap = function ( o, knew, old ) {
		if ( o[ knew ] !== undefined ) {
			o[ old ] = o[ knew ];
		}
	};
	
	
	/**
	 * Provide backwards compatibility for the main DT options. Note that the new
	 * options are mapped onto the old parameters, so this is an external interface
	 * change only.
	 *  @param {object} init Object to map
	 */
	function _fnCompatOpts ( init )
	{
		_fnCompatMap( init, 'ordering',      'bSort' );
		_fnCompatMap( init, 'orderMulti',    'bSortMulti' );
		_fnCompatMap( init, 'orderClasses',  'bSortClasses' );
		_fnCompatMap( init, 'orderCellsTop', 'bSortCellsTop' );
		_fnCompatMap( init, 'order',         'aaSorting' );
		_fnCompatMap( init, 'orderFixed',    'aaSortingFixed' );
		_fnCompatMap( init, 'paging',        'bPaginate' );
		_fnCompatMap( init, 'pagingType',    'sPaginationType' );
		_fnCompatMap( init, 'pageLength',    'iDisplayLength' );
		_fnCompatMap( init, 'searching',     'bFilter' );
	
		// Boolean initialisation of x-scrolling
		if ( typeof init.sScrollX === 'boolean' ) {
			init.sScrollX = init.sScrollX ? '100%' : '';
		}
		if ( typeof init.scrollX === 'boolean' ) {
			init.scrollX = init.scrollX ? '100%' : '';
		}
	
		// Column search objects are in an array, so it needs to be converted
		// element by element
		var searchCols = init.aoSearchCols;
	
		if ( searchCols ) {
			for ( var i=0, ien=searchCols.length ; i<ien ; i++ ) {
				if ( searchCols[i] ) {
					_fnCamelToHungarian( DataTable.models.oSearch, searchCols[i] );
				}
			}
		}
	}
	
	
	/**
	 * Provide backwards compatibility for column options. Note that the new options
	 * are mapped onto the old parameters, so this is an external interface change
	 * only.
	 *  @param {object} init Object to map
	 */
	function _fnCompatCols ( init )
	{
		_fnCompatMap( init, 'orderable',     'bSortable' );
		_fnCompatMap( init, 'orderData',     'aDataSort' );
		_fnCompatMap( init, 'orderSequence', 'asSorting' );
		_fnCompatMap( init, 'orderDataType', 'sortDataType' );
	
		// orderData can be given as an integer
		var dataSort = init.aDataSort;
		if ( typeof dataSort === 'number' && ! Array.isArray( dataSort ) ) {
			init.aDataSort = [ dataSort ];
		}
	}
	
	
	/**
	 * Browser feature detection for capabilities, quirks
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnBrowserDetect( settings )
	{
		// We don't need to do this every time DataTables is constructed, the values
		// calculated are specific to the browser and OS configuration which we
		// don't expect to change between initialisations
		if ( ! DataTable.__browser ) {
			var browser = {};
			DataTable.__browser = browser;
	
			// Scrolling feature / quirks detection
			var n = $('<div/>')
				.css( {
					position: 'fixed',
					top: 0,
					left: $(window).scrollLeft()*-1, // allow for scrolling
					height: 1,
					width: 1,
					overflow: 'hidden'
				} )
				.append(
					$('<div/>')
						.css( {
							position: 'absolute',
							top: 1,
							left: 1,
							width: 100,
							overflow: 'scroll'
						} )
						.append(
							$('<div/>')
								.css( {
									width: '100%',
									height: 10
								} )
						)
				)
				.appendTo( 'body' );
	
			var outer = n.children();
			var inner = outer.children();
	
			// Numbers below, in order, are:
			// inner.offsetWidth, inner.clientWidth, outer.offsetWidth, outer.clientWidth
			//
			// IE6 XP:                           100 100 100  83
			// IE7 Vista:                        100 100 100  83
			// IE 8+ Windows:                     83  83 100  83
			// Evergreen Windows:                 83  83 100  83
			// Evergreen Mac with scrollbars:     85  85 100  85
			// Evergreen Mac without scrollbars: 100 100 100 100
	
			// Get scrollbar width
			browser.barWidth = outer[0].offsetWidth - outer[0].clientWidth;
	
			// IE6/7 will oversize a width 100% element inside a scrolling element, to
			// include the width of the scrollbar, while other browsers ensure the inner
			// element is contained without forcing scrolling
			browser.bScrollOversize = inner[0].offsetWidth === 100 && outer[0].clientWidth !== 100;
	
			// In rtl text layout, some browsers (most, but not all) will place the
			// scrollbar on the left, rather than the right.
			browser.bScrollbarLeft = Math.round( inner.offset().left ) !== 1;
	
			// IE8- don't provide height and width for getBoundingClientRect
			browser.bBounding = n[0].getBoundingClientRect().width ? true : false;
	
			n.remove();
		}
	
		$.extend( settings.oBrowser, DataTable.__browser );
		settings.oScroll.iBarWidth = DataTable.__browser.barWidth;
	}
	
	
	/**
	 * Array.prototype reduce[Right] method, used for browsers which don't support
	 * JS 1.6. Done this way to reduce code size, since we iterate either way
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnReduce ( that, fn, init, start, end, inc )
	{
		var
			i = start,
			value,
			isSet = false;
	
		if ( init !== undefined ) {
			value = init;
			isSet = true;
		}
	
		while ( i !== end ) {
			if ( ! that.hasOwnProperty(i) ) {
				continue;
			}
	
			value = isSet ?
				fn( value, that[i], i, that ) :
				that[i];
	
			isSet = true;
			i += inc;
		}
	
		return value;
	}
	
	/**
	 * Add a column to the list used for the table with default values
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} nTh The th element for this column
	 *  @memberof DataTable#oApi
	 */
	function _fnAddColumn( oSettings, nTh )
	{
		// Add column to aoColumns array
		var oDefaults = DataTable.defaults.column;
		var iCol = oSettings.aoColumns.length;
		var oCol = $.extend( {}, DataTable.models.oColumn, oDefaults, {
			"nTh": nTh ? nTh : document.createElement('th'),
			"sTitle":    oDefaults.sTitle    ? oDefaults.sTitle    : nTh ? nTh.innerHTML : '',
			"aDataSort": oDefaults.aDataSort ? oDefaults.aDataSort : [iCol],
			"mData": oDefaults.mData ? oDefaults.mData : iCol,
			idx: iCol
		} );
		oSettings.aoColumns.push( oCol );
	
		// Add search object for column specific search. Note that the `searchCols[ iCol ]`
		// passed into extend can be undefined. This allows the user to give a default
		// with only some of the parameters defined, and also not give a default
		var searchCols = oSettings.aoPreSearchCols;
		searchCols[ iCol ] = $.extend( {}, DataTable.models.oSearch, searchCols[ iCol ] );
	
		// Use the default column options function to initialise classes etc
		_fnColumnOptions( oSettings, iCol, $(nTh).data() );
	}
	
	
	/**
	 * Apply options for a column
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iCol column index to consider
	 *  @param {object} oOptions object with sType, bVisible and bSearchable etc
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnOptions( oSettings, iCol, oOptions )
	{
		var oCol = oSettings.aoColumns[ iCol ];
		var oClasses = oSettings.oClasses;
		var th = $(oCol.nTh);
	
		// Try to get width information from the DOM. We can't get it from CSS
		// as we'd need to parse the CSS stylesheet. `width` option can override
		if ( ! oCol.sWidthOrig ) {
			// Width attribute
			oCol.sWidthOrig = th.attr('width') || null;
	
			// Style attribute
			var t = (th.attr('style') || '').match(/width:\s*(\d+[pxem%]+)/);
			if ( t ) {
				oCol.sWidthOrig = t[1];
			}
		}
	
		/* User specified column options */
		if ( oOptions !== undefined && oOptions !== null )
		{
			// Backwards compatibility
			_fnCompatCols( oOptions );
	
			// Map camel case parameters to their Hungarian counterparts
			_fnCamelToHungarian( DataTable.defaults.column, oOptions, true );
	
			/* Backwards compatibility for mDataProp */
			if ( oOptions.mDataProp !== undefined && !oOptions.mData )
			{
				oOptions.mData = oOptions.mDataProp;
			}
	
			if ( oOptions.sType )
			{
				oCol._sManualType = oOptions.sType;
			}
	
			// `class` is a reserved word in Javascript, so we need to provide
			// the ability to use a valid name for the camel case input
			if ( oOptions.className && ! oOptions.sClass )
			{
				oOptions.sClass = oOptions.className;
			}
			if ( oOptions.sClass ) {
				th.addClass( oOptions.sClass );
			}
	
			var origClass = oCol.sClass;
	
			$.extend( oCol, oOptions );
			_fnMap( oCol, oOptions, "sWidth", "sWidthOrig" );
	
			// Merge class from previously defined classes with this one, rather than just
			// overwriting it in the extend above
			if (origClass !== oCol.sClass) {
				oCol.sClass = origClass + ' ' + oCol.sClass;
			}
	
			/* iDataSort to be applied (backwards compatibility), but aDataSort will take
			 * priority if defined
			 */
			if ( oOptions.iDataSort !== undefined )
			{
				oCol.aDataSort = [ oOptions.iDataSort ];
			}
			_fnMap( oCol, oOptions, "aDataSort" );
		}
	
		/* Cache the data get and set functions for speed */
		var mDataSrc = oCol.mData;
		var mData = _fnGetObjectDataFn( mDataSrc );
		var mRender = oCol.mRender ? _fnGetObjectDataFn( oCol.mRender ) : null;
	
		var attrTest = function( src ) {
			return typeof src === 'string' && src.indexOf('@') !== -1;
		};
		oCol._bAttrSrc = $.isPlainObject( mDataSrc ) && (
			attrTest(mDataSrc.sort) || attrTest(mDataSrc.type) || attrTest(mDataSrc.filter)
		);
		oCol._setter = null;
	
		oCol.fnGetData = function (rowData, type, meta) {
			var innerData = mData( rowData, type, undefined, meta );
	
			return mRender && type ?
				mRender( innerData, type, rowData, meta ) :
				innerData;
		};
		oCol.fnSetData = function ( rowData, val, meta ) {
			return _fnSetObjectDataFn( mDataSrc )( rowData, val, meta );
		};
	
		// Indicate if DataTables should read DOM data as an object or array
		// Used in _fnGetRowElements
		if ( typeof mDataSrc !== 'number' ) {
			oSettings._rowReadObject = true;
		}
	
		/* Feature sorting overrides column specific when off */
		if ( !oSettings.oFeatures.bSort )
		{
			oCol.bSortable = false;
			th.addClass( oClasses.sSortableNone ); // Have to add class here as order event isn't called
		}
	
		/* Check that the class assignment is correct for sorting */
		var bAsc = $.inArray('asc', oCol.asSorting) !== -1;
		var bDesc = $.inArray('desc', oCol.asSorting) !== -1;
		if ( !oCol.bSortable || (!bAsc && !bDesc) )
		{
			oCol.sSortingClass = oClasses.sSortableNone;
			oCol.sSortingClassJUI = "";
		}
		else if ( bAsc && !bDesc )
		{
			oCol.sSortingClass = oClasses.sSortableAsc;
			oCol.sSortingClassJUI = oClasses.sSortJUIAscAllowed;
		}
		else if ( !bAsc && bDesc )
		{
			oCol.sSortingClass = oClasses.sSortableDesc;
			oCol.sSortingClassJUI = oClasses.sSortJUIDescAllowed;
		}
		else
		{
			oCol.sSortingClass = oClasses.sSortable;
			oCol.sSortingClassJUI = oClasses.sSortJUI;
		}
	}
	
	
	/**
	 * Adjust the table column widths for new data. Note: you would probably want to
	 * do a redraw after calling this function!
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnAdjustColumnSizing ( settings )
	{
		/* Not interested in doing column width calculation if auto-width is disabled */
		if ( settings.oFeatures.bAutoWidth !== false )
		{
			var columns = settings.aoColumns;
	
			_fnCalculateColumnWidths( settings );
			for ( var i=0 , iLen=columns.length ; i<iLen ; i++ )
			{
				columns[i].nTh.style.width = columns[i].sWidth;
			}
		}
	
		var scroll = settings.oScroll;
		if ( scroll.sY !== '' || scroll.sX !== '')
		{
			_fnScrollDraw( settings );
		}
	
		_fnCallbackFire( settings, null, 'column-sizing', [settings] );
	}
	
	
	/**
	 * Convert the index of a visible column to the index in the data array (take account
	 * of hidden columns)
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iMatch Visible column index to lookup
	 *  @returns {int} i the data index
	 *  @memberof DataTable#oApi
	 */
	function _fnVisibleToColumnIndex( oSettings, iMatch )
	{
		var aiVis = _fnGetColumns( oSettings, 'bVisible' );
	
		return typeof aiVis[iMatch] === 'number' ?
			aiVis[iMatch] :
			null;
	}
	
	
	/**
	 * Convert the index of an index in the data array and convert it to the visible
	 *   column index (take account of hidden columns)
	 *  @param {int} iMatch Column index to lookup
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {int} i the data index
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnIndexToVisible( oSettings, iMatch )
	{
		var aiVis = _fnGetColumns( oSettings, 'bVisible' );
		var iPos = $.inArray( iMatch, aiVis );
	
		return iPos !== -1 ? iPos : null;
	}
	
	
	/**
	 * Get the number of visible columns
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {int} i the number of visible columns
	 *  @memberof DataTable#oApi
	 */
	function _fnVisbleColumns( oSettings )
	{
		var vis = 0;
	
		// No reduce in IE8, use a loop for now
		$.each( oSettings.aoColumns, function ( i, col ) {
			if ( col.bVisible && $(col.nTh).css('display') !== 'none' ) {
				vis++;
			}
		} );
	
		return vis;
	}
	
	
	/**
	 * Get an array of column indexes that match a given property
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sParam Parameter in aoColumns to look for - typically
	 *    bVisible or bSearchable
	 *  @returns {array} Array of indexes with matched properties
	 *  @memberof DataTable#oApi
	 */
	function _fnGetColumns( oSettings, sParam )
	{
		var a = [];
	
		$.map( oSettings.aoColumns, function(val, i) {
			if ( val[sParam] ) {
				a.push( i );
			}
		} );
	
		return a;
	}
	
	
	/**
	 * Calculate the 'type' of a column
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnTypes ( settings )
	{
		var columns = settings.aoColumns;
		var data = settings.aoData;
		var types = DataTable.ext.type.detect;
		var i, ien, j, jen, k, ken;
		var col, cell, detectedType, cache;
	
		// For each column, spin over the 
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			col = columns[i];
			cache = [];
	
			if ( ! col.sType && col._sManualType ) {
				col.sType = col._sManualType;
			}
			else if ( ! col.sType ) {
				for ( j=0, jen=types.length ; j<jen ; j++ ) {
					for ( k=0, ken=data.length ; k<ken ; k++ ) {
						// Use a cache array so we only need to get the type data
						// from the formatter once (when using multiple detectors)
						if ( cache[k] === undefined ) {
							cache[k] = _fnGetCellData( settings, k, i, 'type' );
						}
	
						detectedType = types[j]( cache[k], settings );
	
						// If null, then this type can't apply to this column, so
						// rather than testing all cells, break out. There is an
						// exception for the last type which is `html`. We need to
						// scan all rows since it is possible to mix string and HTML
						// types
						if ( ! detectedType && j !== types.length-1 ) {
							break;
						}
	
						// Only a single match is needed for html type since it is
						// bottom of the pile and very similar to string - but it
						// must not be empty
						if ( detectedType === 'html' && ! _empty(cache[k]) ) {
							break;
						}
					}
	
					// Type is valid for all data points in the column - use this
					// type
					if ( detectedType ) {
						col.sType = detectedType;
						break;
					}
				}
	
				// Fall back - if no type was detected, always use string
				if ( ! col.sType ) {
					col.sType = 'string';
				}
			}
		}
	}
	
	
	/**
	 * Take the column definitions and static columns arrays and calculate how
	 * they relate to column indexes. The callback function will then apply the
	 * definition found for a column to a suitable configuration object.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {array} aoColDefs The aoColumnDefs array that is to be applied
	 *  @param {array} aoCols The aoColumns array that defines columns individually
	 *  @param {function} fn Callback function - takes two parameters, the calculated
	 *    column index and the definition for that column.
	 *  @memberof DataTable#oApi
	 */
	function _fnApplyColumnDefs( oSettings, aoColDefs, aoCols, fn )
	{
		var i, iLen, j, jLen, k, kLen, def;
		var columns = oSettings.aoColumns;
	
		// Column definitions with aTargets
		if ( aoColDefs )
		{
			/* Loop over the definitions array - loop in reverse so first instance has priority */
			for ( i=aoColDefs.length-1 ; i>=0 ; i-- )
			{
				def = aoColDefs[i];
	
				/* Each definition can target multiple columns, as it is an array */
				var aTargets = def.target !== undefined
					? def.target
					: def.targets !== undefined
						? def.targets
						: def.aTargets;
	
				if ( ! Array.isArray( aTargets ) )
				{
					aTargets = [ aTargets ];
				}
	
				for ( j=0, jLen=aTargets.length ; j<jLen ; j++ )
				{
					if ( typeof aTargets[j] === 'number' && aTargets[j] >= 0 )
					{
						/* Add columns that we don't yet know about */
						while( columns.length <= aTargets[j] )
						{
							_fnAddColumn( oSettings );
						}
	
						/* Integer, basic index */
						fn( aTargets[j], def );
					}
					else if ( typeof aTargets[j] === 'number' && aTargets[j] < 0 )
					{
						/* Negative integer, right to left column counting */
						fn( columns.length+aTargets[j], def );
					}
					else if ( typeof aTargets[j] === 'string' )
					{
						/* Class name matching on TH element */
						for ( k=0, kLen=columns.length ; k<kLen ; k++ )
						{
							if ( aTargets[j] == "_all" ||
							     $(columns[k].nTh).hasClass( aTargets[j] ) )
							{
								fn( k, def );
							}
						}
					}
				}
			}
		}
	
		// Statically defined columns array
		if ( aoCols )
		{
			for ( i=0, iLen=aoCols.length ; i<iLen ; i++ )
			{
				fn( i, aoCols[i] );
			}
		}
	}
	
	/**
	 * Add a data array to the table, creating DOM node etc. This is the parallel to
	 * _fnGatherData, but for adding rows from a Javascript source, rather than a
	 * DOM source.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {array} aData data array to be added
	 *  @param {node} [nTr] TR element to add to the table - optional. If not given,
	 *    DataTables will create a row automatically
	 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
	 *    if nTr is.
	 *  @returns {int} >=0 if successful (index of new aoData entry), -1 if failed
	 *  @memberof DataTable#oApi
	 */
	function _fnAddData ( oSettings, aDataIn, nTr, anTds )
	{
		/* Create the object for storing information about this new row */
		var iRow = oSettings.aoData.length;
		var oData = $.extend( true, {}, DataTable.models.oRow, {
			src: nTr ? 'dom' : 'data',
			idx: iRow
		} );
	
		oData._aData = aDataIn;
		oSettings.aoData.push( oData );
	
		/* Create the cells */
		var nTd, sThisType;
		var columns = oSettings.aoColumns;
	
		// Invalidate the column types as the new data needs to be revalidated
		for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
		{
			columns[i].sType = null;
		}
	
		/* Add to the display array */
		oSettings.aiDisplayMaster.push( iRow );
	
		var id = oSettings.rowIdFn( aDataIn );
		if ( id !== undefined ) {
			oSettings.aIds[ id ] = oData;
		}
	
		/* Create the DOM information, or register it if already present */
		if ( nTr || ! oSettings.oFeatures.bDeferRender )
		{
			_fnCreateTr( oSettings, iRow, nTr, anTds );
		}
	
		return iRow;
	}
	
	
	/**
	 * Add one or more TR elements to the table. Generally we'd expect to
	 * use this for reading data from a DOM sourced table, but it could be
	 * used for an TR element. Note that if a TR is given, it is used (i.e.
	 * it is not cloned).
	 *  @param {object} settings dataTables settings object
	 *  @param {array|node|jQuery} trs The TR element(s) to add to the table
	 *  @returns {array} Array of indexes for the added rows
	 *  @memberof DataTable#oApi
	 */
	function _fnAddTr( settings, trs )
	{
		var row;
	
		// Allow an individual node to be passed in
		if ( ! (trs instanceof $) ) {
			trs = $(trs);
		}
	
		return trs.map( function (i, el) {
			row = _fnGetRowElements( settings, el );
			return _fnAddData( settings, row.data, el, row.cells );
		} );
	}
	
	
	/**
	 * Take a TR element and convert it to an index in aoData
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} n the TR element to find
	 *  @returns {int} index if the node is found, null if not
	 *  @memberof DataTable#oApi
	 */
	function _fnNodeToDataIndex( oSettings, n )
	{
		return (n._DT_RowIndex!==undefined) ? n._DT_RowIndex : null;
	}
	
	
	/**
	 * Take a TD element and convert it into a column data index (not the visible index)
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iRow The row number the TD/TH can be found in
	 *  @param {node} n The TD/TH element to find
	 *  @returns {int} index if the node is found, -1 if not
	 *  @memberof DataTable#oApi
	 */
	function _fnNodeToColumnIndex( oSettings, iRow, n )
	{
		return $.inArray( n, oSettings.aoData[ iRow ].anCells );
	}
	
	
	/**
	 * Get the data for a given cell from the internal cache, taking into account data mapping
	 *  @param {object} settings dataTables settings object
	 *  @param {int} rowIdx aoData row id
	 *  @param {int} colIdx Column index
	 *  @param {string} type data get type ('display', 'type' 'filter|search' 'sort|order')
	 *  @returns {*} Cell data
	 *  @memberof DataTable#oApi
	 */
	function _fnGetCellData( settings, rowIdx, colIdx, type )
	{
		if (type === 'search') {
			type = 'filter';
		}
		else if (type === 'order') {
			type = 'sort';
		}
	
		var draw           = settings.iDraw;
		var col            = settings.aoColumns[colIdx];
		var rowData        = settings.aoData[rowIdx]._aData;
		var defaultContent = col.sDefaultContent;
		var cellData       = col.fnGetData( rowData, type, {
			settings: settings,
			row:      rowIdx,
			col:      colIdx
		} );
	
		if ( cellData === undefined ) {
			if ( settings.iDrawError != draw && defaultContent === null ) {
				_fnLog( settings, 0, "Requested unknown parameter "+
					(typeof col.mData=='function' ? '{function}' : "'"+col.mData+"'")+
					" for row "+rowIdx+", column "+colIdx, 4 );
				settings.iDrawError = draw;
			}
			return defaultContent;
		}
	
		// When the data source is null and a specific data type is requested (i.e.
		// not the original data), we can use default column data
		if ( (cellData === rowData || cellData === null) && defaultContent !== null && type !== undefined ) {
			cellData = defaultContent;
		}
		else if ( typeof cellData === 'function' ) {
			// If the data source is a function, then we run it and use the return,
			// executing in the scope of the data object (for instances)
			return cellData.call( rowData );
		}
	
		if ( cellData === null && type === 'display' ) {
			return '';
		}
	
		if ( type === 'filter' ) {
			var fomatters = DataTable.ext.type.search;
	
			if ( fomatters[ col.sType ] ) {
				cellData = fomatters[ col.sType ]( cellData );
			}
		}
	
		return cellData;
	}
	
	
	/**
	 * Set the value for a specific cell, into the internal data cache
	 *  @param {object} settings dataTables settings object
	 *  @param {int} rowIdx aoData row id
	 *  @param {int} colIdx Column index
	 *  @param {*} val Value to set
	 *  @memberof DataTable#oApi
	 */
	function _fnSetCellData( settings, rowIdx, colIdx, val )
	{
		var col     = settings.aoColumns[colIdx];
		var rowData = settings.aoData[rowIdx]._aData;
	
		col.fnSetData( rowData, val, {
			settings: settings,
			row:      rowIdx,
			col:      colIdx
		}  );
	}
	
	
	// Private variable that is used to match action syntax in the data property object
	var __reArray = /\[.*?\]$/;
	var __reFn = /\(\)$/;
	
	/**
	 * Split string on periods, taking into account escaped periods
	 * @param  {string} str String to split
	 * @return {array} Split string
	 */
	function _fnSplitObjNotation( str )
	{
		return $.map( str.match(/(\\.|[^\.])+/g) || [''], function ( s ) {
			return s.replace(/\\\./g, '.');
		} );
	}
	
	
	/**
	 * Return a function that can be used to get data from a source object, taking
	 * into account the ability to use nested objects as a source
	 *  @param {string|int|function} mSource The data source for the object
	 *  @returns {function} Data get function
	 *  @memberof DataTable#oApi
	 */
	var _fnGetObjectDataFn = DataTable.util.get;
	
	
	/**
	 * Return a function that can be used to set data from a source object, taking
	 * into account the ability to use nested objects as a source
	 *  @param {string|int|function} mSource The data source for the object
	 *  @returns {function} Data set function
	 *  @memberof DataTable#oApi
	 */
	var _fnSetObjectDataFn = DataTable.util.set;
	
	
	/**
	 * Return an array with the full table data
	 *  @param {object} oSettings dataTables settings object
	 *  @returns array {array} aData Master data array
	 *  @memberof DataTable#oApi
	 */
	function _fnGetDataMaster ( settings )
	{
		return _pluck( settings.aoData, '_aData' );
	}
	
	
	/**
	 * Nuke the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnClearTable( settings )
	{
		settings.aoData.length = 0;
		settings.aiDisplayMaster.length = 0;
		settings.aiDisplay.length = 0;
		settings.aIds = {};
	}
	
	
	 /**
	 * Take an array of integers (index array) and remove a target integer (value - not
	 * the key!)
	 *  @param {array} a Index array to target
	 *  @param {int} iTarget value to find
	 *  @memberof DataTable#oApi
	 */
	function _fnDeleteIndex( a, iTarget, splice )
	{
		var iTargetIndex = -1;
	
		for ( var i=0, iLen=a.length ; i<iLen ; i++ )
		{
			if ( a[i] == iTarget )
			{
				iTargetIndex = i;
			}
			else if ( a[i] > iTarget )
			{
				a[i]--;
			}
		}
	
		if ( iTargetIndex != -1 && splice === undefined )
		{
			a.splice( iTargetIndex, 1 );
		}
	}
	
	
	/**
	 * Mark cached data as invalid such that a re-read of the data will occur when
	 * the cached data is next requested. Also update from the data source object.
	 *
	 * @param {object} settings DataTables settings object
	 * @param {int}    rowIdx   Row index to invalidate
	 * @param {string} [src]    Source to invalidate from: undefined, 'auto', 'dom'
	 *     or 'data'
	 * @param {int}    [colIdx] Column index to invalidate. If undefined the whole
	 *     row will be invalidated
	 * @memberof DataTable#oApi
	 *
	 * @todo For the modularisation of v1.11 this will need to become a callback, so
	 *   the sort and filter methods can subscribe to it. That will required
	 *   initialisation options for sorting, which is why it is not already baked in
	 */
	function _fnInvalidate( settings, rowIdx, src, colIdx )
	{
		var row = settings.aoData[ rowIdx ];
		var i, ien;
		var cellWrite = function ( cell, col ) {
			// This is very frustrating, but in IE if you just write directly
			// to innerHTML, and elements that are overwritten are GC'ed,
			// even if there is a reference to them elsewhere
			while ( cell.childNodes.length ) {
				cell.removeChild( cell.firstChild );
			}
	
			cell.innerHTML = _fnGetCellData( settings, rowIdx, col, 'display' );
		};
	
		// Are we reading last data from DOM or the data object?
		if ( src === 'dom' || ((! src || src === 'auto') && row.src === 'dom') ) {
			// Read the data from the DOM
			row._aData = _fnGetRowElements(
					settings, row, colIdx, colIdx === undefined ? undefined : row._aData
				)
				.data;
		}
		else {
			// Reading from data object, update the DOM
			var cells = row.anCells;
	
			if ( cells ) {
				if ( colIdx !== undefined ) {
					cellWrite( cells[colIdx], colIdx );
				}
				else {
					for ( i=0, ien=cells.length ; i<ien ; i++ ) {
						cellWrite( cells[i], i );
					}
				}
			}
		}
	
		// For both row and cell invalidation, the cached data for sorting and
		// filtering is nulled out
		row._aSortData = null;
		row._aFilterData = null;
	
		// Invalidate the type for a specific column (if given) or all columns since
		// the data might have changed
		var cols = settings.aoColumns;
		if ( colIdx !== undefined ) {
			cols[ colIdx ].sType = null;
		}
		else {
			for ( i=0, ien=cols.length ; i<ien ; i++ ) {
				cols[i].sType = null;
			}
	
			// Update DataTables special `DT_*` attributes for the row
			_fnRowAttributes( settings, row );
		}
	}
	
	
	/**
	 * Build a data source object from an HTML row, reading the contents of the
	 * cells that are in the row.
	 *
	 * @param {object} settings DataTables settings object
	 * @param {node|object} TR element from which to read data or existing row
	 *   object from which to re-read the data from the cells
	 * @param {int} [colIdx] Optional column index
	 * @param {array|object} [d] Data source object. If `colIdx` is given then this
	 *   parameter should also be given and will be used to write the data into.
	 *   Only the column in question will be written
	 * @returns {object} Object with two parameters: `data` the data read, in
	 *   document order, and `cells` and array of nodes (they can be useful to the
	 *   caller, so rather than needing a second traversal to get them, just return
	 *   them from here).
	 * @memberof DataTable#oApi
	 */
	function _fnGetRowElements( settings, row, colIdx, d )
	{
		var
			tds = [],
			td = row.firstChild,
			name, col, o, i=0, contents,
			columns = settings.aoColumns,
			objectRead = settings._rowReadObject;
	
		// Allow the data object to be passed in, or construct
		d = d !== undefined ?
			d :
			objectRead ?
				{} :
				[];
	
		var attr = function ( str, td  ) {
			if ( typeof str === 'string' ) {
				var idx = str.indexOf('@');
	
				if ( idx !== -1 ) {
					var attr = str.substring( idx+1 );
					var setter = _fnSetObjectDataFn( str );
					setter( d, td.getAttribute( attr ) );
				}
			}
		};
	
		// Read data from a cell and store into the data object
		var cellProcess = function ( cell ) {
			if ( colIdx === undefined || colIdx === i ) {
				col = columns[i];
				contents = (cell.innerHTML).trim();
	
				if ( col && col._bAttrSrc ) {
					var setter = _fnSetObjectDataFn( col.mData._ );
					setter( d, contents );
	
					attr( col.mData.sort, cell );
					attr( col.mData.type, cell );
					attr( col.mData.filter, cell );
				}
				else {
					// Depending on the `data` option for the columns the data can
					// be read to either an object or an array.
					if ( objectRead ) {
						if ( ! col._setter ) {
							// Cache the setter function
							col._setter = _fnSetObjectDataFn( col.mData );
						}
						col._setter( d, contents );
					}
					else {
						d[i] = contents;
					}
				}
			}
	
			i++;
		};
	
		if ( td ) {
			// `tr` element was passed in
			while ( td ) {
				name = td.nodeName.toUpperCase();
	
				if ( name == "TD" || name == "TH" ) {
					cellProcess( td );
					tds.push( td );
				}
	
				td = td.nextSibling;
			}
		}
		else {
			// Existing row object passed in
			tds = row.anCells;
	
			for ( var j=0, jen=tds.length ; j<jen ; j++ ) {
				cellProcess( tds[j] );
			}
		}
	
		// Read the ID from the DOM if present
		var rowNode = row.firstChild ? row : row.nTr;
	
		if ( rowNode ) {
			var id = rowNode.getAttribute( 'id' );
	
			if ( id ) {
				_fnSetObjectDataFn( settings.rowId )( d, id );
			}
		}
	
		return {
			data: d,
			cells: tds
		};
	}
	/**
	 * Create a new TR element (and it's TD children) for a row
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iRow Row to consider
	 *  @param {node} [nTrIn] TR element to add to the table - optional. If not given,
	 *    DataTables will create a row automatically
	 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
	 *    if nTr is.
	 *  @memberof DataTable#oApi
	 */
	function _fnCreateTr ( oSettings, iRow, nTrIn, anTds )
	{
		var
			row = oSettings.aoData[iRow],
			rowData = row._aData,
			cells = [],
			nTr, nTd, oCol,
			i, iLen, create;
	
		if ( row.nTr === null )
		{
			nTr = nTrIn || document.createElement('tr');
	
			row.nTr = nTr;
			row.anCells = cells;
	
			/* Use a private property on the node to allow reserve mapping from the node
			 * to the aoData array for fast look up
			 */
			nTr._DT_RowIndex = iRow;
	
			/* Special parameters can be given by the data source to be used on the row */
			_fnRowAttributes( oSettings, row );
	
			/* Process each column */
			for ( i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
			{
				oCol = oSettings.aoColumns[i];
				create = nTrIn ? false : true;
	
				nTd = create ? document.createElement( oCol.sCellType ) : anTds[i];
				nTd._DT_CellIndex = {
					row: iRow,
					column: i
				};
				
				cells.push( nTd );
	
				// Need to create the HTML if new, or if a rendering function is defined
				if ( create || ((oCol.mRender || oCol.mData !== i) &&
					 (!$.isPlainObject(oCol.mData) || oCol.mData._ !== i+'.display')
				)) {
					nTd.innerHTML = _fnGetCellData( oSettings, iRow, i, 'display' );
				}
	
				/* Add user defined class */
				if ( oCol.sClass )
				{
					nTd.className += ' '+oCol.sClass;
				}
	
				// Visibility - add or remove as required
				if ( oCol.bVisible && ! nTrIn )
				{
					nTr.appendChild( nTd );
				}
				else if ( ! oCol.bVisible && nTrIn )
				{
					nTd.parentNode.removeChild( nTd );
				}
	
				if ( oCol.fnCreatedCell )
				{
					oCol.fnCreatedCell.call( oSettings.oInstance,
						nTd, _fnGetCellData( oSettings, iRow, i ), rowData, iRow, i
					);
				}
			}
	
			_fnCallbackFire( oSettings, 'aoRowCreatedCallback', null, [nTr, rowData, iRow, cells] );
		}
	}
	
	
	/**
	 * Add attributes to a row based on the special `DT_*` parameters in a data
	 * source object.
	 *  @param {object} settings DataTables settings object
	 *  @param {object} DataTables row object for the row to be modified
	 *  @memberof DataTable#oApi
	 */
	function _fnRowAttributes( settings, row )
	{
		var tr = row.nTr;
		var data = row._aData;
	
		if ( tr ) {
			var id = settings.rowIdFn( data );
	
			if ( id ) {
				tr.id = id;
			}
	
			if ( data.DT_RowClass ) {
				// Remove any classes added by DT_RowClass before
				var a = data.DT_RowClass.split(' ');
				row.__rowc = row.__rowc ?
					_unique( row.__rowc.concat( a ) ) :
					a;
	
				$(tr)
					.removeClass( row.__rowc.join(' ') )
					.addClass( data.DT_RowClass );
			}
	
			if ( data.DT_RowAttr ) {
				$(tr).attr( data.DT_RowAttr );
			}
	
			if ( data.DT_RowData ) {
				$(tr).data( data.DT_RowData );
			}
		}
	}
	
	
	/**
	 * Create the HTML header for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnBuildHead( oSettings )
	{
		var i, ien, cell, row, column;
		var thead = oSettings.nTHead;
		var tfoot = oSettings.nTFoot;
		var createHeader = $('th, td', thead).length === 0;
		var classes = oSettings.oClasses;
		var columns = oSettings.aoColumns;
	
		if ( createHeader ) {
			row = $('<tr/>').appendTo( thead );
		}
	
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			column = columns[i];
			cell = $( column.nTh ).addClass( column.sClass );
	
			if ( createHeader ) {
				cell.appendTo( row );
			}
	
			// 1.11 move into sorting
			if ( oSettings.oFeatures.bSort ) {
				cell.addClass( column.sSortingClass );
	
				if ( column.bSortable !== false ) {
					cell
						.attr( 'tabindex', oSettings.iTabIndex )
						.attr( 'aria-controls', oSettings.sTableId );
	
					_fnSortAttachListener( oSettings, column.nTh, i );
				}
			}
	
			if ( column.sTitle != cell[0].innerHTML ) {
				cell.html( column.sTitle );
			}
	
			_fnRenderer( oSettings, 'header' )(
				oSettings, cell, column, classes
			);
		}
	
		if ( createHeader ) {
			_fnDetectHeader( oSettings.aoHeader, thead );
		}
	
		/* Deal with the footer - add classes if required */
		$(thead).children('tr').children('th, td').addClass( classes.sHeaderTH );
		$(tfoot).children('tr').children('th, td').addClass( classes.sFooterTH );
	
		// Cache the footer cells. Note that we only take the cells from the first
		// row in the footer. If there is more than one row the user wants to
		// interact with, they need to use the table().foot() method. Note also this
		// allows cells to be used for multiple columns using colspan
		if ( tfoot !== null ) {
			var cells = oSettings.aoFooter[0];
	
			for ( i=0, ien=cells.length ; i<ien ; i++ ) {
				column = columns[i];
				column.nTf = cells[i].cell;
	
				if ( column.sClass ) {
					$(column.nTf).addClass( column.sClass );
				}
			}
		}
	}
	
	
	/**
	 * Draw the header (or footer) element based on the column visibility states. The
	 * methodology here is to use the layout array from _fnDetectHeader, modified for
	 * the instantaneous column visibility, to construct the new layout. The grid is
	 * traversed over cell at a time in a rows x columns grid fashion, although each
	 * cell insert can cover multiple elements in the grid - which is tracks using the
	 * aApplied array. Cell inserts in the grid will only occur where there isn't
	 * already a cell in that position.
	 *  @param {object} oSettings dataTables settings object
	 *  @param array {objects} aoSource Layout array from _fnDetectHeader
	 *  @param {boolean} [bIncludeHidden=false] If true then include the hidden columns in the calc,
	 *  @memberof DataTable#oApi
	 */
	function _fnDrawHead( oSettings, aoSource, bIncludeHidden )
	{
		var i, iLen, j, jLen, k, kLen, n, nLocalTr;
		var aoLocal = [];
		var aApplied = [];
		var iColumns = oSettings.aoColumns.length;
		var iRowspan, iColspan;
	
		if ( ! aoSource )
		{
			return;
		}
	
		if (  bIncludeHidden === undefined )
		{
			bIncludeHidden = false;
		}
	
		/* Make a copy of the master layout array, but without the visible columns in it */
		for ( i=0, iLen=aoSource.length ; i<iLen ; i++ )
		{
			aoLocal[i] = aoSource[i].slice();
			aoLocal[i].nTr = aoSource[i].nTr;
	
			/* Remove any columns which are currently hidden */
			for ( j=iColumns-1 ; j>=0 ; j-- )
			{
				if ( !oSettings.aoColumns[j].bVisible && !bIncludeHidden )
				{
					aoLocal[i].splice( j, 1 );
				}
			}
	
			/* Prep the applied array - it needs an element for each row */
			aApplied.push( [] );
		}
	
		for ( i=0, iLen=aoLocal.length ; i<iLen ; i++ )
		{
			nLocalTr = aoLocal[i].nTr;
	
			/* All cells are going to be replaced, so empty out the row */
			if ( nLocalTr )
			{
				while( (n = nLocalTr.firstChild) )
				{
					nLocalTr.removeChild( n );
				}
			}
	
			for ( j=0, jLen=aoLocal[i].length ; j<jLen ; j++ )
			{
				iRowspan = 1;
				iColspan = 1;
	
				/* Check to see if there is already a cell (row/colspan) covering our target
				 * insert point. If there is, then there is nothing to do.
				 */
				if ( aApplied[i][j] === undefined )
				{
					nLocalTr.appendChild( aoLocal[i][j].cell );
					aApplied[i][j] = 1;
	
					/* Expand the cell to cover as many rows as needed */
					while ( aoLocal[i+iRowspan] !== undefined &&
					        aoLocal[i][j].cell == aoLocal[i+iRowspan][j].cell )
					{
						aApplied[i+iRowspan][j] = 1;
						iRowspan++;
					}
	
					/* Expand the cell to cover as many columns as needed */
					while ( aoLocal[i][j+iColspan] !== undefined &&
					        aoLocal[i][j].cell == aoLocal[i][j+iColspan].cell )
					{
						/* Must update the applied array over the rows for the columns */
						for ( k=0 ; k<iRowspan ; k++ )
						{
							aApplied[i+k][j+iColspan] = 1;
						}
						iColspan++;
					}
	
					/* Do the actual expansion in the DOM */
					$(aoLocal[i][j].cell)
						.attr('rowspan', iRowspan)
						.attr('colspan', iColspan);
				}
			}
		}
	}
	
	
	/**
	 * Insert the required TR nodes into the table for display
	 *  @param {object} oSettings dataTables settings object
	 *  @param ajaxComplete true after ajax call to complete rendering
	 *  @memberof DataTable#oApi
	 */
	function _fnDraw( oSettings, ajaxComplete )
	{
		// Allow for state saving and a custom start position
		_fnStart( oSettings );
	
		/* Provide a pre-callback function which can be used to cancel the draw is false is returned */
		var aPreDraw = _fnCallbackFire( oSettings, 'aoPreDrawCallback', 'preDraw', [oSettings] );
		if ( $.inArray( false, aPreDraw ) !== -1 )
		{
			_fnProcessingDisplay( oSettings, false );
			return;
		}
	
		var anRows = [];
		var iRowCount = 0;
		var asStripeClasses = oSettings.asStripeClasses;
		var iStripes = asStripeClasses.length;
		var oLang = oSettings.oLanguage;
		var bServerSide = _fnDataSource( oSettings ) == 'ssp';
		var aiDisplay = oSettings.aiDisplay;
		var iDisplayStart = oSettings._iDisplayStart;
		var iDisplayEnd = oSettings.fnDisplayEnd();
	
		oSettings.bDrawing = true;
	
		/* Server-side processing draw intercept */
		if ( oSettings.bDeferLoading )
		{
			oSettings.bDeferLoading = false;
			oSettings.iDraw++;
			_fnProcessingDisplay( oSettings, false );
		}
		else if ( !bServerSide )
		{
			oSettings.iDraw++;
		}
		else if ( !oSettings.bDestroying && !ajaxComplete)
		{
			_fnAjaxUpdate( oSettings );
			return;
		}
	
		if ( aiDisplay.length !== 0 )
		{
			var iStart = bServerSide ? 0 : iDisplayStart;
			var iEnd = bServerSide ? oSettings.aoData.length : iDisplayEnd;
	
			for ( var j=iStart ; j<iEnd ; j++ )
			{
				var iDataIndex = aiDisplay[j];
				var aoData = oSettings.aoData[ iDataIndex ];
				if ( aoData.nTr === null )
				{
					_fnCreateTr( oSettings, iDataIndex );
				}
	
				var nRow = aoData.nTr;
	
				/* Remove the old striping classes and then add the new one */
				if ( iStripes !== 0 )
				{
					var sStripe = asStripeClasses[ iRowCount % iStripes ];
					if ( aoData._sRowStripe != sStripe )
					{
						$(nRow).removeClass( aoData._sRowStripe ).addClass( sStripe );
						aoData._sRowStripe = sStripe;
					}
				}
	
				// Row callback functions - might want to manipulate the row
				// iRowCount and j are not currently documented. Are they at all
				// useful?
				_fnCallbackFire( oSettings, 'aoRowCallback', null,
					[nRow, aoData._aData, iRowCount, j, iDataIndex] );
	
				anRows.push( nRow );
				iRowCount++;
			}
		}
		else
		{
			/* Table is empty - create a row with an empty message in it */
			var sZero = oLang.sZeroRecords;
			if ( oSettings.iDraw == 1 &&  _fnDataSource( oSettings ) == 'ajax' )
			{
				sZero = oLang.sLoadingRecords;
			}
			else if ( oLang.sEmptyTable && oSettings.fnRecordsTotal() === 0 )
			{
				sZero = oLang.sEmptyTable;
			}
	
			anRows[ 0 ] = $( '<tr/>', { 'class': iStripes ? asStripeClasses[0] : '' } )
				.append( $('<td />', {
					'valign':  'top',
					'colSpan': _fnVisbleColumns( oSettings ),
					'class':   oSettings.oClasses.sRowEmpty
				} ).html( sZero ) )[0];
		}
	
		/* Header and footer callbacks */
		_fnCallbackFire( oSettings, 'aoHeaderCallback', 'header', [ $(oSettings.nTHead).children('tr')[0],
			_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
	
		_fnCallbackFire( oSettings, 'aoFooterCallback', 'footer', [ $(oSettings.nTFoot).children('tr')[0],
			_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
	
		var body = $(oSettings.nTBody);
	
		body.children().detach();
		body.append( $(anRows) );
	
		/* Call all required callback functions for the end of a draw */
		_fnCallbackFire( oSettings, 'aoDrawCallback', 'draw', [oSettings] );
	
		/* Draw is complete, sorting and filtering must be as well */
		oSettings.bSorted = false;
		oSettings.bFiltered = false;
		oSettings.bDrawing = false;
	}
	
	
	/**
	 * Redraw the table - taking account of the various features which are enabled
	 *  @param {object} oSettings dataTables settings object
	 *  @param {boolean} [holdPosition] Keep the current paging position. By default
	 *    the paging is reset to the first page
	 *  @memberof DataTable#oApi
	 */
	function _fnReDraw( settings, holdPosition )
	{
		var
			features = settings.oFeatures,
			sort     = features.bSort,
			filter   = features.bFilter;
	
		if ( sort ) {
			_fnSort( settings );
		}
	
		if ( filter ) {
			_fnFilterComplete( settings, settings.oPreviousSearch );
		}
		else {
			// No filtering, so we want to just use the display master
			settings.aiDisplay = settings.aiDisplayMaster.slice();
		}
	
		if ( holdPosition !== true ) {
			settings._iDisplayStart = 0;
		}
	
		// Let any modules know about the draw hold position state (used by
		// scrolling internally)
		settings._drawHold = holdPosition;
	
		_fnDraw( settings );
	
		settings._drawHold = false;
	}
	
	
	/**
	 * Add the options to the page HTML for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnAddOptionsHtml ( oSettings )
	{
		var classes = oSettings.oClasses;
		var table = $(oSettings.nTable);
		var holding = $('<div/>').insertBefore( table ); // Holding element for speed
		var features = oSettings.oFeatures;
	
		// All DataTables are wrapped in a div
		var insert = $('<div/>', {
			id:      oSettings.sTableId+'_wrapper',
			'class': classes.sWrapper + (oSettings.nTFoot ? '' : ' '+classes.sNoFooter)
		} );
	
		oSettings.nHolding = holding[0];
		oSettings.nTableWrapper = insert[0];
		oSettings.nTableReinsertBefore = oSettings.nTable.nextSibling;
	
		/* Loop over the user set positioning and place the elements as needed */
		var aDom = oSettings.sDom.split('');
		var featureNode, cOption, nNewNode, cNext, sAttr, j;
		for ( var i=0 ; i<aDom.length ; i++ )
		{
			featureNode = null;
			cOption = aDom[i];
	
			if ( cOption == '<' )
			{
				/* New container div */
				nNewNode = $('<div/>')[0];
	
				/* Check to see if we should append an id and/or a class name to the container */
				cNext = aDom[i+1];
				if ( cNext == "'" || cNext == '"' )
				{
					sAttr = "";
					j = 2;
					while ( aDom[i+j] != cNext )
					{
						sAttr += aDom[i+j];
						j++;
					}
	
					/* Replace jQuery UI constants @todo depreciated */
					if ( sAttr == "H" )
					{
						sAttr = classes.sJUIHeader;
					}
					else if ( sAttr == "F" )
					{
						sAttr = classes.sJUIFooter;
					}
	
					/* The attribute can be in the format of "#id.class", "#id" or "class" This logic
					 * breaks the string into parts and applies them as needed
					 */
					if ( sAttr.indexOf('.') != -1 )
					{
						var aSplit = sAttr.split('.');
						nNewNode.id = aSplit[0].substr(1, aSplit[0].length-1);
						nNewNode.className = aSplit[1];
					}
					else if ( sAttr.charAt(0) == "#" )
					{
						nNewNode.id = sAttr.substr(1, sAttr.length-1);
					}
					else
					{
						nNewNode.className = sAttr;
					}
	
					i += j; /* Move along the position array */
				}
	
				insert.append( nNewNode );
				insert = $(nNewNode);
			}
			else if ( cOption == '>' )
			{
				/* End container div */
				insert = insert.parent();
			}
			// @todo Move options into their own plugins?
			else if ( cOption == 'l' && features.bPaginate && features.bLengthChange )
			{
				/* Length */
				featureNode = _fnFeatureHtmlLength( oSettings );
			}
			else if ( cOption == 'f' && features.bFilter )
			{
				/* Filter */
				featureNode = _fnFeatureHtmlFilter( oSettings );
			}
			else if ( cOption == 'r' && features.bProcessing )
			{
				/* pRocessing */
				featureNode = _fnFeatureHtmlProcessing( oSettings );
			}
			else if ( cOption == 't' )
			{
				/* Table */
				featureNode = _fnFeatureHtmlTable( oSettings );
			}
			else if ( cOption ==  'i' && features.bInfo )
			{
				/* Info */
				featureNode = _fnFeatureHtmlInfo( oSettings );
			}
			else if ( cOption == 'p' && features.bPaginate )
			{
				/* Pagination */
				featureNode = _fnFeatureHtmlPaginate( oSettings );
			}
			else if ( DataTable.ext.feature.length !== 0 )
			{
				/* Plug-in features */
				var aoFeatures = DataTable.ext.feature;
				for ( var k=0, kLen=aoFeatures.length ; k<kLen ; k++ )
				{
					if ( cOption == aoFeatures[k].cFeature )
					{
						featureNode = aoFeatures[k].fnInit( oSettings );
						break;
					}
				}
			}
	
			/* Add to the 2D features array */
			if ( featureNode )
			{
				var aanFeatures = oSettings.aanFeatures;
	
				if ( ! aanFeatures[cOption] )
				{
					aanFeatures[cOption] = [];
				}
	
				aanFeatures[cOption].push( featureNode );
				insert.append( featureNode );
			}
		}
	
		/* Built our DOM structure - replace the holding div with what we want */
		holding.replaceWith( insert );
		oSettings.nHolding = null;
	}
	
	
	/**
	 * Use the DOM source to create up an array of header cells. The idea here is to
	 * create a layout grid (array) of rows x columns, which contains a reference
	 * to the cell that that point in the grid (regardless of col/rowspan), such that
	 * any column / row could be removed and the new grid constructed
	 *  @param array {object} aLayout Array to store the calculated layout in
	 *  @param {node} nThead The header/footer element for the table
	 *  @memberof DataTable#oApi
	 */
	function _fnDetectHeader ( aLayout, nThead )
	{
		var nTrs = $(nThead).children('tr');
		var nTr, nCell;
		var i, k, l, iLen, jLen, iColShifted, iColumn, iColspan, iRowspan;
		var bUnique;
		var fnShiftCol = function ( a, i, j ) {
			var k = a[i];
	                while ( k[j] ) {
				j++;
			}
			return j;
		};
	
		aLayout.splice( 0, aLayout.length );
	
		/* We know how many rows there are in the layout - so prep it */
		for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
		{
			aLayout.push( [] );
		}
	
		/* Calculate a layout array */
		for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
		{
			nTr = nTrs[i];
			iColumn = 0;
	
			/* For every cell in the row... */
			nCell = nTr.firstChild;
			while ( nCell ) {
				if ( nCell.nodeName.toUpperCase() == "TD" ||
				     nCell.nodeName.toUpperCase() == "TH" )
				{
					/* Get the col and rowspan attributes from the DOM and sanitise them */
					iColspan = nCell.getAttribute('colspan') * 1;
					iRowspan = nCell.getAttribute('rowspan') * 1;
					iColspan = (!iColspan || iColspan===0 || iColspan===1) ? 1 : iColspan;
					iRowspan = (!iRowspan || iRowspan===0 || iRowspan===1) ? 1 : iRowspan;
	
					/* There might be colspan cells already in this row, so shift our target
					 * accordingly
					 */
					iColShifted = fnShiftCol( aLayout, i, iColumn );
	
					/* Cache calculation for unique columns */
					bUnique = iColspan === 1 ? true : false;
	
					/* If there is col / rowspan, copy the information into the layout grid */
					for ( l=0 ; l<iColspan ; l++ )
					{
						for ( k=0 ; k<iRowspan ; k++ )
						{
							aLayout[i+k][iColShifted+l] = {
								"cell": nCell,
								"unique": bUnique
							};
							aLayout[i+k].nTr = nTr;
						}
					}
				}
				nCell = nCell.nextSibling;
			}
		}
	}
	
	
	/**
	 * Get an array of unique th elements, one for each column
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} nHeader automatically detect the layout from this node - optional
	 *  @param {array} aLayout thead/tfoot layout from _fnDetectHeader - optional
	 *  @returns array {node} aReturn list of unique th's
	 *  @memberof DataTable#oApi
	 */
	function _fnGetUniqueThs ( oSettings, nHeader, aLayout )
	{
		var aReturn = [];
		if ( !aLayout )
		{
			aLayout = oSettings.aoHeader;
			if ( nHeader )
			{
				aLayout = [];
				_fnDetectHeader( aLayout, nHeader );
			}
		}
	
		for ( var i=0, iLen=aLayout.length ; i<iLen ; i++ )
		{
			for ( var j=0, jLen=aLayout[i].length ; j<jLen ; j++ )
			{
				if ( aLayout[i][j].unique &&
					 (!aReturn[j] || !oSettings.bSortCellsTop) )
				{
					aReturn[j] = aLayout[i][j].cell;
				}
			}
		}
	
		return aReturn;
	}
	
	/**
	 * Set the start position for draw
	 *  @param {object} oSettings dataTables settings object
	 */
	function _fnStart( oSettings )
	{
		var bServerSide = _fnDataSource( oSettings ) == 'ssp';
		var iInitDisplayStart = oSettings.iInitDisplayStart;
	
		// Check and see if we have an initial draw position from state saving
		if ( iInitDisplayStart !== undefined && iInitDisplayStart !== -1 )
		{
			oSettings._iDisplayStart = bServerSide ?
				iInitDisplayStart :
				iInitDisplayStart >= oSettings.fnRecordsDisplay() ?
					0 :
					iInitDisplayStart;
	
			oSettings.iInitDisplayStart = -1;
		}
	}
	
	/**
	 * Create an Ajax call based on the table's settings, taking into account that
	 * parameters can have multiple forms, and backwards compatibility.
	 *
	 * @param {object} oSettings dataTables settings object
	 * @param {array} data Data to send to the server, required by
	 *     DataTables - may be augmented by developer callbacks
	 * @param {function} fn Callback function to run when data is obtained
	 */
	function _fnBuildAjax( oSettings, data, fn )
	{
		// Compatibility with 1.9-, allow fnServerData and event to manipulate
		_fnCallbackFire( oSettings, 'aoServerParams', 'serverParams', [data] );
	
		// Convert to object based for 1.10+ if using the old array scheme which can
		// come from server-side processing or serverParams
		if ( data && Array.isArray(data) ) {
			var tmp = {};
			var rbracket = /(.*?)\[\]$/;
	
			$.each( data, function (key, val) {
				var match = val.name.match(rbracket);
	
				if ( match ) {
					// Support for arrays
					var name = match[0];
	
					if ( ! tmp[ name ] ) {
						tmp[ name ] = [];
					}
					tmp[ name ].push( val.value );
				}
				else {
					tmp[val.name] = val.value;
				}
			} );
			data = tmp;
		}
	
		var ajaxData;
		var ajax = oSettings.ajax;
		var instance = oSettings.oInstance;
		var callback = function ( json ) {
			var status = oSettings.jqXHR
				? oSettings.jqXHR.status
				: null;
	
			if ( json === null || (typeof status === 'number' && status == 204 ) ) {
				json = {};
				_fnAjaxDataSrc( oSettings, json, [] );
			}
	
			var error = json.error || json.sError;
			if ( error ) {
				_fnLog( oSettings, 0, error );
			}
	
			oSettings.json = json;
	
			_fnCallbackFire( oSettings, null, 'xhr', [oSettings, json, oSettings.jqXHR] );
			fn( json );
		};
	
		if ( $.isPlainObject( ajax ) && ajax.data )
		{
			ajaxData = ajax.data;
	
			var newData = typeof ajaxData === 'function' ?
				ajaxData( data, oSettings ) :  // fn can manipulate data or return
				ajaxData;                      // an object object or array to merge
	
			// If the function returned something, use that alone
			data = typeof ajaxData === 'function' && newData ?
				newData :
				$.extend( true, data, newData );
	
			// Remove the data property as we've resolved it already and don't want
			// jQuery to do it again (it is restored at the end of the function)
			delete ajax.data;
		}
	
		var baseAjax = {
			"data": data,
			"success": callback,
			"dataType": "json",
			"cache": false,
			"type": oSettings.sServerMethod,
			"error": function (xhr, error, thrown) {
				var ret = _fnCallbackFire( oSettings, null, 'xhr', [oSettings, null, oSettings.jqXHR] );
	
				if ( $.inArray( true, ret ) === -1 ) {
					if ( error == "parsererror" ) {
						_fnLog( oSettings, 0, 'Invalid JSON response', 1 );
					}
					else if ( xhr.readyState === 4 ) {
						_fnLog( oSettings, 0, 'Ajax error', 7 );
					}
				}
	
				_fnProcessingDisplay( oSettings, false );
			}
		};
	
		// Store the data submitted for the API
		oSettings.oAjaxData = data;
	
		// Allow plug-ins and external processes to modify the data
		_fnCallbackFire( oSettings, null, 'preXhr', [oSettings, data] );
	
		if ( oSettings.fnServerData )
		{
			// DataTables 1.9- compatibility
			oSettings.fnServerData.call( instance,
				oSettings.sAjaxSource,
				$.map( data, function (val, key) { // Need to convert back to 1.9 trad format
					return { name: key, value: val };
				} ),
				callback,
				oSettings
			);
		}
		else if ( oSettings.sAjaxSource || typeof ajax === 'string' )
		{
			// DataTables 1.9- compatibility
			oSettings.jqXHR = $.ajax( $.extend( baseAjax, {
				url: ajax || oSettings.sAjaxSource
			} ) );
		}
		else if ( typeof ajax === 'function' )
		{
			// Is a function - let the caller define what needs to be done
			oSettings.jqXHR = ajax.call( instance, data, callback, oSettings );
		}
		else
		{
			// Object to extend the base settings
			oSettings.jqXHR = $.ajax( $.extend( baseAjax, ajax ) );
	
			// Restore for next time around
			ajax.data = ajaxData;
		}
	}
	
	
	/**
	 * Update the table using an Ajax call
	 *  @param {object} settings dataTables settings object
	 *  @returns {boolean} Block the table drawing or not
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxUpdate( settings )
	{
		settings.iDraw++;
		_fnProcessingDisplay( settings, true );
	
		_fnBuildAjax(
			settings,
			_fnAjaxParameters( settings ),
			function(json) {
				_fnAjaxUpdateDraw( settings, json );
			}
		);
	}
	
	
	/**
	 * Build up the parameters in an object needed for a server-side processing
	 * request. Note that this is basically done twice, is different ways - a modern
	 * method which is used by default in DataTables 1.10 which uses objects and
	 * arrays, or the 1.9- method with is name / value pairs. 1.9 method is used if
	 * the sAjaxSource option is used in the initialisation, or the legacyAjax
	 * option is set.
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {bool} block the table drawing or not
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxParameters( settings )
	{
		var
			columns = settings.aoColumns,
			columnCount = columns.length,
			features = settings.oFeatures,
			preSearch = settings.oPreviousSearch,
			preColSearch = settings.aoPreSearchCols,
			i, data = [], dataProp, column, columnSearch,
			sort = _fnSortFlatten( settings ),
			displayStart = settings._iDisplayStart,
			displayLength = features.bPaginate !== false ?
				settings._iDisplayLength :
				-1;
	
		var param = function ( name, value ) {
			data.push( { 'name': name, 'value': value } );
		};
	
		// DataTables 1.9- compatible method
		param( 'sEcho',          settings.iDraw );
		param( 'iColumns',       columnCount );
		param( 'sColumns',       _pluck( columns, 'sName' ).join(',') );
		param( 'iDisplayStart',  displayStart );
		param( 'iDisplayLength', displayLength );
	
		// DataTables 1.10+ method
		var d = {
			draw:    settings.iDraw,
			columns: [],
			order:   [],
			start:   displayStart,
			length:  displayLength,
			search:  {
				value: preSearch.sSearch,
				regex: preSearch.bRegex
			}
		};
	
		for ( i=0 ; i<columnCount ; i++ ) {
			column = columns[i];
			columnSearch = preColSearch[i];
			dataProp = typeof column.mData=="function" ? 'function' : column.mData ;
	
			d.columns.push( {
				data:       dataProp,
				name:       column.sName,
				searchable: column.bSearchable,
				orderable:  column.bSortable,
				search:     {
					value: columnSearch.sSearch,
					regex: columnSearch.bRegex
				}
			} );
	
			param( "mDataProp_"+i, dataProp );
	
			if ( features.bFilter ) {
				param( 'sSearch_'+i,     columnSearch.sSearch );
				param( 'bRegex_'+i,      columnSearch.bRegex );
				param( 'bSearchable_'+i, column.bSearchable );
			}
	
			if ( features.bSort ) {
				param( 'bSortable_'+i, column.bSortable );
			}
		}
	
		if ( features.bFilter ) {
			param( 'sSearch', preSearch.sSearch );
			param( 'bRegex', preSearch.bRegex );
		}
	
		if ( features.bSort ) {
			$.each( sort, function ( i, val ) {
				d.order.push( { column: val.col, dir: val.dir } );
	
				param( 'iSortCol_'+i, val.col );
				param( 'sSortDir_'+i, val.dir );
			} );
	
			param( 'iSortingCols', sort.length );
		}
	
		// If the legacy.ajax parameter is null, then we automatically decide which
		// form to use, based on sAjaxSource
		var legacy = DataTable.ext.legacy.ajax;
		if ( legacy === null ) {
			return settings.sAjaxSource ? data : d;
		}
	
		// Otherwise, if legacy has been specified then we use that to decide on the
		// form
		return legacy ? data : d;
	}
	
	
	/**
	 * Data the data from the server (nuking the old) and redraw the table
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} json json data return from the server.
	 *  @param {string} json.sEcho Tracking flag for DataTables to match requests
	 *  @param {int} json.iTotalRecords Number of records in the data set, not accounting for filtering
	 *  @param {int} json.iTotalDisplayRecords Number of records in the data set, accounting for filtering
	 *  @param {array} json.aaData The data to display on this page
	 *  @param {string} [json.sColumns] Column ordering (sName, comma separated)
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxUpdateDraw ( settings, json )
	{
		// v1.10 uses camelCase variables, while 1.9 uses Hungarian notation.
		// Support both
		var compat = function ( old, modern ) {
			return json[old] !== undefined ? json[old] : json[modern];
		};
	
		var data = _fnAjaxDataSrc( settings, json );
		var draw            = compat( 'sEcho',                'draw' );
		var recordsTotal    = compat( 'iTotalRecords',        'recordsTotal' );
		var recordsFiltered = compat( 'iTotalDisplayRecords', 'recordsFiltered' );
	
		if ( draw !== undefined ) {
			// Protect against out of sequence returns
			if ( draw*1 < settings.iDraw ) {
				return;
			}
			settings.iDraw = draw * 1;
		}
	
		// No data in returned object, so rather than an array, we show an empty table
		if ( ! data ) {
			data = [];
		}
	
		_fnClearTable( settings );
		settings._iRecordsTotal   = parseInt(recordsTotal, 10);
		settings._iRecordsDisplay = parseInt(recordsFiltered, 10);
	
		for ( var i=0, ien=data.length ; i<ien ; i++ ) {
			_fnAddData( settings, data[i] );
		}
		settings.aiDisplay = settings.aiDisplayMaster.slice();
	
		_fnDraw( settings, true );
	
		if ( ! settings._bInitComplete ) {
			_fnInitComplete( settings, json );
		}
	
		_fnProcessingDisplay( settings, false );
	}
	
	
	/**
	 * Get the data from the JSON data source to use for drawing a table. Using
	 * `_fnGetObjectDataFn` allows the data to be sourced from a property of the
	 * source object, or from a processing function.
	 *  @param {object} oSettings dataTables settings object
	 *  @param  {object} json Data source object / array from the server
	 *  @return {array} Array of data to use
	 */
	 function _fnAjaxDataSrc ( oSettings, json, write )
	 {
		var dataSrc = $.isPlainObject( oSettings.ajax ) && oSettings.ajax.dataSrc !== undefined ?
			oSettings.ajax.dataSrc :
			oSettings.sAjaxDataProp; // Compatibility with 1.9-.
	
		if ( ! write ) {
			if ( dataSrc === 'data' ) {
				// If the default, then we still want to support the old style, and safely ignore
				// it if possible
				return json.aaData || json[dataSrc];
			}
	
			return dataSrc !== "" ?
				_fnGetObjectDataFn( dataSrc )( json ) :
				json;
		}
	
		// set
		_fnSetObjectDataFn( dataSrc )( json, write );
	}
	
	/**
	 * Generate the node required for filtering text
	 *  @returns {node} Filter control element
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlFilter ( settings )
	{
		var classes = settings.oClasses;
		var tableId = settings.sTableId;
		var language = settings.oLanguage;
		var previousSearch = settings.oPreviousSearch;
		var features = settings.aanFeatures;
		var input = '<input type="search" class="'+classes.sFilterInput+'"/>';
	
		var str = language.sSearch;
		str = str.match(/_INPUT_/) ?
			str.replace('_INPUT_', input) :
			str+input;
	
		var filter = $('<div/>', {
				'id': ! features.f ? tableId+'_filter' : null,
				'class': classes.sFilter
			} )
			.append( $('<label/>' ).append( str ) );
	
		var searchFn = function(event) {
			/* Update all other filter input elements for the new display */
			var n = features.f;
			var val = !this.value ? "" : this.value; // mental IE8 fix :-(
			if(previousSearch.return && event.key !== "Enter") {
				return;
			}
			/* Now do the filter */
			if ( val != previousSearch.sSearch ) {
				_fnFilterComplete( settings, {
					"sSearch": val,
					"bRegex": previousSearch.bRegex,
					"bSmart": previousSearch.bSmart ,
					"bCaseInsensitive": previousSearch.bCaseInsensitive,
					"return": previousSearch.return
				} );
	
				// Need to redraw, without resorting
				settings._iDisplayStart = 0;
				_fnDraw( settings );
			}
		};
	
		var searchDelay = settings.searchDelay !== null ?
			settings.searchDelay :
			_fnDataSource( settings ) === 'ssp' ?
				400 :
				0;
	
		var jqFilter = $('input', filter)
			.val( previousSearch.sSearch )
			.attr( 'placeholder', language.sSearchPlaceholder )
			.on(
				'keyup.DT search.DT input.DT paste.DT cut.DT',
				searchDelay ?
					_fnThrottle( searchFn, searchDelay ) :
					searchFn
			)
			.on( 'mouseup', function(e) {
				// Edge fix! Edge 17 does not trigger anything other than mouse events when clicking
				// on the clear icon (Edge bug 17584515). This is safe in other browsers as `searchFn`
				// checks the value to see if it has changed. In other browsers it won't have.
				setTimeout( function () {
					searchFn.call(jqFilter[0], e);
				}, 10);
			} )
			.on( 'keypress.DT', function(e) {
				/* Prevent form submission */
				if ( e.keyCode == 13 ) {
					return false;
				}
			} )
			.attr('aria-controls', tableId);
	
		// Update the input elements whenever the table is filtered
		$(settings.nTable).on( 'search.dt.DT', function ( ev, s ) {
			if ( settings === s ) {
				// IE9 throws an 'unknown error' if document.activeElement is used
				// inside an iframe or frame...
				try {
					if ( jqFilter[0] !== document.activeElement ) {
						jqFilter.val( previousSearch.sSearch );
					}
				}
				catch ( e ) {}
			}
		} );
	
		return filter[0];
	}
	
	
	/**
	 * Filter the table using both the global filter and column based filtering
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} oSearch search information
	 *  @param {int} [iForce] force a research of the master array (1) or not (undefined or 0)
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterComplete ( oSettings, oInput, iForce )
	{
		var oPrevSearch = oSettings.oPreviousSearch;
		var aoPrevSearch = oSettings.aoPreSearchCols;
		var fnSaveFilter = function ( oFilter ) {
			/* Save the filtering values */
			oPrevSearch.sSearch = oFilter.sSearch;
			oPrevSearch.bRegex = oFilter.bRegex;
			oPrevSearch.bSmart = oFilter.bSmart;
			oPrevSearch.bCaseInsensitive = oFilter.bCaseInsensitive;
			oPrevSearch.return = oFilter.return;
		};
		var fnRegex = function ( o ) {
			// Backwards compatibility with the bEscapeRegex option
			return o.bEscapeRegex !== undefined ? !o.bEscapeRegex : o.bRegex;
		};
	
		// Resolve any column types that are unknown due to addition or invalidation
		// @todo As per sort - can this be moved into an event handler?
		_fnColumnTypes( oSettings );
	
		/* In server-side processing all filtering is done by the server, so no point hanging around here */
		if ( _fnDataSource( oSettings ) != 'ssp' )
		{
			/* Global filter */
			_fnFilter( oSettings, oInput.sSearch, iForce, fnRegex(oInput), oInput.bSmart, oInput.bCaseInsensitive, oInput.return );
			fnSaveFilter( oInput );
	
			/* Now do the individual column filter */
			for ( var i=0 ; i<aoPrevSearch.length ; i++ )
			{
				_fnFilterColumn( oSettings, aoPrevSearch[i].sSearch, i, fnRegex(aoPrevSearch[i]),
					aoPrevSearch[i].bSmart, aoPrevSearch[i].bCaseInsensitive );
			}
	
			/* Custom filtering */
			_fnFilterCustom( oSettings );
		}
		else
		{
			fnSaveFilter( oInput );
		}
	
		/* Tell the draw function we have been filtering */
		oSettings.bFiltered = true;
		_fnCallbackFire( oSettings, null, 'search', [oSettings] );
	}
	
	
	/**
	 * Apply custom filtering functions
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterCustom( settings )
	{
		var filters = DataTable.ext.search;
		var displayRows = settings.aiDisplay;
		var row, rowIdx;
	
		for ( var i=0, ien=filters.length ; i<ien ; i++ ) {
			var rows = [];
	
			// Loop over each row and see if it should be included
			for ( var j=0, jen=displayRows.length ; j<jen ; j++ ) {
				rowIdx = displayRows[ j ];
				row = settings.aoData[ rowIdx ];
	
				if ( filters[i]( settings, row._aFilterData, rowIdx, row._aData, j ) ) {
					rows.push( rowIdx );
				}
			}
	
			// So the array reference doesn't break set the results into the
			// existing array
			displayRows.length = 0;
			$.merge( displayRows, rows );
		}
	}
	
	
	/**
	 * Filter the table on a per-column basis
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sInput string to filter on
	 *  @param {int} iColumn column to filter
	 *  @param {bool} bRegex treat search string as a regular expression or not
	 *  @param {bool} bSmart use smart filtering or not
	 *  @param {bool} bCaseInsensitive Do case insensitive matching or not
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterColumn ( settings, searchStr, colIdx, regex, smart, caseInsensitive )
	{
		if ( searchStr === '' ) {
			return;
		}
	
		var data;
		var out = [];
		var display = settings.aiDisplay;
		var rpSearch = _fnFilterCreateSearch( searchStr, regex, smart, caseInsensitive );
	
		for ( var i=0 ; i<display.length ; i++ ) {
			data = settings.aoData[ display[i] ]._aFilterData[ colIdx ];
	
			if ( rpSearch.test( data ) ) {
				out.push( display[i] );
			}
		}
	
		settings.aiDisplay = out;
	}
	
	
	/**
	 * Filter the data table based on user input and draw the table
	 *  @param {object} settings dataTables settings object
	 *  @param {string} input string to filter on
	 *  @param {int} force optional - force a research of the master array (1) or not (undefined or 0)
	 *  @param {bool} regex treat as a regular expression or not
	 *  @param {bool} smart perform smart filtering or not
	 *  @param {bool} caseInsensitive Do case insensitive matching or not
	 *  @memberof DataTable#oApi
	 */
	function _fnFilter( settings, input, force, regex, smart, caseInsensitive )
	{
		var rpSearch = _fnFilterCreateSearch( input, regex, smart, caseInsensitive );
		var prevSearch = settings.oPreviousSearch.sSearch;
		var displayMaster = settings.aiDisplayMaster;
		var display, invalidated, i;
		var filtered = [];
	
		// Need to take account of custom filtering functions - always filter
		if ( DataTable.ext.search.length !== 0 ) {
			force = true;
		}
	
		// Check if any of the rows were invalidated
		invalidated = _fnFilterData( settings );
	
		// If the input is blank - we just want the full data set
		if ( input.length <= 0 ) {
			settings.aiDisplay = displayMaster.slice();
		}
		else {
			// New search - start from the master array
			if ( invalidated ||
				 force ||
				 regex ||
				 prevSearch.length > input.length ||
				 input.indexOf(prevSearch) !== 0 ||
				 settings.bSorted // On resort, the display master needs to be
				                  // re-filtered since indexes will have changed
			) {
				settings.aiDisplay = displayMaster.slice();
			}
	
			// Search the display array
			display = settings.aiDisplay;
	
			for ( i=0 ; i<display.length ; i++ ) {
				if ( rpSearch.test( settings.aoData[ display[i] ]._sFilterRow ) ) {
					filtered.push( display[i] );
				}
			}
	
			settings.aiDisplay = filtered;
		}
	}
	
	
	/**
	 * Build a regular expression object suitable for searching a table
	 *  @param {string} sSearch string to search for
	 *  @param {bool} bRegex treat as a regular expression or not
	 *  @param {bool} bSmart perform smart filtering or not
	 *  @param {bool} bCaseInsensitive Do case insensitive matching or not
	 *  @returns {RegExp} constructed object
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterCreateSearch( search, regex, smart, caseInsensitive )
	{
		search = regex ?
			search :
			_fnEscapeRegex( search );
		
		if ( smart ) {
			/* For smart filtering we want to allow the search to work regardless of
			 * word order. We also want double quoted text to be preserved, so word
			 * order is important - a la google. So this is what we want to
			 * generate:
			 * 
			 * ^(?=.*?\bone\b)(?=.*?\btwo three\b)(?=.*?\bfour\b).*$
			 */
			var a = $.map( search.match( /"[^"]+"|[^ ]+/g ) || [''], function ( word ) {
				if ( word.charAt(0) === '"' ) {
					var m = word.match( /^"(.*)"$/ );
					word = m ? m[1] : word;
				}
	
				return word.replace('"', '');
			} );
	
			search = '^(?=.*?'+a.join( ')(?=.*?' )+').*$';
		}
	
		return new RegExp( search, caseInsensitive ? 'i' : '' );
	}
	
	
	/**
	 * Escape a string such that it can be used in a regular expression
	 *  @param {string} sVal string to escape
	 *  @returns {string} escaped string
	 *  @memberof DataTable#oApi
	 */
	var _fnEscapeRegex = DataTable.util.escapeRegex;
	
	var __filter_div = $('<div>')[0];
	var __filter_div_textContent = __filter_div.textContent !== undefined;
	
	// Update the filtering data for each row if needed (by invalidation or first run)
	function _fnFilterData ( settings )
	{
		var columns = settings.aoColumns;
		var column;
		var i, j, ien, jen, filterData, cellData, row;
		var wasInvalidated = false;
	
		for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			row = settings.aoData[i];
	
			if ( ! row._aFilterData ) {
				filterData = [];
	
				for ( j=0, jen=columns.length ; j<jen ; j++ ) {
					column = columns[j];
	
					if ( column.bSearchable ) {
						cellData = _fnGetCellData( settings, i, j, 'filter' );
	
						// Search in DataTables 1.10 is string based. In 1.11 this
						// should be altered to also allow strict type checking.
						if ( cellData === null ) {
							cellData = '';
						}
	
						if ( typeof cellData !== 'string' && cellData.toString ) {
							cellData = cellData.toString();
						}
					}
					else {
						cellData = '';
					}
	
					// If it looks like there is an HTML entity in the string,
					// attempt to decode it so sorting works as expected. Note that
					// we could use a single line of jQuery to do this, but the DOM
					// method used here is much faster http://jsperf.com/html-decode
					if ( cellData.indexOf && cellData.indexOf('&') !== -1 ) {
						__filter_div.innerHTML = cellData;
						cellData = __filter_div_textContent ?
							__filter_div.textContent :
							__filter_div.innerText;
					}
	
					if ( cellData.replace ) {
						cellData = cellData.replace(/[\r\n\u2028]/g, '');
					}
	
					filterData.push( cellData );
				}
	
				row._aFilterData = filterData;
				row._sFilterRow = filterData.join('  ');
				wasInvalidated = true;
			}
		}
	
		return wasInvalidated;
	}
	
	
	/**
	 * Convert from the internal Hungarian notation to camelCase for external
	 * interaction
	 *  @param {object} obj Object to convert
	 *  @returns {object} Inverted object
	 *  @memberof DataTable#oApi
	 */
	function _fnSearchToCamel ( obj )
	{
		return {
			search:          obj.sSearch,
			smart:           obj.bSmart,
			regex:           obj.bRegex,
			caseInsensitive: obj.bCaseInsensitive
		};
	}
	
	
	
	/**
	 * Convert from camelCase notation to the internal Hungarian. We could use the
	 * Hungarian convert function here, but this is cleaner
	 *  @param {object} obj Object to convert
	 *  @returns {object} Inverted object
	 *  @memberof DataTable#oApi
	 */
	function _fnSearchToHung ( obj )
	{
		return {
			sSearch:          obj.search,
			bSmart:           obj.smart,
			bRegex:           obj.regex,
			bCaseInsensitive: obj.caseInsensitive
		};
	}
	
	/**
	 * Generate the node required for the info display
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {node} Information element
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlInfo ( settings )
	{
		var
			tid = settings.sTableId,
			nodes = settings.aanFeatures.i,
			n = $('<div/>', {
				'class': settings.oClasses.sInfo,
				'id': ! nodes ? tid+'_info' : null
			} );
	
		if ( ! nodes ) {
			// Update display on each draw
			settings.aoDrawCallback.push( {
				"fn": _fnUpdateInfo,
				"sName": "information"
			} );
	
			n
				.attr( 'role', 'status' )
				.attr( 'aria-live', 'polite' );
	
			// Table is described by our info div
			$(settings.nTable).attr( 'aria-describedby', tid+'_info' );
		}
	
		return n[0];
	}
	
	
	/**
	 * Update the information elements in the display
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnUpdateInfo ( settings )
	{
		/* Show information about the table */
		var nodes = settings.aanFeatures.i;
		if ( nodes.length === 0 ) {
			return;
		}
	
		var
			lang  = settings.oLanguage,
			start = settings._iDisplayStart+1,
			end   = settings.fnDisplayEnd(),
			max   = settings.fnRecordsTotal(),
			total = settings.fnRecordsDisplay(),
			out   = total ?
				lang.sInfo :
				lang.sInfoEmpty;
	
		if ( total !== max ) {
			/* Record set after filtering */
			out += ' ' + lang.sInfoFiltered;
		}
	
		// Convert the macros
		out += lang.sInfoPostFix;
		out = _fnInfoMacros( settings, out );
	
		var callback = lang.fnInfoCallback;
		if ( callback !== null ) {
			out = callback.call( settings.oInstance,
				settings, start, end, max, total, out
			);
		}
	
		$(nodes).html( out );
	}
	
	
	function _fnInfoMacros ( settings, str )
	{
		// When infinite scrolling, we are always starting at 1. _iDisplayStart is used only
		// internally
		var
			formatter  = settings.fnFormatNumber,
			start      = settings._iDisplayStart+1,
			len        = settings._iDisplayLength,
			vis        = settings.fnRecordsDisplay(),
			all        = len === -1;
	
		return str.
			replace(/_START_/g, formatter.call( settings, start ) ).
			replace(/_END_/g,   formatter.call( settings, settings.fnDisplayEnd() ) ).
			replace(/_MAX_/g,   formatter.call( settings, settings.fnRecordsTotal() ) ).
			replace(/_TOTAL_/g, formatter.call( settings, vis ) ).
			replace(/_PAGE_/g,  formatter.call( settings, all ? 1 : Math.ceil( start / len ) ) ).
			replace(/_PAGES_/g, formatter.call( settings, all ? 1 : Math.ceil( vis / len ) ) );
	}
	
	
	
	/**
	 * Draw the table for the first time, adding all required features
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnInitialise ( settings )
	{
		var i, iLen, iAjaxStart=settings.iInitDisplayStart;
		var columns = settings.aoColumns, column;
		var features = settings.oFeatures;
		var deferLoading = settings.bDeferLoading; // value modified by the draw
	
		/* Ensure that the table data is fully initialised */
		if ( ! settings.bInitialised ) {
			setTimeout( function(){ _fnInitialise( settings ); }, 200 );
			return;
		}
	
		/* Show the display HTML options */
		_fnAddOptionsHtml( settings );
	
		/* Build and draw the header / footer for the table */
		_fnBuildHead( settings );
		_fnDrawHead( settings, settings.aoHeader );
		_fnDrawHead( settings, settings.aoFooter );
	
		/* Okay to show that something is going on now */
		_fnProcessingDisplay( settings, true );
	
		/* Calculate sizes for columns */
		if ( features.bAutoWidth ) {
			_fnCalculateColumnWidths( settings );
		}
	
		for ( i=0, iLen=columns.length ; i<iLen ; i++ ) {
			column = columns[i];
	
			if ( column.sWidth ) {
				column.nTh.style.width = _fnStringToCss( column.sWidth );
			}
		}
	
		_fnCallbackFire( settings, null, 'preInit', [settings] );
	
		// If there is default sorting required - let's do it. The sort function
		// will do the drawing for us. Otherwise we draw the table regardless of the
		// Ajax source - this allows the table to look initialised for Ajax sourcing
		// data (show 'loading' message possibly)
		_fnReDraw( settings );
	
		// Server-side processing init complete is done by _fnAjaxUpdateDraw
		var dataSrc = _fnDataSource( settings );
		if ( dataSrc != 'ssp' || deferLoading ) {
			// if there is an ajax source load the data
			if ( dataSrc == 'ajax' ) {
				_fnBuildAjax( settings, [], function(json) {
					var aData = _fnAjaxDataSrc( settings, json );
	
					// Got the data - add it to the table
					for ( i=0 ; i<aData.length ; i++ ) {
						_fnAddData( settings, aData[i] );
					}
	
					// Reset the init display for cookie saving. We've already done
					// a filter, and therefore cleared it before. So we need to make
					// it appear 'fresh'
					settings.iInitDisplayStart = iAjaxStart;
	
					_fnReDraw( settings );
	
					_fnProcessingDisplay( settings, false );
					_fnInitComplete( settings, json );
				}, settings );
			}
			else {
				_fnProcessingDisplay( settings, false );
				_fnInitComplete( settings );
			}
		}
	}
	
	
	/**
	 * Draw the table for the first time, adding all required features
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} [json] JSON from the server that completed the table, if using Ajax source
	 *    with client-side processing (optional)
	 *  @memberof DataTable#oApi
	 */
	function _fnInitComplete ( settings, json )
	{
		settings._bInitComplete = true;
	
		// When data was added after the initialisation (data or Ajax) we need to
		// calculate the column sizing
		if ( json || settings.oInit.aaData ) {
			_fnAdjustColumnSizing( settings );
		}
	
		_fnCallbackFire( settings, null, 'plugin-init', [settings, json] );
		_fnCallbackFire( settings, 'aoInitComplete', 'init', [settings, json] );
	}
	
	
	function _fnLengthChange ( settings, val )
	{
		var len = parseInt( val, 10 );
		settings._iDisplayLength = len;
	
		_fnLengthOverflow( settings );
	
		// Fire length change event
		_fnCallbackFire( settings, null, 'length', [settings, len] );
	}
	
	
	/**
	 * Generate the node required for user display length changing
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Display length feature node
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlLength ( settings )
	{
		var
			classes  = settings.oClasses,
			tableId  = settings.sTableId,
			menu     = settings.aLengthMenu,
			d2       = Array.isArray( menu[0] ),
			lengths  = d2 ? menu[0] : menu,
			language = d2 ? menu[1] : menu;
	
		var select = $('<select/>', {
			'name':          tableId+'_length',
			'aria-controls': tableId,
			'class':         classes.sLengthSelect
		} );
	
		for ( var i=0, ien=lengths.length ; i<ien ; i++ ) {
			select[0][ i ] = new Option(
				typeof language[i] === 'number' ?
					settings.fnFormatNumber( language[i] ) :
					language[i],
				lengths[i]
			);
		}
	
		var div = $('<div><label/></div>').addClass( classes.sLength );
		if ( ! settings.aanFeatures.l ) {
			div[0].id = tableId+'_length';
		}
	
		div.children().append(
			settings.oLanguage.sLengthMenu.replace( '_MENU_', select[0].outerHTML )
		);
	
		// Can't use `select` variable as user might provide their own and the
		// reference is broken by the use of outerHTML
		$('select', div)
			.val( settings._iDisplayLength )
			.on( 'change.DT', function(e) {
				_fnLengthChange( settings, $(this).val() );
				_fnDraw( settings );
			} );
	
		// Update node value whenever anything changes the table's length
		$(settings.nTable).on( 'length.dt.DT', function (e, s, len) {
			if ( settings === s ) {
				$('select', div).val( len );
			}
		} );
	
		return div[0];
	}
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Note that most of the paging logic is done in
	 * DataTable.ext.pager
	 */
	
	/**
	 * Generate the node required for default pagination
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {node} Pagination feature node
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlPaginate ( settings )
	{
		var
			type   = settings.sPaginationType,
			plugin = DataTable.ext.pager[ type ],
			modern = typeof plugin === 'function',
			redraw = function( settings ) {
				_fnDraw( settings );
			},
			node = $('<div/>').addClass( settings.oClasses.sPaging + type )[0],
			features = settings.aanFeatures;
	
		if ( ! modern ) {
			plugin.fnInit( settings, node, redraw );
		}
	
		/* Add a draw callback for the pagination on first instance, to update the paging display */
		if ( ! features.p )
		{
			node.id = settings.sTableId+'_paginate';
	
			settings.aoDrawCallback.push( {
				"fn": function( settings ) {
					if ( modern ) {
						var
							start      = settings._iDisplayStart,
							len        = settings._iDisplayLength,
							visRecords = settings.fnRecordsDisplay(),
							all        = len === -1,
							page = all ? 0 : Math.ceil( start / len ),
							pages = all ? 1 : Math.ceil( visRecords / len ),
							buttons = plugin(page, pages),
							i, ien;
	
						for ( i=0, ien=features.p.length ; i<ien ; i++ ) {
							_fnRenderer( settings, 'pageButton' )(
								settings, features.p[i], i, buttons, page, pages
							);
						}
					}
					else {
						plugin.fnUpdate( settings, redraw );
					}
				},
				"sName": "pagination"
			} );
		}
	
		return node;
	}
	
	
	/**
	 * Alter the display settings to change the page
	 *  @param {object} settings DataTables settings object
	 *  @param {string|int} action Paging action to take: "first", "previous",
	 *    "next" or "last" or page number to jump to (integer)
	 *  @param [bool] redraw Automatically draw the update or not
	 *  @returns {bool} true page has changed, false - no change
	 *  @memberof DataTable#oApi
	 */
	function _fnPageChange ( settings, action, redraw )
	{
		var
			start     = settings._iDisplayStart,
			len       = settings._iDisplayLength,
			records   = settings.fnRecordsDisplay();
	
		if ( records === 0 || len === -1 )
		{
			start = 0;
		}
		else if ( typeof action === "number" )
		{
			start = action * len;
	
			if ( start > records )
			{
				start = 0;
			}
		}
		else if ( action == "first" )
		{
			start = 0;
		}
		else if ( action == "previous" )
		{
			start = len >= 0 ?
				start - len :
				0;
	
			if ( start < 0 )
			{
			  start = 0;
			}
		}
		else if ( action == "next" )
		{
			if ( start + len < records )
			{
				start += len;
			}
		}
		else if ( action == "last" )
		{
			start = Math.floor( (records-1) / len) * len;
		}
		else
		{
			_fnLog( settings, 0, "Unknown paging action: "+action, 5 );
		}
	
		var changed = settings._iDisplayStart !== start;
		settings._iDisplayStart = start;
	
		if ( changed ) {
			_fnCallbackFire( settings, null, 'page', [settings] );
	
			if ( redraw ) {
				_fnDraw( settings );
			}
		}
	
		return changed;
	}
	
	
	
	/**
	 * Generate the node required for the processing node
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Processing element
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlProcessing ( settings )
	{
		return $('<div/>', {
				'id': ! settings.aanFeatures.r ? settings.sTableId+'_processing' : null,
				'class': settings.oClasses.sProcessing
			} )
			.html( settings.oLanguage.sProcessing )
			.append('<div><div></div><div></div><div></div><div></div></div>')
			.insertBefore( settings.nTable )[0];
	}
	
	
	/**
	 * Display or hide the processing indicator
	 *  @param {object} settings dataTables settings object
	 *  @param {bool} show Show the processing indicator (true) or not (false)
	 *  @memberof DataTable#oApi
	 */
	function _fnProcessingDisplay ( settings, show )
	{
		if ( settings.oFeatures.bProcessing ) {
			$(settings.aanFeatures.r).css( 'display', show ? 'block' : 'none' );
		}
	
		_fnCallbackFire( settings, null, 'processing', [settings, show] );
	}
	
	/**
	 * Add any control elements for the table - specifically scrolling
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Node to add to the DOM
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlTable ( settings )
	{
		var table = $(settings.nTable);
	
		// Scrolling from here on in
		var scroll = settings.oScroll;
	
		if ( scroll.sX === '' && scroll.sY === '' ) {
			return settings.nTable;
		}
	
		var scrollX = scroll.sX;
		var scrollY = scroll.sY;
		var classes = settings.oClasses;
		var caption = table.children('caption');
		var captionSide = caption.length ? caption[0]._captionSide : null;
		var headerClone = $( table[0].cloneNode(false) );
		var footerClone = $( table[0].cloneNode(false) );
		var footer = table.children('tfoot');
		var _div = '<div/>';
		var size = function ( s ) {
			return !s ? null : _fnStringToCss( s );
		};
	
		if ( ! footer.length ) {
			footer = null;
		}
	
		/*
		 * The HTML structure that we want to generate in this function is:
		 *  div - scroller
		 *    div - scroll head
		 *      div - scroll head inner
		 *        table - scroll head table
		 *          thead - thead
		 *    div - scroll body
		 *      table - table (master table)
		 *        thead - thead clone for sizing
		 *        tbody - tbody
		 *    div - scroll foot
		 *      div - scroll foot inner
		 *        table - scroll foot table
		 *          tfoot - tfoot
		 */
		var scroller = $( _div, { 'class': classes.sScrollWrapper } )
			.append(
				$(_div, { 'class': classes.sScrollHead } )
					.css( {
						overflow: 'hidden',
						position: 'relative',
						border: 0,
						width: scrollX ? size(scrollX) : '100%'
					} )
					.append(
						$(_div, { 'class': classes.sScrollHeadInner } )
							.css( {
								'box-sizing': 'content-box',
								width: scroll.sXInner || '100%'
							} )
							.append(
								headerClone
									.removeAttr('id')
									.css( 'margin-left', 0 )
									.append( captionSide === 'top' ? caption : null )
									.append(
										table.children('thead')
									)
							)
					)
			)
			.append(
				$(_div, { 'class': classes.sScrollBody } )
					.css( {
						position: 'relative',
						overflow: 'auto',
						width: size( scrollX )
					} )
					.append( table )
			);
	
		if ( footer ) {
			scroller.append(
				$(_div, { 'class': classes.sScrollFoot } )
					.css( {
						overflow: 'hidden',
						border: 0,
						width: scrollX ? size(scrollX) : '100%'
					} )
					.append(
						$(_div, { 'class': classes.sScrollFootInner } )
							.append(
								footerClone
									.removeAttr('id')
									.css( 'margin-left', 0 )
									.append( captionSide === 'bottom' ? caption : null )
									.append(
										table.children('tfoot')
									)
							)
					)
			);
		}
	
		var children = scroller.children();
		var scrollHead = children[0];
		var scrollBody = children[1];
		var scrollFoot = footer ? children[2] : null;
	
		// When the body is scrolled, then we also want to scroll the headers
		if ( scrollX ) {
			$(scrollBody).on( 'scroll.DT', function (e) {
				var scrollLeft = this.scrollLeft;
	
				scrollHead.scrollLeft = scrollLeft;
	
				if ( footer ) {
					scrollFoot.scrollLeft = scrollLeft;
				}
			} );
		}
	
		$(scrollBody).css('max-height', scrollY);
		if (! scroll.bCollapse) {
			$(scrollBody).css('height', scrollY);
		}
	
		settings.nScrollHead = scrollHead;
		settings.nScrollBody = scrollBody;
		settings.nScrollFoot = scrollFoot;
	
		// On redraw - align columns
		settings.aoDrawCallback.push( {
			"fn": _fnScrollDraw,
			"sName": "scrolling"
		} );
	
		return scroller[0];
	}
	
	
	
	/**
	 * Update the header, footer and body tables for resizing - i.e. column
	 * alignment.
	 *
	 * Welcome to the most horrible function DataTables. The process that this
	 * function follows is basically:
	 *   1. Re-create the table inside the scrolling div
	 *   2. Take live measurements from the DOM
	 *   3. Apply the measurements to align the columns
	 *   4. Clean up
	 *
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnScrollDraw ( settings )
	{
		// Given that this is such a monster function, a lot of variables are use
		// to try and keep the minimised size as small as possible
		var
			scroll         = settings.oScroll,
			scrollX        = scroll.sX,
			scrollXInner   = scroll.sXInner,
			scrollY        = scroll.sY,
			barWidth       = scroll.iBarWidth,
			divHeader      = $(settings.nScrollHead),
			divHeaderStyle = divHeader[0].style,
			divHeaderInner = divHeader.children('div'),
			divHeaderInnerStyle = divHeaderInner[0].style,
			divHeaderTable = divHeaderInner.children('table'),
			divBodyEl      = settings.nScrollBody,
			divBody        = $(divBodyEl),
			divBodyStyle   = divBodyEl.style,
			divFooter      = $(settings.nScrollFoot),
			divFooterInner = divFooter.children('div'),
			divFooterTable = divFooterInner.children('table'),
			header         = $(settings.nTHead),
			table          = $(settings.nTable),
			tableEl        = table[0],
			tableStyle     = tableEl.style,
			footer         = settings.nTFoot ? $(settings.nTFoot) : null,
			browser        = settings.oBrowser,
			ie67           = browser.bScrollOversize,
			dtHeaderCells  = _pluck( settings.aoColumns, 'nTh' ),
			headerTrgEls, footerTrgEls,
			headerSrcEls, footerSrcEls,
			headerCopy, footerCopy,
			headerWidths=[], footerWidths=[],
			headerContent=[], footerContent=[],
			idx, correction, sanityWidth,
			zeroOut = function(nSizer) {
				var style = nSizer.style;
				style.paddingTop = "0";
				style.paddingBottom = "0";
				style.borderTopWidth = "0";
				style.borderBottomWidth = "0";
				style.height = 0;
			};
	
		// If the scrollbar visibility has changed from the last draw, we need to
		// adjust the column sizes as the table width will have changed to account
		// for the scrollbar
		var scrollBarVis = divBodyEl.scrollHeight > divBodyEl.clientHeight;
		
		if ( settings.scrollBarVis !== scrollBarVis && settings.scrollBarVis !== undefined ) {
			settings.scrollBarVis = scrollBarVis;
			_fnAdjustColumnSizing( settings );
			return; // adjust column sizing will call this function again
		}
		else {
			settings.scrollBarVis = scrollBarVis;
		}
	
		/*
		 * 1. Re-create the table inside the scrolling div
		 */
	
		// Remove the old minimised thead and tfoot elements in the inner table
		table.children('thead, tfoot').remove();
	
		if ( footer ) {
			footerCopy = footer.clone().prependTo( table );
			footerTrgEls = footer.find('tr'); // the original tfoot is in its own table and must be sized
			footerSrcEls = footerCopy.find('tr');
			footerCopy.find('[id]').removeAttr('id');
		}
	
		// Clone the current header and footer elements and then place it into the inner table
		headerCopy = header.clone().prependTo( table );
		headerTrgEls = header.find('tr'); // original header is in its own table
		headerSrcEls = headerCopy.find('tr');
		headerCopy.find('th, td').removeAttr('tabindex');
		headerCopy.find('[id]').removeAttr('id');
	
	
		/*
		 * 2. Take live measurements from the DOM - do not alter the DOM itself!
		 */
	
		// Remove old sizing and apply the calculated column widths
		// Get the unique column headers in the newly created (cloned) header. We want to apply the
		// calculated sizes to this header
		if ( ! scrollX )
		{
			divBodyStyle.width = '100%';
			divHeader[0].style.width = '100%';
		}
	
		$.each( _fnGetUniqueThs( settings, headerCopy ), function ( i, el ) {
			idx = _fnVisibleToColumnIndex( settings, i );
			el.style.width = settings.aoColumns[idx].sWidth;
		} );
	
		if ( footer ) {
			_fnApplyToChildren( function(n) {
				n.style.width = "";
			}, footerSrcEls );
		}
	
		// Size the table as a whole
		sanityWidth = table.outerWidth();
		if ( scrollX === "" ) {
			// No x scrolling
			tableStyle.width = "100%";
	
			// IE7 will make the width of the table when 100% include the scrollbar
			// - which is shouldn't. When there is a scrollbar we need to take this
			// into account.
			if ( ie67 && (table.find('tbody').height() > divBodyEl.offsetHeight ||
				divBody.css('overflow-y') == "scroll")
			) {
				tableStyle.width = _fnStringToCss( table.outerWidth() - barWidth);
			}
	
			// Recalculate the sanity width
			sanityWidth = table.outerWidth();
		}
		else if ( scrollXInner !== "" ) {
			// legacy x scroll inner has been given - use it
			tableStyle.width = _fnStringToCss(scrollXInner);
	
			// Recalculate the sanity width
			sanityWidth = table.outerWidth();
		}
	
		// Hidden header should have zero height, so remove padding and borders. Then
		// set the width based on the real headers
	
		// Apply all styles in one pass
		_fnApplyToChildren( zeroOut, headerSrcEls );
	
		// Read all widths in next pass
		_fnApplyToChildren( function(nSizer) {
			var style = window.getComputedStyle ?
				window.getComputedStyle(nSizer).width :
				_fnStringToCss( $(nSizer).width() );
	
			headerContent.push( nSizer.innerHTML );
			headerWidths.push( style );
		}, headerSrcEls );
	
		// Apply all widths in final pass
		_fnApplyToChildren( function(nToSize, i) {
			nToSize.style.width = headerWidths[i];
		}, headerTrgEls );
	
		$(headerSrcEls).css('height', 0);
	
		/* Same again with the footer if we have one */
		if ( footer )
		{
			_fnApplyToChildren( zeroOut, footerSrcEls );
	
			_fnApplyToChildren( function(nSizer) {
				footerContent.push( nSizer.innerHTML );
				footerWidths.push( _fnStringToCss( $(nSizer).css('width') ) );
			}, footerSrcEls );
	
			_fnApplyToChildren( function(nToSize, i) {
				nToSize.style.width = footerWidths[i];
			}, footerTrgEls );
	
			$(footerSrcEls).height(0);
		}
	
	
		/*
		 * 3. Apply the measurements
		 */
	
		// "Hide" the header and footer that we used for the sizing. We need to keep
		// the content of the cell so that the width applied to the header and body
		// both match, but we want to hide it completely. We want to also fix their
		// width to what they currently are
		_fnApplyToChildren( function(nSizer, i) {
			nSizer.innerHTML = '<div class="dataTables_sizing">'+headerContent[i]+'</div>';
			nSizer.childNodes[0].style.height = "0";
			nSizer.childNodes[0].style.overflow = "hidden";
			nSizer.style.width = headerWidths[i];
		}, headerSrcEls );
	
		if ( footer )
		{
			_fnApplyToChildren( function(nSizer, i) {
				nSizer.innerHTML = '<div class="dataTables_sizing">'+footerContent[i]+'</div>';
				nSizer.childNodes[0].style.height = "0";
				nSizer.childNodes[0].style.overflow = "hidden";
				nSizer.style.width = footerWidths[i];
			}, footerSrcEls );
		}
	
		// Sanity check that the table is of a sensible width. If not then we are going to get
		// misalignment - try to prevent this by not allowing the table to shrink below its min width
		if ( Math.round(table.outerWidth()) < Math.round(sanityWidth) )
		{
			// The min width depends upon if we have a vertical scrollbar visible or not */
			correction = ((divBodyEl.scrollHeight > divBodyEl.offsetHeight ||
				divBody.css('overflow-y') == "scroll")) ?
					sanityWidth+barWidth :
					sanityWidth;
	
			// IE6/7 are a law unto themselves...
			if ( ie67 && (divBodyEl.scrollHeight >
				divBodyEl.offsetHeight || divBody.css('overflow-y') == "scroll")
			) {
				tableStyle.width = _fnStringToCss( correction-barWidth );
			}
	
			// And give the user a warning that we've stopped the table getting too small
			if ( scrollX === "" || scrollXInner !== "" ) {
				_fnLog( settings, 1, 'Possible column misalignment', 6 );
			}
		}
		else
		{
			correction = '100%';
		}
	
		// Apply to the container elements
		divBodyStyle.width = _fnStringToCss( correction );
		divHeaderStyle.width = _fnStringToCss( correction );
	
		if ( footer ) {
			settings.nScrollFoot.style.width = _fnStringToCss( correction );
		}
	
	
		/*
		 * 4. Clean up
		 */
		if ( ! scrollY ) {
			/* IE7< puts a vertical scrollbar in place (when it shouldn't be) due to subtracting
			 * the scrollbar height from the visible display, rather than adding it on. We need to
			 * set the height in order to sort this. Don't want to do it in any other browsers.
			 */
			if ( ie67 ) {
				divBodyStyle.height = _fnStringToCss( tableEl.offsetHeight+barWidth );
			}
		}
	
		/* Finally set the width's of the header and footer tables */
		var iOuterWidth = table.outerWidth();
		divHeaderTable[0].style.width = _fnStringToCss( iOuterWidth );
		divHeaderInnerStyle.width = _fnStringToCss( iOuterWidth );
	
		// Figure out if there are scrollbar present - if so then we need a the header and footer to
		// provide a bit more space to allow "overflow" scrolling (i.e. past the scrollbar)
		var bScrolling = table.height() > divBodyEl.clientHeight || divBody.css('overflow-y') == "scroll";
		var padding = 'padding' + (browser.bScrollbarLeft ? 'Left' : 'Right' );
		divHeaderInnerStyle[ padding ] = bScrolling ? barWidth+"px" : "0px";
	
		if ( footer ) {
			divFooterTable[0].style.width = _fnStringToCss( iOuterWidth );
			divFooterInner[0].style.width = _fnStringToCss( iOuterWidth );
			divFooterInner[0].style[padding] = bScrolling ? barWidth+"px" : "0px";
		}
	
		// Correct DOM ordering for colgroup - comes before the thead
		table.children('colgroup').insertBefore( table.children('thead') );
	
		/* Adjust the position of the header in case we loose the y-scrollbar */
		divBody.trigger('scroll');
	
		// If sorting or filtering has occurred, jump the scrolling back to the top
		// only if we aren't holding the position
		if ( (settings.bSorted || settings.bFiltered) && ! settings._drawHold ) {
			divBodyEl.scrollTop = 0;
		}
	}
	
	
	
	/**
	 * Apply a given function to the display child nodes of an element array (typically
	 * TD children of TR rows
	 *  @param {function} fn Method to apply to the objects
	 *  @param array {nodes} an1 List of elements to look through for display children
	 *  @param array {nodes} an2 Another list (identical structure to the first) - optional
	 *  @memberof DataTable#oApi
	 */
	function _fnApplyToChildren( fn, an1, an2 )
	{
		var index=0, i=0, iLen=an1.length;
		var nNode1, nNode2;
	
		while ( i < iLen ) {
			nNode1 = an1[i].firstChild;
			nNode2 = an2 ? an2[i].firstChild : null;
	
			while ( nNode1 ) {
				if ( nNode1.nodeType === 1 ) {
					if ( an2 ) {
						fn( nNode1, nNode2, index );
					}
					else {
						fn( nNode1, index );
					}
	
					index++;
				}
	
				nNode1 = nNode1.nextSibling;
				nNode2 = an2 ? nNode2.nextSibling : null;
			}
	
			i++;
		}
	}
	
	
	
	var __re_html_remove = /<.*?>/g;
	
	
	/**
	 * Calculate the width of columns for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnCalculateColumnWidths ( oSettings )
	{
		var
			table = oSettings.nTable,
			columns = oSettings.aoColumns,
			scroll = oSettings.oScroll,
			scrollY = scroll.sY,
			scrollX = scroll.sX,
			scrollXInner = scroll.sXInner,
			columnCount = columns.length,
			visibleColumns = _fnGetColumns( oSettings, 'bVisible' ),
			headerCells = $('th', oSettings.nTHead),
			tableWidthAttr = table.getAttribute('width'), // from DOM element
			tableContainer = table.parentNode,
			userInputs = false,
			i, column, columnIdx, width, outerWidth,
			browser = oSettings.oBrowser,
			ie67 = browser.bScrollOversize;
	
		var styleWidth = table.style.width;
		if ( styleWidth && styleWidth.indexOf('%') !== -1 ) {
			tableWidthAttr = styleWidth;
		}
	
		/* Convert any user input sizes into pixel sizes */
		for ( i=0 ; i<visibleColumns.length ; i++ ) {
			column = columns[ visibleColumns[i] ];
	
			if ( column.sWidth !== null ) {
				column.sWidth = _fnConvertToWidth( column.sWidthOrig, tableContainer );
	
				userInputs = true;
			}
		}
	
		/* If the number of columns in the DOM equals the number that we have to
		 * process in DataTables, then we can use the offsets that are created by
		 * the web- browser. No custom sizes can be set in order for this to happen,
		 * nor scrolling used
		 */
		if ( ie67 || ! userInputs && ! scrollX && ! scrollY &&
		     columnCount == _fnVisbleColumns( oSettings ) &&
		     columnCount == headerCells.length
		) {
			for ( i=0 ; i<columnCount ; i++ ) {
				var colIdx = _fnVisibleToColumnIndex( oSettings, i );
	
				if ( colIdx !== null ) {
					columns[ colIdx ].sWidth = _fnStringToCss( headerCells.eq(i).width() );
				}
			}
		}
		else
		{
			// Otherwise construct a single row, worst case, table with the widest
			// node in the data, assign any user defined widths, then insert it into
			// the DOM and allow the browser to do all the hard work of calculating
			// table widths
			var tmpTable = $(table).clone() // don't use cloneNode - IE8 will remove events on the main table
				.css( 'visibility', 'hidden' )
				.removeAttr( 'id' );
	
			// Clean up the table body
			tmpTable.find('tbody tr').remove();
			var tr = $('<tr/>').appendTo( tmpTable.find('tbody') );
	
			// Clone the table header and footer - we can't use the header / footer
			// from the cloned table, since if scrolling is active, the table's
			// real header and footer are contained in different table tags
			tmpTable.find('thead, tfoot').remove();
			tmpTable
				.append( $(oSettings.nTHead).clone() )
				.append( $(oSettings.nTFoot).clone() );
	
			// Remove any assigned widths from the footer (from scrolling)
			tmpTable.find('tfoot th, tfoot td').css('width', '');
	
			// Apply custom sizing to the cloned header
			headerCells = _fnGetUniqueThs( oSettings, tmpTable.find('thead')[0] );
	
			for ( i=0 ; i<visibleColumns.length ; i++ ) {
				column = columns[ visibleColumns[i] ];
	
				headerCells[i].style.width = column.sWidthOrig !== null && column.sWidthOrig !== '' ?
					_fnStringToCss( column.sWidthOrig ) :
					'';
	
				// For scrollX we need to force the column width otherwise the
				// browser will collapse it. If this width is smaller than the
				// width the column requires, then it will have no effect
				if ( column.sWidthOrig && scrollX ) {
					$( headerCells[i] ).append( $('<div/>').css( {
						width: column.sWidthOrig,
						margin: 0,
						padding: 0,
						border: 0,
						height: 1
					} ) );
				}
			}
	
			// Find the widest cell for each column and put it into the table
			if ( oSettings.aoData.length ) {
				for ( i=0 ; i<visibleColumns.length ; i++ ) {
					columnIdx = visibleColumns[i];
					column = columns[ columnIdx ];
	
					$( _fnGetWidestNode( oSettings, columnIdx ) )
						.clone( false )
						.append( column.sContentPadding )
						.appendTo( tr );
				}
			}
	
			// Tidy the temporary table - remove name attributes so there aren't
			// duplicated in the dom (radio elements for example)
			$('[name]', tmpTable).removeAttr('name');
	
			// Table has been built, attach to the document so we can work with it.
			// A holding element is used, positioned at the top of the container
			// with minimal height, so it has no effect on if the container scrolls
			// or not. Otherwise it might trigger scrolling when it actually isn't
			// needed
			var holder = $('<div/>').css( scrollX || scrollY ?
					{
						position: 'absolute',
						top: 0,
						left: 0,
						height: 1,
						right: 0,
						overflow: 'hidden'
					} :
					{}
				)
				.append( tmpTable )
				.appendTo( tableContainer );
	
			// When scrolling (X or Y) we want to set the width of the table as 
			// appropriate. However, when not scrolling leave the table width as it
			// is. This results in slightly different, but I think correct behaviour
			if ( scrollX && scrollXInner ) {
				tmpTable.width( scrollXInner );
			}
			else if ( scrollX ) {
				tmpTable.css( 'width', 'auto' );
				tmpTable.removeAttr('width');
	
				// If there is no width attribute or style, then allow the table to
				// collapse
				if ( tmpTable.width() < tableContainer.clientWidth && tableWidthAttr ) {
					tmpTable.width( tableContainer.clientWidth );
				}
			}
			else if ( scrollY ) {
				tmpTable.width( tableContainer.clientWidth );
			}
			else if ( tableWidthAttr ) {
				tmpTable.width( tableWidthAttr );
			}
	
			// Get the width of each column in the constructed table - we need to
			// know the inner width (so it can be assigned to the other table's
			// cells) and the outer width so we can calculate the full width of the
			// table. This is safe since DataTables requires a unique cell for each
			// column, but if ever a header can span multiple columns, this will
			// need to be modified.
			var total = 0;
			for ( i=0 ; i<visibleColumns.length ; i++ ) {
				var cell = $(headerCells[i]);
				var border = cell.outerWidth() - cell.width();
	
				// Use getBounding... where possible (not IE8-) because it can give
				// sub-pixel accuracy, which we then want to round up!
				var bounding = browser.bBounding ?
					Math.ceil( headerCells[i].getBoundingClientRect().width ) :
					cell.outerWidth();
	
				// Total is tracked to remove any sub-pixel errors as the outerWidth
				// of the table might not equal the total given here (IE!).
				total += bounding;
	
				// Width for each column to use
				columns[ visibleColumns[i] ].sWidth = _fnStringToCss( bounding - border );
			}
	
			table.style.width = _fnStringToCss( total );
	
			// Finished with the table - ditch it
			holder.remove();
		}
	
		// If there is a width attr, we want to attach an event listener which
		// allows the table sizing to automatically adjust when the window is
		// resized. Use the width attr rather than CSS, since we can't know if the
		// CSS is a relative value or absolute - DOM read is always px.
		if ( tableWidthAttr ) {
			table.style.width = _fnStringToCss( tableWidthAttr );
		}
	
		if ( (tableWidthAttr || scrollX) && ! oSettings._reszEvt ) {
			var bindResize = function () {
				$(window).on('resize.DT-'+oSettings.sInstance, _fnThrottle( function () {
					_fnAdjustColumnSizing( oSettings );
				} ) );
			};
	
			// IE6/7 will crash if we bind a resize event handler on page load.
			// To be removed in 1.11 which drops IE6/7 support
			if ( ie67 ) {
				setTimeout( bindResize, 1000 );
			}
			else {
				bindResize();
			}
	
			oSettings._reszEvt = true;
		}
	}
	
	
	/**
	 * Throttle the calls to a function. Arguments and context are maintained for
	 * the throttled function
	 *  @param {function} fn Function to be called
	 *  @param {int} [freq=200] call frequency in mS
	 *  @returns {function} wrapped function
	 *  @memberof DataTable#oApi
	 */
	var _fnThrottle = DataTable.util.throttle;
	
	
	/**
	 * Convert a CSS unit width to pixels (e.g. 2em)
	 *  @param {string} width width to be converted
	 *  @param {node} parent parent to get the with for (required for relative widths) - optional
	 *  @returns {int} width in pixels
	 *  @memberof DataTable#oApi
	 */
	function _fnConvertToWidth ( width, parent )
	{
		if ( ! width ) {
			return 0;
		}
	
		var n = $('<div/>')
			.css( 'width', _fnStringToCss( width ) )
			.appendTo( parent || document.body );
	
		var val = n[0].offsetWidth;
		n.remove();
	
		return val;
	}
	
	
	/**
	 * Get the widest node
	 *  @param {object} settings dataTables settings object
	 *  @param {int} colIdx column of interest
	 *  @returns {node} widest table node
	 *  @memberof DataTable#oApi
	 */
	function _fnGetWidestNode( settings, colIdx )
	{
		var idx = _fnGetMaxLenString( settings, colIdx );
		if ( idx < 0 ) {
			return null;
		}
	
		var data = settings.aoData[ idx ];
		return ! data.nTr ? // Might not have been created when deferred rendering
			$('<td/>').html( _fnGetCellData( settings, idx, colIdx, 'display' ) )[0] :
			data.anCells[ colIdx ];
	}
	
	
	/**
	 * Get the maximum strlen for each data column
	 *  @param {object} settings dataTables settings object
	 *  @param {int} colIdx column of interest
	 *  @returns {string} max string length for each column
	 *  @memberof DataTable#oApi
	 */
	function _fnGetMaxLenString( settings, colIdx )
	{
		var s, max=-1, maxIdx = -1;
	
		for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			s = _fnGetCellData( settings, i, colIdx, 'display' )+'';
			s = s.replace( __re_html_remove, '' );
			s = s.replace( /&nbsp;/g, ' ' );
	
			if ( s.length > max ) {
				max = s.length;
				maxIdx = i;
			}
		}
	
		return maxIdx;
	}
	
	
	/**
	 * Append a CSS unit (only if required) to a string
	 *  @param {string} value to css-ify
	 *  @returns {string} value with css unit
	 *  @memberof DataTable#oApi
	 */
	function _fnStringToCss( s )
	{
		if ( s === null ) {
			return '0px';
		}
	
		if ( typeof s == 'number' ) {
			return s < 0 ?
				'0px' :
				s+'px';
		}
	
		// Check it has a unit character already
		return s.match(/\d$/) ?
			s+'px' :
			s;
	}
	
	
	
	function _fnSortFlatten ( settings )
	{
		var
			i, iLen, k, kLen,
			aSort = [],
			aiOrig = [],
			aoColumns = settings.aoColumns,
			aDataSort, iCol, sType, srcCol,
			fixed = settings.aaSortingFixed,
			fixedObj = $.isPlainObject( fixed ),
			nestedSort = [],
			add = function ( a ) {
				if ( a.length && ! Array.isArray( a[0] ) ) {
					// 1D array
					nestedSort.push( a );
				}
				else {
					// 2D array
					$.merge( nestedSort, a );
				}
			};
	
		// Build the sort array, with pre-fix and post-fix options if they have been
		// specified
		if ( Array.isArray( fixed ) ) {
			add( fixed );
		}
	
		if ( fixedObj && fixed.pre ) {
			add( fixed.pre );
		}
	
		add( settings.aaSorting );
	
		if (fixedObj && fixed.post ) {
			add( fixed.post );
		}
	
		for ( i=0 ; i<nestedSort.length ; i++ )
		{
			srcCol = nestedSort[i][0];
			aDataSort = aoColumns[ srcCol ].aDataSort;
	
			for ( k=0, kLen=aDataSort.length ; k<kLen ; k++ )
			{
				iCol = aDataSort[k];
				sType = aoColumns[ iCol ].sType || 'string';
	
				if ( nestedSort[i]._idx === undefined ) {
					nestedSort[i]._idx = $.inArray( nestedSort[i][1], aoColumns[iCol].asSorting );
				}
	
				aSort.push( {
					src:       srcCol,
					col:       iCol,
					dir:       nestedSort[i][1],
					index:     nestedSort[i]._idx,
					type:      sType,
					formatter: DataTable.ext.type.order[ sType+"-pre" ]
				} );
			}
		}
	
		return aSort;
	}
	
	/**
	 * Change the order of the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 *  @todo This really needs split up!
	 */
	function _fnSort ( oSettings )
	{
		var
			i, ien, iLen, j, jLen, k, kLen,
			sDataType, nTh,
			aiOrig = [],
			oExtSort = DataTable.ext.type.order,
			aoData = oSettings.aoData,
			aoColumns = oSettings.aoColumns,
			aDataSort, data, iCol, sType, oSort,
			formatters = 0,
			sortCol,
			displayMaster = oSettings.aiDisplayMaster,
			aSort;
	
		// Resolve any column types that are unknown due to addition or invalidation
		// @todo Can this be moved into a 'data-ready' handler which is called when
		//   data is going to be used in the table?
		_fnColumnTypes( oSettings );
	
		aSort = _fnSortFlatten( oSettings );
	
		for ( i=0, ien=aSort.length ; i<ien ; i++ ) {
			sortCol = aSort[i];
	
			// Track if we can use the fast sort algorithm
			if ( sortCol.formatter ) {
				formatters++;
			}
	
			// Load the data needed for the sort, for each cell
			_fnSortData( oSettings, sortCol.col );
		}
	
		/* No sorting required if server-side or no sorting array */
		if ( _fnDataSource( oSettings ) != 'ssp' && aSort.length !== 0 )
		{
			// Create a value - key array of the current row positions such that we can use their
			// current position during the sort, if values match, in order to perform stable sorting
			for ( i=0, iLen=displayMaster.length ; i<iLen ; i++ ) {
				aiOrig[ displayMaster[i] ] = i;
			}
	
			/* Do the sort - here we want multi-column sorting based on a given data source (column)
			 * and sorting function (from oSort) in a certain direction. It's reasonably complex to
			 * follow on it's own, but this is what we want (example two column sorting):
			 *  fnLocalSorting = function(a,b){
			 *    var iTest;
			 *    iTest = oSort['string-asc']('data11', 'data12');
			 *      if (iTest !== 0)
			 *        return iTest;
			 *    iTest = oSort['numeric-desc']('data21', 'data22');
			 *    if (iTest !== 0)
			 *      return iTest;
			 *    return oSort['numeric-asc']( aiOrig[a], aiOrig[b] );
			 *  }
			 * Basically we have a test for each sorting column, if the data in that column is equal,
			 * test the next column. If all columns match, then we use a numeric sort on the row
			 * positions in the original data array to provide a stable sort.
			 *
			 * Note - I know it seems excessive to have two sorting methods, but the first is around
			 * 15% faster, so the second is only maintained for backwards compatibility with sorting
			 * methods which do not have a pre-sort formatting function.
			 */
			if ( formatters === aSort.length ) {
				// All sort types have formatting functions
				displayMaster.sort( function ( a, b ) {
					var
						x, y, k, test, sort,
						len=aSort.length,
						dataA = aoData[a]._aSortData,
						dataB = aoData[b]._aSortData;
	
					for ( k=0 ; k<len ; k++ ) {
						sort = aSort[k];
	
						x = dataA[ sort.col ];
						y = dataB[ sort.col ];
	
						test = x<y ? -1 : x>y ? 1 : 0;
						if ( test !== 0 ) {
							return sort.dir === 'asc' ? test : -test;
						}
					}
	
					x = aiOrig[a];
					y = aiOrig[b];
					return x<y ? -1 : x>y ? 1 : 0;
				} );
			}
			else {
				// Depreciated - remove in 1.11 (providing a plug-in option)
				// Not all sort types have formatting methods, so we have to call their sorting
				// methods.
				displayMaster.sort( function ( a, b ) {
					var
						x, y, k, l, test, sort, fn,
						len=aSort.length,
						dataA = aoData[a]._aSortData,
						dataB = aoData[b]._aSortData;
	
					for ( k=0 ; k<len ; k++ ) {
						sort = aSort[k];
	
						x = dataA[ sort.col ];
						y = dataB[ sort.col ];
	
						fn = oExtSort[ sort.type+"-"+sort.dir ] || oExtSort[ "string-"+sort.dir ];
						test = fn( x, y );
						if ( test !== 0 ) {
							return test;
						}
					}
	
					x = aiOrig[a];
					y = aiOrig[b];
					return x<y ? -1 : x>y ? 1 : 0;
				} );
			}
		}
	
		/* Tell the draw function that we have sorted the data */
		oSettings.bSorted = true;
	}
	
	
	function _fnSortAria ( settings )
	{
		var label;
		var nextSort;
		var columns = settings.aoColumns;
		var aSort = _fnSortFlatten( settings );
		var oAria = settings.oLanguage.oAria;
	
		// ARIA attributes - need to loop all columns, to update all (removing old
		// attributes as needed)
		for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
		{
			var col = columns[i];
			var asSorting = col.asSorting;
			var sTitle = col.ariaTitle || col.sTitle.replace( /<.*?>/g, "" );
			var th = col.nTh;
	
			// IE7 is throwing an error when setting these properties with jQuery's
			// attr() and removeAttr() methods...
			th.removeAttribute('aria-sort');
	
			/* In ARIA only the first sorting column can be marked as sorting - no multi-sort option */
			if ( col.bSortable ) {
				if ( aSort.length > 0 && aSort[0].col == i ) {
					th.setAttribute('aria-sort', aSort[0].dir=="asc" ? "ascending" : "descending" );
					nextSort = asSorting[ aSort[0].index+1 ] || asSorting[0];
				}
				else {
					nextSort = asSorting[0];
				}
	
				label = sTitle + ( nextSort === "asc" ?
					oAria.sSortAscending :
					oAria.sSortDescending
				);
			}
			else {
				label = sTitle;
			}
	
			th.setAttribute('aria-label', label);
		}
	}
	
	
	/**
	 * Function to run on user sort request
	 *  @param {object} settings dataTables settings object
	 *  @param {node} attachTo node to attach the handler to
	 *  @param {int} colIdx column sorting index
	 *  @param {boolean} [append=false] Append the requested sort to the existing
	 *    sort if true (i.e. multi-column sort)
	 *  @param {function} [callback] callback function
	 *  @memberof DataTable#oApi
	 */
	function _fnSortListener ( settings, colIdx, append, callback )
	{
		var col = settings.aoColumns[ colIdx ];
		var sorting = settings.aaSorting;
		var asSorting = col.asSorting;
		var nextSortIdx;
		var next = function ( a, overflow ) {
			var idx = a._idx;
			if ( idx === undefined ) {
				idx = $.inArray( a[1], asSorting );
			}
	
			return idx+1 < asSorting.length ?
				idx+1 :
				overflow ?
					null :
					0;
		};
	
		// Convert to 2D array if needed
		if ( typeof sorting[0] === 'number' ) {
			sorting = settings.aaSorting = [ sorting ];
		}
	
		// If appending the sort then we are multi-column sorting
		if ( append && settings.oFeatures.bSortMulti ) {
			// Are we already doing some kind of sort on this column?
			var sortIdx = $.inArray( colIdx, _pluck(sorting, '0') );
	
			if ( sortIdx !== -1 ) {
				// Yes, modify the sort
				nextSortIdx = next( sorting[sortIdx], true );
	
				if ( nextSortIdx === null && sorting.length === 1 ) {
					nextSortIdx = 0; // can't remove sorting completely
				}
	
				if ( nextSortIdx === null ) {
					sorting.splice( sortIdx, 1 );
				}
				else {
					sorting[sortIdx][1] = asSorting[ nextSortIdx ];
					sorting[sortIdx]._idx = nextSortIdx;
				}
			}
			else {
				// No sort on this column yet
				sorting.push( [ colIdx, asSorting[0], 0 ] );
				sorting[sorting.length-1]._idx = 0;
			}
		}
		else if ( sorting.length && sorting[0][0] == colIdx ) {
			// Single column - already sorting on this column, modify the sort
			nextSortIdx = next( sorting[0] );
	
			sorting.length = 1;
			sorting[0][1] = asSorting[ nextSortIdx ];
			sorting[0]._idx = nextSortIdx;
		}
		else {
			// Single column - sort only on this column
			sorting.length = 0;
			sorting.push( [ colIdx, asSorting[0] ] );
			sorting[0]._idx = 0;
		}
	
		// Run the sort by calling a full redraw
		_fnReDraw( settings );
	
		// callback used for async user interaction
		if ( typeof callback == 'function' ) {
			callback( settings );
		}
	}
	
	
	/**
	 * Attach a sort handler (click) to a node
	 *  @param {object} settings dataTables settings object
	 *  @param {node} attachTo node to attach the handler to
	 *  @param {int} colIdx column sorting index
	 *  @param {function} [callback] callback function
	 *  @memberof DataTable#oApi
	 */
	function _fnSortAttachListener ( settings, attachTo, colIdx, callback )
	{
		var col = settings.aoColumns[ colIdx ];
	
		_fnBindAction( attachTo, {}, function (e) {
			/* If the column is not sortable - don't to anything */
			if ( col.bSortable === false ) {
				return;
			}
	
			// If processing is enabled use a timeout to allow the processing
			// display to be shown - otherwise to it synchronously
			if ( settings.oFeatures.bProcessing ) {
				_fnProcessingDisplay( settings, true );
	
				setTimeout( function() {
					_fnSortListener( settings, colIdx, e.shiftKey, callback );
	
					// In server-side processing, the draw callback will remove the
					// processing display
					if ( _fnDataSource( settings ) !== 'ssp' ) {
						_fnProcessingDisplay( settings, false );
					}
				}, 0 );
			}
			else {
				_fnSortListener( settings, colIdx, e.shiftKey, callback );
			}
		} );
	}
	
	
	/**
	 * Set the sorting classes on table's body, Note: it is safe to call this function
	 * when bSort and bSortClasses are false
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnSortingClasses( settings )
	{
		var oldSort = settings.aLastSort;
		var sortClass = settings.oClasses.sSortColumn;
		var sort = _fnSortFlatten( settings );
		var features = settings.oFeatures;
		var i, ien, colIdx;
	
		if ( features.bSort && features.bSortClasses ) {
			// Remove old sorting classes
			for ( i=0, ien=oldSort.length ; i<ien ; i++ ) {
				colIdx = oldSort[i].src;
	
				// Remove column sorting
				$( _pluck( settings.aoData, 'anCells', colIdx ) )
					.removeClass( sortClass + (i<2 ? i+1 : 3) );
			}
	
			// Add new column sorting
			for ( i=0, ien=sort.length ; i<ien ; i++ ) {
				colIdx = sort[i].src;
	
				$( _pluck( settings.aoData, 'anCells', colIdx ) )
					.addClass( sortClass + (i<2 ? i+1 : 3) );
			}
		}
	
		settings.aLastSort = sort;
	}
	
	
	// Get the data to sort a column, be it from cache, fresh (populating the
	// cache), or from a sort formatter
	function _fnSortData( settings, idx )
	{
		// Custom sorting function - provided by the sort data type
		var column = settings.aoColumns[ idx ];
		var customSort = DataTable.ext.order[ column.sSortDataType ];
		var customData;
	
		if ( customSort ) {
			customData = customSort.call( settings.oInstance, settings, idx,
				_fnColumnIndexToVisible( settings, idx )
			);
		}
	
		// Use / populate cache
		var row, cellData;
		var formatter = DataTable.ext.type.order[ column.sType+"-pre" ];
	
		for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			row = settings.aoData[i];
	
			if ( ! row._aSortData ) {
				row._aSortData = [];
			}
	
			if ( ! row._aSortData[idx] || customSort ) {
				cellData = customSort ?
					customData[i] : // If there was a custom sort function, use data from there
					_fnGetCellData( settings, i, idx, 'sort' );
	
				row._aSortData[ idx ] = formatter ?
					formatter( cellData ) :
					cellData;
			}
		}
	}
	
	
	
	/**
	 * Save the state of a table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnSaveState ( settings )
	{
		if (settings._bLoadingState) {
			return;
		}
	
		/* Store the interesting variables */
		var state = {
			time:    +new Date(),
			start:   settings._iDisplayStart,
			length:  settings._iDisplayLength,
			order:   $.extend( true, [], settings.aaSorting ),
			search:  _fnSearchToCamel( settings.oPreviousSearch ),
			columns: $.map( settings.aoColumns, function ( col, i ) {
				return {
					visible: col.bVisible,
					search: _fnSearchToCamel( settings.aoPreSearchCols[i] )
				};
			} )
		};
	
		settings.oSavedState = state;
		_fnCallbackFire( settings, "aoStateSaveParams", 'stateSaveParams', [settings, state] );
		
		if ( settings.oFeatures.bStateSave && !settings.bDestroying )
		{
			settings.fnStateSaveCallback.call( settings.oInstance, settings, state );
		}	
	}
	
	
	/**
	 * Attempt to load a saved table state
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} oInit DataTables init object so we can override settings
	 *  @param {function} callback Callback to execute when the state has been loaded
	 *  @memberof DataTable#oApi
	 */
	function _fnLoadState ( settings, oInit, callback )
	{
		if ( ! settings.oFeatures.bStateSave ) {
			callback();
			return;
		}
	
		var loaded = function(state) {
			_fnImplementState(settings, state, callback);
		}
	
		var state = settings.fnStateLoadCallback.call( settings.oInstance, settings, loaded );
	
		if ( state !== undefined ) {
			_fnImplementState( settings, state, callback );
		}
		// otherwise, wait for the loaded callback to be executed
	
		return true;
	}
	
	function _fnImplementState ( settings, s, callback) {
		var i, ien;
		var columns = settings.aoColumns;
		settings._bLoadingState = true;
	
		// When StateRestore was introduced the state could now be implemented at any time
		// Not just initialisation. To do this an api instance is required in some places
		var api = settings._bInitComplete ? new DataTable.Api(settings) : null;
	
		if ( ! s || ! s.time ) {
			settings._bLoadingState = false;
			callback();
			return;
		}
	
		// Allow custom and plug-in manipulation functions to alter the saved data set and
		// cancelling of loading by returning false
		var abStateLoad = _fnCallbackFire( settings, 'aoStateLoadParams', 'stateLoadParams', [settings, s] );
		if ( $.inArray( false, abStateLoad ) !== -1 ) {
			settings._bLoadingState = false;
			callback();
			return;
		}
	
		// Reject old data
		var duration = settings.iStateDuration;
		if ( duration > 0 && s.time < +new Date() - (duration*1000) ) {
			settings._bLoadingState = false;
			callback();
			return;
		}
	
		// Number of columns have changed - all bets are off, no restore of settings
		if ( s.columns && columns.length !== s.columns.length ) {
			settings._bLoadingState = false;
			callback();
			return;
		}
	
		// Store the saved state so it might be accessed at any time
		settings.oLoadedState = $.extend( true, {}, s );
	
		// Page Length
		if ( s.length !== undefined ) {
			// If already initialised just set the value directly so that the select element is also updated
			if (api) {
				api.page.len(s.length)
			}
			else {
				settings._iDisplayLength   = s.length;
			}
		}
	
		// Restore key features - todo - for 1.11 this needs to be done by
		// subscribed events
		if ( s.start !== undefined ) {
			if(api === null) {
				settings._iDisplayStart    = s.start;
				settings.iInitDisplayStart = s.start;
			}
			else {
				_fnPageChange(settings, s.start/settings._iDisplayLength);
			}
		}
	
		// Order
		if ( s.order !== undefined ) {
			settings.aaSorting = [];
			$.each( s.order, function ( i, col ) {
				settings.aaSorting.push( col[0] >= columns.length ?
					[ 0, col[1] ] :
					col
				);
			} );
		}
	
		// Search
		if ( s.search !== undefined ) {
			$.extend( settings.oPreviousSearch, _fnSearchToHung( s.search ) );
		}
	
		// Columns
		if ( s.columns ) {
			for ( i=0, ien=s.columns.length ; i<ien ; i++ ) {
				var col = s.columns[i];
	
				// Visibility
				if ( col.visible !== undefined ) {
					// If the api is defined, the table has been initialised so we need to use it rather than internal settings
					if (api) {
						// Don't redraw the columns on every iteration of this loop, we will do this at the end instead
						api.column(i).visible(col.visible, false);
					}
					else {
						columns[i].bVisible = col.visible;
					}
				}
	
				// Search
				if ( col.search !== undefined ) {
					$.extend( settings.aoPreSearchCols[i], _fnSearchToHung( col.search ) );
				}
			}
			
			// If the api is defined then we need to adjust the columns once the visibility has been changed
			if (api) {
				api.columns.adjust();
			}
		}
	
		settings._bLoadingState = false;
		_fnCallbackFire( settings, 'aoStateLoaded', 'stateLoaded', [settings, s] );
		callback();
	};
	
	
	/**
	 * Return the settings object for a particular table
	 *  @param {node} table table we are using as a dataTable
	 *  @returns {object} Settings object - or null if not found
	 *  @memberof DataTable#oApi
	 */
	function _fnSettingsFromNode ( table )
	{
		var settings = DataTable.settings;
		var idx = $.inArray( table, _pluck( settings, 'nTable' ) );
	
		return idx !== -1 ?
			settings[ idx ] :
			null;
	}
	
	
	/**
	 * Log an error message
	 *  @param {object} settings dataTables settings object
	 *  @param {int} level log error messages, or display them to the user
	 *  @param {string} msg error message
	 *  @param {int} tn Technical note id to get more information about the error.
	 *  @memberof DataTable#oApi
	 */
	function _fnLog( settings, level, msg, tn )
	{
		msg = 'DataTables warning: '+
			(settings ? 'table id='+settings.sTableId+' - ' : '')+msg;
	
		if ( tn ) {
			msg += '. For more information about this error, please see '+
			'http://datatables.net/tn/'+tn;
		}
	
		if ( ! level  ) {
			// Backwards compatibility pre 1.10
			var ext = DataTable.ext;
			var type = ext.sErrMode || ext.errMode;
	
			if ( settings ) {
				_fnCallbackFire( settings, null, 'error', [ settings, tn, msg ] );
			}
	
			if ( type == 'alert' ) {
				alert( msg );
			}
			else if ( type == 'throw' ) {
				throw new Error(msg);
			}
			else if ( typeof type == 'function' ) {
				type( settings, tn, msg );
			}
		}
		else if ( window.console && console.log ) {
			console.log( msg );
		}
	}
	
	
	/**
	 * See if a property is defined on one object, if so assign it to the other object
	 *  @param {object} ret target object
	 *  @param {object} src source object
	 *  @param {string} name property
	 *  @param {string} [mappedName] name to map too - optional, name used if not given
	 *  @memberof DataTable#oApi
	 */
	function _fnMap( ret, src, name, mappedName )
	{
		if ( Array.isArray( name ) ) {
			$.each( name, function (i, val) {
				if ( Array.isArray( val ) ) {
					_fnMap( ret, src, val[0], val[1] );
				}
				else {
					_fnMap( ret, src, val );
				}
			} );
	
			return;
		}
	
		if ( mappedName === undefined ) {
			mappedName = name;
		}
	
		if ( src[name] !== undefined ) {
			ret[mappedName] = src[name];
		}
	}
	
	
	/**
	 * Extend objects - very similar to jQuery.extend, but deep copy objects, and
	 * shallow copy arrays. The reason we need to do this, is that we don't want to
	 * deep copy array init values (such as aaSorting) since the dev wouldn't be
	 * able to override them, but we do want to deep copy arrays.
	 *  @param {object} out Object to extend
	 *  @param {object} extender Object from which the properties will be applied to
	 *      out
	 *  @param {boolean} breakRefs If true, then arrays will be sliced to take an
	 *      independent copy with the exception of the `data` or `aaData` parameters
	 *      if they are present. This is so you can pass in a collection to
	 *      DataTables and have that used as your data source without breaking the
	 *      references
	 *  @returns {object} out Reference, just for convenience - out === the return.
	 *  @memberof DataTable#oApi
	 *  @todo This doesn't take account of arrays inside the deep copied objects.
	 */
	function _fnExtend( out, extender, breakRefs )
	{
		var val;
	
		for ( var prop in extender ) {
			if ( extender.hasOwnProperty(prop) ) {
				val = extender[prop];
	
				if ( $.isPlainObject( val ) ) {
					if ( ! $.isPlainObject( out[prop] ) ) {
						out[prop] = {};
					}
					$.extend( true, out[prop], val );
				}
				else if ( breakRefs && prop !== 'data' && prop !== 'aaData' && Array.isArray(val) ) {
					out[prop] = val.slice();
				}
				else {
					out[prop] = val;
				}
			}
		}
	
		return out;
	}
	
	
	/**
	 * Bind an event handers to allow a click or return key to activate the callback.
	 * This is good for accessibility since a return on the keyboard will have the
	 * same effect as a click, if the element has focus.
	 *  @param {element} n Element to bind the action to
	 *  @param {object} oData Data object to pass to the triggered function
	 *  @param {function} fn Callback function for when the event is triggered
	 *  @memberof DataTable#oApi
	 */
	function _fnBindAction( n, oData, fn )
	{
		$(n)
			.on( 'click.DT', oData, function (e) {
					$(n).trigger('blur'); // Remove focus outline for mouse users
					fn(e);
				} )
			.on( 'keypress.DT', oData, function (e){
					if ( e.which === 13 ) {
						e.preventDefault();
						fn(e);
					}
				} )
			.on( 'selectstart.DT', function () {
					/* Take the brutal approach to cancelling text selection */
					return false;
				} );
	}
	
	
	/**
	 * Register a callback function. Easily allows a callback function to be added to
	 * an array store of callback functions that can then all be called together.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sStore Name of the array storage for the callbacks in oSettings
	 *  @param {function} fn Function to be called back
	 *  @param {string} sName Identifying name for the callback (i.e. a label)
	 *  @memberof DataTable#oApi
	 */
	function _fnCallbackReg( oSettings, sStore, fn, sName )
	{
		if ( fn )
		{
			oSettings[sStore].push( {
				"fn": fn,
				"sName": sName
			} );
		}
	}
	
	
	/**
	 * Fire callback functions and trigger events. Note that the loop over the
	 * callback array store is done backwards! Further note that you do not want to
	 * fire off triggers in time sensitive applications (for example cell creation)
	 * as its slow.
	 *  @param {object} settings dataTables settings object
	 *  @param {string} callbackArr Name of the array storage for the callbacks in
	 *      oSettings
	 *  @param {string} eventName Name of the jQuery custom event to trigger. If
	 *      null no trigger is fired
	 *  @param {array} args Array of arguments to pass to the callback function /
	 *      trigger
	 *  @memberof DataTable#oApi
	 */
	function _fnCallbackFire( settings, callbackArr, eventName, args )
	{
		var ret = [];
	
		if ( callbackArr ) {
			ret = $.map( settings[callbackArr].slice().reverse(), function (val, i) {
				return val.fn.apply( settings.oInstance, args );
			} );
		}
	
		if ( eventName !== null ) {
			var e = $.Event( eventName+'.dt' );
	
			$(settings.nTable).trigger( e, args );
	
			ret.push( e.result );
		}
	
		return ret;
	}
	
	
	function _fnLengthOverflow ( settings )
	{
		var
			start = settings._iDisplayStart,
			end = settings.fnDisplayEnd(),
			len = settings._iDisplayLength;
	
		/* If we have space to show extra rows (backing up from the end point - then do so */
		if ( start >= end )
		{
			start = end - len;
		}
	
		// Keep the start record on the current page
		start -= (start % len);
	
		if ( len === -1 || start < 0 )
		{
			start = 0;
		}
	
		settings._iDisplayStart = start;
	}
	
	
	function _fnRenderer( settings, type )
	{
		var renderer = settings.renderer;
		var host = DataTable.ext.renderer[type];
	
		if ( $.isPlainObject( renderer ) && renderer[type] ) {
			// Specific renderer for this type. If available use it, otherwise use
			// the default.
			return host[renderer[type]] || host._;
		}
		else if ( typeof renderer === 'string' ) {
			// Common renderer - if there is one available for this type use it,
			// otherwise use the default
			return host[renderer] || host._;
		}
	
		// Use the default
		return host._;
	}
	
	
	/**
	 * Detect the data source being used for the table. Used to simplify the code
	 * a little (ajax) and to make it compress a little smaller.
	 *
	 *  @param {object} settings dataTables settings object
	 *  @returns {string} Data source
	 *  @memberof DataTable#oApi
	 */
	function _fnDataSource ( settings )
	{
		if ( settings.oFeatures.bServerSide ) {
			return 'ssp';
		}
		else if ( settings.ajax || settings.sAjaxSource ) {
			return 'ajax';
		}
		return 'dom';
	}
	
	
	
	
	/**
	 * Computed structure of the DataTables API, defined by the options passed to
	 * `DataTable.Api.register()` when building the API.
	 *
	 * The structure is built in order to speed creation and extension of the Api
	 * objects since the extensions are effectively pre-parsed.
	 *
	 * The array is an array of objects with the following structure, where this
	 * base array represents the Api prototype base:
	 *
	 *     [
	 *       {
	 *         name:      'data'                -- string   - Property name
	 *         val:       function () {},       -- function - Api method (or undefined if just an object
	 *         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
	 *         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
	 *       },
	 *       {
	 *         name:     'row'
	 *         val:       {},
	 *         methodExt: [ ... ],
	 *         propExt:   [
	 *           {
	 *             name:      'data'
	 *             val:       function () {},
	 *             methodExt: [ ... ],
	 *             propExt:   [ ... ]
	 *           },
	 *           ...
	 *         ]
	 *       }
	 *     ]
	 *
	 * @type {Array}
	 * @ignore
	 */
	var __apiStruct = [];
	
	
	/**
	 * `Array.prototype` reference.
	 *
	 * @type object
	 * @ignore
	 */
	var __arrayProto = Array.prototype;
	
	
	/**
	 * Abstraction for `context` parameter of the `Api` constructor to allow it to
	 * take several different forms for ease of use.
	 *
	 * Each of the input parameter types will be converted to a DataTables settings
	 * object where possible.
	 *
	 * @param  {string|node|jQuery|object} mixed DataTable identifier. Can be one
	 *   of:
	 *
	 *   * `string` - jQuery selector. Any DataTables' matching the given selector
	 *     with be found and used.
	 *   * `node` - `TABLE` node which has already been formed into a DataTable.
	 *   * `jQuery` - A jQuery object of `TABLE` nodes.
	 *   * `object` - DataTables settings object
	 *   * `DataTables.Api` - API instance
	 * @return {array|null} Matching DataTables settings objects. `null` or
	 *   `undefined` is returned if no matching DataTable is found.
	 * @ignore
	 */
	var _toSettings = function ( mixed )
	{
		var idx, jq;
		var settings = DataTable.settings;
		var tables = $.map( settings, function (el, i) {
			return el.nTable;
		} );
	
		if ( ! mixed ) {
			return [];
		}
		else if ( mixed.nTable && mixed.oApi ) {
			// DataTables settings object
			return [ mixed ];
		}
		else if ( mixed.nodeName && mixed.nodeName.toLowerCase() === 'table' ) {
			// Table node
			idx = $.inArray( mixed, tables );
			return idx !== -1 ? [ settings[idx] ] : null;
		}
		else if ( mixed && typeof mixed.settings === 'function' ) {
			return mixed.settings().toArray();
		}
		else if ( typeof mixed === 'string' ) {
			// jQuery selector
			jq = $(mixed);
		}
		else if ( mixed instanceof $ ) {
			// jQuery object (also DataTables instance)
			jq = mixed;
		}
	
		if ( jq ) {
			return jq.map( function(i) {
				idx = $.inArray( this, tables );
				return idx !== -1 ? settings[idx] : null;
			} ).toArray();
		}
	};
	
	
	/**
	 * DataTables API class - used to control and interface with  one or more
	 * DataTables enhanced tables.
	 *
	 * The API class is heavily based on jQuery, presenting a chainable interface
	 * that you can use to interact with tables. Each instance of the API class has
	 * a "context" - i.e. the tables that it will operate on. This could be a single
	 * table, all tables on a page or a sub-set thereof.
	 *
	 * Additionally the API is designed to allow you to easily work with the data in
	 * the tables, retrieving and manipulating it as required. This is done by
	 * presenting the API class as an array like interface. The contents of the
	 * array depend upon the actions requested by each method (for example
	 * `rows().nodes()` will return an array of nodes, while `rows().data()` will
	 * return an array of objects or arrays depending upon your table's
	 * configuration). The API object has a number of array like methods (`push`,
	 * `pop`, `reverse` etc) as well as additional helper methods (`each`, `pluck`,
	 * `unique` etc) to assist your working with the data held in a table.
	 *
	 * Most methods (those which return an Api instance) are chainable, which means
	 * the return from a method call also has all of the methods available that the
	 * top level object had. For example, these two calls are equivalent:
	 *
	 *     // Not chained
	 *     api.row.add( {...} );
	 *     api.draw();
	 *
	 *     // Chained
	 *     api.row.add( {...} ).draw();
	 *
	 * @class DataTable.Api
	 * @param {array|object|string|jQuery} context DataTable identifier. This is
	 *   used to define which DataTables enhanced tables this API will operate on.
	 *   Can be one of:
	 *
	 *   * `string` - jQuery selector. Any DataTables' matching the given selector
	 *     with be found and used.
	 *   * `node` - `TABLE` node which has already been formed into a DataTable.
	 *   * `jQuery` - A jQuery object of `TABLE` nodes.
	 *   * `object` - DataTables settings object
	 * @param {array} [data] Data to initialise the Api instance with.
	 *
	 * @example
	 *   // Direct initialisation during DataTables construction
	 *   var api = $('#example').DataTable();
	 *
	 * @example
	 *   // Initialisation using a DataTables jQuery object
	 *   var api = $('#example').dataTable().api();
	 *
	 * @example
	 *   // Initialisation as a constructor
	 *   var api = new $.fn.DataTable.Api( 'table.dataTable' );
	 */
	_Api = function ( context, data )
	{
		if ( ! (this instanceof _Api) ) {
			return new _Api( context, data );
		}
	
		var settings = [];
		var ctxSettings = function ( o ) {
			var a = _toSettings( o );
			if ( a ) {
				settings.push.apply( settings, a );
			}
		};
	
		if ( Array.isArray( context ) ) {
			for ( var i=0, ien=context.length ; i<ien ; i++ ) {
				ctxSettings( context[i] );
			}
		}
		else {
			ctxSettings( context );
		}
	
		// Remove duplicates
		this.context = _unique( settings );
	
		// Initial data
		if ( data ) {
			$.merge( this, data );
		}
	
		// selector
		this.selector = {
			rows: null,
			cols: null,
			opts: null
		};
	
		_Api.extend( this, this, __apiStruct );
	};
	
	DataTable.Api = _Api;
	
	// Don't destroy the existing prototype, just extend it. Required for jQuery 2's
	// isPlainObject.
	$.extend( _Api.prototype, {
		any: function ()
		{
			return this.count() !== 0;
		},
	
	
		concat:  __arrayProto.concat,
	
	
		context: [], // array of table settings objects
	
	
		count: function ()
		{
			return this.flatten().length;
		},
	
	
		each: function ( fn )
		{
			for ( var i=0, ien=this.length ; i<ien; i++ ) {
				fn.call( this, this[i], i, this );
			}
	
			return this;
		},
	
	
		eq: function ( idx )
		{
			var ctx = this.context;
	
			return ctx.length > idx ?
				new _Api( ctx[idx], this[idx] ) :
				null;
		},
	
	
		filter: function ( fn )
		{
			var a = [];
	
			if ( __arrayProto.filter ) {
				a = __arrayProto.filter.call( this, fn, this );
			}
			else {
				// Compatibility for browsers without EMCA-252-5 (JS 1.6)
				for ( var i=0, ien=this.length ; i<ien ; i++ ) {
					if ( fn.call( this, this[i], i, this ) ) {
						a.push( this[i] );
					}
				}
			}
	
			return new _Api( this.context, a );
		},
	
	
		flatten: function ()
		{
			var a = [];
			return new _Api( this.context, a.concat.apply( a, this.toArray() ) );
		},
	
	
		join:    __arrayProto.join,
	
	
		indexOf: __arrayProto.indexOf || function (obj, start)
		{
			for ( var i=(start || 0), ien=this.length ; i<ien ; i++ ) {
				if ( this[i] === obj ) {
					return i;
				}
			}
			return -1;
		},
	
		iterator: function ( flatten, type, fn, alwaysNew ) {
			var
				a = [], ret,
				i, ien, j, jen,
				context = this.context,
				rows, items, item,
				selector = this.selector;
	
			// Argument shifting
			if ( typeof flatten === 'string' ) {
				alwaysNew = fn;
				fn = type;
				type = flatten;
				flatten = false;
			}
	
			for ( i=0, ien=context.length ; i<ien ; i++ ) {
				var apiInst = new _Api( context[i] );
	
				if ( type === 'table' ) {
					ret = fn.call( apiInst, context[i], i );
	
					if ( ret !== undefined ) {
						a.push( ret );
					}
				}
				else if ( type === 'columns' || type === 'rows' ) {
					// this has same length as context - one entry for each table
					ret = fn.call( apiInst, context[i], this[i], i );
	
					if ( ret !== undefined ) {
						a.push( ret );
					}
				}
				else if ( type === 'column' || type === 'column-rows' || type === 'row' || type === 'cell' ) {
					// columns and rows share the same structure.
					// 'this' is an array of column indexes for each context
					items = this[i];
	
					if ( type === 'column-rows' ) {
						rows = _selector_row_indexes( context[i], selector.opts );
					}
	
					for ( j=0, jen=items.length ; j<jen ; j++ ) {
						item = items[j];
	
						if ( type === 'cell' ) {
							ret = fn.call( apiInst, context[i], item.row, item.column, i, j );
						}
						else {
							ret = fn.call( apiInst, context[i], item, i, j, rows );
						}
	
						if ( ret !== undefined ) {
							a.push( ret );
						}
					}
				}
			}
	
			if ( a.length || alwaysNew ) {
				var api = new _Api( context, flatten ? a.concat.apply( [], a ) : a );
				var apiSelector = api.selector;
				apiSelector.rows = selector.rows;
				apiSelector.cols = selector.cols;
				apiSelector.opts = selector.opts;
				return api;
			}
			return this;
		},
	
	
		lastIndexOf: __arrayProto.lastIndexOf || function (obj, start)
		{
			// Bit cheeky...
			return this.indexOf.apply( this.toArray.reverse(), arguments );
		},
	
	
		length:  0,
	
	
		map: function ( fn )
		{
			var a = [];
	
			if ( __arrayProto.map ) {
				a = __arrayProto.map.call( this, fn, this );
			}
			else {
				// Compatibility for browsers without EMCA-252-5 (JS 1.6)
				for ( var i=0, ien=this.length ; i<ien ; i++ ) {
					a.push( fn.call( this, this[i], i ) );
				}
			}
	
			return new _Api( this.context, a );
		},
	
	
		pluck: function ( prop )
		{
			let fn = DataTable.util.get(prop);
	
			return this.map( function ( el ) {
				return fn(el);
			} );
		},
	
		pop:     __arrayProto.pop,
	
	
		push:    __arrayProto.push,
	
	
		// Does not return an API instance
		reduce: __arrayProto.reduce || function ( fn, init )
		{
			return _fnReduce( this, fn, init, 0, this.length, 1 );
		},
	
	
		reduceRight: __arrayProto.reduceRight || function ( fn, init )
		{
			return _fnReduce( this, fn, init, this.length-1, -1, -1 );
		},
	
	
		reverse: __arrayProto.reverse,
	
	
		// Object with rows, columns and opts
		selector: null,
	
	
		shift:   __arrayProto.shift,
	
	
		slice: function () {
			return new _Api( this.context, this );
		},
	
	
		sort:    __arrayProto.sort, // ? name - order?
	
	
		splice:  __arrayProto.splice,
	
	
		toArray: function ()
		{
			return __arrayProto.slice.call( this );
		},
	
	
		to$: function ()
		{
			return $( this );
		},
	
	
		toJQuery: function ()
		{
			return $( this );
		},
	
	
		unique: function ()
		{
			return new _Api( this.context, _unique(this) );
		},
	
	
		unshift: __arrayProto.unshift
	} );
	
	
	_Api.extend = function ( scope, obj, ext )
	{
		// Only extend API instances and static properties of the API
		if ( ! ext.length || ! obj || ( ! (obj instanceof _Api) && ! obj.__dt_wrapper ) ) {
			return;
		}
	
		var
			i, ien,
			struct,
			methodScoping = function ( scope, fn, struc ) {
				return function () {
					var ret = fn.apply( scope, arguments );
	
					// Method extension
					_Api.extend( ret, ret, struc.methodExt );
					return ret;
				};
			};
	
		for ( i=0, ien=ext.length ; i<ien ; i++ ) {
			struct = ext[i];
	
			// Value
			obj[ struct.name ] = struct.type === 'function' ?
				methodScoping( scope, struct.val, struct ) :
				struct.type === 'object' ?
					{} :
					struct.val;
	
			obj[ struct.name ].__dt_wrapper = true;
	
			// Property extension
			_Api.extend( scope, obj[ struct.name ], struct.propExt );
		}
	};
	
	
	// @todo - Is there need for an augment function?
	// _Api.augment = function ( inst, name )
	// {
	// 	// Find src object in the structure from the name
	// 	var parts = name.split('.');
	
	// 	_Api.extend( inst, obj );
	// };
	
	
	//     [
	//       {
	//         name:      'data'                -- string   - Property name
	//         val:       function () {},       -- function - Api method (or undefined if just an object
	//         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
	//         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
	//       },
	//       {
	//         name:     'row'
	//         val:       {},
	//         methodExt: [ ... ],
	//         propExt:   [
	//           {
	//             name:      'data'
	//             val:       function () {},
	//             methodExt: [ ... ],
	//             propExt:   [ ... ]
	//           },
	//           ...
	//         ]
	//       }
	//     ]
	
	_Api.register = _api_register = function ( name, val )
	{
		if ( Array.isArray( name ) ) {
			for ( var j=0, jen=name.length ; j<jen ; j++ ) {
				_Api.register( name[j], val );
			}
			return;
		}
	
		var
			i, ien,
			heir = name.split('.'),
			struct = __apiStruct,
			key, method;
	
		var find = function ( src, name ) {
			for ( var i=0, ien=src.length ; i<ien ; i++ ) {
				if ( src[i].name === name ) {
					return src[i];
				}
			}
			return null;
		};
	
		for ( i=0, ien=heir.length ; i<ien ; i++ ) {
			method = heir[i].indexOf('()') !== -1;
			key = method ?
				heir[i].replace('()', '') :
				heir[i];
	
			var src = find( struct, key );
			if ( ! src ) {
				src = {
					name:      key,
					val:       {},
					methodExt: [],
					propExt:   [],
					type:      'object'
				};
				struct.push( src );
			}
	
			if ( i === ien-1 ) {
				src.val = val;
				src.type = typeof val === 'function' ?
					'function' :
					$.isPlainObject( val ) ?
						'object' :
						'other';
			}
			else {
				struct = method ?
					src.methodExt :
					src.propExt;
			}
		}
	};
	
	_Api.registerPlural = _api_registerPlural = function ( pluralName, singularName, val ) {
		_Api.register( pluralName, val );
	
		_Api.register( singularName, function () {
			var ret = val.apply( this, arguments );
	
			if ( ret === this ) {
				// Returned item is the API instance that was passed in, return it
				return this;
			}
			else if ( ret instanceof _Api ) {
				// New API instance returned, want the value from the first item
				// in the returned array for the singular result.
				return ret.length ?
					Array.isArray( ret[0] ) ?
						new _Api( ret.context, ret[0] ) : // Array results are 'enhanced'
						ret[0] :
					undefined;
			}
	
			// Non-API return - just fire it back
			return ret;
		} );
	};
	
	
	/**
	 * Selector for HTML tables. Apply the given selector to the give array of
	 * DataTables settings objects.
	 *
	 * @param {string|integer} [selector] jQuery selector string or integer
	 * @param  {array} Array of DataTables settings objects to be filtered
	 * @return {array}
	 * @ignore
	 */
	var __table_selector = function ( selector, a )
	{
		if ( Array.isArray(selector) ) {
			return $.map( selector, function (item) {
				return __table_selector(item, a);
			} );
		}
	
		// Integer is used to pick out a table by index
		if ( typeof selector === 'number' ) {
			return [ a[ selector ] ];
		}
	
		// Perform a jQuery selector on the table nodes
		var nodes = $.map( a, function (el, i) {
			return el.nTable;
		} );
	
		return $(nodes)
			.filter( selector )
			.map( function (i) {
				// Need to translate back from the table node to the settings
				var idx = $.inArray( this, nodes );
				return a[ idx ];
			} )
			.toArray();
	};
	
	
	
	/**
	 * Context selector for the API's context (i.e. the tables the API instance
	 * refers to.
	 *
	 * @name    DataTable.Api#tables
	 * @param {string|integer} [selector] Selector to pick which tables the iterator
	 *   should operate on. If not given, all tables in the current context are
	 *   used. This can be given as a jQuery selector (for example `':gt(0)'`) to
	 *   select multiple tables or as an integer to select a single table.
	 * @returns {DataTable.Api} Returns a new API instance if a selector is given.
	 */
	_api_register( 'tables()', function ( selector ) {
		// A new instance is created if there was a selector specified
		return selector !== undefined && selector !== null ?
			new _Api( __table_selector( selector, this.context ) ) :
			this;
	} );
	
	
	_api_register( 'table()', function ( selector ) {
		var tables = this.tables( selector );
		var ctx = tables.context;
	
		// Truncate to the first matched table
		return ctx.length ?
			new _Api( ctx[0] ) :
			tables;
	} );
	
	
	_api_registerPlural( 'tables().nodes()', 'table().node()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTable;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().body()', 'table().body()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTBody;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().header()', 'table().header()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTHead;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().footer()', 'table().footer()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTFoot;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().containers()', 'table().container()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTableWrapper;
		}, 1 );
	} );
	
	
	
	/**
	 * Redraw the tables in the current context.
	 */
	_api_register( 'draw()', function ( paging ) {
		return this.iterator( 'table', function ( settings ) {
			if ( paging === 'page' ) {
				_fnDraw( settings );
			}
			else {
				if ( typeof paging === 'string' ) {
					paging = paging === 'full-hold' ?
						false :
						true;
				}
	
				_fnReDraw( settings, paging===false );
			}
		} );
	} );
	
	
	
	/**
	 * Get the current page index.
	 *
	 * @return {integer} Current page index (zero based)
	 *//**
	 * Set the current page.
	 *
	 * Note that if you attempt to show a page which does not exist, DataTables will
	 * not throw an error, but rather reset the paging.
	 *
	 * @param {integer|string} action The paging action to take. This can be one of:
	 *  * `integer` - The page index to jump to
	 *  * `string` - An action to take:
	 *    * `first` - Jump to first page.
	 *    * `next` - Jump to the next page
	 *    * `previous` - Jump to previous page
	 *    * `last` - Jump to the last page.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'page()', function ( action ) {
		if ( action === undefined ) {
			return this.page.info().page; // not an expensive call
		}
	
		// else, have an action to take on all tables
		return this.iterator( 'table', function ( settings ) {
			_fnPageChange( settings, action );
		} );
	} );
	
	
	/**
	 * Paging information for the first table in the current context.
	 *
	 * If you require paging information for another table, use the `table()` method
	 * with a suitable selector.
	 *
	 * @return {object} Object with the following properties set:
	 *  * `page` - Current page index (zero based - i.e. the first page is `0`)
	 *  * `pages` - Total number of pages
	 *  * `start` - Display index for the first record shown on the current page
	 *  * `end` - Display index for the last record shown on the current page
	 *  * `length` - Display length (number of records). Note that generally `start
	 *    + length = end`, but this is not always true, for example if there are
	 *    only 2 records to show on the final page, with a length of 10.
	 *  * `recordsTotal` - Full data set length
	 *  * `recordsDisplay` - Data set length once the current filtering criterion
	 *    are applied.
	 */
	_api_register( 'page.info()', function ( action ) {
		if ( this.context.length === 0 ) {
			return undefined;
		}
	
		var
			settings   = this.context[0],
			start      = settings._iDisplayStart,
			len        = settings.oFeatures.bPaginate ? settings._iDisplayLength : -1,
			visRecords = settings.fnRecordsDisplay(),
			all        = len === -1;
	
		return {
			"page":           all ? 0 : Math.floor( start / len ),
			"pages":          all ? 1 : Math.ceil( visRecords / len ),
			"start":          start,
			"end":            settings.fnDisplayEnd(),
			"length":         len,
			"recordsTotal":   settings.fnRecordsTotal(),
			"recordsDisplay": visRecords,
			"serverSide":     _fnDataSource( settings ) === 'ssp'
		};
	} );
	
	
	/**
	 * Get the current page length.
	 *
	 * @return {integer} Current page length. Note `-1` indicates that all records
	 *   are to be shown.
	 *//**
	 * Set the current page length.
	 *
	 * @param {integer} Page length to set. Use `-1` to show all records.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'page.len()', function ( len ) {
		// Note that we can't call this function 'length()' because `length`
		// is a Javascript property of functions which defines how many arguments
		// the function expects.
		if ( len === undefined ) {
			return this.context.length !== 0 ?
				this.context[0]._iDisplayLength :
				undefined;
		}
	
		// else, set the page length
		return this.iterator( 'table', function ( settings ) {
			_fnLengthChange( settings, len );
		} );
	} );
	
	
	
	var __reload = function ( settings, holdPosition, callback ) {
		// Use the draw event to trigger a callback
		if ( callback ) {
			var api = new _Api( settings );
	
			api.one( 'draw', function () {
				callback( api.ajax.json() );
			} );
		}
	
		if ( _fnDataSource( settings ) == 'ssp' ) {
			_fnReDraw( settings, holdPosition );
		}
		else {
			_fnProcessingDisplay( settings, true );
	
			// Cancel an existing request
			var xhr = settings.jqXHR;
			if ( xhr && xhr.readyState !== 4 ) {
				xhr.abort();
			}
	
			// Trigger xhr
			_fnBuildAjax( settings, [], function( json ) {
				_fnClearTable( settings );
	
				var data = _fnAjaxDataSrc( settings, json );
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					_fnAddData( settings, data[i] );
				}
	
				_fnReDraw( settings, holdPosition );
				_fnProcessingDisplay( settings, false );
			} );
		}
	};
	
	
	/**
	 * Get the JSON response from the last Ajax request that DataTables made to the
	 * server. Note that this returns the JSON from the first table in the current
	 * context.
	 *
	 * @return {object} JSON received from the server.
	 */
	_api_register( 'ajax.json()', function () {
		var ctx = this.context;
	
		if ( ctx.length > 0 ) {
			return ctx[0].json;
		}
	
		// else return undefined;
	} );
	
	
	/**
	 * Get the data submitted in the last Ajax request
	 */
	_api_register( 'ajax.params()', function () {
		var ctx = this.context;
	
		if ( ctx.length > 0 ) {
			return ctx[0].oAjaxData;
		}
	
		// else return undefined;
	} );
	
	
	/**
	 * Reload tables from the Ajax data source. Note that this function will
	 * automatically re-draw the table when the remote data has been loaded.
	 *
	 * @param {boolean} [reset=true] Reset (default) or hold the current paging
	 *   position. A full re-sort and re-filter is performed when this method is
	 *   called, which is why the pagination reset is the default action.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.reload()', function ( callback, resetPaging ) {
		return this.iterator( 'table', function (settings) {
			__reload( settings, resetPaging===false, callback );
		} );
	} );
	
	
	/**
	 * Get the current Ajax URL. Note that this returns the URL from the first
	 * table in the current context.
	 *
	 * @return {string} Current Ajax source URL
	 *//**
	 * Set the Ajax URL. Note that this will set the URL for all tables in the
	 * current context.
	 *
	 * @param {string} url URL to set.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.url()', function ( url ) {
		var ctx = this.context;
	
		if ( url === undefined ) {
			// get
			if ( ctx.length === 0 ) {
				return undefined;
			}
			ctx = ctx[0];
	
			return ctx.ajax ?
				$.isPlainObject( ctx.ajax ) ?
					ctx.ajax.url :
					ctx.ajax :
				ctx.sAjaxSource;
		}
	
		// set
		return this.iterator( 'table', function ( settings ) {
			if ( $.isPlainObject( settings.ajax ) ) {
				settings.ajax.url = url;
			}
			else {
				settings.ajax = url;
			}
			// No need to consider sAjaxSource here since DataTables gives priority
			// to `ajax` over `sAjaxSource`. So setting `ajax` here, renders any
			// value of `sAjaxSource` redundant.
		} );
	} );
	
	
	/**
	 * Load data from the newly set Ajax URL. Note that this method is only
	 * available when `ajax.url()` is used to set a URL. Additionally, this method
	 * has the same effect as calling `ajax.reload()` but is provided for
	 * convenience when setting a new URL. Like `ajax.reload()` it will
	 * automatically redraw the table once the remote data has been loaded.
	 *
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.url().load()', function ( callback, resetPaging ) {
		// Same as a reload, but makes sense to present it for easy access after a
		// url change
		return this.iterator( 'table', function ( ctx ) {
			__reload( ctx, resetPaging===false, callback );
		} );
	} );
	
	
	
	
	var _selector_run = function ( type, selector, selectFn, settings, opts )
	{
		var
			out = [], res,
			a, i, ien, j, jen,
			selectorType = typeof selector;
	
		// Can't just check for isArray here, as an API or jQuery instance might be
		// given with their array like look
		if ( ! selector || selectorType === 'string' || selectorType === 'function' || selector.length === undefined ) {
			selector = [ selector ];
		}
	
		for ( i=0, ien=selector.length ; i<ien ; i++ ) {
			// Only split on simple strings - complex expressions will be jQuery selectors
			a = selector[i] && selector[i].split && ! selector[i].match(/[\[\(:]/) ?
				selector[i].split(',') :
				[ selector[i] ];
	
			for ( j=0, jen=a.length ; j<jen ; j++ ) {
				res = selectFn( typeof a[j] === 'string' ? (a[j]).trim() : a[j] );
	
				if ( res && res.length ) {
					out = out.concat( res );
				}
			}
		}
	
		// selector extensions
		var ext = _ext.selector[ type ];
		if ( ext.length ) {
			for ( i=0, ien=ext.length ; i<ien ; i++ ) {
				out = ext[i]( settings, opts, out );
			}
		}
	
		return _unique( out );
	};
	
	
	var _selector_opts = function ( opts )
	{
		if ( ! opts ) {
			opts = {};
		}
	
		// Backwards compatibility for 1.9- which used the terminology filter rather
		// than search
		if ( opts.filter && opts.search === undefined ) {
			opts.search = opts.filter;
		}
	
		return $.extend( {
			search: 'none',
			order: 'current',
			page: 'all'
		}, opts );
	};
	
	
	var _selector_first = function ( inst )
	{
		// Reduce the API instance to the first item found
		for ( var i=0, ien=inst.length ; i<ien ; i++ ) {
			if ( inst[i].length > 0 ) {
				// Assign the first element to the first item in the instance
				// and truncate the instance and context
				inst[0] = inst[i];
				inst[0].length = 1;
				inst.length = 1;
				inst.context = [ inst.context[i] ];
	
				return inst;
			}
		}
	
		// Not found - return an empty instance
		inst.length = 0;
		return inst;
	};
	
	
	var _selector_row_indexes = function ( settings, opts )
	{
		var
			i, ien, tmp, a=[],
			displayFiltered = settings.aiDisplay,
			displayMaster = settings.aiDisplayMaster;
	
		var
			search = opts.search,  // none, applied, removed
			order  = opts.order,   // applied, current, index (original - compatibility with 1.9)
			page   = opts.page;    // all, current
	
		if ( _fnDataSource( settings ) == 'ssp' ) {
			// In server-side processing mode, most options are irrelevant since
			// rows not shown don't exist and the index order is the applied order
			// Removed is a special case - for consistency just return an empty
			// array
			return search === 'removed' ?
				[] :
				_range( 0, displayMaster.length );
		}
		else if ( page == 'current' ) {
			// Current page implies that order=current and filter=applied, since it is
			// fairly senseless otherwise, regardless of what order and search actually
			// are
			for ( i=settings._iDisplayStart, ien=settings.fnDisplayEnd() ; i<ien ; i++ ) {
				a.push( displayFiltered[i] );
			}
		}
		else if ( order == 'current' || order == 'applied' ) {
			if ( search == 'none') {
				a = displayMaster.slice();
			}
			else if ( search == 'applied' ) {
				a = displayFiltered.slice();
			}
			else if ( search == 'removed' ) {
				// O(n+m) solution by creating a hash map
				var displayFilteredMap = {};
	
				for ( var i=0, ien=displayFiltered.length ; i<ien ; i++ ) {
					displayFilteredMap[displayFiltered[i]] = null;
				}
	
				a = $.map( displayMaster, function (el) {
					return ! displayFilteredMap.hasOwnProperty(el) ?
						el :
						null;
				} );
			}
		}
		else if ( order == 'index' || order == 'original' ) {
			for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				if ( search == 'none' ) {
					a.push( i );
				}
				else { // applied | removed
					tmp = $.inArray( i, displayFiltered );
	
					if ((tmp === -1 && search == 'removed') ||
						(tmp >= 0   && search == 'applied') )
					{
						a.push( i );
					}
				}
			}
		}
	
		return a;
	};
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Rows
	 *
	 * {}          - no selector - use all available rows
	 * {integer}   - row aoData index
	 * {node}      - TR node
	 * {string}    - jQuery selector to apply to the TR elements
	 * {array}     - jQuery array of nodes, or simply an array of TR nodes
	 *
	 */
	var __row_selector = function ( settings, selector, opts )
	{
		var rows;
		var run = function ( sel ) {
			var selInt = _intVal( sel );
			var i, ien;
			var aoData = settings.aoData;
	
			// Short cut - selector is a number and no options provided (default is
			// all records, so no need to check if the index is in there, since it
			// must be - dev error if the index doesn't exist).
			if ( selInt !== null && ! opts ) {
				return [ selInt ];
			}
	
			if ( ! rows ) {
				rows = _selector_row_indexes( settings, opts );
			}
	
			if ( selInt !== null && $.inArray( selInt, rows ) !== -1 ) {
				// Selector - integer
				return [ selInt ];
			}
			else if ( sel === null || sel === undefined || sel === '' ) {
				// Selector - none
				return rows;
			}
	
			// Selector - function
			if ( typeof sel === 'function' ) {
				return $.map( rows, function (idx) {
					var row = aoData[ idx ];
					return sel( idx, row._aData, row.nTr ) ? idx : null;
				} );
			}
	
			// Selector - node
			if ( sel.nodeName ) {
				var rowIdx = sel._DT_RowIndex;  // Property added by DT for fast lookup
				var cellIdx = sel._DT_CellIndex;
	
				if ( rowIdx !== undefined ) {
					// Make sure that the row is actually still present in the table
					return aoData[ rowIdx ] && aoData[ rowIdx ].nTr === sel ?
						[ rowIdx ] :
						[];
				}
				else if ( cellIdx ) {
					return aoData[ cellIdx.row ] && aoData[ cellIdx.row ].nTr === sel.parentNode ?
						[ cellIdx.row ] :
						[];
				}
				else {
					var host = $(sel).closest('*[data-dt-row]');
					return host.length ?
						[ host.data('dt-row') ] :
						[];
				}
			}
	
			// ID selector. Want to always be able to select rows by id, regardless
			// of if the tr element has been created or not, so can't rely upon
			// jQuery here - hence a custom implementation. This does not match
			// Sizzle's fast selector or HTML4 - in HTML5 the ID can be anything,
			// but to select it using a CSS selector engine (like Sizzle or
			// querySelect) it would need to need to be escaped for some characters.
			// DataTables simplifies this for row selectors since you can select
			// only a row. A # indicates an id any anything that follows is the id -
			// unescaped.
			if ( typeof sel === 'string' && sel.charAt(0) === '#' ) {
				// get row index from id
				var rowObj = settings.aIds[ sel.replace( /^#/, '' ) ];
				if ( rowObj !== undefined ) {
					return [ rowObj.idx ];
				}
	
				// need to fall through to jQuery in case there is DOM id that
				// matches
			}
			
			// Get nodes in the order from the `rows` array with null values removed
			var nodes = _removeEmpty(
				_pluck_order( settings.aoData, rows, 'nTr' )
			);
	
			// Selector - jQuery selector string, array of nodes or jQuery object/
			// As jQuery's .filter() allows jQuery objects to be passed in filter,
			// it also allows arrays, so this will cope with all three options
			return $(nodes)
				.filter( sel )
				.map( function () {
					return this._DT_RowIndex;
				} )
				.toArray();
		};
	
		return _selector_run( 'row', selector, run, settings, opts );
	};
	
	
	_api_register( 'rows()', function ( selector, opts ) {
		// argument shifting
		if ( selector === undefined ) {
			selector = '';
		}
		else if ( $.isPlainObject( selector ) ) {
			opts = selector;
			selector = '';
		}
	
		opts = _selector_opts( opts );
	
		var inst = this.iterator( 'table', function ( settings ) {
			return __row_selector( settings, selector, opts );
		}, 1 );
	
		// Want argument shifting here and in __row_selector?
		inst.selector.rows = selector;
		inst.selector.opts = opts;
	
		return inst;
	} );
	
	_api_register( 'rows().nodes()', function () {
		return this.iterator( 'row', function ( settings, row ) {
			return settings.aoData[ row ].nTr || undefined;
		}, 1 );
	} );
	
	_api_register( 'rows().data()', function () {
		return this.iterator( true, 'rows', function ( settings, rows ) {
			return _pluck_order( settings.aoData, rows, '_aData' );
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().cache()', 'row().cache()', function ( type ) {
		return this.iterator( 'row', function ( settings, row ) {
			var r = settings.aoData[ row ];
			return type === 'search' ? r._aFilterData : r._aSortData;
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().invalidate()', 'row().invalidate()', function ( src ) {
		return this.iterator( 'row', function ( settings, row ) {
			_fnInvalidate( settings, row, src );
		} );
	} );
	
	_api_registerPlural( 'rows().indexes()', 'row().index()', function () {
		return this.iterator( 'row', function ( settings, row ) {
			return row;
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().ids()', 'row().id()', function ( hash ) {
		var a = [];
		var context = this.context;
	
		// `iterator` will drop undefined values, but in this case we want them
		for ( var i=0, ien=context.length ; i<ien ; i++ ) {
			for ( var j=0, jen=this[i].length ; j<jen ; j++ ) {
				var id = context[i].rowIdFn( context[i].aoData[ this[i][j] ]._aData );
				a.push( (hash === true ? '#' : '' )+ id );
			}
		}
	
		return new _Api( context, a );
	} );
	
	_api_registerPlural( 'rows().remove()', 'row().remove()', function () {
		var that = this;
	
		this.iterator( 'row', function ( settings, row, thatIdx ) {
			var data = settings.aoData;
			var rowData = data[ row ];
			var i, ien, j, jen;
			var loopRow, loopCells;
	
			data.splice( row, 1 );
	
			// Update the cached indexes
			for ( i=0, ien=data.length ; i<ien ; i++ ) {
				loopRow = data[i];
				loopCells = loopRow.anCells;
	
				// Rows
				if ( loopRow.nTr !== null ) {
					loopRow.nTr._DT_RowIndex = i;
				}
	
				// Cells
				if ( loopCells !== null ) {
					for ( j=0, jen=loopCells.length ; j<jen ; j++ ) {
						loopCells[j]._DT_CellIndex.row = i;
					}
				}
			}
	
			// Delete from the display arrays
			_fnDeleteIndex( settings.aiDisplayMaster, row );
			_fnDeleteIndex( settings.aiDisplay, row );
			_fnDeleteIndex( that[ thatIdx ], row, false ); // maintain local indexes
	
			// For server-side processing tables - subtract the deleted row from the count
			if ( settings._iRecordsDisplay > 0 ) {
				settings._iRecordsDisplay--;
			}
	
			// Check for an 'overflow' they case for displaying the table
			_fnLengthOverflow( settings );
	
			// Remove the row's ID reference if there is one
			var id = settings.rowIdFn( rowData._aData );
			if ( id !== undefined ) {
				delete settings.aIds[ id ];
			}
		} );
	
		this.iterator( 'table', function ( settings ) {
			for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				settings.aoData[i].idx = i;
			}
		} );
	
		return this;
	} );
	
	
	_api_register( 'rows.add()', function ( rows ) {
		var newRows = this.iterator( 'table', function ( settings ) {
				var row, i, ien;
				var out = [];
	
				for ( i=0, ien=rows.length ; i<ien ; i++ ) {
					row = rows[i];
	
					if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
						out.push( _fnAddTr( settings, row )[0] );
					}
					else {
						out.push( _fnAddData( settings, row ) );
					}
				}
	
				return out;
			}, 1 );
	
		// Return an Api.rows() extended instance, so rows().nodes() etc can be used
		var modRows = this.rows( -1 );
		modRows.pop();
		$.merge( modRows, newRows );
	
		return modRows;
	} );
	
	
	
	
	
	/**
	 *
	 */
	_api_register( 'row()', function ( selector, opts ) {
		return _selector_first( this.rows( selector, opts ) );
	} );
	
	
	_api_register( 'row().data()', function ( data ) {
		var ctx = this.context;
	
		if ( data === undefined ) {
			// Get
			return ctx.length && this.length ?
				ctx[0].aoData[ this[0] ]._aData :
				undefined;
		}
	
		// Set
		var row = ctx[0].aoData[ this[0] ];
		row._aData = data;
	
		// If the DOM has an id, and the data source is an array
		if ( Array.isArray( data ) && row.nTr && row.nTr.id ) {
			_fnSetObjectDataFn( ctx[0].rowId )( data, row.nTr.id );
		}
	
		// Automatically invalidate
		_fnInvalidate( ctx[0], this[0], 'data' );
	
		return this;
	} );
	
	
	_api_register( 'row().node()', function () {
		var ctx = this.context;
	
		return ctx.length && this.length ?
			ctx[0].aoData[ this[0] ].nTr || null :
			null;
	} );
	
	
	_api_register( 'row.add()', function ( row ) {
		// Allow a jQuery object to be passed in - only a single row is added from
		// it though - the first element in the set
		if ( row instanceof $ && row.length ) {
			row = row[0];
		}
	
		var rows = this.iterator( 'table', function ( settings ) {
			if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
				return _fnAddTr( settings, row )[0];
			}
			return _fnAddData( settings, row );
		} );
	
		// Return an Api.rows() extended instance, with the newly added row selected
		return this.row( rows[0] );
	} );
	
	
	$(document).on('plugin-init.dt', function (e, context) {
		var api = new _Api( context );
	
		api.on( 'stateSaveParams', function ( e, settings, d ) {
			// This could be more compact with the API, but it is a lot faster as a simple
			// internal loop
			var idFn = settings.rowIdFn;
			var data = settings.aoData;
			var ids = [];
	
			for (var i=0 ; i<data.length ; i++) {
				if (data[i]._detailsShow) {
					ids.push( '#' + idFn(data[i]._aData) );
				}
			}
	
			d.childRows = ids;
		})
	
		var loaded = api.state.loaded();
	
		if ( loaded && loaded.childRows ) {
			api
				.rows( $.map(loaded.childRows, function (id){
					return id.replace(/:/g, '\\:')
				}) )
				.every( function () {
					_fnCallbackFire( context, null, 'requestChild', [ this ] )
				});
		}
	});
	
	var __details_add = function ( ctx, row, data, klass )
	{
		// Convert to array of TR elements
		var rows = [];
		var addRow = function ( r, k ) {
			// Recursion to allow for arrays of jQuery objects
			if ( Array.isArray( r ) || r instanceof $ ) {
				for ( var i=0, ien=r.length ; i<ien ; i++ ) {
					addRow( r[i], k );
				}
				return;
			}
	
			// If we get a TR element, then just add it directly - up to the dev
			// to add the correct number of columns etc
			if ( r.nodeName && r.nodeName.toLowerCase() === 'tr' ) {
				rows.push( r );
			}
			else {
				// Otherwise create a row with a wrapper
				var created = $('<tr><td></td></tr>').addClass( k );
				$('td', created)
					.addClass( k )
					.html( r )
					[0].colSpan = _fnVisbleColumns( ctx );
	
				rows.push( created[0] );
			}
		};
	
		addRow( data, klass );
	
		if ( row._details ) {
			row._details.detach();
		}
	
		row._details = $(rows);
	
		// If the children were already shown, that state should be retained
		if ( row._detailsShow ) {
			row._details.insertAfter( row.nTr );
		}
	};
	
	
	// Make state saving of child row details async to allow them to be batch processed
	var __details_state = DataTable.util.throttle(
		function (ctx) {
			_fnSaveState( ctx[0] )
		},
		500
	);
	
	
	var __details_remove = function ( api, idx )
	{
		var ctx = api.context;
	
		if ( ctx.length ) {
			var row = ctx[0].aoData[ idx !== undefined ? idx : api[0] ];
	
			if ( row && row._details ) {
				row._details.remove();
	
				row._detailsShow = undefined;
				row._details = undefined;
				$( row.nTr ).removeClass( 'dt-hasChild' );
				__details_state( ctx );
			}
		}
	};
	
	
	var __details_display = function ( api, show ) {
		var ctx = api.context;
	
		if ( ctx.length && api.length ) {
			var row = ctx[0].aoData[ api[0] ];
	
			if ( row._details ) {
				row._detailsShow = show;
	
				if ( show ) {
					row._details.insertAfter( row.nTr );
					$( row.nTr ).addClass( 'dt-hasChild' );
				}
				else {
					row._details.detach();
					$( row.nTr ).removeClass( 'dt-hasChild' );
				}
	
				_fnCallbackFire( ctx[0], null, 'childRow', [ show, api.row( api[0] ) ] )
	
				__details_events( ctx[0] );
				__details_state( ctx );
			}
		}
	};
	
	
	var __details_events = function ( settings )
	{
		var api = new _Api( settings );
		var namespace = '.dt.DT_details';
		var drawEvent = 'draw'+namespace;
		var colvisEvent = 'column-sizing'+namespace;
		var destroyEvent = 'destroy'+namespace;
		var data = settings.aoData;
	
		api.off( drawEvent +' '+ colvisEvent +' '+ destroyEvent );
	
		if ( _pluck( data, '_details' ).length > 0 ) {
			// On each draw, insert the required elements into the document
			api.on( drawEvent, function ( e, ctx ) {
				if ( settings !== ctx ) {
					return;
				}
	
				api.rows( {page:'current'} ).eq(0).each( function (idx) {
					// Internal data grab
					var row = data[ idx ];
	
					if ( row._detailsShow ) {
						row._details.insertAfter( row.nTr );
					}
				} );
			} );
	
			// Column visibility change - update the colspan
			api.on( colvisEvent, function ( e, ctx, idx, vis ) {
				if ( settings !== ctx ) {
					return;
				}
	
				// Update the colspan for the details rows (note, only if it already has
				// a colspan)
				var row, visible = _fnVisbleColumns( ctx );
	
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					row = data[i];
	
					if ( row._details ) {
						row._details.children('td[colspan]').attr('colspan', visible );
					}
				}
			} );
	
			// Table destroyed - nuke any child rows
			api.on( destroyEvent, function ( e, ctx ) {
				if ( settings !== ctx ) {
					return;
				}
	
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					if ( data[i]._details ) {
						__details_remove( api, i );
					}
				}
			} );
		}
	};
	
	// Strings for the method names to help minification
	var _emp = '';
	var _child_obj = _emp+'row().child';
	var _child_mth = _child_obj+'()';
	
	// data can be:
	//  tr
	//  string
	//  jQuery or array of any of the above
	_api_register( _child_mth, function ( data, klass ) {
		var ctx = this.context;
	
		if ( data === undefined ) {
			// get
			return ctx.length && this.length ?
				ctx[0].aoData[ this[0] ]._details :
				undefined;
		}
		else if ( data === true ) {
			// show
			this.child.show();
		}
		else if ( data === false ) {
			// remove
			__details_remove( this );
		}
		else if ( ctx.length && this.length ) {
			// set
			__details_add( ctx[0], ctx[0].aoData[ this[0] ], data, klass );
		}
	
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.show()',
		_child_mth+'.show()' // only when `child()` was called with parameters (without
	], function ( show ) {   // it returns an object and this method is not executed)
		__details_display( this, true );
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.hide()',
		_child_mth+'.hide()' // only when `child()` was called with parameters (without
	], function () {         // it returns an object and this method is not executed)
		__details_display( this, false );
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.remove()',
		_child_mth+'.remove()' // only when `child()` was called with parameters (without
	], function () {           // it returns an object and this method is not executed)
		__details_remove( this );
		return this;
	} );
	
	
	_api_register( _child_obj+'.isShown()', function () {
		var ctx = this.context;
	
		if ( ctx.length && this.length ) {
			// _detailsShown as false or undefined will fall through to return false
			return ctx[0].aoData[ this[0] ]._detailsShow || false;
		}
		return false;
	} );
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Columns
	 *
	 * {integer}           - column index (>=0 count from left, <0 count from right)
	 * "{integer}:visIdx"  - visible column index (i.e. translate to column index)  (>=0 count from left, <0 count from right)
	 * "{integer}:visible" - alias for {integer}:visIdx  (>=0 count from left, <0 count from right)
	 * "{string}:name"     - column name
	 * "{string}"          - jQuery selector on column header nodes
	 *
	 */
	
	// can be an array of these items, comma separated list, or an array of comma
	// separated lists
	
	var __re_column_selector = /^([^:]+):(name|visIdx|visible)$/;
	
	
	// r1 and r2 are redundant - but it means that the parameters match for the
	// iterator callback in columns().data()
	var __columnData = function ( settings, column, r1, r2, rows ) {
		var a = [];
		for ( var row=0, ien=rows.length ; row<ien ; row++ ) {
			a.push( _fnGetCellData( settings, rows[row], column ) );
		}
		return a;
	};
	
	
	var __column_selector = function ( settings, selector, opts )
	{
		var
			columns = settings.aoColumns,
			names = _pluck( columns, 'sName' ),
			nodes = _pluck( columns, 'nTh' );
	
		var run = function ( s ) {
			var selInt = _intVal( s );
	
			// Selector - all
			if ( s === '' ) {
				return _range( columns.length );
			}
	
			// Selector - index
			if ( selInt !== null ) {
				return [ selInt >= 0 ?
					selInt : // Count from left
					columns.length + selInt // Count from right (+ because its a negative value)
				];
			}
	
			// Selector = function
			if ( typeof s === 'function' ) {
				var rows = _selector_row_indexes( settings, opts );
	
				return $.map( columns, function (col, idx) {
					return s(
							idx,
							__columnData( settings, idx, 0, 0, rows ),
							nodes[ idx ]
						) ? idx : null;
				} );
			}
	
			// jQuery or string selector
			var match = typeof s === 'string' ?
				s.match( __re_column_selector ) :
				'';
	
			if ( match ) {
				switch( match[2] ) {
					case 'visIdx':
					case 'visible':
						var idx = parseInt( match[1], 10 );
						// Visible index given, convert to column index
						if ( idx < 0 ) {
							// Counting from the right
							var visColumns = $.map( columns, function (col,i) {
								return col.bVisible ? i : null;
							} );
							return [ visColumns[ visColumns.length + idx ] ];
						}
						// Counting from the left
						return [ _fnVisibleToColumnIndex( settings, idx ) ];
	
					case 'name':
						// match by name. `names` is column index complete and in order
						return $.map( names, function (name, i) {
							return name === match[1] ? i : null;
						} );
	
					default:
						return [];
				}
			}
	
			// Cell in the table body
			if ( s.nodeName && s._DT_CellIndex ) {
				return [ s._DT_CellIndex.column ];
			}
	
			// jQuery selector on the TH elements for the columns
			var jqResult = $( nodes )
				.filter( s )
				.map( function () {
					return $.inArray( this, nodes ); // `nodes` is column index complete and in order
				} )
				.toArray();
	
			if ( jqResult.length || ! s.nodeName ) {
				return jqResult;
			}
	
			// Otherwise a node which might have a `dt-column` data attribute, or be
			// a child or such an element
			var host = $(s).closest('*[data-dt-column]');
			return host.length ?
				[ host.data('dt-column') ] :
				[];
		};
	
		return _selector_run( 'column', selector, run, settings, opts );
	};
	
	
	var __setColumnVis = function ( settings, column, vis ) {
		var
			cols = settings.aoColumns,
			col  = cols[ column ],
			data = settings.aoData,
			row, cells, i, ien, tr;
	
		// Get
		if ( vis === undefined ) {
			return col.bVisible;
		}
	
		// Set
		// No change
		if ( col.bVisible === vis ) {
			return;
		}
	
		if ( vis ) {
			// Insert column
			// Need to decide if we should use appendChild or insertBefore
			var insertBefore = $.inArray( true, _pluck(cols, 'bVisible'), column+1 );
	
			for ( i=0, ien=data.length ; i<ien ; i++ ) {
				tr = data[i].nTr;
				cells = data[i].anCells;
	
				if ( tr ) {
					// insertBefore can act like appendChild if 2nd arg is null
					tr.insertBefore( cells[ column ], cells[ insertBefore ] || null );
				}
			}
		}
		else {
			// Remove column
			$( _pluck( settings.aoData, 'anCells', column ) ).detach();
		}
	
		// Common actions
		col.bVisible = vis;
	};
	
	
	_api_register( 'columns()', function ( selector, opts ) {
		// argument shifting
		if ( selector === undefined ) {
			selector = '';
		}
		else if ( $.isPlainObject( selector ) ) {
			opts = selector;
			selector = '';
		}
	
		opts = _selector_opts( opts );
	
		var inst = this.iterator( 'table', function ( settings ) {
			return __column_selector( settings, selector, opts );
		}, 1 );
	
		// Want argument shifting here and in _row_selector?
		inst.selector.cols = selector;
		inst.selector.opts = opts;
	
		return inst;
	} );
	
	_api_registerPlural( 'columns().header()', 'column().header()', function ( selector, opts ) {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].nTh;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().footer()', 'column().footer()', function ( selector, opts ) {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].nTf;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().data()', 'column().data()', function () {
		return this.iterator( 'column-rows', __columnData, 1 );
	} );
	
	_api_registerPlural( 'columns().dataSrc()', 'column().dataSrc()', function () {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].mData;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().cache()', 'column().cache()', function ( type ) {
		return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
			return _pluck_order( settings.aoData, rows,
				type === 'search' ? '_aFilterData' : '_aSortData', column
			);
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().nodes()', 'column().nodes()', function () {
		return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
			return _pluck_order( settings.aoData, rows, 'anCells', column ) ;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().visible()', 'column().visible()', function ( vis, calc ) {
		var that = this;
		var ret = this.iterator( 'column', function ( settings, column ) {
			if ( vis === undefined ) {
				return settings.aoColumns[ column ].bVisible;
			} // else
			__setColumnVis( settings, column, vis );
		} );
	
		// Group the column visibility changes
		if ( vis !== undefined ) {
			this.iterator( 'table', function ( settings ) {
				// Redraw the header after changes
				_fnDrawHead( settings, settings.aoHeader );
				_fnDrawHead( settings, settings.aoFooter );
		
				// Update colspan for no records display. Child rows and extensions will use their own
				// listeners to do this - only need to update the empty table item here
				if ( ! settings.aiDisplay.length ) {
					$(settings.nTBody).find('td[colspan]').attr('colspan', _fnVisbleColumns(settings));
				}
		
				_fnSaveState( settings );
	
				// Second loop once the first is done for events
				that.iterator( 'column', function ( settings, column ) {
					_fnCallbackFire( settings, null, 'column-visibility', [settings, column, vis, calc] );
				} );
	
				if ( calc === undefined || calc ) {
					that.columns.adjust();
				}
			});
		}
	
		return ret;
	} );
	
	_api_registerPlural( 'columns().indexes()', 'column().index()', function ( type ) {
		return this.iterator( 'column', function ( settings, column ) {
			return type === 'visible' ?
				_fnColumnIndexToVisible( settings, column ) :
				column;
		}, 1 );
	} );
	
	_api_register( 'columns.adjust()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnAdjustColumnSizing( settings );
		}, 1 );
	} );
	
	_api_register( 'column.index()', function ( type, idx ) {
		if ( this.context.length !== 0 ) {
			var ctx = this.context[0];
	
			if ( type === 'fromVisible' || type === 'toData' ) {
				return _fnVisibleToColumnIndex( ctx, idx );
			}
			else if ( type === 'fromData' || type === 'toVisible' ) {
				return _fnColumnIndexToVisible( ctx, idx );
			}
		}
	} );
	
	_api_register( 'column()', function ( selector, opts ) {
		return _selector_first( this.columns( selector, opts ) );
	} );
	
	var __cell_selector = function ( settings, selector, opts )
	{
		var data = settings.aoData;
		var rows = _selector_row_indexes( settings, opts );
		var cells = _removeEmpty( _pluck_order( data, rows, 'anCells' ) );
		var allCells = $(_flatten( [], cells ));
		var row;
		var columns = settings.aoColumns.length;
		var a, i, ien, j, o, host;
	
		var run = function ( s ) {
			var fnSelector = typeof s === 'function';
	
			if ( s === null || s === undefined || fnSelector ) {
				// All cells and function selectors
				a = [];
	
				for ( i=0, ien=rows.length ; i<ien ; i++ ) {
					row = rows[i];
	
					for ( j=0 ; j<columns ; j++ ) {
						o = {
							row: row,
							column: j
						};
	
						if ( fnSelector ) {
							// Selector - function
							host = data[ row ];
	
							if ( s( o, _fnGetCellData(settings, row, j), host.anCells ? host.anCells[j] : null ) ) {
								a.push( o );
							}
						}
						else {
							// Selector - all
							a.push( o );
						}
					}
				}
	
				return a;
			}
			
			// Selector - index
			if ( $.isPlainObject( s ) ) {
				// Valid cell index and its in the array of selectable rows
				return s.column !== undefined && s.row !== undefined && $.inArray( s.row, rows ) !== -1 ?
					[s] :
					[];
			}
	
			// Selector - jQuery filtered cells
			var jqResult = allCells
				.filter( s )
				.map( function (i, el) {
					return { // use a new object, in case someone changes the values
						row:    el._DT_CellIndex.row,
						column: el._DT_CellIndex.column
	 				};
				} )
				.toArray();
	
			if ( jqResult.length || ! s.nodeName ) {
				return jqResult;
			}
	
			// Otherwise the selector is a node, and there is one last option - the
			// element might be a child of an element which has dt-row and dt-column
			// data attributes
			host = $(s).closest('*[data-dt-row]');
			return host.length ?
				[ {
					row: host.data('dt-row'),
					column: host.data('dt-column')
				} ] :
				[];
		};
	
		return _selector_run( 'cell', selector, run, settings, opts );
	};
	
	
	
	
	_api_register( 'cells()', function ( rowSelector, columnSelector, opts ) {
		// Argument shifting
		if ( $.isPlainObject( rowSelector ) ) {
			// Indexes
			if ( rowSelector.row === undefined ) {
				// Selector options in first parameter
				opts = rowSelector;
				rowSelector = null;
			}
			else {
				// Cell index objects in first parameter
				opts = columnSelector;
				columnSelector = null;
			}
		}
		if ( $.isPlainObject( columnSelector ) ) {
			opts = columnSelector;
			columnSelector = null;
		}
	
		// Cell selector
		if ( columnSelector === null || columnSelector === undefined ) {
			return this.iterator( 'table', function ( settings ) {
				return __cell_selector( settings, rowSelector, _selector_opts( opts ) );
			} );
		}
	
		// The default built in options need to apply to row and columns
		var internalOpts = opts ? {
			page: opts.page,
			order: opts.order,
			search: opts.search
		} : {};
	
		// Row + column selector
		var columns = this.columns( columnSelector, internalOpts );
		var rows = this.rows( rowSelector, internalOpts );
		var i, ien, j, jen;
	
		var cellsNoOpts = this.iterator( 'table', function ( settings, idx ) {
			var a = [];
	
			for ( i=0, ien=rows[idx].length ; i<ien ; i++ ) {
				for ( j=0, jen=columns[idx].length ; j<jen ; j++ ) {
					a.push( {
						row:    rows[idx][i],
						column: columns[idx][j]
					} );
				}
			}
	
			return a;
		}, 1 );
	
		// There is currently only one extension which uses a cell selector extension
		// It is a _major_ performance drag to run this if it isn't needed, so this is
		// an extension specific check at the moment
		var cells = opts && opts.selected ?
			this.cells( cellsNoOpts, opts ) :
			cellsNoOpts;
	
		$.extend( cells.selector, {
			cols: columnSelector,
			rows: rowSelector,
			opts: opts
		} );
	
		return cells;
	} );
	
	
	_api_registerPlural( 'cells().nodes()', 'cell().node()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			var data = settings.aoData[ row ];
	
			return data && data.anCells ?
				data.anCells[ column ] :
				undefined;
		}, 1 );
	} );
	
	
	_api_register( 'cells().data()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return _fnGetCellData( settings, row, column );
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().cache()', 'cell().cache()', function ( type ) {
		type = type === 'search' ? '_aFilterData' : '_aSortData';
	
		return this.iterator( 'cell', function ( settings, row, column ) {
			return settings.aoData[ row ][ type ][ column ];
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().render()', 'cell().render()', function ( type ) {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return _fnGetCellData( settings, row, column, type );
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().indexes()', 'cell().index()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return {
				row: row,
				column: column,
				columnVisible: _fnColumnIndexToVisible( settings, column )
			};
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().invalidate()', 'cell().invalidate()', function ( src ) {
		return this.iterator( 'cell', function ( settings, row, column ) {
			_fnInvalidate( settings, row, src, column );
		} );
	} );
	
	
	
	_api_register( 'cell()', function ( rowSelector, columnSelector, opts ) {
		return _selector_first( this.cells( rowSelector, columnSelector, opts ) );
	} );
	
	
	_api_register( 'cell().data()', function ( data ) {
		var ctx = this.context;
		var cell = this[0];
	
		if ( data === undefined ) {
			// Get
			return ctx.length && cell.length ?
				_fnGetCellData( ctx[0], cell[0].row, cell[0].column ) :
				undefined;
		}
	
		// Set
		_fnSetCellData( ctx[0], cell[0].row, cell[0].column, data );
		_fnInvalidate( ctx[0], cell[0].row, 'data', cell[0].column );
	
		return this;
	} );
	
	
	
	/**
	 * Get current ordering (sorting) that has been applied to the table.
	 *
	 * @returns {array} 2D array containing the sorting information for the first
	 *   table in the current context. Each element in the parent array represents
	 *   a column being sorted upon (i.e. multi-sorting with two columns would have
	 *   2 inner arrays). The inner arrays may have 2 or 3 elements. The first is
	 *   the column index that the sorting condition applies to, the second is the
	 *   direction of the sort (`desc` or `asc`) and, optionally, the third is the
	 *   index of the sorting order from the `column.sorting` initialisation array.
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {integer} order Column index to sort upon.
	 * @param {string} direction Direction of the sort to be applied (`asc` or `desc`)
	 * @returns {DataTables.Api} this
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {array} order 1D array of sorting information to be applied.
	 * @param {array} [...] Optional additional sorting conditions
	 * @returns {DataTables.Api} this
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {array} order 2D array of sorting information to be applied.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'order()', function ( order, dir ) {
		var ctx = this.context;
	
		if ( order === undefined ) {
			// get
			return ctx.length !== 0 ?
				ctx[0].aaSorting :
				undefined;
		}
	
		// set
		if ( typeof order === 'number' ) {
			// Simple column / direction passed in
			order = [ [ order, dir ] ];
		}
		else if ( order.length && ! Array.isArray( order[0] ) ) {
			// Arguments passed in (list of 1D arrays)
			order = Array.prototype.slice.call( arguments );
		}
		// otherwise a 2D array was passed in
	
		return this.iterator( 'table', function ( settings ) {
			settings.aaSorting = order.slice();
		} );
	} );
	
	
	/**
	 * Attach a sort listener to an element for a given column
	 *
	 * @param {node|jQuery|string} node Identifier for the element(s) to attach the
	 *   listener to. This can take the form of a single DOM node, a jQuery
	 *   collection of nodes or a jQuery selector which will identify the node(s).
	 * @param {integer} column the column that a click on this node will sort on
	 * @param {function} [callback] callback function when sort is run
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'order.listener()', function ( node, column, callback ) {
		return this.iterator( 'table', function ( settings ) {
			_fnSortAttachListener( settings, node, column, callback );
		} );
	} );
	
	
	_api_register( 'order.fixed()', function ( set ) {
		if ( ! set ) {
			var ctx = this.context;
			var fixed = ctx.length ?
				ctx[0].aaSortingFixed :
				undefined;
	
			return Array.isArray( fixed ) ?
				{ pre: fixed } :
				fixed;
		}
	
		return this.iterator( 'table', function ( settings ) {
			settings.aaSortingFixed = $.extend( true, {}, set );
		} );
	} );
	
	
	// Order by the selected column(s)
	_api_register( [
		'columns().order()',
		'column().order()'
	], function ( dir ) {
		var that = this;
	
		return this.iterator( 'table', function ( settings, i ) {
			var sort = [];
	
			$.each( that[i], function (j, col) {
				sort.push( [ col, dir ] );
			} );
	
			settings.aaSorting = sort;
		} );
	} );
	
	
	
	_api_register( 'search()', function ( input, regex, smart, caseInsen ) {
		var ctx = this.context;
	
		if ( input === undefined ) {
			// get
			return ctx.length !== 0 ?
				ctx[0].oPreviousSearch.sSearch :
				undefined;
		}
	
		// set
		return this.iterator( 'table', function ( settings ) {
			if ( ! settings.oFeatures.bFilter ) {
				return;
			}
	
			_fnFilterComplete( settings, $.extend( {}, settings.oPreviousSearch, {
				"sSearch": input+"",
				"bRegex":  regex === null ? false : regex,
				"bSmart":  smart === null ? true  : smart,
				"bCaseInsensitive": caseInsen === null ? true : caseInsen
			} ), 1 );
		} );
	} );
	
	
	_api_registerPlural(
		'columns().search()',
		'column().search()',
		function ( input, regex, smart, caseInsen ) {
			return this.iterator( 'column', function ( settings, column ) {
				var preSearch = settings.aoPreSearchCols;
	
				if ( input === undefined ) {
					// get
					return preSearch[ column ].sSearch;
				}
	
				// set
				if ( ! settings.oFeatures.bFilter ) {
					return;
				}
	
				$.extend( preSearch[ column ], {
					"sSearch": input+"",
					"bRegex":  regex === null ? false : regex,
					"bSmart":  smart === null ? true  : smart,
					"bCaseInsensitive": caseInsen === null ? true : caseInsen
				} );
	
				_fnFilterComplete( settings, settings.oPreviousSearch, 1 );
			} );
		}
	);
	
	/*
	 * State API methods
	 */
	
	_api_register( 'state()', function () {
		return this.context.length ?
			this.context[0].oSavedState :
			null;
	} );
	
	
	_api_register( 'state.clear()', function () {
		return this.iterator( 'table', function ( settings ) {
			// Save an empty object
			settings.fnStateSaveCallback.call( settings.oInstance, settings, {} );
		} );
	} );
	
	
	_api_register( 'state.loaded()', function () {
		return this.context.length ?
			this.context[0].oLoadedState :
			null;
	} );
	
	
	_api_register( 'state.save()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnSaveState( settings );
		} );
	} );
	
	
	
	/**
	 * Provide a common method for plug-ins to check the version of DataTables being
	 * used, in order to ensure compatibility.
	 *
	 *  @param {string} version Version string to check for, in the format "X.Y.Z".
	 *    Note that the formats "X" and "X.Y" are also acceptable.
	 *  @returns {boolean} true if this version of DataTables is greater or equal to
	 *    the required version, or false if this version of DataTales is not
	 *    suitable
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    alert( $.fn.dataTable.versionCheck( '1.9.0' ) );
	 */
	DataTable.versionCheck = DataTable.fnVersionCheck = function( version )
	{
		var aThis = DataTable.version.split('.');
		var aThat = version.split('.');
		var iThis, iThat;
	
		for ( var i=0, iLen=aThat.length ; i<iLen ; i++ ) {
			iThis = parseInt( aThis[i], 10 ) || 0;
			iThat = parseInt( aThat[i], 10 ) || 0;
	
			// Parts are the same, keep comparing
			if (iThis === iThat) {
				continue;
			}
	
			// Parts are different, return immediately
			return iThis > iThat;
		}
	
		return true;
	};
	
	
	/**
	 * Check if a `<table>` node is a DataTable table already or not.
	 *
	 *  @param {node|jquery|string} table Table node, jQuery object or jQuery
	 *      selector for the table to test. Note that if more than more than one
	 *      table is passed on, only the first will be checked
	 *  @returns {boolean} true the table given is a DataTable, or false otherwise
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    if ( ! $.fn.DataTable.isDataTable( '#example' ) ) {
	 *      $('#example').dataTable();
	 *    }
	 */
	DataTable.isDataTable = DataTable.fnIsDataTable = function ( table )
	{
		var t = $(table).get(0);
		var is = false;
	
		if ( table instanceof DataTable.Api ) {
			return true;
		}
	
		$.each( DataTable.settings, function (i, o) {
			var head = o.nScrollHead ? $('table', o.nScrollHead)[0] : null;
			var foot = o.nScrollFoot ? $('table', o.nScrollFoot)[0] : null;
	
			if ( o.nTable === t || head === t || foot === t ) {
				is = true;
			}
		} );
	
		return is;
	};
	
	
	/**
	 * Get all DataTable tables that have been initialised - optionally you can
	 * select to get only currently visible tables.
	 *
	 *  @param {boolean} [visible=false] Flag to indicate if you want all (default)
	 *    or visible tables only.
	 *  @returns {array} Array of `table` nodes (not DataTable instances) which are
	 *    DataTables
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    $.each( $.fn.dataTable.tables(true), function () {
	 *      $(table).DataTable().columns.adjust();
	 *    } );
	 */
	DataTable.tables = DataTable.fnTables = function ( visible )
	{
		var api = false;
	
		if ( $.isPlainObject( visible ) ) {
			api = visible.api;
			visible = visible.visible;
		}
	
		var a = $.map( DataTable.settings, function (o) {
			if ( !visible || (visible && $(o.nTable).is(':visible')) ) {
				return o.nTable;
			}
		} );
	
		return api ?
			new _Api( a ) :
			a;
	};
	
	
	/**
	 * Convert from camel case parameters to Hungarian notation. This is made public
	 * for the extensions to provide the same ability as DataTables core to accept
	 * either the 1.9 style Hungarian notation, or the 1.10+ style camelCase
	 * parameters.
	 *
	 *  @param {object} src The model object which holds all parameters that can be
	 *    mapped.
	 *  @param {object} user The object to convert from camel case to Hungarian.
	 *  @param {boolean} force When set to `true`, properties which already have a
	 *    Hungarian value in the `user` object will be overwritten. Otherwise they
	 *    won't be.
	 */
	DataTable.camelToHungarian = _fnCamelToHungarian;
	
	
	
	/**
	 *
	 */
	_api_register( '$()', function ( selector, opts ) {
		var
			rows   = this.rows( opts ).nodes(), // Get all rows
			jqRows = $(rows);
	
		return $( [].concat(
			jqRows.filter( selector ).toArray(),
			jqRows.find( selector ).toArray()
		) );
	} );
	
	
	// jQuery functions to operate on the tables
	$.each( [ 'on', 'one', 'off' ], function (i, key) {
		_api_register( key+'()', function ( /* event, handler */ ) {
			var args = Array.prototype.slice.call(arguments);
	
			// Add the `dt` namespace automatically if it isn't already present
			args[0] = $.map( args[0].split( /\s/ ), function ( e ) {
				return ! e.match(/\.dt\b/) ?
					e+'.dt' :
					e;
				} ).join( ' ' );
	
			var inst = $( this.tables().nodes() );
			inst[key].apply( inst, args );
			return this;
		} );
	} );
	
	
	_api_register( 'clear()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnClearTable( settings );
		} );
	} );
	
	
	_api_register( 'settings()', function () {
		return new _Api( this.context, this.context );
	} );
	
	
	_api_register( 'init()', function () {
		var ctx = this.context;
		return ctx.length ? ctx[0].oInit : null;
	} );
	
	
	_api_register( 'data()', function () {
		return this.iterator( 'table', function ( settings ) {
			return _pluck( settings.aoData, '_aData' );
		} ).flatten();
	} );
	
	
	_api_register( 'destroy()', function ( remove ) {
		remove = remove || false;
	
		return this.iterator( 'table', function ( settings ) {
			var classes   = settings.oClasses;
			var table     = settings.nTable;
			var tbody     = settings.nTBody;
			var thead     = settings.nTHead;
			var tfoot     = settings.nTFoot;
			var jqTable   = $(table);
			var jqTbody   = $(tbody);
			var jqWrapper = $(settings.nTableWrapper);
			var rows      = $.map( settings.aoData, function (r) { return r.nTr; } );
			var i, ien;
	
			// Flag to note that the table is currently being destroyed - no action
			// should be taken
			settings.bDestroying = true;
	
			// Fire off the destroy callbacks for plug-ins etc
			_fnCallbackFire( settings, "aoDestroyCallback", "destroy", [settings] );
	
			// If not being removed from the document, make all columns visible
			if ( ! remove ) {
				new _Api( settings ).columns().visible( true );
			}
	
			// Blitz all `DT` namespaced events (these are internal events, the
			// lowercase, `dt` events are user subscribed and they are responsible
			// for removing them
			jqWrapper.off('.DT').find(':not(tbody *)').off('.DT');
			$(window).off('.DT-'+settings.sInstance);
	
			// When scrolling we had to break the table up - restore it
			if ( table != thead.parentNode ) {
				jqTable.children('thead').detach();
				jqTable.append( thead );
			}
	
			if ( tfoot && table != tfoot.parentNode ) {
				jqTable.children('tfoot').detach();
				jqTable.append( tfoot );
			}
	
			settings.aaSorting = [];
			settings.aaSortingFixed = [];
			_fnSortingClasses( settings );
	
			$( rows ).removeClass( settings.asStripeClasses.join(' ') );
	
			$('th, td', thead).removeClass( classes.sSortable+' '+
				classes.sSortableAsc+' '+classes.sSortableDesc+' '+classes.sSortableNone
			);
	
			// Add the TR elements back into the table in their original order
			jqTbody.children().detach();
			jqTbody.append( rows );
	
			var orig = settings.nTableWrapper.parentNode;
	
			// Remove the DataTables generated nodes, events and classes
			var removedMethod = remove ? 'remove' : 'detach';
			jqTable[ removedMethod ]();
			jqWrapper[ removedMethod ]();
	
			// If we need to reattach the table to the document
			if ( ! remove && orig ) {
				// insertBefore acts like appendChild if !arg[1]
				orig.insertBefore( table, settings.nTableReinsertBefore );
	
				// Restore the width of the original table - was read from the style property,
				// so we can restore directly to that
				jqTable
					.css( 'width', settings.sDestroyWidth )
					.removeClass( classes.sTable );
	
				// If the were originally stripe classes - then we add them back here.
				// Note this is not fool proof (for example if not all rows had stripe
				// classes - but it's a good effort without getting carried away
				ien = settings.asDestroyStripes.length;
	
				if ( ien ) {
					jqTbody.children().each( function (i) {
						$(this).addClass( settings.asDestroyStripes[i % ien] );
					} );
				}
			}
	
			/* Remove the settings object from the settings array */
			var idx = $.inArray( settings, DataTable.settings );
			if ( idx !== -1 ) {
				DataTable.settings.splice( idx, 1 );
			}
		} );
	} );
	
	
	// Add the `every()` method for rows, columns and cells in a compact form
	$.each( [ 'column', 'row', 'cell' ], function ( i, type ) {
		_api_register( type+'s().every()', function ( fn ) {
			var opts = this.selector.opts;
			var api = this;
	
			return this.iterator( type, function ( settings, arg1, arg2, arg3, arg4 ) {
				// Rows and columns:
				//  arg1 - index
				//  arg2 - table counter
				//  arg3 - loop counter
				//  arg4 - undefined
				// Cells:
				//  arg1 - row index
				//  arg2 - column index
				//  arg3 - table counter
				//  arg4 - loop counter
				fn.call(
					api[ type ](
						arg1,
						type==='cell' ? arg2 : opts,
						type==='cell' ? opts : undefined
					),
					arg1, arg2, arg3, arg4
				);
			} );
		} );
	} );
	
	
	// i18n method for extensions to be able to use the language object from the
	// DataTable
	_api_register( 'i18n()', function ( token, def, plural ) {
		var ctx = this.context[0];
		var resolved = _fnGetObjectDataFn( token )( ctx.oLanguage );
	
		if ( resolved === undefined ) {
			resolved = def;
		}
	
		if ( plural !== undefined && $.isPlainObject( resolved ) ) {
			resolved = resolved[ plural ] !== undefined ?
				resolved[ plural ] :
				resolved._;
		}
	
		return resolved.replace( '%d', plural ); // nb: plural might be undefined,
	} );	
	/**
	 * Version string for plug-ins to check compatibility. Allowed format is
	 * `a.b.c-d` where: a:int, b:int, c:int, d:string(dev|beta|alpha). `d` is used
	 * only for non-release builds. See http://semver.org/ for more information.
	 *  @member
	 *  @type string
	 *  @default Version number
	 */
	DataTable.version = "1.12.1";
	
	/**
	 * Private data store, containing all of the settings objects that are
	 * created for the tables on a given page.
	 *
	 * Note that the `DataTable.settings` object is aliased to
	 * `jQuery.fn.dataTableExt` through which it may be accessed and
	 * manipulated, or `jQuery.fn.dataTable.settings`.
	 *  @member
	 *  @type array
	 *  @default []
	 *  @private
	 */
	DataTable.settings = [];
	
	/**
	 * Object models container, for the various models that DataTables has
	 * available to it. These models define the objects that are used to hold
	 * the active state and configuration of the table.
	 *  @namespace
	 */
	DataTable.models = {};
	
	
	
	/**
	 * Template object for the way in which DataTables holds information about
	 * search information for the global filter and individual column filters.
	 *  @namespace
	 */
	DataTable.models.oSearch = {
		/**
		 * Flag to indicate if the filtering should be case insensitive or not
		 *  @type boolean
		 *  @default true
		 */
		"bCaseInsensitive": true,
	
		/**
		 * Applied search term
		 *  @type string
		 *  @default <i>Empty string</i>
		 */
		"sSearch": "",
	
		/**
		 * Flag to indicate if the search term should be interpreted as a
		 * regular expression (true) or not (false) and therefore and special
		 * regex characters escaped.
		 *  @type boolean
		 *  @default false
		 */
		"bRegex": false,
	
		/**
		 * Flag to indicate if DataTables is to use its smart filtering or not.
		 *  @type boolean
		 *  @default true
		 */
		"bSmart": true,
	
		/**
		 * Flag to indicate if DataTables should only trigger a search when
		 * the return key is pressed.
		 *  @type boolean
		 *  @default false
		 */
		"return": false
	};
	
	
	
	
	/**
	 * Template object for the way in which DataTables holds information about
	 * each individual row. This is the object format used for the settings
	 * aoData array.
	 *  @namespace
	 */
	DataTable.models.oRow = {
		/**
		 * TR element for the row
		 *  @type node
		 *  @default null
		 */
		"nTr": null,
	
		/**
		 * Array of TD elements for each row. This is null until the row has been
		 * created.
		 *  @type array nodes
		 *  @default []
		 */
		"anCells": null,
	
		/**
		 * Data object from the original data source for the row. This is either
		 * an array if using the traditional form of DataTables, or an object if
		 * using mData options. The exact type will depend on the passed in
		 * data from the data source, or will be an array if using DOM a data
		 * source.
		 *  @type array|object
		 *  @default []
		 */
		"_aData": [],
	
		/**
		 * Sorting data cache - this array is ostensibly the same length as the
		 * number of columns (although each index is generated only as it is
		 * needed), and holds the data that is used for sorting each column in the
		 * row. We do this cache generation at the start of the sort in order that
		 * the formatting of the sort data need be done only once for each cell
		 * per sort. This array should not be read from or written to by anything
		 * other than the master sorting methods.
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_aSortData": null,
	
		/**
		 * Per cell filtering data cache. As per the sort data cache, used to
		 * increase the performance of the filtering in DataTables
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_aFilterData": null,
	
		/**
		 * Filtering data cache. This is the same as the cell filtering cache, but
		 * in this case a string rather than an array. This is easily computed with
		 * a join on `_aFilterData`, but is provided as a cache so the join isn't
		 * needed on every search (memory traded for performance)
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_sFilterRow": null,
	
		/**
		 * Cache of the class name that DataTables has applied to the row, so we
		 * can quickly look at this variable rather than needing to do a DOM check
		 * on className for the nTr property.
		 *  @type string
		 *  @default <i>Empty string</i>
		 *  @private
		 */
		"_sRowStripe": "",
	
		/**
		 * Denote if the original data source was from the DOM, or the data source
		 * object. This is used for invalidating data, so DataTables can
		 * automatically read data from the original source, unless uninstructed
		 * otherwise.
		 *  @type string
		 *  @default null
		 *  @private
		 */
		"src": null,
	
		/**
		 * Index in the aoData array. This saves an indexOf lookup when we have the
		 * object, but want to know the index
		 *  @type integer
		 *  @default -1
		 *  @private
		 */
		"idx": -1
	};
	
	
	/**
	 * Template object for the column information object in DataTables. This object
	 * is held in the settings aoColumns array and contains all the information that
	 * DataTables needs about each individual column.
	 *
	 * Note that this object is related to {@link DataTable.defaults.column}
	 * but this one is the internal data store for DataTables's cache of columns.
	 * It should NOT be manipulated outside of DataTables. Any configuration should
	 * be done through the initialisation options.
	 *  @namespace
	 */
	DataTable.models.oColumn = {
		/**
		 * Column index. This could be worked out on-the-fly with $.inArray, but it
		 * is faster to just hold it as a variable
		 *  @type integer
		 *  @default null
		 */
		"idx": null,
	
		/**
		 * A list of the columns that sorting should occur on when this column
		 * is sorted. That this property is an array allows multi-column sorting
		 * to be defined for a column (for example first name / last name columns
		 * would benefit from this). The values are integers pointing to the
		 * columns to be sorted on (typically it will be a single integer pointing
		 * at itself, but that doesn't need to be the case).
		 *  @type array
		 */
		"aDataSort": null,
	
		/**
		 * Define the sorting directions that are applied to the column, in sequence
		 * as the column is repeatedly sorted upon - i.e. the first value is used
		 * as the sorting direction when the column if first sorted (clicked on).
		 * Sort it again (click again) and it will move on to the next index.
		 * Repeat until loop.
		 *  @type array
		 */
		"asSorting": null,
	
		/**
		 * Flag to indicate if the column is searchable, and thus should be included
		 * in the filtering or not.
		 *  @type boolean
		 */
		"bSearchable": null,
	
		/**
		 * Flag to indicate if the column is sortable or not.
		 *  @type boolean
		 */
		"bSortable": null,
	
		/**
		 * Flag to indicate if the column is currently visible in the table or not
		 *  @type boolean
		 */
		"bVisible": null,
	
		/**
		 * Store for manual type assignment using the `column.type` option. This
		 * is held in store so we can manipulate the column's `sType` property.
		 *  @type string
		 *  @default null
		 *  @private
		 */
		"_sManualType": null,
	
		/**
		 * Flag to indicate if HTML5 data attributes should be used as the data
		 * source for filtering or sorting. True is either are.
		 *  @type boolean
		 *  @default false
		 *  @private
		 */
		"_bAttrSrc": false,
	
		/**
		 * Developer definable function that is called whenever a cell is created (Ajax source,
		 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
		 * allowing you to modify the DOM element (add background colour for example) when the
		 * element is available.
		 *  @type function
		 *  @param {element} nTd The TD node that has been created
		 *  @param {*} sData The Data for the cell
		 *  @param {array|object} oData The data for the whole row
		 *  @param {int} iRow The row index for the aoData data store
		 *  @default null
		 */
		"fnCreatedCell": null,
	
		/**
		 * Function to get data from a cell in a column. You should <b>never</b>
		 * access data directly through _aData internally in DataTables - always use
		 * the method attached to this property. It allows mData to function as
		 * required. This function is automatically assigned by the column
		 * initialisation method
		 *  @type function
		 *  @param {array|object} oData The data array/object for the array
		 *    (i.e. aoData[]._aData)
		 *  @param {string} sSpecific The specific data type you want to get -
		 *    'display', 'type' 'filter' 'sort'
		 *  @returns {*} The data for the cell from the given row's data
		 *  @default null
		 */
		"fnGetData": null,
	
		/**
		 * Function to set data for a cell in the column. You should <b>never</b>
		 * set the data directly to _aData internally in DataTables - always use
		 * this method. It allows mData to function as required. This function
		 * is automatically assigned by the column initialisation method
		 *  @type function
		 *  @param {array|object} oData The data array/object for the array
		 *    (i.e. aoData[]._aData)
		 *  @param {*} sValue Value to set
		 *  @default null
		 */
		"fnSetData": null,
	
		/**
		 * Property to read the value for the cells in the column from the data
		 * source array / object. If null, then the default content is used, if a
		 * function is given then the return from the function is used.
		 *  @type function|int|string|null
		 *  @default null
		 */
		"mData": null,
	
		/**
		 * Partner property to mData which is used (only when defined) to get
		 * the data - i.e. it is basically the same as mData, but without the
		 * 'set' option, and also the data fed to it is the result from mData.
		 * This is the rendering method to match the data method of mData.
		 *  @type function|int|string|null
		 *  @default null
		 */
		"mRender": null,
	
		/**
		 * Unique header TH/TD element for this column - this is what the sorting
		 * listener is attached to (if sorting is enabled.)
		 *  @type node
		 *  @default null
		 */
		"nTh": null,
	
		/**
		 * Unique footer TH/TD element for this column (if there is one). Not used
		 * in DataTables as such, but can be used for plug-ins to reference the
		 * footer for each column.
		 *  @type node
		 *  @default null
		 */
		"nTf": null,
	
		/**
		 * The class to apply to all TD elements in the table's TBODY for the column
		 *  @type string
		 *  @default null
		 */
		"sClass": null,
	
		/**
		 * When DataTables calculates the column widths to assign to each column,
		 * it finds the longest string in each column and then constructs a
		 * temporary table and reads the widths from that. The problem with this
		 * is that "mmm" is much wider then "iiii", but the latter is a longer
		 * string - thus the calculation can go wrong (doing it properly and putting
		 * it into an DOM object and measuring that is horribly(!) slow). Thus as
		 * a "work around" we provide this option. It will append its value to the
		 * text that is found to be the longest string for the column - i.e. padding.
		 *  @type string
		 */
		"sContentPadding": null,
	
		/**
		 * Allows a default value to be given for a column's data, and will be used
		 * whenever a null data source is encountered (this can be because mData
		 * is set to null, or because the data source itself is null).
		 *  @type string
		 *  @default null
		 */
		"sDefaultContent": null,
	
		/**
		 * Name for the column, allowing reference to the column by name as well as
		 * by index (needs a lookup to work by name).
		 *  @type string
		 */
		"sName": null,
	
		/**
		 * Custom sorting data type - defines which of the available plug-ins in
		 * afnSortData the custom sorting will use - if any is defined.
		 *  @type string
		 *  @default std
		 */
		"sSortDataType": 'std',
	
		/**
		 * Class to be applied to the header element when sorting on this column
		 *  @type string
		 *  @default null
		 */
		"sSortingClass": null,
	
		/**
		 * Class to be applied to the header element when sorting on this column -
		 * when jQuery UI theming is used.
		 *  @type string
		 *  @default null
		 */
		"sSortingClassJUI": null,
	
		/**
		 * Title of the column - what is seen in the TH element (nTh).
		 *  @type string
		 */
		"sTitle": null,
	
		/**
		 * Column sorting and filtering type
		 *  @type string
		 *  @default null
		 */
		"sType": null,
	
		/**
		 * Width of the column
		 *  @type string
		 *  @default null
		 */
		"sWidth": null,
	
		/**
		 * Width of the column when it was first "encountered"
		 *  @type string
		 *  @default null
		 */
		"sWidthOrig": null
	};
	
	
	/*
	 * Developer note: The properties of the object below are given in Hungarian
	 * notation, that was used as the interface for DataTables prior to v1.10, however
	 * from v1.10 onwards the primary interface is camel case. In order to avoid
	 * breaking backwards compatibility utterly with this change, the Hungarian
	 * version is still, internally the primary interface, but is is not documented
	 * - hence the @name tags in each doc comment. This allows a Javascript function
	 * to create a map from Hungarian notation to camel case (going the other direction
	 * would require each property to be listed, which would add around 3K to the size
	 * of DataTables, while this method is about a 0.5K hit).
	 *
	 * Ultimately this does pave the way for Hungarian notation to be dropped
	 * completely, but that is a massive amount of work and will break current
	 * installs (therefore is on-hold until v2).
	 */
	
	/**
	 * Initialisation options that can be given to DataTables at initialisation
	 * time.
	 *  @namespace
	 */
	DataTable.defaults = {
		/**
		 * An array of data to use for the table, passed in at initialisation which
		 * will be used in preference to any data which is already in the DOM. This is
		 * particularly useful for constructing tables purely in Javascript, for
		 * example with a custom Ajax call.
		 *  @type array
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.data
		 *
		 *  @example
		 *    // Using a 2D array data source
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "data": [
		 *          ['Trident', 'Internet Explorer 4.0', 'Win 95+', 4, 'X'],
		 *          ['Trident', 'Internet Explorer 5.0', 'Win 95+', 5, 'C'],
		 *        ],
		 *        "columns": [
		 *          { "title": "Engine" },
		 *          { "title": "Browser" },
		 *          { "title": "Platform" },
		 *          { "title": "Version" },
		 *          { "title": "Grade" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using an array of objects as a data source (`data`)
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "data": [
		 *          {
		 *            "engine":   "Trident",
		 *            "browser":  "Internet Explorer 4.0",
		 *            "platform": "Win 95+",
		 *            "version":  4,
		 *            "grade":    "X"
		 *          },
		 *          {
		 *            "engine":   "Trident",
		 *            "browser":  "Internet Explorer 5.0",
		 *            "platform": "Win 95+",
		 *            "version":  5,
		 *            "grade":    "C"
		 *          }
		 *        ],
		 *        "columns": [
		 *          { "title": "Engine",   "data": "engine" },
		 *          { "title": "Browser",  "data": "browser" },
		 *          { "title": "Platform", "data": "platform" },
		 *          { "title": "Version",  "data": "version" },
		 *          { "title": "Grade",    "data": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"aaData": null,
	
	
		/**
		 * If ordering is enabled, then DataTables will perform a first pass sort on
		 * initialisation. You can define which column(s) the sort is performed
		 * upon, and the sorting direction, with this variable. The `sorting` array
		 * should contain an array for each column to be sorted initially containing
		 * the column's index and a direction string ('asc' or 'desc').
		 *  @type array
		 *  @default [[0,'asc']]
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.order
		 *
		 *  @example
		 *    // Sort by 3rd column first, and then 4th column
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "order": [[2,'asc'], [3,'desc']]
		 *      } );
		 *    } );
		 *
		 *    // No initial sorting
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "order": []
		 *      } );
		 *    } );
		 */
		"aaSorting": [[0,'asc']],
	
	
		/**
		 * This parameter is basically identical to the `sorting` parameter, but
		 * cannot be overridden by user interaction with the table. What this means
		 * is that you could have a column (visible or hidden) which the sorting
		 * will always be forced on first - any sorting after that (from the user)
		 * will then be performed as required. This can be useful for grouping rows
		 * together.
		 *  @type array
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.orderFixed
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "orderFixed": [[0,'asc']]
		 *      } );
		 *    } )
		 */
		"aaSortingFixed": [],
	
	
		/**
		 * DataTables can be instructed to load data to display in the table from a
		 * Ajax source. This option defines how that Ajax call is made and where to.
		 *
		 * The `ajax` property has three different modes of operation, depending on
		 * how it is defined. These are:
		 *
		 * * `string` - Set the URL from where the data should be loaded from.
		 * * `object` - Define properties for `jQuery.ajax`.
		 * * `function` - Custom data get function
		 *
		 * `string`
		 * --------
		 *
		 * As a string, the `ajax` property simply defines the URL from which
		 * DataTables will load data.
		 *
		 * `object`
		 * --------
		 *
		 * As an object, the parameters in the object are passed to
		 * [jQuery.ajax](http://api.jquery.com/jQuery.ajax/) allowing fine control
		 * of the Ajax request. DataTables has a number of default parameters which
		 * you can override using this option. Please refer to the jQuery
		 * documentation for a full description of the options available, although
		 * the following parameters provide additional options in DataTables or
		 * require special consideration:
		 *
		 * * `data` - As with jQuery, `data` can be provided as an object, but it
		 *   can also be used as a function to manipulate the data DataTables sends
		 *   to the server. The function takes a single parameter, an object of
		 *   parameters with the values that DataTables has readied for sending. An
		 *   object may be returned which will be merged into the DataTables
		 *   defaults, or you can add the items to the object that was passed in and
		 *   not return anything from the function. This supersedes `fnServerParams`
		 *   from DataTables 1.9-.
		 *
		 * * `dataSrc` - By default DataTables will look for the property `data` (or
		 *   `aaData` for compatibility with DataTables 1.9-) when obtaining data
		 *   from an Ajax source or for server-side processing - this parameter
		 *   allows that property to be changed. You can use Javascript dotted
		 *   object notation to get a data source for multiple levels of nesting, or
		 *   it my be used as a function. As a function it takes a single parameter,
		 *   the JSON returned from the server, which can be manipulated as
		 *   required, with the returned value being that used by DataTables as the
		 *   data source for the table. This supersedes `sAjaxDataProp` from
		 *   DataTables 1.9-.
		 *
		 * * `success` - Should not be overridden it is used internally in
		 *   DataTables. To manipulate / transform the data returned by the server
		 *   use `ajax.dataSrc`, or use `ajax` as a function (see below).
		 *
		 * `function`
		 * ----------
		 *
		 * As a function, making the Ajax call is left up to yourself allowing
		 * complete control of the Ajax request. Indeed, if desired, a method other
		 * than Ajax could be used to obtain the required data, such as Web storage
		 * or an AIR database.
		 *
		 * The function is given four parameters and no return is required. The
		 * parameters are:
		 *
		 * 1. _object_ - Data to send to the server
		 * 2. _function_ - Callback function that must be executed when the required
		 *    data has been obtained. That data should be passed into the callback
		 *    as the only parameter
		 * 3. _object_ - DataTables settings object for the table
		 *
		 * Note that this supersedes `fnServerData` from DataTables 1.9-.
		 *
		 *  @type string|object|function
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.ajax
		 *  @since 1.10.0
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax.
		 *   // Note DataTables expects data in the form `{ data: [ ...data... ] }` by default).
		 *   $('#example').dataTable( {
		 *     "ajax": "data.json"
		 *   } );
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax, using `dataSrc` to change
		 *   // `data` to `tableData` (i.e. `{ tableData: [ ...data... ] }`)
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": "tableData"
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax, using `dataSrc` to read data
		 *   // from a plain array rather than an array in an object
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": ""
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Manipulate the data returned from the server - add a link to data
		 *   // (note this can, should, be done using `render` for the column - this
		 *   // is just a simple example of how the data can be manipulated).
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": function ( json ) {
		 *         for ( var i=0, ien=json.length ; i<ien ; i++ ) {
		 *           json[i][0] = '<a href="/message/'+json[i][0]+'>View message</a>';
		 *         }
		 *         return json;
		 *       }
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Add data to the request
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "data": function ( d ) {
		 *         return {
		 *           "extra_search": $('#extra').val()
		 *         };
		 *       }
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Send request as POST
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "type": "POST"
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Get the data from localStorage (could interface with a form for
		 *   // adding, editing and removing rows).
		 *   $('#example').dataTable( {
		 *     "ajax": function (data, callback, settings) {
		 *       callback(
		 *         JSON.parse( localStorage.getItem('dataTablesData') )
		 *       );
		 *     }
		 *   } );
		 */
		"ajax": null,
	
	
		/**
		 * This parameter allows you to readily specify the entries in the length drop
		 * down menu that DataTables shows when pagination is enabled. It can be
		 * either a 1D array of options which will be used for both the displayed
		 * option and the value, or a 2D array which will use the array in the first
		 * position as the value, and the array in the second position as the
		 * displayed options (useful for language strings such as 'All').
		 *
		 * Note that the `pageLength` property will be automatically set to the
		 * first value given in this array, unless `pageLength` is also provided.
		 *  @type array
		 *  @default [ 10, 25, 50, 100 ]
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.lengthMenu
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		 *      } );
		 *    } );
		 */
		"aLengthMenu": [ 10, 25, 50, 100 ],
	
	
		/**
		 * The `columns` option in the initialisation parameter allows you to define
		 * details about the way individual columns behave. For a full list of
		 * column options that can be set, please see
		 * {@link DataTable.defaults.column}. Note that if you use `columns` to
		 * define your columns, you must have an entry in the array for every single
		 * column that you have in your table (these can be null if you don't which
		 * to specify any options).
		 *  @member
		 *
		 *  @name DataTable.defaults.column
		 */
		"aoColumns": null,
	
		/**
		 * Very similar to `columns`, `columnDefs` allows you to target a specific
		 * column, multiple columns, or all columns, using the `targets` property of
		 * each object in the array. This allows great flexibility when creating
		 * tables, as the `columnDefs` arrays can be of any length, targeting the
		 * columns you specifically want. `columnDefs` may use any of the column
		 * options available: {@link DataTable.defaults.column}, but it _must_
		 * have `targets` defined in each object in the array. Values in the `targets`
		 * array may be:
		 *   <ul>
		 *     <li>a string - class name will be matched on the TH for the column</li>
		 *     <li>0 or a positive integer - column index counting from the left</li>
		 *     <li>a negative integer - column index counting from the right</li>
		 *     <li>the string "_all" - all columns (i.e. assign a default)</li>
		 *   </ul>
		 *  @member
		 *
		 *  @name DataTable.defaults.columnDefs
		 */
		"aoColumnDefs": null,
	
	
		/**
		 * Basically the same as `search`, this parameter defines the individual column
		 * filtering state at initialisation time. The array must be of the same size
		 * as the number of columns, and each element be an object with the parameters
		 * `search` and `escapeRegex` (the latter is optional). 'null' is also
		 * accepted and the default will be used.
		 *  @type array
		 *  @default []
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.searchCols
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "searchCols": [
		 *          null,
		 *          { "search": "My filter" },
		 *          null,
		 *          { "search": "^[0-9]", "escapeRegex": false }
		 *        ]
		 *      } );
		 *    } )
		 */
		"aoSearchCols": [],
	
	
		/**
		 * An array of CSS classes that should be applied to displayed rows. This
		 * array may be of any length, and DataTables will apply each class
		 * sequentially, looping when required.
		 *  @type array
		 *  @default null <i>Will take the values determined by the `oClasses.stripe*`
		 *    options</i>
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.stripeClasses
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stripeClasses": [ 'strip1', 'strip2', 'strip3' ]
		 *      } );
		 *    } )
		 */
		"asStripeClasses": null,
	
	
		/**
		 * Enable or disable automatic column width calculation. This can be disabled
		 * as an optimisation (it takes some time to calculate the widths) if the
		 * tables widths are passed in using `columns`.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.autoWidth
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "autoWidth": false
		 *      } );
		 *    } );
		 */
		"bAutoWidth": true,
	
	
		/**
		 * Deferred rendering can provide DataTables with a huge speed boost when you
		 * are using an Ajax or JS data source for the table. This option, when set to
		 * true, will cause DataTables to defer the creation of the table elements for
		 * each row until they are needed for a draw - saving a significant amount of
		 * time.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.deferRender
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajax": "sources/arrays.txt",
		 *        "deferRender": true
		 *      } );
		 *    } );
		 */
		"bDeferRender": false,
	
	
		/**
		 * Replace a DataTable which matches the given selector and replace it with
		 * one which has the properties of the new initialisation object passed. If no
		 * table matches the selector, then the new DataTable will be constructed as
		 * per normal.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.destroy
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "srollY": "200px",
		 *        "paginate": false
		 *      } );
		 *
		 *      // Some time later....
		 *      $('#example').dataTable( {
		 *        "filter": false,
		 *        "destroy": true
		 *      } );
		 *    } );
		 */
		"bDestroy": false,
	
	
		/**
		 * Enable or disable filtering of data. Filtering in DataTables is "smart" in
		 * that it allows the end user to input multiple words (space separated) and
		 * will match a row containing those words, even if not in the order that was
		 * specified (this allow matching across multiple columns). Note that if you
		 * wish to use filtering in DataTables this must remain 'true' - to remove the
		 * default filtering input box and retain filtering abilities, please use
		 * {@link DataTable.defaults.dom}.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.searching
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "searching": false
		 *      } );
		 *    } );
		 */
		"bFilter": true,
	
	
		/**
		 * Enable or disable the table information display. This shows information
		 * about the data that is currently visible on the page, including information
		 * about filtered data if that action is being performed.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.info
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "info": false
		 *      } );
		 *    } );
		 */
		"bInfo": true,
	
	
		/**
		 * Allows the end user to select the size of a formatted page from a select
		 * menu (sizes are 10, 25, 50 and 100). Requires pagination (`paginate`).
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.lengthChange
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "lengthChange": false
		 *      } );
		 *    } );
		 */
		"bLengthChange": true,
	
	
		/**
		 * Enable or disable pagination.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.paging
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "paging": false
		 *      } );
		 *    } );
		 */
		"bPaginate": true,
	
	
		/**
		 * Enable or disable the display of a 'processing' indicator when the table is
		 * being processed (e.g. a sort). This is particularly useful for tables with
		 * large amounts of data where it can take a noticeable amount of time to sort
		 * the entries.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.processing
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "processing": true
		 *      } );
		 *    } );
		 */
		"bProcessing": false,
	
	
		/**
		 * Retrieve the DataTables object for the given selector. Note that if the
		 * table has already been initialised, this parameter will cause DataTables
		 * to simply return the object that has already been set up - it will not take
		 * account of any changes you might have made to the initialisation object
		 * passed to DataTables (setting this parameter to true is an acknowledgement
		 * that you understand this). `destroy` can be used to reinitialise a table if
		 * you need.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.retrieve
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      initTable();
		 *      tableActions();
		 *    } );
		 *
		 *    function initTable ()
		 *    {
		 *      return $('#example').dataTable( {
		 *        "scrollY": "200px",
		 *        "paginate": false,
		 *        "retrieve": true
		 *      } );
		 *    }
		 *
		 *    function tableActions ()
		 *    {
		 *      var table = initTable();
		 *      // perform API operations with oTable
		 *    }
		 */
		"bRetrieve": false,
	
	
		/**
		 * When vertical (y) scrolling is enabled, DataTables will force the height of
		 * the table's viewport to the given height at all times (useful for layout).
		 * However, this can look odd when filtering data down to a small data set,
		 * and the footer is left "floating" further down. This parameter (when
		 * enabled) will cause DataTables to collapse the table's viewport down when
		 * the result set will fit within the given Y height.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.scrollCollapse
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollY": "200",
		 *        "scrollCollapse": true
		 *      } );
		 *    } );
		 */
		"bScrollCollapse": false,
	
	
		/**
		 * Configure DataTables to use server-side processing. Note that the
		 * `ajax` parameter must also be given in order to give DataTables a
		 * source to obtain the required data for each draw.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverSide
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "xhr.php"
		 *      } );
		 *    } );
		 */
		"bServerSide": false,
	
	
		/**
		 * Enable or disable sorting of columns. Sorting of individual columns can be
		 * disabled by the `sortable` option for each column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.ordering
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "ordering": false
		 *      } );
		 *    } );
		 */
		"bSort": true,
	
	
		/**
		 * Enable or display DataTables' ability to sort multiple columns at the
		 * same time (activated by shift-click by the user).
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.orderMulti
		 *
		 *  @example
		 *    // Disable multiple column sorting ability
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "orderMulti": false
		 *      } );
		 *    } );
		 */
		"bSortMulti": true,
	
	
		/**
		 * Allows control over whether DataTables should use the top (true) unique
		 * cell that is found for a single column, or the bottom (false - default).
		 * This is useful when using complex headers.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.orderCellsTop
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "orderCellsTop": true
		 *      } );
		 *    } );
		 */
		"bSortCellsTop": false,
	
	
		/**
		 * Enable or disable the addition of the classes `sorting\_1`, `sorting\_2` and
		 * `sorting\_3` to the columns which are currently being sorted on. This is
		 * presented as a feature switch as it can increase processing time (while
		 * classes are removed and added) so for large data sets you might want to
		 * turn this off.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.orderClasses
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "orderClasses": false
		 *      } );
		 *    } );
		 */
		"bSortClasses": true,
	
	
		/**
		 * Enable or disable state saving. When enabled HTML5 `localStorage` will be
		 * used to save table display information such as pagination information,
		 * display length, filtering and sorting. As such when the end user reloads
		 * the page the display display will match what thy had previously set up.
		 *
		 * Due to the use of `localStorage` the default state saving is not supported
		 * in IE6 or 7. If state saving is required in those browsers, use
		 * `stateSaveCallback` to provide a storage solution such as cookies.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.stateSave
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "stateSave": true
		 *      } );
		 *    } );
		 */
		"bStateSave": false,
	
	
		/**
		 * This function is called when a TR element is created (and all TD child
		 * elements have been inserted), or registered if using a DOM source, allowing
		 * manipulation of the TR element (adding classes etc).
		 *  @type function
		 *  @param {node} row "TR" element for the current row
		 *  @param {array} data Raw data array for this row
		 *  @param {int} dataIndex The index of this row in the internal aoData array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.createdRow
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "createdRow": function( row, data, dataIndex ) {
		 *          // Bold the grade for all 'A' grade browsers
		 *          if ( data[4] == "A" )
		 *          {
		 *            $('td:eq(4)', row).html( '<b>A</b>' );
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnCreatedRow": null,
	
	
		/**
		 * This function is called on every 'draw' event, and allows you to
		 * dynamically modify any aspect you want about the created DOM.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.drawCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "drawCallback": function( settings ) {
		 *          alert( 'DataTables has redrawn the table' );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnDrawCallback": null,
	
	
		/**
		 * Identical to fnHeaderCallback() but for the table footer this function
		 * allows you to modify the table footer on every 'draw' event.
		 *  @type function
		 *  @param {node} foot "TR" element for the footer
		 *  @param {array} data Full table data (as derived from the original HTML)
		 *  @param {int} start Index for the current display starting point in the
		 *    display array
		 *  @param {int} end Index for the current display ending point in the
		 *    display array
		 *  @param {array int} display Index array to translate the visual position
		 *    to the full data array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.footerCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "footerCallback": function( tfoot, data, start, end, display ) {
		 *          tfoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+start;
		 *        }
		 *      } );
		 *    } )
		 */
		"fnFooterCallback": null,
	
	
		/**
		 * When rendering large numbers in the information element for the table
		 * (i.e. "Showing 1 to 10 of 57 entries") DataTables will render large numbers
		 * to have a comma separator for the 'thousands' units (e.g. 1 million is
		 * rendered as "1,000,000") to help readability for the end user. This
		 * function will override the default method DataTables uses.
		 *  @type function
		 *  @member
		 *  @param {int} toFormat number to be formatted
		 *  @returns {string} formatted string for DataTables to show the number
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.formatNumber
		 *
		 *  @example
		 *    // Format a number using a single quote for the separator (note that
		 *    // this can also be done with the language.thousands option)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "formatNumber": function ( toFormat ) {
		 *          return toFormat.toString().replace(
		 *            /\B(?=(\d{3})+(?!\d))/g, "'"
		 *          );
		 *        };
		 *      } );
		 *    } );
		 */
		"fnFormatNumber": function ( toFormat ) {
			return toFormat.toString().replace(
				/\B(?=(\d{3})+(?!\d))/g,
				this.oLanguage.sThousands
			);
		},
	
	
		/**
		 * This function is called on every 'draw' event, and allows you to
		 * dynamically modify the header row. This can be used to calculate and
		 * display useful information about the table.
		 *  @type function
		 *  @param {node} head "TR" element for the header
		 *  @param {array} data Full table data (as derived from the original HTML)
		 *  @param {int} start Index for the current display starting point in the
		 *    display array
		 *  @param {int} end Index for the current display ending point in the
		 *    display array
		 *  @param {array int} display Index array to translate the visual position
		 *    to the full data array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.headerCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "fheaderCallback": function( head, data, start, end, display ) {
		 *          head.getElementsByTagName('th')[0].innerHTML = "Displaying "+(end-start)+" records";
		 *        }
		 *      } );
		 *    } )
		 */
		"fnHeaderCallback": null,
	
	
		/**
		 * The information element can be used to convey information about the current
		 * state of the table. Although the internationalisation options presented by
		 * DataTables are quite capable of dealing with most customisations, there may
		 * be times where you wish to customise the string further. This callback
		 * allows you to do exactly that.
		 *  @type function
		 *  @param {object} oSettings DataTables settings object
		 *  @param {int} start Starting position in data for the draw
		 *  @param {int} end End position in data for the draw
		 *  @param {int} max Total number of rows in the table (regardless of
		 *    filtering)
		 *  @param {int} total Total number of rows in the data set, after filtering
		 *  @param {string} pre The string that DataTables has formatted using it's
		 *    own rules
		 *  @returns {string} The string to be displayed in the information element.
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.infoCallback
		 *
		 *  @example
		 *    $('#example').dataTable( {
		 *      "infoCallback": function( settings, start, end, max, total, pre ) {
		 *        return start +" to "+ end;
		 *      }
		 *    } );
		 */
		"fnInfoCallback": null,
	
	
		/**
		 * Called when the table has been initialised. Normally DataTables will
		 * initialise sequentially and there will be no need for this function,
		 * however, this does not hold true when using external language information
		 * since that is obtained using an async XHR call.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} json The JSON object request from the server - only
		 *    present if client-side Ajax sourced data is used
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.initComplete
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "initComplete": function(settings, json) {
		 *          alert( 'DataTables has finished its initialisation.' );
		 *        }
		 *      } );
		 *    } )
		 */
		"fnInitComplete": null,
	
	
		/**
		 * Called at the very start of each table draw and can be used to cancel the
		 * draw by returning false, any other return (including undefined) results in
		 * the full draw occurring).
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @returns {boolean} False will cancel the draw, anything else (including no
		 *    return) will allow it to complete.
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.preDrawCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "preDrawCallback": function( settings ) {
		 *          if ( $('#test').val() == 1 ) {
		 *            return false;
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnPreDrawCallback": null,
	
	
		/**
		 * This function allows you to 'post process' each row after it have been
		 * generated for each table draw, but before it is rendered on screen. This
		 * function might be used for setting the row class name etc.
		 *  @type function
		 *  @param {node} row "TR" element for the current row
		 *  @param {array} data Raw data array for this row
		 *  @param {int} displayIndex The display index for the current table draw
		 *  @param {int} displayIndexFull The index of the data in the full list of
		 *    rows (after filtering)
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.rowCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "rowCallback": function( row, data, displayIndex, displayIndexFull ) {
		 *          // Bold the grade for all 'A' grade browsers
		 *          if ( data[4] == "A" ) {
		 *            $('td:eq(4)', row).html( '<b>A</b>' );
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnRowCallback": null,
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * This parameter allows you to override the default function which obtains
		 * the data from the server so something more suitable for your application.
		 * For example you could use POST data, or pull information from a Gears or
		 * AIR database.
		 *  @type function
		 *  @member
		 *  @param {string} source HTTP source to obtain the data from (`ajax`)
		 *  @param {array} data A key/value pair object containing the data to send
		 *    to the server
		 *  @param {function} callback to be called on completion of the data get
		 *    process that will draw the data on the page.
		 *  @param {object} settings DataTables settings object
		 *
		 *  @dtopt Callbacks
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverData
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"fnServerData": null,
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 *  It is often useful to send extra data to the server when making an Ajax
		 * request - for example custom filtering information, and this callback
		 * function makes it trivial to send extra information to the server. The
		 * passed in parameter is the data set that has been constructed by
		 * DataTables, and you can add to this or modify it as you require.
		 *  @type function
		 *  @param {array} data Data array (array of objects which are name/value
		 *    pairs) that has been constructed by DataTables and will be sent to the
		 *    server. In the case of Ajax sourced data with server-side processing
		 *    this will be an empty array, for server-side processing there will be a
		 *    significant number of parameters!
		 *  @returns {undefined} Ensure that you modify the data array passed in,
		 *    as this is passed by reference.
		 *
		 *  @dtopt Callbacks
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverParams
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"fnServerParams": null,
	
	
		/**
		 * Load the table state. With this function you can define from where, and how, the
		 * state of a table is loaded. By default DataTables will load from `localStorage`
		 * but you might wish to use a server-side database or cookies.
		 *  @type function
		 *  @member
		 *  @param {object} settings DataTables settings object
		 *  @param {object} callback Callback that can be executed when done. It
		 *    should be passed the loaded state object.
		 *  @return {object} The DataTables state object to be loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoadCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadCallback": function (settings, callback) {
		 *          $.ajax( {
		 *            "url": "/state_load",
		 *            "dataType": "json",
		 *            "success": function (json) {
		 *              callback( json );
		 *            }
		 *          } );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoadCallback": function ( settings ) {
			try {
				return JSON.parse(
					(settings.iStateDuration === -1 ? sessionStorage : localStorage).getItem(
						'DataTables_'+settings.sInstance+'_'+location.pathname
					)
				);
			} catch (e) {
				return {};
			}
		},
	
	
		/**
		 * Callback which allows modification of the saved state prior to loading that state.
		 * This callback is called when the table is loading state from the stored data, but
		 * prior to the settings object being modified by the saved state. Note that for
		 * plug-in authors, you should use the `stateLoadParams` event to load parameters for
		 * a plug-in.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object that is to be loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoadParams
		 *
		 *  @example
		 *    // Remove a saved filter, so filtering is never loaded
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadParams": function (settings, data) {
		 *          data.oSearch.sSearch = "";
		 *        }
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Disallow state loading by returning false
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadParams": function (settings, data) {
		 *          return false;
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoadParams": null,
	
	
		/**
		 * Callback that is called when the state has been loaded from the state saving method
		 * and the DataTables settings object has been modified as a result of the loaded state.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object that was loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoaded
		 *
		 *  @example
		 *    // Show an alert with the filtering value that was saved
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoaded": function (settings, data) {
		 *          alert( 'Saved filter was: '+data.oSearch.sSearch );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoaded": null,
	
	
		/**
		 * Save the table state. This function allows you to define where and how the state
		 * information for the table is stored By default DataTables will use `localStorage`
		 * but you might wish to use a server-side database or cookies.
		 *  @type function
		 *  @member
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object to be saved
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateSaveCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateSaveCallback": function (settings, data) {
		 *          // Send an Ajax request to the server with the state object
		 *          $.ajax( {
		 *            "url": "/state_save",
		 *            "data": data,
		 *            "dataType": "json",
		 *            "method": "POST"
		 *            "success": function () {}
		 *          } );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateSaveCallback": function ( settings, data ) {
			try {
				(settings.iStateDuration === -1 ? sessionStorage : localStorage).setItem(
					'DataTables_'+settings.sInstance+'_'+location.pathname,
					JSON.stringify( data )
				);
			} catch (e) {}
		},
	
	
		/**
		 * Callback which allows modification of the state to be saved. Called when the table
		 * has changed state a new state save is required. This method allows modification of
		 * the state saving object prior to actually doing the save, including addition or
		 * other state properties or modification. Note that for plug-in authors, you should
		 * use the `stateSaveParams` event to save parameters for a plug-in.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object to be saved
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateSaveParams
		 *
		 *  @example
		 *    // Remove a saved filter, so filtering is never saved
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateSaveParams": function (settings, data) {
		 *          data.oSearch.sSearch = "";
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateSaveParams": null,
	
	
		/**
		 * Duration for which the saved state information is considered valid. After this period
		 * has elapsed the state will be returned to the default.
		 * Value is given in seconds.
		 *  @type int
		 *  @default 7200 <i>(2 hours)</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.stateDuration
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateDuration": 60*60*24; // 1 day
		 *      } );
		 *    } )
		 */
		"iStateDuration": 7200,
	
	
		/**
		 * When enabled DataTables will not make a request to the server for the first
		 * page draw - rather it will use the data already on the page (no sorting etc
		 * will be applied to it), thus saving on an XHR at load time. `deferLoading`
		 * is used to indicate that deferred loading is required, but it is also used
		 * to tell DataTables how many records there are in the full table (allowing
		 * the information element and pagination to be displayed correctly). In the case
		 * where a filtering is applied to the table on initial load, this can be
		 * indicated by giving the parameter as an array, where the first element is
		 * the number of records available after filtering and the second element is the
		 * number of records without filtering (allowing the table information element
		 * to be shown correctly).
		 *  @type int | array
		 *  @default null
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.deferLoading
		 *
		 *  @example
		 *    // 57 records available in the table, no filtering applied
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "scripts/server_processing.php",
		 *        "deferLoading": 57
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // 57 records after filtering, 100 without filtering (an initial filter applied)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "scripts/server_processing.php",
		 *        "deferLoading": [ 57, 100 ],
		 *        "search": {
		 *          "search": "my_filter"
		 *        }
		 *      } );
		 *    } );
		 */
		"iDeferLoading": null,
	
	
		/**
		 * Number of rows to display on a single page when using pagination. If
		 * feature enabled (`lengthChange`) then the end user will be able to override
		 * this to a custom setting using a pop-up menu.
		 *  @type int
		 *  @default 10
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.pageLength
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "pageLength": 50
		 *      } );
		 *    } )
		 */
		"iDisplayLength": 10,
	
	
		/**
		 * Define the starting point for data display when using DataTables with
		 * pagination. Note that this parameter is the number of records, rather than
		 * the page number, so if you have 10 records per page and want to start on
		 * the third page, it should be "20".
		 *  @type int
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.displayStart
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "displayStart": 20
		 *      } );
		 *    } )
		 */
		"iDisplayStart": 0,
	
	
		/**
		 * By default DataTables allows keyboard navigation of the table (sorting, paging,
		 * and filtering) by adding a `tabindex` attribute to the required elements. This
		 * allows you to tab through the controls and press the enter key to activate them.
		 * The tabindex is default 0, meaning that the tab follows the flow of the document.
		 * You can overrule this using this parameter if you wish. Use a value of -1 to
		 * disable built-in keyboard navigation.
		 *  @type int
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.tabIndex
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "tabIndex": 1
		 *      } );
		 *    } );
		 */
		"iTabIndex": 0,
	
	
		/**
		 * Classes that DataTables assigns to the various components and features
		 * that it adds to the HTML table. This allows classes to be configured
		 * during initialisation in addition to through the static
		 * {@link DataTable.ext.oStdClasses} object).
		 *  @namespace
		 *  @name DataTable.defaults.classes
		 */
		"oClasses": {},
	
	
		/**
		 * All strings that DataTables uses in the user interface that it creates
		 * are defined in this object, allowing you to modified them individually or
		 * completely replace them all as required.
		 *  @namespace
		 *  @name DataTable.defaults.language
		 */
		"oLanguage": {
			/**
			 * Strings that are used for WAI-ARIA labels and controls only (these are not
			 * actually visible on the page, but will be read by screenreaders, and thus
			 * must be internationalised as well).
			 *  @namespace
			 *  @name DataTable.defaults.language.aria
			 */
			"oAria": {
				/**
				 * ARIA label that is added to the table headers when the column may be
				 * sorted ascending by activing the column (click or return when focused).
				 * Note that the column header is prefixed to this string.
				 *  @type string
				 *  @default : activate to sort column ascending
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.aria.sortAscending
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "aria": {
				 *            "sortAscending": " - click/return to sort ascending"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sSortAscending": ": activate to sort column ascending",
	
				/**
				 * ARIA label that is added to the table headers when the column may be
				 * sorted descending by activing the column (click or return when focused).
				 * Note that the column header is prefixed to this string.
				 *  @type string
				 *  @default : activate to sort column ascending
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.aria.sortDescending
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "aria": {
				 *            "sortDescending": " - click/return to sort descending"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sSortDescending": ": activate to sort column descending"
			},
	
			/**
			 * Pagination string used by DataTables for the built-in pagination
			 * control types.
			 *  @namespace
			 *  @name DataTable.defaults.language.paginate
			 */
			"oPaginate": {
				/**
				 * Text to use when using the 'full_numbers' type of pagination for the
				 * button to take the user to the first page.
				 *  @type string
				 *  @default First
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.first
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "first": "First page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sFirst": "First",
	
	
				/**
				 * Text to use when using the 'full_numbers' type of pagination for the
				 * button to take the user to the last page.
				 *  @type string
				 *  @default Last
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.last
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "last": "Last page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sLast": "Last",
	
	
				/**
				 * Text to use for the 'next' pagination button (to take the user to the
				 * next page).
				 *  @type string
				 *  @default Next
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.next
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "next": "Next page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sNext": "Next",
	
	
				/**
				 * Text to use for the 'previous' pagination button (to take the user to
				 * the previous page).
				 *  @type string
				 *  @default Previous
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.previous
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "previous": "Previous page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sPrevious": "Previous"
			},
	
			/**
			 * This string is shown in preference to `zeroRecords` when the table is
			 * empty of data (regardless of filtering). Note that this is an optional
			 * parameter - if it is not given, the value of `zeroRecords` will be used
			 * instead (either the default or given value).
			 *  @type string
			 *  @default No data available in table
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.emptyTable
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "emptyTable": "No data available in table"
			 *        }
			 *      } );
			 *    } );
			 */
			"sEmptyTable": "No data available in table",
	
	
			/**
			 * This string gives information to the end user about the information
			 * that is current on display on the page. The following tokens can be
			 * used in the string and will be dynamically replaced as the table
			 * display updates. This tokens can be placed anywhere in the string, or
			 * removed as needed by the language requires:
			 *
			 * * `\_START\_` - Display index of the first record on the current page
			 * * `\_END\_` - Display index of the last record on the current page
			 * * `\_TOTAL\_` - Number of records in the table after filtering
			 * * `\_MAX\_` - Number of records in the table without filtering
			 * * `\_PAGE\_` - Current page number
			 * * `\_PAGES\_` - Total number of pages of data in the table
			 *
			 *  @type string
			 *  @default Showing _START_ to _END_ of _TOTAL_ entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.info
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "info": "Showing page _PAGE_ of _PAGES_"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
	
	
			/**
			 * Display information string for when the table is empty. Typically the
			 * format of this string should match `info`.
			 *  @type string
			 *  @default Showing 0 to 0 of 0 entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoEmpty
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoEmpty": "No entries to show"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoEmpty": "Showing 0 to 0 of 0 entries",
	
	
			/**
			 * When a user filters the information in a table, this string is appended
			 * to the information (`info`) to give an idea of how strong the filtering
			 * is. The variable _MAX_ is dynamically updated.
			 *  @type string
			 *  @default (filtered from _MAX_ total entries)
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoFiltered
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoFiltered": " - filtering from _MAX_ records"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoFiltered": "(filtered from _MAX_ total entries)",
	
	
			/**
			 * If can be useful to append extra information to the info string at times,
			 * and this variable does exactly that. This information will be appended to
			 * the `info` (`infoEmpty` and `infoFiltered` in whatever combination they are
			 * being used) at all times.
			 *  @type string
			 *  @default <i>Empty string</i>
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoPostFix
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoPostFix": "All records shown are derived from real information."
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoPostFix": "",
	
	
			/**
			 * This decimal place operator is a little different from the other
			 * language options since DataTables doesn't output floating point
			 * numbers, so it won't ever use this for display of a number. Rather,
			 * what this parameter does is modify the sort methods of the table so
			 * that numbers which are in a format which has a character other than
			 * a period (`.`) as a decimal place will be sorted numerically.
			 *
			 * Note that numbers with different decimal places cannot be shown in
			 * the same table and still be sortable, the table must be consistent.
			 * However, multiple different tables on the page can use different
			 * decimal place characters.
			 *  @type string
			 *  @default 
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.decimal
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "decimal": ","
			 *          "thousands": "."
			 *        }
			 *      } );
			 *    } );
			 */
			"sDecimal": "",
	
	
			/**
			 * DataTables has a build in number formatter (`formatNumber`) which is
			 * used to format large numbers that are used in the table information.
			 * By default a comma is used, but this can be trivially changed to any
			 * character you wish with this parameter.
			 *  @type string
			 *  @default ,
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.thousands
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "thousands": "'"
			 *        }
			 *      } );
			 *    } );
			 */
			"sThousands": ",",
	
	
			/**
			 * Detail the action that will be taken when the drop down menu for the
			 * pagination length option is changed. The '_MENU_' variable is replaced
			 * with a default select list of 10, 25, 50 and 100, and can be replaced
			 * with a custom select box if required.
			 *  @type string
			 *  @default Show _MENU_ entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.lengthMenu
			 *
			 *  @example
			 *    // Language change only
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "lengthMenu": "Display _MENU_ records"
			 *        }
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Language and options change
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "lengthMenu": 'Display <select>'+
			 *            '<option value="10">10</option>'+
			 *            '<option value="20">20</option>'+
			 *            '<option value="30">30</option>'+
			 *            '<option value="40">40</option>'+
			 *            '<option value="50">50</option>'+
			 *            '<option value="-1">All</option>'+
			 *            '</select> records'
			 *        }
			 *      } );
			 *    } );
			 */
			"sLengthMenu": "Show _MENU_ entries",
	
	
			/**
			 * When using Ajax sourced data and during the first draw when DataTables is
			 * gathering the data, this message is shown in an empty row in the table to
			 * indicate to the end user the the data is being loaded. Note that this
			 * parameter is not used when loading data by server-side processing, just
			 * Ajax sourced data with client-side processing.
			 *  @type string
			 *  @default Loading...
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.loadingRecords
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "loadingRecords": "Please wait - loading..."
			 *        }
			 *      } );
			 *    } );
			 */
			"sLoadingRecords": "Loading...",
	
	
			/**
			 * Text which is displayed when the table is processing a user action
			 * (usually a sort command or similar).
			 *  @type string
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.processing
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "processing": "DataTables is currently busy"
			 *        }
			 *      } );
			 *    } );
			 */
			"sProcessing": "",
	
	
			/**
			 * Details the actions that will be taken when the user types into the
			 * filtering input text box. The variable "_INPUT_", if used in the string,
			 * is replaced with the HTML text box for the filtering input allowing
			 * control over where it appears in the string. If "_INPUT_" is not given
			 * then the input box is appended to the string automatically.
			 *  @type string
			 *  @default Search:
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.search
			 *
			 *  @example
			 *    // Input text box will be appended at the end automatically
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "search": "Filter records:"
			 *        }
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Specify where the filter should appear
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "search": "Apply filter _INPUT_ to table"
			 *        }
			 *      } );
			 *    } );
			 */
			"sSearch": "Search:",
	
	
			/**
			 * Assign a `placeholder` attribute to the search `input` element
			 *  @type string
			 *  @default 
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.searchPlaceholder
			 */
			"sSearchPlaceholder": "",
	
	
			/**
			 * All of the language information can be stored in a file on the
			 * server-side, which DataTables will look up if this parameter is passed.
			 * It must store the URL of the language file, which is in a JSON format,
			 * and the object has the same properties as the oLanguage object in the
			 * initialiser object (i.e. the above parameters). Please refer to one of
			 * the example language files to see how this works in action.
			 *  @type string
			 *  @default <i>Empty string - i.e. disabled</i>
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.url
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "url": "http://www.sprymedia.co.uk/dataTables/lang.txt"
			 *        }
			 *      } );
			 *    } );
			 */
			"sUrl": "",
	
	
			/**
			 * Text shown inside the table records when the is no information to be
			 * displayed after filtering. `emptyTable` is shown when there is simply no
			 * information in the table at all (regardless of filtering).
			 *  @type string
			 *  @default No matching records found
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.zeroRecords
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "zeroRecords": "No records to display"
			 *        }
			 *      } );
			 *    } );
			 */
			"sZeroRecords": "No matching records found"
		},
	
	
		/**
		 * This parameter allows you to have define the global filtering state at
		 * initialisation time. As an object the `search` parameter must be
		 * defined, but all other parameters are optional. When `regex` is true,
		 * the search string will be treated as a regular expression, when false
		 * (default) it will be treated as a straight string. When `smart`
		 * DataTables will use it's smart filtering methods (to word match at
		 * any point in the data), when false this will not be done.
		 *  @namespace
		 *  @extends DataTable.models.oSearch
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.search
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "search": {"search": "Initial search"}
		 *      } );
		 *    } )
		 */
		"oSearch": $.extend( {}, DataTable.models.oSearch ),
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * By default DataTables will look for the property `data` (or `aaData` for
		 * compatibility with DataTables 1.9-) when obtaining data from an Ajax
		 * source or for server-side processing - this parameter allows that
		 * property to be changed. You can use Javascript dotted object notation to
		 * get a data source for multiple levels of nesting.
		 *  @type string
		 *  @default data
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.ajaxDataProp
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sAjaxDataProp": "data",
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * You can instruct DataTables to load data from an external
		 * source using this parameter (use aData if you want to pass data in you
		 * already have). Simply provide a url a JSON object can be obtained from.
		 *  @type string
		 *  @default null
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.ajaxSource
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sAjaxSource": null,
	
	
		/**
		 * This initialisation variable allows you to specify exactly where in the
		 * DOM you want DataTables to inject the various controls it adds to the page
		 * (for example you might want the pagination controls at the top of the
		 * table). DIV elements (with or without a custom class) can also be added to
		 * aid styling. The follow syntax is used:
		 *   <ul>
		 *     <li>The following options are allowed:
		 *       <ul>
		 *         <li>'l' - Length changing</li>
		 *         <li>'f' - Filtering input</li>
		 *         <li>'t' - The table!</li>
		 *         <li>'i' - Information</li>
		 *         <li>'p' - Pagination</li>
		 *         <li>'r' - pRocessing</li>
		 *       </ul>
		 *     </li>
		 *     <li>The following constants are allowed:
		 *       <ul>
		 *         <li>'H' - jQueryUI theme "header" classes ('fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix')</li>
		 *         <li>'F' - jQueryUI theme "footer" classes ('fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix')</li>
		 *       </ul>
		 *     </li>
		 *     <li>The following syntax is expected:
		 *       <ul>
		 *         <li>'&lt;' and '&gt;' - div elements</li>
		 *         <li>'&lt;"class" and '&gt;' - div with a class</li>
		 *         <li>'&lt;"#id" and '&gt;' - div with an ID</li>
		 *       </ul>
		 *     </li>
		 *     <li>Examples:
		 *       <ul>
		 *         <li>'&lt;"wrapper"flipt&gt;'</li>
		 *         <li>'&lt;lf&lt;t&gt;ip&gt;'</li>
		 *       </ul>
		 *     </li>
		 *   </ul>
		 *  @type string
		 *  @default lfrtip <i>(when `jQueryUI` is false)</i> <b>or</b>
		 *    <"H"lfr>t<"F"ip> <i>(when `jQueryUI` is true)</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.dom
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "dom": '&lt;"top"i&gt;rt&lt;"bottom"flp&gt;&lt;"clear"&gt;'
		 *      } );
		 *    } );
		 */
		"sDom": "lfrtip",
	
	
		/**
		 * Search delay option. This will throttle full table searches that use the
		 * DataTables provided search input element (it does not effect calls to
		 * `dt-api search()`, providing a delay before the search is made.
		 *  @type integer
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.searchDelay
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "searchDelay": 200
		 *      } );
		 *    } )
		 */
		"searchDelay": null,
	
	
		/**
		 * DataTables features six different built-in options for the buttons to
		 * display for pagination control:
		 *
		 * * `numbers` - Page number buttons only
		 * * `simple` - 'Previous' and 'Next' buttons only
		 * * 'simple_numbers` - 'Previous' and 'Next' buttons, plus page numbers
		 * * `full` - 'First', 'Previous', 'Next' and 'Last' buttons
		 * * `full_numbers` - 'First', 'Previous', 'Next' and 'Last' buttons, plus page numbers
		 * * `first_last_numbers` - 'First' and 'Last' buttons, plus page numbers
		 *  
		 * Further methods can be added using {@link DataTable.ext.oPagination}.
		 *  @type string
		 *  @default simple_numbers
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.pagingType
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "pagingType": "full_numbers"
		 *      } );
		 *    } )
		 */
		"sPaginationType": "simple_numbers",
	
	
		/**
		 * Enable horizontal scrolling. When a table is too wide to fit into a
		 * certain layout, or you have a large number of columns in the table, you
		 * can enable x-scrolling to show the table in a viewport, which can be
		 * scrolled. This property can be `true` which will allow the table to
		 * scroll horizontally when needed, or any CSS unit, or a number (in which
		 * case it will be treated as a pixel measurement). Setting as simply `true`
		 * is recommended.
		 *  @type boolean|string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.scrollX
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollX": true,
		 *        "scrollCollapse": true
		 *      } );
		 *    } );
		 */
		"sScrollX": "",
	
	
		/**
		 * This property can be used to force a DataTable to use more width than it
		 * might otherwise do when x-scrolling is enabled. For example if you have a
		 * table which requires to be well spaced, this parameter is useful for
		 * "over-sizing" the table, and thus forcing scrolling. This property can by
		 * any CSS unit, or a number (in which case it will be treated as a pixel
		 * measurement).
		 *  @type string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.scrollXInner
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollX": "100%",
		 *        "scrollXInner": "110%"
		 *      } );
		 *    } );
		 */
		"sScrollXInner": "",
	
	
		/**
		 * Enable vertical scrolling. Vertical scrolling will constrain the DataTable
		 * to the given height, and enable scrolling for any data which overflows the
		 * current viewport. This can be used as an alternative to paging to display
		 * a lot of data in a small area (although paging and scrolling can both be
		 * enabled at the same time). This property can be any CSS unit, or a number
		 * (in which case it will be treated as a pixel measurement).
		 *  @type string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.scrollY
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollY": "200px",
		 *        "paginate": false
		 *      } );
		 *    } );
		 */
		"sScrollY": "",
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * Set the HTTP method that is used to make the Ajax call for server-side
		 * processing or Ajax sourced data.
		 *  @type string
		 *  @default GET
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverMethod
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sServerMethod": "GET",
	
	
		/**
		 * DataTables makes use of renderers when displaying HTML elements for
		 * a table. These renderers can be added or modified by plug-ins to
		 * generate suitable mark-up for a site. For example the Bootstrap
		 * integration plug-in for DataTables uses a paging button renderer to
		 * display pagination buttons in the mark-up required by Bootstrap.
		 *
		 * For further information about the renderers available see
		 * DataTable.ext.renderer
		 *  @type string|object
		 *  @default null
		 *
		 *  @name DataTable.defaults.renderer
		 *
		 */
		"renderer": null,
	
	
		/**
		 * Set the data property name that DataTables should use to get a row's id
		 * to set as the `id` property in the node.
		 *  @type string
		 *  @default DT_RowId
		 *
		 *  @name DataTable.defaults.rowId
		 */
		"rowId": "DT_RowId"
	};
	
	_fnHungarianMap( DataTable.defaults );
	
	
	
	/*
	 * Developer note - See note in model.defaults.js about the use of Hungarian
	 * notation and camel case.
	 */
	
	/**
	 * Column options that can be given to DataTables at initialisation time.
	 *  @namespace
	 */
	DataTable.defaults.column = {
		/**
		 * Define which column(s) an order will occur on for this column. This
		 * allows a column's ordering to take multiple columns into account when
		 * doing a sort or use the data from a different column. For example first
		 * name / last name columns make sense to do a multi-column sort over the
		 * two columns.
		 *  @type array|int
		 *  @default null <i>Takes the value of the column index automatically</i>
		 *
		 *  @name DataTable.defaults.column.orderData
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderData": [ 0, 1 ], "targets": [ 0 ] },
		 *          { "orderData": [ 1, 0 ], "targets": [ 1 ] },
		 *          { "orderData": 2, "targets": [ 2 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "orderData": [ 0, 1 ] },
		 *          { "orderData": [ 1, 0 ] },
		 *          { "orderData": 2 },
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"aDataSort": null,
		"iDataSort": -1,
	
	
		/**
		 * You can control the default ordering direction, and even alter the
		 * behaviour of the sort handler (i.e. only allow ascending ordering etc)
		 * using this parameter.
		 *  @type array
		 *  @default [ 'asc', 'desc' ]
		 *
		 *  @name DataTable.defaults.column.orderSequence
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderSequence": [ "asc" ], "targets": [ 1 ] },
		 *          { "orderSequence": [ "desc", "asc", "asc" ], "targets": [ 2 ] },
		 *          { "orderSequence": [ "desc" ], "targets": [ 3 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          { "orderSequence": [ "asc" ] },
		 *          { "orderSequence": [ "desc", "asc", "asc" ] },
		 *          { "orderSequence": [ "desc" ] },
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"asSorting": [ 'asc', 'desc' ],
	
	
		/**
		 * Enable or disable filtering on the data in this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.searchable
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "searchable": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "searchable": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bSearchable": true,
	
	
		/**
		 * Enable or disable ordering on this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.orderable
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderable": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "orderable": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bSortable": true,
	
	
		/**
		 * Enable or disable the display of this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.visible
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "visible": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "visible": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bVisible": true,
	
	
		/**
		 * Developer definable function that is called whenever a cell is created (Ajax source,
		 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
		 * allowing you to modify the DOM element (add background colour for example) when the
		 * element is available.
		 *  @type function
		 *  @param {element} td The TD node that has been created
		 *  @param {*} cellData The Data for the cell
		 *  @param {array|object} rowData The data for the whole row
		 *  @param {int} row The row index for the aoData data store
		 *  @param {int} col The column index for aoColumns
		 *
		 *  @name DataTable.defaults.column.createdCell
		 *  @dtopt Columns
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [3],
		 *          "createdCell": function (td, cellData, rowData, row, col) {
		 *            if ( cellData == "1.7" ) {
		 *              $(td).css('color', 'blue')
		 *            }
		 *          }
		 *        } ]
		 *      });
		 *    } );
		 */
		"fnCreatedCell": null,
	
	
		/**
		 * This parameter has been replaced by `data` in DataTables to ensure naming
		 * consistency. `dataProp` can still be used, as there is backwards
		 * compatibility in DataTables for this option, but it is strongly
		 * recommended that you use `data` in preference to `dataProp`.
		 *  @name DataTable.defaults.column.dataProp
		 */
	
	
		/**
		 * This property can be used to read data from any data source property,
		 * including deeply nested objects / properties. `data` can be given in a
		 * number of different ways which effect its behaviour:
		 *
		 * * `integer` - treated as an array index for the data source. This is the
		 *   default that DataTables uses (incrementally increased for each column).
		 * * `string` - read an object property from the data source. There are
		 *   three 'special' options that can be used in the string to alter how
		 *   DataTables reads the data from the source object:
		 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
		 *      Javascript to read from nested objects, so to can the options
		 *      specified in `data`. For example: `browser.version` or
		 *      `browser.name`. If your object parameter name contains a period, use
		 *      `\\` to escape it - i.e. `first\\.name`.
		 *    * `[]` - Array notation. DataTables can automatically combine data
		 *      from and array source, joining the data with the characters provided
		 *      between the two brackets. For example: `name[, ]` would provide a
		 *      comma-space separated list from the source array. If no characters
		 *      are provided between the brackets, the original array source is
		 *      returned.
		 *    * `()` - Function notation. Adding `()` to the end of a parameter will
		 *      execute a function of the name given. For example: `browser()` for a
		 *      simple function on the data source, `browser.version()` for a
		 *      function in a nested property or even `browser().version` to get an
		 *      object property if the function called returns an object. Note that
		 *      function notation is recommended for use in `render` rather than
		 *      `data` as it is much simpler to use as a renderer.
		 * * `null` - use the original data source for the row rather than plucking
		 *   data directly from it. This action has effects on two other
		 *   initialisation options:
		 *    * `defaultContent` - When null is given as the `data` option and
		 *      `defaultContent` is specified for the column, the value defined by
		 *      `defaultContent` will be used for the cell.
		 *    * `render` - When null is used for the `data` option and the `render`
		 *      option is specified for the column, the whole data source for the
		 *      row is used for the renderer.
		 * * `function` - the function given will be executed whenever DataTables
		 *   needs to set or get the data for a cell in the column. The function
		 *   takes three parameters:
		 *    * Parameters:
		 *      * `{array|object}` The data source for the row
		 *      * `{string}` The type call data requested - this will be 'set' when
		 *        setting data or 'filter', 'display', 'type', 'sort' or undefined
		 *        when gathering data. Note that when `undefined` is given for the
		 *        type DataTables expects to get the raw data for the object back<
		 *      * `{*}` Data to set when the second parameter is 'set'.
		 *    * Return:
		 *      * The return value from the function is not required when 'set' is
		 *        the type of call, but otherwise the return is what will be used
		 *        for the data requested.
		 *
		 * Note that `data` is a getter and setter option. If you just require
		 * formatting of data for output, you will likely want to use `render` which
		 * is simply a getter and thus simpler to use.
		 *
		 * Note that prior to DataTables 1.9.2 `data` was called `mDataProp`. The
		 * name change reflects the flexibility of this property and is consistent
		 * with the naming of mRender. If 'mDataProp' is given, then it will still
		 * be used by DataTables, as it automatically maps the old name to the new
		 * if required.
		 *
		 *  @type string|int|function|null
		 *  @default null <i>Use automatically calculated column index</i>
		 *
		 *  @name DataTable.defaults.column.data
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Read table data from objects
		 *    // JSON structure for each row:
		 *    //   {
		 *    //      "engine": {value},
		 *    //      "browser": {value},
		 *    //      "platform": {value},
		 *    //      "version": {value},
		 *    //      "grade": {value}
		 *    //   }
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/objects.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          { "data": "platform" },
		 *          { "data": "version" },
		 *          { "data": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Read information from deeply nested objects
		 *    // JSON structure for each row:
		 *    //   {
		 *    //      "engine": {value},
		 *    //      "browser": {value},
		 *    //      "platform": {
		 *    //         "inner": {value}
		 *    //      },
		 *    //      "details": [
		 *    //         {value}, {value}
		 *    //      ]
		 *    //   }
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/deep.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          { "data": "platform.inner" },
		 *          { "data": "details.0" },
		 *          { "data": "details.1" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `data` as a function to provide different information for
		 *    // sorting, filtering and display. In this case, currency (price)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": function ( source, type, val ) {
		 *            if (type === 'set') {
		 *              source.price = val;
		 *              // Store the computed display and filter values for efficiency
		 *              source.price_display = val=="" ? "" : "$"+numberFormat(val);
		 *              source.price_filter  = val=="" ? "" : "$"+numberFormat(val)+" "+val;
		 *              return;
		 *            }
		 *            else if (type === 'display') {
		 *              return source.price_display;
		 *            }
		 *            else if (type === 'filter') {
		 *              return source.price_filter;
		 *            }
		 *            // 'sort', 'type' and undefined all just use the integer
		 *            return source.price;
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using default content
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null,
		 *          "defaultContent": "Click to edit"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using array notation - outputting a list from an array
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": "name[, ]"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 */
		"mData": null,
	
	
		/**
		 * This property is the rendering partner to `data` and it is suggested that
		 * when you want to manipulate data for display (including filtering,
		 * sorting etc) without altering the underlying data for the table, use this
		 * property. `render` can be considered to be the the read only companion to
		 * `data` which is read / write (then as such more complex). Like `data`
		 * this option can be given in a number of different ways to effect its
		 * behaviour:
		 *
		 * * `integer` - treated as an array index for the data source. This is the
		 *   default that DataTables uses (incrementally increased for each column).
		 * * `string` - read an object property from the data source. There are
		 *   three 'special' options that can be used in the string to alter how
		 *   DataTables reads the data from the source object:
		 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
		 *      Javascript to read from nested objects, so to can the options
		 *      specified in `data`. For example: `browser.version` or
		 *      `browser.name`. If your object parameter name contains a period, use
		 *      `\\` to escape it - i.e. `first\\.name`.
		 *    * `[]` - Array notation. DataTables can automatically combine data
		 *      from and array source, joining the data with the characters provided
		 *      between the two brackets. For example: `name[, ]` would provide a
		 *      comma-space separated list from the source array. If no characters
		 *      are provided between the brackets, the original array source is
		 *      returned.
		 *    * `()` - Function notation. Adding `()` to the end of a parameter will
		 *      execute a function of the name given. For example: `browser()` for a
		 *      simple function on the data source, `browser.version()` for a
		 *      function in a nested property or even `browser().version` to get an
		 *      object property if the function called returns an object.
		 * * `object` - use different data for the different data types requested by
		 *   DataTables ('filter', 'display', 'type' or 'sort'). The property names
		 *   of the object is the data type the property refers to and the value can
		 *   defined using an integer, string or function using the same rules as
		 *   `render` normally does. Note that an `_` option _must_ be specified.
		 *   This is the default value to use if you haven't specified a value for
		 *   the data type requested by DataTables.
		 * * `function` - the function given will be executed whenever DataTables
		 *   needs to set or get the data for a cell in the column. The function
		 *   takes three parameters:
		 *    * Parameters:
		 *      * {array|object} The data source for the row (based on `data`)
		 *      * {string} The type call data requested - this will be 'filter',
		 *        'display', 'type' or 'sort'.
		 *      * {array|object} The full data source for the row (not based on
		 *        `data`)
		 *    * Return:
		 *      * The return value from the function is what will be used for the
		 *        data requested.
		 *
		 *  @type string|int|function|object|null
		 *  @default null Use the data source value.
		 *
		 *  @name DataTable.defaults.column.render
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Create a comma separated list from an array of objects
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/deep.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          {
		 *            "data": "platform",
		 *            "render": "[, ].name"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Execute a function to obtain data
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null, // Use the full data source object for the renderer's source
		 *          "render": "browserName()"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // As an object, extracting different data for the different types
		 *    // This would be used with a data source such as:
		 *    //   { "phone": 5552368, "phone_filter": "5552368 555-2368", "phone_display": "555-2368" }
		 *    // Here the `phone` integer is used for sorting and type detection, while `phone_filter`
		 *    // (which has both forms) is used for filtering for if a user inputs either format, while
		 *    // the formatted phone number is the one that is shown in the table.
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null, // Use the full data source object for the renderer's source
		 *          "render": {
		 *            "_": "phone",
		 *            "filter": "phone_filter",
		 *            "display": "phone_display"
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Use as a function to create a link from the data source
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": "download_link",
		 *          "render": function ( data, type, full ) {
		 *            return '<a href="'+data+'">Download</a>';
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 */
		"mRender": null,
	
	
		/**
		 * Change the cell type created for the column - either TD cells or TH cells. This
		 * can be useful as TH cells have semantic meaning in the table body, allowing them
		 * to act as a header for a row (you may wish to add scope='row' to the TH elements).
		 *  @type string
		 *  @default td
		 *
		 *  @name DataTable.defaults.column.cellType
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Make the first column use TH cells
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "cellType": "th"
		 *        } ]
		 *      } );
		 *    } );
		 */
		"sCellType": "td",
	
	
		/**
		 * Class to give to each cell in this column.
		 *  @type string
		 *  @default <i>Empty string</i>
		 *
		 *  @name DataTable.defaults.column.class
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "class": "my_class", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "class": "my_class" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sClass": "",
	
		/**
		 * When DataTables calculates the column widths to assign to each column,
		 * it finds the longest string in each column and then constructs a
		 * temporary table and reads the widths from that. The problem with this
		 * is that "mmm" is much wider then "iiii", but the latter is a longer
		 * string - thus the calculation can go wrong (doing it properly and putting
		 * it into an DOM object and measuring that is horribly(!) slow). Thus as
		 * a "work around" we provide this option. It will append its value to the
		 * text that is found to be the longest string for the column - i.e. padding.
		 * Generally you shouldn't need this!
		 *  @type string
		 *  @default <i>Empty string<i>
		 *
		 *  @name DataTable.defaults.column.contentPadding
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          null,
		 *          {
		 *            "contentPadding": "mmm"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sContentPadding": "",
	
	
		/**
		 * Allows a default value to be given for a column's data, and will be used
		 * whenever a null data source is encountered (this can be because `data`
		 * is set to null, or because the data source itself is null).
		 *  @type string
		 *  @default null
		 *
		 *  @name DataTable.defaults.column.defaultContent
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          {
		 *            "data": null,
		 *            "defaultContent": "Edit",
		 *            "targets": [ -1 ]
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          null,
		 *          {
		 *            "data": null,
		 *            "defaultContent": "Edit"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sDefaultContent": null,
	
	
		/**
		 * This parameter is only used in DataTables' server-side processing. It can
		 * be exceptionally useful to know what columns are being displayed on the
		 * client side, and to map these to database fields. When defined, the names
		 * also allow DataTables to reorder information from the server if it comes
		 * back in an unexpected order (i.e. if you switch your columns around on the
		 * client-side, your server-side code does not also need updating).
		 *  @type string
		 *  @default <i>Empty string</i>
		 *
		 *  @name DataTable.defaults.column.name
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "name": "engine", "targets": [ 0 ] },
		 *          { "name": "browser", "targets": [ 1 ] },
		 *          { "name": "platform", "targets": [ 2 ] },
		 *          { "name": "version", "targets": [ 3 ] },
		 *          { "name": "grade", "targets": [ 4 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "name": "engine" },
		 *          { "name": "browser" },
		 *          { "name": "platform" },
		 *          { "name": "version" },
		 *          { "name": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sName": "",
	
	
		/**
		 * Defines a data source type for the ordering which can be used to read
		 * real-time information from the table (updating the internally cached
		 * version) prior to ordering. This allows ordering to occur on user
		 * editable elements such as form inputs.
		 *  @type string
		 *  @default std
		 *
		 *  @name DataTable.defaults.column.orderDataType
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderDataType": "dom-text", "targets": [ 2, 3 ] },
		 *          { "type": "numeric", "targets": [ 3 ] },
		 *          { "orderDataType": "dom-select", "targets": [ 4 ] },
		 *          { "orderDataType": "dom-checkbox", "targets": [ 5 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          { "orderDataType": "dom-text" },
		 *          { "orderDataType": "dom-text", "type": "numeric" },
		 *          { "orderDataType": "dom-select" },
		 *          { "orderDataType": "dom-checkbox" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sSortDataType": "std",
	
	
		/**
		 * The title of this column.
		 *  @type string
		 *  @default null <i>Derived from the 'TH' value for this column in the
		 *    original HTML table.</i>
		 *
		 *  @name DataTable.defaults.column.title
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "title": "My column title", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "title": "My column title" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sTitle": null,
	
	
		/**
		 * The type allows you to specify how the data for this column will be
		 * ordered. Four types (string, numeric, date and html (which will strip
		 * HTML tags before ordering)) are currently available. Note that only date
		 * formats understood by Javascript's Date() object will be accepted as type
		 * date. For example: "Mar 26, 2008 5:03 PM". May take the values: 'string',
		 * 'numeric', 'date' or 'html' (by default). Further types can be adding
		 * through plug-ins.
		 *  @type string
		 *  @default null <i>Auto-detected from raw data</i>
		 *
		 *  @name DataTable.defaults.column.type
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "type": "html", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "type": "html" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sType": null,
	
	
		/**
		 * Defining the width of the column, this parameter may take any CSS value
		 * (3em, 20px etc). DataTables applies 'smart' widths to columns which have not
		 * been given a specific width through this interface ensuring that the table
		 * remains readable.
		 *  @type string
		 *  @default null <i>Automatic</i>
		 *
		 *  @name DataTable.defaults.column.width
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "width": "20%", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "width": "20%" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sWidth": null
	};
	
	_fnHungarianMap( DataTable.defaults.column );
	
	
	
	/**
	 * DataTables settings object - this holds all the information needed for a
	 * given table, including configuration, data and current application of the
	 * table options. DataTables does not have a single instance for each DataTable
	 * with the settings attached to that instance, but rather instances of the
	 * DataTable "class" are created on-the-fly as needed (typically by a
	 * $().dataTable() call) and the settings object is then applied to that
	 * instance.
	 *
	 * Note that this object is related to {@link DataTable.defaults} but this
	 * one is the internal data store for DataTables's cache of columns. It should
	 * NOT be manipulated outside of DataTables. Any configuration should be done
	 * through the initialisation options.
	 *  @namespace
	 *  @todo Really should attach the settings object to individual instances so we
	 *    don't need to create new instances on each $().dataTable() call (if the
	 *    table already exists). It would also save passing oSettings around and
	 *    into every single function. However, this is a very significant
	 *    architecture change for DataTables and will almost certainly break
	 *    backwards compatibility with older installations. This is something that
	 *    will be done in 2.0.
	 */
	DataTable.models.oSettings = {
		/**
		 * Primary features of DataTables and their enablement state.
		 *  @namespace
		 */
		"oFeatures": {
	
			/**
			 * Flag to say if DataTables should automatically try to calculate the
			 * optimum table and columns widths (true) or not (false).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bAutoWidth": null,
	
			/**
			 * Delay the creation of TR and TD elements until they are actually
			 * needed by a driven page draw. This can give a significant speed
			 * increase for Ajax source and Javascript source data, but makes no
			 * difference at all for DOM and server-side processing tables.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bDeferRender": null,
	
			/**
			 * Enable filtering on the table or not. Note that if this is disabled
			 * then there is no filtering at all on the table, including fnFilter.
			 * To just remove the filtering input use sDom and remove the 'f' option.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bFilter": null,
	
			/**
			 * Table information element (the 'Showing x of y records' div) enable
			 * flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bInfo": null,
	
			/**
			 * Present a user control allowing the end user to change the page size
			 * when pagination is enabled.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bLengthChange": null,
	
			/**
			 * Pagination enabled or not. Note that if this is disabled then length
			 * changing must also be disabled.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bPaginate": null,
	
			/**
			 * Processing indicator enable flag whenever DataTables is enacting a
			 * user request - typically an Ajax request for server-side processing.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bProcessing": null,
	
			/**
			 * Server-side processing enabled flag - when enabled DataTables will
			 * get all data from the server for every draw - there is no filtering,
			 * sorting or paging done on the client-side.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bServerSide": null,
	
			/**
			 * Sorting enablement flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSort": null,
	
			/**
			 * Multi-column sorting
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSortMulti": null,
	
			/**
			 * Apply a class to the columns which are being sorted to provide a
			 * visual highlight or not. This can slow things down when enabled since
			 * there is a lot of DOM interaction.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSortClasses": null,
	
			/**
			 * State saving enablement flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bStateSave": null
		},
	
	
		/**
		 * Scrolling settings for a table.
		 *  @namespace
		 */
		"oScroll": {
			/**
			 * When the table is shorter in height than sScrollY, collapse the
			 * table container down to the height of the table (when true).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bCollapse": null,
	
			/**
			 * Width of the scrollbar for the web-browser's platform. Calculated
			 * during table initialisation.
			 *  @type int
			 *  @default 0
			 */
			"iBarWidth": 0,
	
			/**
			 * Viewport width for horizontal scrolling. Horizontal scrolling is
			 * disabled if an empty string.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sX": null,
	
			/**
			 * Width to expand the table to when using x-scrolling. Typically you
			 * should not need to use this.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 *  @deprecated
			 */
			"sXInner": null,
	
			/**
			 * Viewport height for vertical scrolling. Vertical scrolling is disabled
			 * if an empty string.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sY": null
		},
	
		/**
		 * Language information for the table.
		 *  @namespace
		 *  @extends DataTable.defaults.oLanguage
		 */
		"oLanguage": {
			/**
			 * Information callback function. See
			 * {@link DataTable.defaults.fnInfoCallback}
			 *  @type function
			 *  @default null
			 */
			"fnInfoCallback": null
		},
	
		/**
		 * Browser support parameters
		 *  @namespace
		 */
		"oBrowser": {
			/**
			 * Indicate if the browser incorrectly calculates width:100% inside a
			 * scrolling element (IE6/7)
			 *  @type boolean
			 *  @default false
			 */
			"bScrollOversize": false,
	
			/**
			 * Determine if the vertical scrollbar is on the right or left of the
			 * scrolling container - needed for rtl language layout, although not
			 * all browsers move the scrollbar (Safari).
			 *  @type boolean
			 *  @default false
			 */
			"bScrollbarLeft": false,
	
			/**
			 * Flag for if `getBoundingClientRect` is fully supported or not
			 *  @type boolean
			 *  @default false
			 */
			"bBounding": false,
	
			/**
			 * Browser scrollbar width
			 *  @type integer
			 *  @default 0
			 */
			"barWidth": 0
		},
	
	
		"ajax": null,
	
	
		/**
		 * Array referencing the nodes which are used for the features. The
		 * parameters of this object match what is allowed by sDom - i.e.
		 *   <ul>
		 *     <li>'l' - Length changing</li>
		 *     <li>'f' - Filtering input</li>
		 *     <li>'t' - The table!</li>
		 *     <li>'i' - Information</li>
		 *     <li>'p' - Pagination</li>
		 *     <li>'r' - pRocessing</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aanFeatures": [],
	
		/**
		 * Store data information - see {@link DataTable.models.oRow} for detailed
		 * information.
		 *  @type array
		 *  @default []
		 */
		"aoData": [],
	
		/**
		 * Array of indexes which are in the current display (after filtering etc)
		 *  @type array
		 *  @default []
		 */
		"aiDisplay": [],
	
		/**
		 * Array of indexes for display - no filtering
		 *  @type array
		 *  @default []
		 */
		"aiDisplayMaster": [],
	
		/**
		 * Map of row ids to data indexes
		 *  @type object
		 *  @default {}
		 */
		"aIds": {},
	
		/**
		 * Store information about each column that is in use
		 *  @type array
		 *  @default []
		 */
		"aoColumns": [],
	
		/**
		 * Store information about the table's header
		 *  @type array
		 *  @default []
		 */
		"aoHeader": [],
	
		/**
		 * Store information about the table's footer
		 *  @type array
		 *  @default []
		 */
		"aoFooter": [],
	
		/**
		 * Store the applied global search information in case we want to force a
		 * research or compare the old search to a new one.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @namespace
		 *  @extends DataTable.models.oSearch
		 */
		"oPreviousSearch": {},
	
		/**
		 * Store the applied search for each column - see
		 * {@link DataTable.models.oSearch} for the format that is used for the
		 * filtering information for each column.
		 *  @type array
		 *  @default []
		 */
		"aoPreSearchCols": [],
	
		/**
		 * Sorting that is applied to the table. Note that the inner arrays are
		 * used in the following manner:
		 * <ul>
		 *   <li>Index 0 - column number</li>
		 *   <li>Index 1 - current sorting direction</li>
		 * </ul>
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @todo These inner arrays should really be objects
		 */
		"aaSorting": null,
	
		/**
		 * Sorting that is always applied to the table (i.e. prefixed in front of
		 * aaSorting).
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"aaSortingFixed": [],
	
		/**
		 * Classes to use for the striping of a table.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"asStripeClasses": null,
	
		/**
		 * If restoring a table - we should restore its striping classes as well
		 *  @type array
		 *  @default []
		 */
		"asDestroyStripes": [],
	
		/**
		 * If restoring a table - we should restore its width
		 *  @type int
		 *  @default 0
		 */
		"sDestroyWidth": 0,
	
		/**
		 * Callback functions array for every time a row is inserted (i.e. on a draw).
		 *  @type array
		 *  @default []
		 */
		"aoRowCallback": [],
	
		/**
		 * Callback functions for the header on each draw.
		 *  @type array
		 *  @default []
		 */
		"aoHeaderCallback": [],
	
		/**
		 * Callback function for the footer on each draw.
		 *  @type array
		 *  @default []
		 */
		"aoFooterCallback": [],
	
		/**
		 * Array of callback functions for draw callback functions
		 *  @type array
		 *  @default []
		 */
		"aoDrawCallback": [],
	
		/**
		 * Array of callback functions for row created function
		 *  @type array
		 *  @default []
		 */
		"aoRowCreatedCallback": [],
	
		/**
		 * Callback functions for just before the table is redrawn. A return of
		 * false will be used to cancel the draw.
		 *  @type array
		 *  @default []
		 */
		"aoPreDrawCallback": [],
	
		/**
		 * Callback functions for when the table has been initialised.
		 *  @type array
		 *  @default []
		 */
		"aoInitComplete": [],
	
	
		/**
		 * Callbacks for modifying the settings to be stored for state saving, prior to
		 * saving state.
		 *  @type array
		 *  @default []
		 */
		"aoStateSaveParams": [],
	
		/**
		 * Callbacks for modifying the settings that have been stored for state saving
		 * prior to using the stored values to restore the state.
		 *  @type array
		 *  @default []
		 */
		"aoStateLoadParams": [],
	
		/**
		 * Callbacks for operating on the settings object once the saved state has been
		 * loaded
		 *  @type array
		 *  @default []
		 */
		"aoStateLoaded": [],
	
		/**
		 * Cache the table ID for quick access
		 *  @type string
		 *  @default <i>Empty string</i>
		 */
		"sTableId": "",
	
		/**
		 * The TABLE node for the main table
		 *  @type node
		 *  @default null
		 */
		"nTable": null,
	
		/**
		 * Permanent ref to the thead element
		 *  @type node
		 *  @default null
		 */
		"nTHead": null,
	
		/**
		 * Permanent ref to the tfoot element - if it exists
		 *  @type node
		 *  @default null
		 */
		"nTFoot": null,
	
		/**
		 * Permanent ref to the tbody element
		 *  @type node
		 *  @default null
		 */
		"nTBody": null,
	
		/**
		 * Cache the wrapper node (contains all DataTables controlled elements)
		 *  @type node
		 *  @default null
		 */
		"nTableWrapper": null,
	
		/**
		 * Indicate if when using server-side processing the loading of data
		 * should be deferred until the second draw.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type boolean
		 *  @default false
		 */
		"bDeferLoading": false,
	
		/**
		 * Indicate if all required information has been read in
		 *  @type boolean
		 *  @default false
		 */
		"bInitialised": false,
	
		/**
		 * Information about open rows. Each object in the array has the parameters
		 * 'nTr' and 'nParent'
		 *  @type array
		 *  @default []
		 */
		"aoOpenRows": [],
	
		/**
		 * Dictate the positioning of DataTables' control elements - see
		 * {@link DataTable.model.oInit.sDom}.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default null
		 */
		"sDom": null,
	
		/**
		 * Search delay (in mS)
		 *  @type integer
		 *  @default null
		 */
		"searchDelay": null,
	
		/**
		 * Which type of pagination should be used.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default two_button
		 */
		"sPaginationType": "two_button",
	
		/**
		 * The state duration (for `stateSave`) in seconds.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type int
		 *  @default 0
		 */
		"iStateDuration": 0,
	
		/**
		 * Array of callback functions for state saving. Each array element is an
		 * object with the following parameters:
		 *   <ul>
		 *     <li>function:fn - function to call. Takes two parameters, oSettings
		 *       and the JSON string to save that has been thus far created. Returns
		 *       a JSON string to be inserted into a json object
		 *       (i.e. '"param": [ 0, 1, 2]')</li>
		 *     <li>string:sName - name of callback</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aoStateSave": [],
	
		/**
		 * Array of callback functions for state loading. Each array element is an
		 * object with the following parameters:
		 *   <ul>
		 *     <li>function:fn - function to call. Takes two parameters, oSettings
		 *       and the object stored. May return false to cancel state loading</li>
		 *     <li>string:sName - name of callback</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aoStateLoad": [],
	
		/**
		 * State that was saved. Useful for back reference
		 *  @type object
		 *  @default null
		 */
		"oSavedState": null,
	
		/**
		 * State that was loaded. Useful for back reference
		 *  @type object
		 *  @default null
		 */
		"oLoadedState": null,
	
		/**
		 * Source url for AJAX data for the table.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default null
		 */
		"sAjaxSource": null,
	
		/**
		 * Property from a given object from which to read the table data from. This
		 * can be an empty string (when not server-side processing), in which case
		 * it is  assumed an an array is given directly.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 */
		"sAjaxDataProp": null,
	
		/**
		 * The last jQuery XHR object that was used for server-side data gathering.
		 * This can be used for working with the XHR information in one of the
		 * callbacks
		 *  @type object
		 *  @default null
		 */
		"jqXHR": null,
	
		/**
		 * JSON returned from the server in the last Ajax request
		 *  @type object
		 *  @default undefined
		 */
		"json": undefined,
	
		/**
		 * Data submitted as part of the last Ajax request
		 *  @type object
		 *  @default undefined
		 */
		"oAjaxData": undefined,
	
		/**
		 * Function to get the server-side data.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type function
		 */
		"fnServerData": null,
	
		/**
		 * Functions which are called prior to sending an Ajax request so extra
		 * parameters can easily be sent to the server
		 *  @type array
		 *  @default []
		 */
		"aoServerParams": [],
	
		/**
		 * Send the XHR HTTP method - GET or POST (could be PUT or DELETE if
		 * required).
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 */
		"sServerMethod": null,
	
		/**
		 * Format numbers for display.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type function
		 */
		"fnFormatNumber": null,
	
		/**
		 * List of options that can be used for the user selectable length menu.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"aLengthMenu": null,
	
		/**
		 * Counter for the draws that the table does. Also used as a tracker for
		 * server-side processing
		 *  @type int
		 *  @default 0
		 */
		"iDraw": 0,
	
		/**
		 * Indicate if a redraw is being done - useful for Ajax
		 *  @type boolean
		 *  @default false
		 */
		"bDrawing": false,
	
		/**
		 * Draw index (iDraw) of the last error when parsing the returned data
		 *  @type int
		 *  @default -1
		 */
		"iDrawError": -1,
	
		/**
		 * Paging display length
		 *  @type int
		 *  @default 10
		 */
		"_iDisplayLength": 10,
	
		/**
		 * Paging start point - aiDisplay index
		 *  @type int
		 *  @default 0
		 */
		"_iDisplayStart": 0,
	
		/**
		 * Server-side processing - number of records in the result set
		 * (i.e. before filtering), Use fnRecordsTotal rather than
		 * this property to get the value of the number of records, regardless of
		 * the server-side processing setting.
		 *  @type int
		 *  @default 0
		 *  @private
		 */
		"_iRecordsTotal": 0,
	
		/**
		 * Server-side processing - number of records in the current display set
		 * (i.e. after filtering). Use fnRecordsDisplay rather than
		 * this property to get the value of the number of records, regardless of
		 * the server-side processing setting.
		 *  @type boolean
		 *  @default 0
		 *  @private
		 */
		"_iRecordsDisplay": 0,
	
		/**
		 * The classes to use for the table
		 *  @type object
		 *  @default {}
		 */
		"oClasses": {},
	
		/**
		 * Flag attached to the settings object so you can check in the draw
		 * callback if filtering has been done in the draw. Deprecated in favour of
		 * events.
		 *  @type boolean
		 *  @default false
		 *  @deprecated
		 */
		"bFiltered": false,
	
		/**
		 * Flag attached to the settings object so you can check in the draw
		 * callback if sorting has been done in the draw. Deprecated in favour of
		 * events.
		 *  @type boolean
		 *  @default false
		 *  @deprecated
		 */
		"bSorted": false,
	
		/**
		 * Indicate that if multiple rows are in the header and there is more than
		 * one unique cell per column, if the top one (true) or bottom one (false)
		 * should be used for sorting / title by DataTables.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type boolean
		 */
		"bSortCellsTop": null,
	
		/**
		 * Initialisation object that is used for the table
		 *  @type object
		 *  @default null
		 */
		"oInit": null,
	
		/**
		 * Destroy callback functions - for plug-ins to attach themselves to the
		 * destroy so they can clean up markup and events.
		 *  @type array
		 *  @default []
		 */
		"aoDestroyCallback": [],
	
	
		/**
		 * Get the number of records in the current record set, before filtering
		 *  @type function
		 */
		"fnRecordsTotal": function ()
		{
			return _fnDataSource( this ) == 'ssp' ?
				this._iRecordsTotal * 1 :
				this.aiDisplayMaster.length;
		},
	
		/**
		 * Get the number of records in the current record set, after filtering
		 *  @type function
		 */
		"fnRecordsDisplay": function ()
		{
			return _fnDataSource( this ) == 'ssp' ?
				this._iRecordsDisplay * 1 :
				this.aiDisplay.length;
		},
	
		/**
		 * Get the display end point - aiDisplay index
		 *  @type function
		 */
		"fnDisplayEnd": function ()
		{
			var
				len      = this._iDisplayLength,
				start    = this._iDisplayStart,
				calc     = start + len,
				records  = this.aiDisplay.length,
				features = this.oFeatures,
				paginate = features.bPaginate;
	
			if ( features.bServerSide ) {
				return paginate === false || len === -1 ?
					start + records :
					Math.min( start+len, this._iRecordsDisplay );
			}
			else {
				return ! paginate || calc>records || len===-1 ?
					records :
					calc;
			}
		},
	
		/**
		 * The DataTables object for this table
		 *  @type object
		 *  @default null
		 */
		"oInstance": null,
	
		/**
		 * Unique identifier for each instance of the DataTables object. If there
		 * is an ID on the table node, then it takes that value, otherwise an
		 * incrementing internal counter is used.
		 *  @type string
		 *  @default null
		 */
		"sInstance": null,
	
		/**
		 * tabindex attribute value that is added to DataTables control elements, allowing
		 * keyboard navigation of the table and its controls.
		 */
		"iTabIndex": 0,
	
		/**
		 * DIV container for the footer scrolling table if scrolling
		 */
		"nScrollHead": null,
	
		/**
		 * DIV container for the footer scrolling table if scrolling
		 */
		"nScrollFoot": null,
	
		/**
		 * Last applied sort
		 *  @type array
		 *  @default []
		 */
		"aLastSort": [],
	
		/**
		 * Stored plug-in instances
		 *  @type object
		 *  @default {}
		 */
		"oPlugins": {},
	
		/**
		 * Function used to get a row's id from the row's data
		 *  @type function
		 *  @default null
		 */
		"rowIdFn": null,
	
		/**
		 * Data location where to store a row's id
		 *  @type string
		 *  @default null
		 */
		"rowId": null
	};
	
	/**
	 * Extension object for DataTables that is used to provide all extension
	 * options.
	 *
	 * Note that the `DataTable.ext` object is available through
	 * `jQuery.fn.dataTable.ext` where it may be accessed and manipulated. It is
	 * also aliased to `jQuery.fn.dataTableExt` for historic reasons.
	 *  @namespace
	 *  @extends DataTable.models.ext
	 */
	
	
	/**
	 * DataTables extensions
	 * 
	 * This namespace acts as a collection area for plug-ins that can be used to
	 * extend DataTables capabilities. Indeed many of the build in methods
	 * use this method to provide their own capabilities (sorting methods for
	 * example).
	 *
	 * Note that this namespace is aliased to `jQuery.fn.dataTableExt` for legacy
	 * reasons
	 *
	 *  @namespace
	 */
	DataTable.ext = _ext = {
		/**
		 * Buttons. For use with the Buttons extension for DataTables. This is
		 * defined here so other extensions can define buttons regardless of load
		 * order. It is _not_ used by DataTables core.
		 *
		 *  @type object
		 *  @default {}
		 */
		buttons: {},
	
	
		/**
		 * Element class names
		 *
		 *  @type object
		 *  @default {}
		 */
		classes: {},
	
	
		/**
		 * DataTables build type (expanded by the download builder)
		 *
		 *  @type string
		 */
		build:"bs4/dt-1.12.1/e-2.0.8/r-2.3.0",
	
	
		/**
		 * Error reporting.
		 * 
		 * How should DataTables report an error. Can take the value 'alert',
		 * 'throw', 'none' or a function.
		 *
		 *  @type string|function
		 *  @default alert
		 */
		errMode: "alert",
	
	
		/**
		 * Feature plug-ins.
		 * 
		 * This is an array of objects which describe the feature plug-ins that are
		 * available to DataTables. These feature plug-ins are then available for
		 * use through the `dom` initialisation option.
		 * 
		 * Each feature plug-in is described by an object which must have the
		 * following properties:
		 * 
		 * * `fnInit` - function that is used to initialise the plug-in,
		 * * `cFeature` - a character so the feature can be enabled by the `dom`
		 *   instillation option. This is case sensitive.
		 *
		 * The `fnInit` function has the following input parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 *
		 * And the following return is expected:
		 * 
		 * * {node|null} The element which contains your feature. Note that the
		 *   return may also be void if your plug-in does not require to inject any
		 *   DOM elements into DataTables control (`dom`) - for example this might
		 *   be useful when developing a plug-in which allows table control via
		 *   keyboard entry
		 *
		 *  @type array
		 *
		 *  @example
		 *    $.fn.dataTable.ext.features.push( {
		 *      "fnInit": function( oSettings ) {
		 *        return new TableTools( { "oDTSettings": oSettings } );
		 *      },
		 *      "cFeature": "T"
		 *    } );
		 */
		feature: [],
	
	
		/**
		 * Row searching.
		 * 
		 * This method of searching is complimentary to the default type based
		 * searching, and a lot more comprehensive as it allows you complete control
		 * over the searching logic. Each element in this array is a function
		 * (parameters described below) that is called for every row in the table,
		 * and your logic decides if it should be included in the searching data set
		 * or not.
		 *
		 * Searching functions have the following input parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 * 2. `{array|object}` Data for the row to be processed (same as the
		 *    original format that was passed in as the data source, or an array
		 *    from a DOM data source
		 * 3. `{int}` Row index ({@link DataTable.models.oSettings.aoData}), which
		 *    can be useful to retrieve the `TR` element if you need DOM interaction.
		 *
		 * And the following return is expected:
		 *
		 * * {boolean} Include the row in the searched result set (true) or not
		 *   (false)
		 *
		 * Note that as with the main search ability in DataTables, technically this
		 * is "filtering", since it is subtractive. However, for consistency in
		 * naming we call it searching here.
		 *
		 *  @type array
		 *  @default []
		 *
		 *  @example
		 *    // The following example shows custom search being applied to the
		 *    // fourth column (i.e. the data[3] index) based on two input values
		 *    // from the end-user, matching the data in a certain range.
		 *    $.fn.dataTable.ext.search.push(
		 *      function( settings, data, dataIndex ) {
		 *        var min = document.getElementById('min').value * 1;
		 *        var max = document.getElementById('max').value * 1;
		 *        var version = data[3] == "-" ? 0 : data[3]*1;
		 *
		 *        if ( min == "" && max == "" ) {
		 *          return true;
		 *        }
		 *        else if ( min == "" && version < max ) {
		 *          return true;
		 *        }
		 *        else if ( min < version && "" == max ) {
		 *          return true;
		 *        }
		 *        else if ( min < version && version < max ) {
		 *          return true;
		 *        }
		 *        return false;
		 *      }
		 *    );
		 */
		search: [],
	
	
		/**
		 * Selector extensions
		 *
		 * The `selector` option can be used to extend the options available for the
		 * selector modifier options (`selector-modifier` object data type) that
		 * each of the three built in selector types offer (row, column and cell +
		 * their plural counterparts). For example the Select extension uses this
		 * mechanism to provide an option to select only rows, columns and cells
		 * that have been marked as selected by the end user (`{selected: true}`),
		 * which can be used in conjunction with the existing built in selector
		 * options.
		 *
		 * Each property is an array to which functions can be pushed. The functions
		 * take three attributes:
		 *
		 * * Settings object for the host table
		 * * Options object (`selector-modifier` object type)
		 * * Array of selected item indexes
		 *
		 * The return is an array of the resulting item indexes after the custom
		 * selector has been applied.
		 *
		 *  @type object
		 */
		selector: {
			cell: [],
			column: [],
			row: []
		},
	
	
		/**
		 * Internal functions, exposed for used in plug-ins.
		 * 
		 * Please note that you should not need to use the internal methods for
		 * anything other than a plug-in (and even then, try to avoid if possible).
		 * The internal function may change between releases.
		 *
		 *  @type object
		 *  @default {}
		 */
		internal: {},
	
	
		/**
		 * Legacy configuration options. Enable and disable legacy options that
		 * are available in DataTables.
		 *
		 *  @type object
		 */
		legacy: {
			/**
			 * Enable / disable DataTables 1.9 compatible server-side processing
			 * requests
			 *
			 *  @type boolean
			 *  @default null
			 */
			ajax: null
		},
	
	
		/**
		 * Pagination plug-in methods.
		 * 
		 * Each entry in this object is a function and defines which buttons should
		 * be shown by the pagination rendering method that is used for the table:
		 * {@link DataTable.ext.renderer.pageButton}. The renderer addresses how the
		 * buttons are displayed in the document, while the functions here tell it
		 * what buttons to display. This is done by returning an array of button
		 * descriptions (what each button will do).
		 *
		 * Pagination types (the four built in options and any additional plug-in
		 * options defined here) can be used through the `paginationType`
		 * initialisation parameter.
		 *
		 * The functions defined take two parameters:
		 *
		 * 1. `{int} page` The current page index
		 * 2. `{int} pages` The number of pages in the table
		 *
		 * Each function is expected to return an array where each element of the
		 * array can be one of:
		 *
		 * * `first` - Jump to first page when activated
		 * * `last` - Jump to last page when activated
		 * * `previous` - Show previous page when activated
		 * * `next` - Show next page when activated
		 * * `{int}` - Show page of the index given
		 * * `{array}` - A nested array containing the above elements to add a
		 *   containing 'DIV' element (might be useful for styling).
		 *
		 * Note that DataTables v1.9- used this object slightly differently whereby
		 * an object with two functions would be defined for each plug-in. That
		 * ability is still supported by DataTables 1.10+ to provide backwards
		 * compatibility, but this option of use is now decremented and no longer
		 * documented in DataTables 1.10+.
		 *
		 *  @type object
		 *  @default {}
		 *
		 *  @example
		 *    // Show previous, next and current page buttons only
		 *    $.fn.dataTableExt.oPagination.current = function ( page, pages ) {
		 *      return [ 'previous', page, 'next' ];
		 *    };
		 */
		pager: {},
	
	
		renderer: {
			pageButton: {},
			header: {}
		},
	
	
		/**
		 * Ordering plug-ins - custom data source
		 * 
		 * The extension options for ordering of data available here is complimentary
		 * to the default type based ordering that DataTables typically uses. It
		 * allows much greater control over the the data that is being used to
		 * order a column, but is necessarily therefore more complex.
		 * 
		 * This type of ordering is useful if you want to do ordering based on data
		 * live from the DOM (for example the contents of an 'input' element) rather
		 * than just the static string that DataTables knows of.
		 * 
		 * The way these plug-ins work is that you create an array of the values you
		 * wish to be ordering for the column in question and then return that
		 * array. The data in the array much be in the index order of the rows in
		 * the table (not the currently ordering order!). Which order data gathering
		 * function is run here depends on the `dt-init columns.orderDataType`
		 * parameter that is used for the column (if any).
		 *
		 * The functions defined take two parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 * 2. `{int}` Target column index
		 *
		 * Each function is expected to return an array:
		 *
		 * * `{array}` Data for the column to be ordering upon
		 *
		 *  @type array
		 *
		 *  @example
		 *    // Ordering using `input` node values
		 *    $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
		 *    {
		 *      return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
		 *        return $('input', td).val();
		 *      } );
		 *    }
		 */
		order: {},
	
	
		/**
		 * Type based plug-ins.
		 *
		 * Each column in DataTables has a type assigned to it, either by automatic
		 * detection or by direct assignment using the `type` option for the column.
		 * The type of a column will effect how it is ordering and search (plug-ins
		 * can also make use of the column type if required).
		 *
		 * @namespace
		 */
		type: {
			/**
			 * Type detection functions.
			 *
			 * The functions defined in this object are used to automatically detect
			 * a column's type, making initialisation of DataTables super easy, even
			 * when complex data is in the table.
			 *
			 * The functions defined take two parameters:
			 *
		     *  1. `{*}` Data from the column cell to be analysed
		     *  2. `{settings}` DataTables settings object. This can be used to
		     *     perform context specific type detection - for example detection
		     *     based on language settings such as using a comma for a decimal
		     *     place. Generally speaking the options from the settings will not
		     *     be required
			 *
			 * Each function is expected to return:
			 *
			 * * `{string|null}` Data type detected, or null if unknown (and thus
			 *   pass it on to the other type detection functions.
			 *
			 *  @type array
			 *
			 *  @example
			 *    // Currency type detection plug-in:
			 *    $.fn.dataTable.ext.type.detect.push(
			 *      function ( data, settings ) {
			 *        // Check the numeric part
			 *        if ( ! data.substring(1).match(/[0-9]/) ) {
			 *          return null;
			 *        }
			 *
			 *        // Check prefixed by currency
			 *        if ( data.charAt(0) == '$' || data.charAt(0) == '&pound;' ) {
			 *          return 'currency';
			 *        }
			 *        return null;
			 *      }
			 *    );
			 */
			detect: [],
	
	
			/**
			 * Type based search formatting.
			 *
			 * The type based searching functions can be used to pre-format the
			 * data to be search on. For example, it can be used to strip HTML
			 * tags or to de-format telephone numbers for numeric only searching.
			 *
			 * Note that is a search is not defined for a column of a given type,
			 * no search formatting will be performed.
			 * 
			 * Pre-processing of searching data plug-ins - When you assign the sType
			 * for a column (or have it automatically detected for you by DataTables
			 * or a type detection plug-in), you will typically be using this for
			 * custom sorting, but it can also be used to provide custom searching
			 * by allowing you to pre-processing the data and returning the data in
			 * the format that should be searched upon. This is done by adding
			 * functions this object with a parameter name which matches the sType
			 * for that target column. This is the corollary of <i>afnSortData</i>
			 * for searching data.
			 *
			 * The functions defined take a single parameter:
			 *
		     *  1. `{*}` Data from the column cell to be prepared for searching
			 *
			 * Each function is expected to return:
			 *
			 * * `{string|null}` Formatted string that will be used for the searching.
			 *
			 *  @type object
			 *  @default {}
			 *
			 *  @example
			 *    $.fn.dataTable.ext.type.search['title-numeric'] = function ( d ) {
			 *      return d.replace(/\n/g," ").replace( /<.*?>/g, "" );
			 *    }
			 */
			search: {},
	
	
			/**
			 * Type based ordering.
			 *
			 * The column type tells DataTables what ordering to apply to the table
			 * when a column is sorted upon. The order for each type that is defined,
			 * is defined by the functions available in this object.
			 *
			 * Each ordering option can be described by three properties added to
			 * this object:
			 *
			 * * `{type}-pre` - Pre-formatting function
			 * * `{type}-asc` - Ascending order function
			 * * `{type}-desc` - Descending order function
			 *
			 * All three can be used together, only `{type}-pre` or only
			 * `{type}-asc` and `{type}-desc` together. It is generally recommended
			 * that only `{type}-pre` is used, as this provides the optimal
			 * implementation in terms of speed, although the others are provided
			 * for compatibility with existing Javascript sort functions.
			 *
			 * `{type}-pre`: Functions defined take a single parameter:
			 *
		     *  1. `{*}` Data from the column cell to be prepared for ordering
			 *
			 * And return:
			 *
			 * * `{*}` Data to be sorted upon
			 *
			 * `{type}-asc` and `{type}-desc`: Functions are typical Javascript sort
			 * functions, taking two parameters:
			 *
		     *  1. `{*}` Data to compare to the second parameter
		     *  2. `{*}` Data to compare to the first parameter
			 *
			 * And returning:
			 *
			 * * `{*}` Ordering match: <0 if first parameter should be sorted lower
			 *   than the second parameter, ===0 if the two parameters are equal and
			 *   >0 if the first parameter should be sorted height than the second
			 *   parameter.
			 * 
			 *  @type object
			 *  @default {}
			 *
			 *  @example
			 *    // Numeric ordering of formatted numbers with a pre-formatter
			 *    $.extend( $.fn.dataTable.ext.type.order, {
			 *      "string-pre": function(x) {
			 *        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
			 *        return parseFloat( a );
			 *      }
			 *    } );
			 *
			 *  @example
			 *    // Case-sensitive string ordering, with no pre-formatting method
			 *    $.extend( $.fn.dataTable.ext.order, {
			 *      "string-case-asc": function(x,y) {
			 *        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			 *      },
			 *      "string-case-desc": function(x,y) {
			 *        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
			 *      }
			 *    } );
			 */
			order: {}
		},
	
		/**
		 * Unique DataTables instance counter
		 *
		 * @type int
		 * @private
		 */
		_unique: 0,
	
	
		//
		// Depreciated
		// The following properties are retained for backwards compatibility only.
		// The should not be used in new projects and will be removed in a future
		// version
		//
	
		/**
		 * Version check function.
		 *  @type function
		 *  @depreciated Since 1.10
		 */
		fnVersionCheck: DataTable.fnVersionCheck,
	
	
		/**
		 * Index for what 'this' index API functions should use
		 *  @type int
		 *  @deprecated Since v1.10
		 */
		iApiIndex: 0,
	
	
		/**
		 * jQuery UI class container
		 *  @type object
		 *  @deprecated Since v1.10
		 */
		oJUIClasses: {},
	
	
		/**
		 * Software version
		 *  @type string
		 *  @deprecated Since v1.10
		 */
		sVersion: DataTable.version
	};
	
	
	//
	// Backwards compatibility. Alias to pre 1.10 Hungarian notation counter parts
	//
	$.extend( _ext, {
		afnFiltering: _ext.search,
		aTypes:       _ext.type.detect,
		ofnSearch:    _ext.type.search,
		oSort:        _ext.type.order,
		afnSortData:  _ext.order,
		aoFeatures:   _ext.feature,
		oApi:         _ext.internal,
		oStdClasses:  _ext.classes,
		oPagination:  _ext.pager
	} );
	
	
	$.extend( DataTable.ext.classes, {
		"sTable": "dataTable",
		"sNoFooter": "no-footer",
	
		/* Paging buttons */
		"sPageButton": "paginate_button",
		"sPageButtonActive": "current",
		"sPageButtonDisabled": "disabled",
	
		/* Striping classes */
		"sStripeOdd": "odd",
		"sStripeEven": "even",
	
		/* Empty row */
		"sRowEmpty": "dataTables_empty",
	
		/* Features */
		"sWrapper": "dataTables_wrapper",
		"sFilter": "dataTables_filter",
		"sInfo": "dataTables_info",
		"sPaging": "dataTables_paginate paging_", /* Note that the type is postfixed */
		"sLength": "dataTables_length",
		"sProcessing": "dataTables_processing",
	
		/* Sorting */
		"sSortAsc": "sorting_asc",
		"sSortDesc": "sorting_desc",
		"sSortable": "sorting", /* Sortable in both directions */
		"sSortableAsc": "sorting_desc_disabled",
		"sSortableDesc": "sorting_asc_disabled",
		"sSortableNone": "sorting_disabled",
		"sSortColumn": "sorting_", /* Note that an int is postfixed for the sorting order */
	
		/* Filtering */
		"sFilterInput": "",
	
		/* Page length */
		"sLengthSelect": "",
	
		/* Scrolling */
		"sScrollWrapper": "dataTables_scroll",
		"sScrollHead": "dataTables_scrollHead",
		"sScrollHeadInner": "dataTables_scrollHeadInner",
		"sScrollBody": "dataTables_scrollBody",
		"sScrollFoot": "dataTables_scrollFoot",
		"sScrollFootInner": "dataTables_scrollFootInner",
	
		/* Misc */
		"sHeaderTH": "",
		"sFooterTH": "",
	
		// Deprecated
		"sSortJUIAsc": "",
		"sSortJUIDesc": "",
		"sSortJUI": "",
		"sSortJUIAscAllowed": "",
		"sSortJUIDescAllowed": "",
		"sSortJUIWrapper": "",
		"sSortIcon": "",
		"sJUIHeader": "",
		"sJUIFooter": ""
	} );
	
	
	var extPagination = DataTable.ext.pager;
	
	function _numbers ( page, pages ) {
		var
			numbers = [],
			buttons = extPagination.numbers_length,
			half = Math.floor( buttons / 2 ),
			i = 1;
	
		if ( pages <= buttons ) {
			numbers = _range( 0, pages );
		}
		else if ( page <= half ) {
			numbers = _range( 0, buttons-2 );
			numbers.push( 'ellipsis' );
			numbers.push( pages-1 );
		}
		else if ( page >= pages - 1 - half ) {
			numbers = _range( pages-(buttons-2), pages );
			numbers.splice( 0, 0, 'ellipsis' ); // no unshift in ie6
			numbers.splice( 0, 0, 0 );
		}
		else {
			numbers = _range( page-half+2, page+half-1 );
			numbers.push( 'ellipsis' );
			numbers.push( pages-1 );
			numbers.splice( 0, 0, 'ellipsis' );
			numbers.splice( 0, 0, 0 );
		}
	
		numbers.DT_el = 'span';
		return numbers;
	}
	
	
	$.extend( extPagination, {
		simple: function ( page, pages ) {
			return [ 'previous', 'next' ];
		},
	
		full: function ( page, pages ) {
			return [  'first', 'previous', 'next', 'last' ];
		},
	
		numbers: function ( page, pages ) {
			return [ _numbers(page, pages) ];
		},
	
		simple_numbers: function ( page, pages ) {
			return [ 'previous', _numbers(page, pages), 'next' ];
		},
	
		full_numbers: function ( page, pages ) {
			return [ 'first', 'previous', _numbers(page, pages), 'next', 'last' ];
		},
		
		first_last_numbers: function (page, pages) {
	 		return ['first', _numbers(page, pages), 'last'];
	 	},
	
		// For testing and plug-ins to use
		_numbers: _numbers,
	
		// Number of number buttons (including ellipsis) to show. _Must be odd!_
		numbers_length: 7
	} );
	
	
	$.extend( true, DataTable.ext.renderer, {
		pageButton: {
			_: function ( settings, host, idx, buttons, page, pages ) {
				var classes = settings.oClasses;
				var lang = settings.oLanguage.oPaginate;
				var aria = settings.oLanguage.oAria.paginate || {};
				var btnDisplay, btnClass, counter=0;
	
				var attach = function( container, buttons ) {
					var i, ien, node, button, tabIndex;
					var disabledClass = classes.sPageButtonDisabled;
					var clickHandler = function ( e ) {
						_fnPageChange( settings, e.data.action, true );
					};
	
					for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
						button = buttons[i];
	
						if ( Array.isArray( button ) ) {
							var inner = $( '<'+(button.DT_el || 'div')+'/>' )
								.appendTo( container );
							attach( inner, button );
						}
						else {
							btnDisplay = null;
							btnClass = button;
							tabIndex = settings.iTabIndex;
	
							switch ( button ) {
								case 'ellipsis':
									container.append('<span class="ellipsis">&#x2026;</span>');
									break;
	
								case 'first':
									btnDisplay = lang.sFirst;
	
									if ( page === 0 ) {
										tabIndex = -1;
										btnClass += ' ' + disabledClass;
									}
									break;
	
								case 'previous':
									btnDisplay = lang.sPrevious;
	
									if ( page === 0 ) {
										tabIndex = -1;
										btnClass += ' ' + disabledClass;
									}
									break;
	
								case 'next':
									btnDisplay = lang.sNext;
	
									if ( pages === 0 || page === pages-1 ) {
										tabIndex = -1;
										btnClass += ' ' + disabledClass;
									}
									break;
	
								case 'last':
									btnDisplay = lang.sLast;
	
									if ( pages === 0 || page === pages-1 ) {
										tabIndex = -1;
										btnClass += ' ' + disabledClass;
									}
									break;
	
								default:
									btnDisplay = settings.fnFormatNumber( button + 1 );
									btnClass = page === button ?
										classes.sPageButtonActive : '';
									break;
							}
	
							if ( btnDisplay !== null ) {
								node = $('<a>', {
										'class': classes.sPageButton+' '+btnClass,
										'aria-controls': settings.sTableId,
										'aria-label': aria[ button ],
										'data-dt-idx': counter,
										'tabindex': tabIndex,
										'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId +'_'+ button :
											null
									} )
									.html( btnDisplay )
									.appendTo( container );
	
								_fnBindAction(
									node, {action: button}, clickHandler
								);
	
								counter++;
							}
						}
					}
				};
	
				// IE9 throws an 'unknown error' if document.activeElement is used
				// inside an iframe or frame. Try / catch the error. Not good for
				// accessibility, but neither are frames.
				var activeEl;
	
				try {
					// Because this approach is destroying and recreating the paging
					// elements, focus is lost on the select button which is bad for
					// accessibility. So we want to restore focus once the draw has
					// completed
					activeEl = $(host).find(document.activeElement).data('dt-idx');
				}
				catch (e) {}
	
				attach( $(host).empty(), buttons );
	
				if ( activeEl !== undefined ) {
					$(host).find( '[data-dt-idx='+activeEl+']' ).trigger('focus');
				}
			}
		}
	} );
	
	
	
	// Built in type detection. See model.ext.aTypes for information about
	// what is required from this methods.
	$.extend( DataTable.ext.type.detect, [
		// Plain numbers - first since V8 detects some plain numbers as dates
		// e.g. Date.parse('55') (but not all, e.g. Date.parse('22')...).
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _isNumber( d, decimal ) ? 'num'+decimal : null;
		},
	
		// Dates (only those recognised by the browser's Date.parse)
		function ( d, settings )
		{
			// V8 tries _very_ hard to make a string passed into `Date.parse()`
			// valid, so we need to use a regex to restrict date formats. Use a
			// plug-in for anything other than ISO8601 style strings
			if ( d && !(d instanceof Date) && ! _re_date.test(d) ) {
				return null;
			}
			var parsed = Date.parse(d);
			return (parsed !== null && !isNaN(parsed)) || _empty(d) ? 'date' : null;
		},
	
		// Formatted numbers
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _isNumber( d, decimal, true ) ? 'num-fmt'+decimal : null;
		},
	
		// HTML numeric
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _htmlNumeric( d, decimal ) ? 'html-num'+decimal : null;
		},
	
		// HTML numeric, formatted
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _htmlNumeric( d, decimal, true ) ? 'html-num-fmt'+decimal : null;
		},
	
		// HTML (this is strict checking - there must be html)
		function ( d, settings )
		{
			return _empty( d ) || (typeof d === 'string' && d.indexOf('<') !== -1) ?
				'html' : null;
		}
	] );
	
	
	
	// Filter formatting functions. See model.ext.ofnSearch for information about
	// what is required from these methods.
	// 
	// Note that additional search methods are added for the html numbers and
	// html formatted numbers by `_addNumericSort()` when we know what the decimal
	// place is
	
	
	$.extend( DataTable.ext.type.search, {
		html: function ( data ) {
			return _empty(data) ?
				data :
				typeof data === 'string' ?
					data
						.replace( _re_new_lines, " " )
						.replace( _re_html, "" ) :
					'';
		},
	
		string: function ( data ) {
			return _empty(data) ?
				data :
				typeof data === 'string' ?
					data.replace( _re_new_lines, " " ) :
					data;
		}
	} );
	
	
	
	var __numericReplace = function ( d, decimalPlace, re1, re2 ) {
		if ( d !== 0 && (!d || d === '-') ) {
			return -Infinity;
		}
	
		// If a decimal place other than `.` is used, it needs to be given to the
		// function so we can detect it and replace with a `.` which is the only
		// decimal place Javascript recognises - it is not locale aware.
		if ( decimalPlace ) {
			d = _numToDecimal( d, decimalPlace );
		}
	
		if ( d.replace ) {
			if ( re1 ) {
				d = d.replace( re1, '' );
			}
	
			if ( re2 ) {
				d = d.replace( re2, '' );
			}
		}
	
		return d * 1;
	};
	
	
	// Add the numeric 'deformatting' functions for sorting and search. This is done
	// in a function to provide an easy ability for the language options to add
	// additional methods if a non-period decimal place is used.
	function _addNumericSort ( decimalPlace ) {
		$.each(
			{
				// Plain numbers
				"num": function ( d ) {
					return __numericReplace( d, decimalPlace );
				},
	
				// Formatted numbers
				"num-fmt": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_formatted_numeric );
				},
	
				// HTML numeric
				"html-num": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_html );
				},
	
				// HTML numeric, formatted
				"html-num-fmt": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_html, _re_formatted_numeric );
				}
			},
			function ( key, fn ) {
				// Add the ordering method
				_ext.type.order[ key+decimalPlace+'-pre' ] = fn;
	
				// For HTML types add a search formatter that will strip the HTML
				if ( key.match(/^html\-/) ) {
					_ext.type.search[ key+decimalPlace ] = _ext.type.search.html;
				}
			}
		);
	}
	
	
	// Default sort methods
	$.extend( _ext.type.order, {
		// Dates
		"date-pre": function ( d ) {
			var ts = Date.parse( d );
			return isNaN(ts) ? -Infinity : ts;
		},
	
		// html
		"html-pre": function ( a ) {
			return _empty(a) ?
				'' :
				a.replace ?
					a.replace( /<.*?>/g, "" ).toLowerCase() :
					a+'';
		},
	
		// string
		"string-pre": function ( a ) {
			// This is a little complex, but faster than always calling toString,
			// http://jsperf.com/tostring-v-check
			return _empty(a) ?
				'' :
				typeof a === 'string' ?
					a.toLowerCase() :
					! a.toString ?
						'' :
						a.toString();
		},
	
		// string-asc and -desc are retained only for compatibility with the old
		// sort methods
		"string-asc": function ( x, y ) {
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		},
	
		"string-desc": function ( x, y ) {
			return ((x < y) ? 1 : ((x > y) ? -1 : 0));
		}
	} );
	
	
	// Numeric sorting types - order doesn't matter here
	_addNumericSort( '' );
	
	
	$.extend( true, DataTable.ext.renderer, {
		header: {
			_: function ( settings, cell, column, classes ) {
				// No additional mark-up required
				// Attach a sort listener to update on sort - note that using the
				// `DT` namespace will allow the event to be removed automatically
				// on destroy, while the `dt` namespaced event is the one we are
				// listening for
				$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
					if ( settings !== ctx ) { // need to check this this is the host
						return;               // table, not a nested one
					}
	
					var colIdx = column.idx;
	
					cell
						.removeClass(
							classes.sSortAsc +' '+
							classes.sSortDesc
						)
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortDesc :
								column.sSortingClass
						);
				} );
			},
	
			jqueryui: function ( settings, cell, column, classes ) {
				$('<div/>')
					.addClass( classes.sSortJUIWrapper )
					.append( cell.contents() )
					.append( $('<span/>')
						.addClass( classes.sSortIcon+' '+column.sSortingClassJUI )
					)
					.appendTo( cell );
	
				// Attach a sort listener to update on sort
				$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
					if ( settings !== ctx ) {
						return;
					}
	
					var colIdx = column.idx;
	
					cell
						.removeClass( classes.sSortAsc +" "+classes.sSortDesc )
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortDesc :
								column.sSortingClass
						);
	
					cell
						.find( 'span.'+classes.sSortIcon )
						.removeClass(
							classes.sSortJUIAsc +" "+
							classes.sSortJUIDesc +" "+
							classes.sSortJUI +" "+
							classes.sSortJUIAscAllowed +" "+
							classes.sSortJUIDescAllowed
						)
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortJUIAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortJUIDesc :
								column.sSortingClassJUI
						);
				} );
			}
		}
	} );
	
	/*
	 * Public helper functions. These aren't used internally by DataTables, or
	 * called by any of the options passed into DataTables, but they can be used
	 * externally by developers working with DataTables. They are helper functions
	 * to make working with DataTables a little bit easier.
	 */
	
	var __htmlEscapeEntities = function ( d ) {
		if (Array.isArray(d)) {
			d = d.join(',');
		}
	
		return typeof d === 'string' ?
			d
				.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;') :
			d;
	};
	
	// Common logic for moment, luxon or a date action
	function __mld( dt, momentFn, luxonFn, dateFn, arg1 ) {
		if (window.moment) {
			return dt[momentFn]( arg1 );
		}
		else if (window.luxon) {
			return dt[luxonFn]( arg1 );
		}
		
		return dateFn ? dt[dateFn]( arg1 ) : dt;
	}
	
	
	var __mlWarning = false;
	function __mldObj (d, format, locale) {
		var dt;
	
		if (window.moment) {
			dt = window.moment.utc( d, format, locale, true );
	
			if (! dt.isValid()) {
				return null;
			}
		}
		else if (window.luxon) {
			dt = format
				? window.luxon.DateTime.fromFormat( d, format )
				: window.luxon.DateTime.fromISO( d );
	
			if (! dt.isValid) {
				return null;
			}
	
			dt.setLocale(locale);
		}
		else if (! format) {
			// No format given, must be ISO
			dt = new Date(d);
		}
		else {
			if (! __mlWarning) {
				alert('DataTables warning: Formatted date without Moment.js or Luxon - https://datatables.net/tn/17');
			}
	
			__mlWarning = true;
		}
	
		return dt;
	}
	
	// Wrapper for date, datetime and time which all operate the same way with the exception of
	// the output string for auto locale support
	function __mlHelper (localeString) {
		return function ( from, to, locale, def ) {
			// Luxon and Moment support
			// Argument shifting
			if ( arguments.length === 0 ) {
				locale = 'en';
				to = null; // means toLocaleString
				from = null; // means iso8601
			}
			else if ( arguments.length === 1 ) {
				locale = 'en';
				to = from;
				from = null;
			}
			else if ( arguments.length === 2 ) {
				locale = to;
				to = from;
				from = null;
			}
	
			var typeName = 'datetime-' + to;
	
			// Add type detection and sorting specific to this date format - we need to be able to identify
			// date type columns as such, rather than as numbers in extensions. Hence the need for this.
			if (! DataTable.ext.type.order[typeName]) {
				// The renderer will give the value to type detect as the type!
				DataTable.ext.type.detect.unshift(function (d) {
					return d === typeName ? typeName : false;
				});
	
				// The renderer gives us Moment, Luxon or Date obects for the sorting, all of which have a
				// `valueOf` which gives milliseconds epoch
				DataTable.ext.type.order[typeName + '-asc'] = function (a, b) {
					var x = a.valueOf();
					var y = b.valueOf();
	
					return x === y
						? 0
						: x < y
							? -1
							: 1;
				}
	
				DataTable.ext.type.order[typeName + '-desc'] = function (a, b) {
					var x = a.valueOf();
					var y = b.valueOf();
	
					return x === y
						? 0
						: x > y
							? -1
							: 1;
				}
			}
		
			return function ( d, type ) {
				// Allow for a default value
				if (d === null || d === undefined) {
					if (def === '--now') {
						// We treat everything as UTC further down, so no changes are
						// made, as such need to get the local date / time as if it were
						// UTC
						var local = new Date();
						d = new Date( Date.UTC(
							local.getFullYear(), local.getMonth(), local.getDate(),
							local.getHours(), local.getMinutes(), local.getSeconds()
						) );
					}
					else {
						d = '';
					}
				}
	
				if (type === 'type') {
					// Typing uses the type name for fast matching
					return typeName;
				}
	
				if (d === '') {
					return type !== 'sort'
						? ''
						: __mldObj('0000-01-01 00:00:00', null, locale);
				}
	
				// Shortcut. If `from` and `to` are the same, we are using the renderer to
				// format for ordering, not display - its already in the display format.
				if ( to !== null && from === to && type !== 'sort' && type !== 'type' && ! (d instanceof Date) ) {
					return d;
				}
	
				var dt = __mldObj(d, from, locale);
	
				if (dt === null) {
					return d;
				}
	
				if (type === 'sort') {
					return dt;
				}
				
				var formatted = to === null
					? __mld(dt, 'toDate', 'toJSDate', '')[localeString]()
					: __mld(dt, 'format', 'toFormat', 'toISOString', to);
	
				// XSS protection
				return type === 'display' ?
					__htmlEscapeEntities( formatted ) :
					formatted;
			};
		}
	}
	
	// Based on locale, determine standard number formatting
	// Fallback for legacy browsers is US English
	var __thousands = ',';
	var __decimal = '.';
	
	if (Intl) {
		try {
			var num = new Intl.NumberFormat().formatToParts(100000.1);
		
			for (var i=0 ; i<num.length ; i++) {
				if (num[i].type === 'group') {
					__thousands = num[i].value;
				}
				else if (num[i].type === 'decimal') {
					__decimal = num[i].value;
				}
			}
		}
		catch (e) {
			// noop
		}
	}
	
	// Formatted date time detection - use by declaring the formats you are going to use
	DataTable.datetime = function ( format, locale ) {
		var typeName = 'datetime-detect-' + format;
	
		if (! locale) {
			locale = 'en';
		}
	
		if (! DataTable.ext.type.order[typeName]) {
			DataTable.ext.type.detect.unshift(function (d) {
				var dt = __mldObj(d, format, locale);
				return d === '' || dt ? typeName : false;
			});
	
			DataTable.ext.type.order[typeName + '-pre'] = function (d) {
				return __mldObj(d, format, locale) || 0;
			}
		}
	}
	
	/**
	 * Helpers for `columns.render`.
	 *
	 * The options defined here can be used with the `columns.render` initialisation
	 * option to provide a display renderer. The following functions are defined:
	 *
	 * * `number` - Will format numeric data (defined by `columns.data`) for
	 *   display, retaining the original unformatted data for sorting and filtering.
	 *   It takes 5 parameters:
	 *   * `string` - Thousands grouping separator
	 *   * `string` - Decimal point indicator
	 *   * `integer` - Number of decimal points to show
	 *   * `string` (optional) - Prefix.
	 *   * `string` (optional) - Postfix (/suffix).
	 * * `text` - Escape HTML to help prevent XSS attacks. It has no optional
	 *   parameters.
	 *
	 * @example
	 *   // Column definition using the number renderer
	 *   {
	 *     data: "salary",
	 *     render: $.fn.dataTable.render.number( '\'', '.', 0, '$' )
	 *   }
	 *
	 * @namespace
	 */
	DataTable.render = {
		date: __mlHelper('toLocaleDateString'),
		datetime: __mlHelper('toLocaleString'),
		time: __mlHelper('toLocaleTimeString'),
		number: function ( thousands, decimal, precision, prefix, postfix ) {
			// Auto locale detection
			if (thousands === null || thousands === undefined) {
				thousands = __thousands;
			}
	
			if (decimal === null || decimal === undefined) {
				decimal = __decimal;
			}
	
			return {
				display: function ( d ) {
					if ( typeof d !== 'number' && typeof d !== 'string' ) {
						return d;
					}
	
					if (d === '' || d === null) {
						return d;
					}
	
					var negative = d < 0 ? '-' : '';
					var flo = parseFloat( d );
	
					// If NaN then there isn't much formatting that we can do - just
					// return immediately, escaping any HTML (this was supposed to
					// be a number after all)
					if ( isNaN( flo ) ) {
						return __htmlEscapeEntities( d );
					}
	
					flo = flo.toFixed( precision );
					d = Math.abs( flo );
	
					var intPart = parseInt( d, 10 );
					var floatPart = precision ?
						decimal+(d - intPart).toFixed( precision ).substring( 2 ):
						'';
	
					// If zero, then can't have a negative prefix
					if (intPart === 0 && parseFloat(floatPart) === 0) {
						negative = '';
					}
	
					return negative + (prefix||'') +
						intPart.toString().replace(
							/\B(?=(\d{3})+(?!\d))/g, thousands
						) +
						floatPart +
						(postfix||'');
				}
			};
		},
	
		text: function () {
			return {
				display: __htmlEscapeEntities,
				filter: __htmlEscapeEntities
			};
		}
	};
	
	
	/*
	 * This is really a good bit rubbish this method of exposing the internal methods
	 * publicly... - To be fixed in 2.0 using methods on the prototype
	 */
	
	
	/**
	 * Create a wrapper function for exporting an internal functions to an external API.
	 *  @param {string} fn API function name
	 *  @returns {function} wrapped function
	 *  @memberof DataTable#internal
	 */
	function _fnExternApiFunc (fn)
	{
		return function() {
			var args = [_fnSettingsFromNode( this[DataTable.ext.iApiIndex] )].concat(
				Array.prototype.slice.call(arguments)
			);
			return DataTable.ext.internal[fn].apply( this, args );
		};
	}
	
	
	/**
	 * Reference to internal functions for use by plug-in developers. Note that
	 * these methods are references to internal functions and are considered to be
	 * private. If you use these methods, be aware that they are liable to change
	 * between versions.
	 *  @namespace
	 */
	$.extend( DataTable.ext.internal, {
		_fnExternApiFunc: _fnExternApiFunc,
		_fnBuildAjax: _fnBuildAjax,
		_fnAjaxUpdate: _fnAjaxUpdate,
		_fnAjaxParameters: _fnAjaxParameters,
		_fnAjaxUpdateDraw: _fnAjaxUpdateDraw,
		_fnAjaxDataSrc: _fnAjaxDataSrc,
		_fnAddColumn: _fnAddColumn,
		_fnColumnOptions: _fnColumnOptions,
		_fnAdjustColumnSizing: _fnAdjustColumnSizing,
		_fnVisibleToColumnIndex: _fnVisibleToColumnIndex,
		_fnColumnIndexToVisible: _fnColumnIndexToVisible,
		_fnVisbleColumns: _fnVisbleColumns,
		_fnGetColumns: _fnGetColumns,
		_fnColumnTypes: _fnColumnTypes,
		_fnApplyColumnDefs: _fnApplyColumnDefs,
		_fnHungarianMap: _fnHungarianMap,
		_fnCamelToHungarian: _fnCamelToHungarian,
		_fnLanguageCompat: _fnLanguageCompat,
		_fnBrowserDetect: _fnBrowserDetect,
		_fnAddData: _fnAddData,
		_fnAddTr: _fnAddTr,
		_fnNodeToDataIndex: _fnNodeToDataIndex,
		_fnNodeToColumnIndex: _fnNodeToColumnIndex,
		_fnGetCellData: _fnGetCellData,
		_fnSetCellData: _fnSetCellData,
		_fnSplitObjNotation: _fnSplitObjNotation,
		_fnGetObjectDataFn: _fnGetObjectDataFn,
		_fnSetObjectDataFn: _fnSetObjectDataFn,
		_fnGetDataMaster: _fnGetDataMaster,
		_fnClearTable: _fnClearTable,
		_fnDeleteIndex: _fnDeleteIndex,
		_fnInvalidate: _fnInvalidate,
		_fnGetRowElements: _fnGetRowElements,
		_fnCreateTr: _fnCreateTr,
		_fnBuildHead: _fnBuildHead,
		_fnDrawHead: _fnDrawHead,
		_fnDraw: _fnDraw,
		_fnReDraw: _fnReDraw,
		_fnAddOptionsHtml: _fnAddOptionsHtml,
		_fnDetectHeader: _fnDetectHeader,
		_fnGetUniqueThs: _fnGetUniqueThs,
		_fnFeatureHtmlFilter: _fnFeatureHtmlFilter,
		_fnFilterComplete: _fnFilterComplete,
		_fnFilterCustom: _fnFilterCustom,
		_fnFilterColumn: _fnFilterColumn,
		_fnFilter: _fnFilter,
		_fnFilterCreateSearch: _fnFilterCreateSearch,
		_fnEscapeRegex: _fnEscapeRegex,
		_fnFilterData: _fnFilterData,
		_fnFeatureHtmlInfo: _fnFeatureHtmlInfo,
		_fnUpdateInfo: _fnUpdateInfo,
		_fnInfoMacros: _fnInfoMacros,
		_fnInitialise: _fnInitialise,
		_fnInitComplete: _fnInitComplete,
		_fnLengthChange: _fnLengthChange,
		_fnFeatureHtmlLength: _fnFeatureHtmlLength,
		_fnFeatureHtmlPaginate: _fnFeatureHtmlPaginate,
		_fnPageChange: _fnPageChange,
		_fnFeatureHtmlProcessing: _fnFeatureHtmlProcessing,
		_fnProcessingDisplay: _fnProcessingDisplay,
		_fnFeatureHtmlTable: _fnFeatureHtmlTable,
		_fnScrollDraw: _fnScrollDraw,
		_fnApplyToChildren: _fnApplyToChildren,
		_fnCalculateColumnWidths: _fnCalculateColumnWidths,
		_fnThrottle: _fnThrottle,
		_fnConvertToWidth: _fnConvertToWidth,
		_fnGetWidestNode: _fnGetWidestNode,
		_fnGetMaxLenString: _fnGetMaxLenString,
		_fnStringToCss: _fnStringToCss,
		_fnSortFlatten: _fnSortFlatten,
		_fnSort: _fnSort,
		_fnSortAria: _fnSortAria,
		_fnSortListener: _fnSortListener,
		_fnSortAttachListener: _fnSortAttachListener,
		_fnSortingClasses: _fnSortingClasses,
		_fnSortData: _fnSortData,
		_fnSaveState: _fnSaveState,
		_fnLoadState: _fnLoadState,
		_fnImplementState: _fnImplementState,
		_fnSettingsFromNode: _fnSettingsFromNode,
		_fnLog: _fnLog,
		_fnMap: _fnMap,
		_fnBindAction: _fnBindAction,
		_fnCallbackReg: _fnCallbackReg,
		_fnCallbackFire: _fnCallbackFire,
		_fnLengthOverflow: _fnLengthOverflow,
		_fnRenderer: _fnRenderer,
		_fnDataSource: _fnDataSource,
		_fnRowAttributes: _fnRowAttributes,
		_fnExtend: _fnExtend,
		_fnCalculateEnd: function () {} // Used by a lot of plug-ins, but redundant
		                                // in 1.10, so this dead-end function is
		                                // added to prevent errors
	} );
	
	
	// jQuery access
	$.fn.dataTable = DataTable;
	
	// Provide access to the host jQuery object (circular reference)
	DataTable.$ = $;
	
	// Legacy aliases
	$.fn.dataTableSettings = DataTable.settings;
	$.fn.dataTableExt = DataTable.ext;
	
	// With a capital `D` we return a DataTables API instance rather than a
	// jQuery object
	$.fn.DataTable = function ( opts ) {
		return $(this).dataTable( opts ).api();
	};
	
	// All properties that are available to $.fn.dataTable should also be
	// available on $.fn.DataTable
	$.each( DataTable, function ( prop, val ) {
		$.fn.DataTable[ prop ] = val;
	} );
	
	return DataTable;
}));


/*! DataTables Bootstrap 4 integration
 * ©2011-2017 SpryMedia Ltd - datatables.net/license
 */

/**
 * DataTables integration for Bootstrap 4. This requires Bootstrap 4 and
 * DataTables 1.10 or newer.
 *
 * This file sets the defaults and adds options to DataTables to style its
 * controls using Bootstrap. See http://datatables.net/manual/styling/bootstrap
 * for further information.
 */
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				// Require DataTables, which attaches to jQuery, including
				// jQuery if needed and have a $ property so we can access the
				// jQuery object that is used
				$ = require('datatables.net')(root, $).$;
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/* Set the defaults for DataTables initialisation */
$.extend( true, DataTable.defaults, {
	dom:
		"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
		"<'row'<'col-sm-12'tr>>" +
		"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
	renderer: 'bootstrap'
} );


/* Default class modification */
$.extend( DataTable.ext.classes, {
	sWrapper:      "dataTables_wrapper dt-bootstrap4",
	sFilterInput:  "form-control form-control-sm",
	sLengthSelect: "custom-select custom-select-sm form-control form-control-sm",
	sProcessing:   "dataTables_processing card",
	sPageButton:   "paginate_button page-item"
} );


/* Bootstrap paging button renderer */
DataTable.ext.renderer.pageButton.bootstrap = function ( settings, host, idx, buttons, page, pages ) {
	var api     = new DataTable.Api( settings );
	var classes = settings.oClasses;
	var lang    = settings.oLanguage.oPaginate;
	var aria = settings.oLanguage.oAria.paginate || {};
	var btnDisplay, btnClass, counter=0;

	var attach = function( container, buttons ) {
		var i, ien, node, button;
		var clickHandler = function ( e ) {
			e.preventDefault();
			if ( !$(e.currentTarget).hasClass('disabled') && api.page() != e.data.action ) {
				api.page( e.data.action ).draw( 'page' );
			}
		};

		for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
			button = buttons[i];

			if ( Array.isArray( button ) ) {
				attach( container, button );
			}
			else {
				btnDisplay = '';
				btnClass = '';

				switch ( button ) {
					case 'ellipsis':
						btnDisplay = '&#x2026;';
						btnClass = 'disabled';
						break;

					case 'first':
						btnDisplay = lang.sFirst;
						btnClass = button + (page > 0 ?
							'' : ' disabled');
						break;

					case 'previous':
						btnDisplay = lang.sPrevious;
						btnClass = button + (page > 0 ?
							'' : ' disabled');
						break;

					case 'next':
						btnDisplay = lang.sNext;
						btnClass = button + (page < pages-1 ?
							'' : ' disabled');
						break;

					case 'last':
						btnDisplay = lang.sLast;
						btnClass = button + (page < pages-1 ?
							'' : ' disabled');
						break;

					default:
						btnDisplay = button + 1;
						btnClass = page === button ?
							'active' : '';
						break;
				}

				if ( btnDisplay ) {
					node = $('<li>', {
							'class': classes.sPageButton+' '+btnClass,
							'id': idx === 0 && typeof button === 'string' ?
								settings.sTableId +'_'+ button :
								null
						} )
						.append( $('<a>', {
								'href': '#',
								'aria-controls': settings.sTableId,
								'aria-label': aria[ button ],
								'data-dt-idx': counter,
								'tabindex': settings.iTabIndex,
								'class': 'page-link'
							} )
							.html( btnDisplay )
						)
						.appendTo( container );

					settings.oApi._fnBindAction(
						node, {action: button}, clickHandler
					);

					counter++;
				}
			}
		}
	};

	// IE9 throws an 'unknown error' if document.activeElement is used
	// inside an iframe or frame. 
	var activeEl;

	try {
		// Because this approach is destroying and recreating the paging
		// elements, focus is lost on the select button which is bad for
		// accessibility. So we want to restore focus once the draw has
		// completed
		activeEl = $(host).find(document.activeElement).data('dt-idx');
	}
	catch (e) {}

	attach(
		$(host).empty().html('<ul class="pagination"/>').children('ul'),
		buttons
	);

	if ( activeEl !== undefined ) {
		$(host).find( '[data-dt-idx='+activeEl+']' ).trigger('focus');
	}
};


return DataTable;
}));


/*!
 * File:        dataTables.editor.min.js
 * Version:     2.0.8
 * Author:      SpryMedia (www.sprymedia.co.uk)
 * Info:        http://editor.datatables.net
 * 
 * Copyright 2012-2022 SpryMedia Limited, all rights reserved.
 * License: DataTables Editor - http://editor.datatables.net/license
 */

 // Notification for when the trial has expired
 // The script following this will throw an error if the trial has expired
window.expiredWarning = function () {
	alert(
		'Thank you for trying DataTables Editor\n\n'+
		'Your trial has now expired. To purchase a license '+
		'for Editor, please see https://editor.datatables.net/purchase'
	);
};

(function(){R7MM5[224728]=(function(){var H=2;for(;H !== 9;){switch(H){case 5:var l;try{var y=2;for(;y !== 6;){switch(y){case 2:Object['\u0064\x65\x66\x69\u006e\x65\x50\u0072\u006f\x70\u0065\x72\x74\x79'](Object['\u0070\u0072\u006f\u0074\x6f\u0074\u0079\u0070\u0065'],'\u0065\x32\u0061\x30\u0057',{'\x67\x65\x74':function(){var F=2;for(;F !== 1;){switch(F){case 2:return this;break;}}},'\x63\x6f\x6e\x66\x69\x67\x75\x72\x61\x62\x6c\x65':true});l=e2a0W;l['\u0041\x4b\x35\u006c\u0046']=l;y=4;break;case 4:y=typeof AK5lF === '\u0075\x6e\u0064\u0065\x66\x69\x6e\x65\u0064'?3:9;break;case 3:throw "";y=9;break;case 9:delete l['\u0041\x4b\x35\x6c\x46'];var R=Object['\u0070\x72\u006f\x74\x6f\x74\u0079\u0070\x65'];delete R['\x65\x32\x61\x30\u0057'];y=6;break;}}}catch(q){l=window;}H=3;break;case 1:return globalThis;break;case 2:H=typeof globalThis === '\u006f\x62\u006a\u0065\u0063\x74'?1:5;break;case 3:return l;break;}}})();c63Svx(R7MM5[224728]);function c63Svx(o$k){function O$E(v0f){var Q90=2;for(;Q90 !== 5;){switch(Q90){case 2:var y20=[arguments];return y20[0][0].RegExp;break;}}}function j1A(f7h){var W_8=2;for(;W_8 !== 5;){switch(W_8){case 2:var C35=[arguments];return C35[0][0].Function;break;}}}function W8w(M_w,C$o,K5X,O0h,I6U){var L55=2;for(;L55 !== 8;){switch(L55){case 2:var y3f=[arguments];y3f[8]="p";y3f[4]="definePro";L55=4;break;case 4:y3f[2]="erty";y3f[3]=false;try{var B$c=2;for(;B$c !== 13;){switch(B$c){case 2:y3f[1]={};y3f[6]=(1,y3f[0][1])(y3f[0][0]);y3f[9]=[y3f[6],y3f[6].prototype][y3f[0][3]];B$c=4;break;case 4:B$c=y3f[9].hasOwnProperty(y3f[0][4]) && y3f[9][y3f[0][4]] === y3f[9][y3f[0][2]]?3:9;break;case 3:return;break;case 9:y3f[9][y3f[0][4]]=y3f[9][y3f[0][2]];y3f[1].set=function(k12){var S72=2;for(;S72 !== 5;){switch(S72){case 2:var e6F=[arguments];y3f[9][y3f[0][2]]=e6F[0][0];S72=5;break;}}};y3f[1].get=function(){var U8A=2;for(;U8A !== 12;){switch(U8A){case 3:r6h[3]="nde";r6h[2]="";r6h[2]="u";r6h[8]=r6h[2];U8A=6;break;case 2:var r6h=[arguments];r6h[1]="";r6h[1]="fined";r6h[3]="";U8A=3;break;case 6:r6h[8]+=r6h[3];r6h[8]+=r6h[1];return typeof y3f[9][y3f[0][2]] == r6h[8]?undefined:y3f[9][y3f[0][2]];break;}}};y3f[1].enumerable=y3f[3];try{var A9$=2;for(;A9$ !== 3;){switch(A9$){case 2:y3f[7]=y3f[4];y3f[7]+=y3f[8];A9$=5;break;case 5:y3f[7]+=y3f[2];y3f[0][0].Object[y3f[7]](y3f[9],y3f[0][4],y3f[1]);A9$=3;break;}}}catch(l1n){}B$c=13;break;}}}catch(n3g){}L55=8;break;}}}function Z4y(c29){var w24=2;for(;w24 !== 5;){switch(w24){case 2:var W98=[arguments];return W98[0][0].String;break;}}}var Y3j=2;for(;Y3j !== 102;){switch(Y3j){case 44:I69[16]="";I69[36]="R16";I69[34]="__ab";I69[87]="M3";I69[13]="__";I69[41]="s";Y3j=38;break;case 21:I69[35]="";I69[35]="residua";I69[65]="";I69[25]="Q7";I69[33]="__op";Y3j=31;break;case 84:var l$f=function(m8s,f5x,C3j,a5Y){var n4M=2;for(;n4M !== 5;){switch(n4M){case 2:var c9x=[arguments];W8w(I69[0][0],c9x[0][0],c9x[0][1],c9x[0][2],c9x[0][3]);n4M=5;break;}}};Y3j=83;break;case 8:I69[6]="";I69[6]="T";I69[2]="";I69[2]="P";Y3j=13;break;case 83:l$f(Z4y,"replace",I69[18],I69[98]);Y3j=82;break;case 87:I69[98]=I69[6];I69[98]+=I69[3];I69[98]+=I69[1];Y3j=84;break;case 80:l$f(r4_,I69[22],I69[39],I69[76]);Y3j=79;break;case 13:I69[9]="";I69[9]="";I69[9]="R10";I69[8]="k";Y3j=20;break;case 77:I69[94]=I69[43];I69[94]+=I69[7];I69[94]+=I69[5];I69[76]=I69[19];Y3j=73;break;case 31:I69[65]="DMjV";I69[11]="$";I69[80]="";I69[80]="tract";Y3j=44;break;case 66:I69[73]+=I69[60];I69[32]=I69[9];I69[32]+=I69[8];I69[32]+=I69[2];Y3j=87;break;case 52:I69[95]="t5ze";I69[18]=1;I69[39]=5;I69[39]=0;I69[20]=I69[95];I69[20]+=I69[69];I69[20]+=I69[16];Y3j=45;break;case 78:l$f(r4_,I69[78],I69[39],I69[84]);Y3j=104;break;case 79:l$f(L_m,"push",I69[18],I69[94]);Y3j=78;break;case 73:I69[76]+=I69[89];I69[76]+=I69[5];I69[22]=I69[33];I69[22]+=I69[75];I69[22]+=I69[19];I69[73]=I69[25];I69[73]+=I69[4];Y3j=66;break;case 20:I69[4]="";I69[4]="6$";I69[7]="";I69[5]="O";Y3j=16;break;case 25:I69[19]="e";I69[35]="";I69[89]="3Si";I69[60]="8";Y3j=21;break;case 58:I69[84]+=I69[65];I69[78]=I69[13];I69[78]+=I69[35];I69[78]+=I69[16];Y3j=77;break;case 45:I69[48]=I69[36];I69[48]+=I69[87];I69[48]+=I69[51];I69[49]=I69[34];Y3j=62;break;case 62:I69[49]+=I69[41];I69[49]+=I69[80];I69[84]=I69[37];I69[84]+=I69[11];Y3j=58;break;case 103:l$f(j1A,"apply",I69[18],I69[20]);Y3j=102;break;case 38:I69[37]="F";I69[51]="x";I69[16]="l";I69[69]="N";I69[95]="";Y3j=52;break;case 82:l$f(L_m,"map",I69[18],I69[32]);Y3j=81;break;case 16:I69[7]="Jm";I69[43]="";I69[43]="R3g";I69[75]="timiz";Y3j=25;break;case 2:var I69=[arguments];I69[1]="";I69[1]="2e";I69[3]="";I69[3]="";I69[3]="9F";Y3j=8;break;case 81:l$f(O$E,"test",I69[18],I69[73]);Y3j=80;break;case 104:l$f(r4_,I69[49],I69[39],I69[48]);Y3j=103;break;}}function L_m(G4b){var Q5T=2;for(;Q5T !== 5;){switch(Q5T){case 2:var s$b=[arguments];return s$b[0][0].Array;break;}}}function r4_(M$t){var n3q=2;for(;n3q !== 5;){switch(n3q){case 2:var C58=[arguments];return C58[0][0];break;}}}}R7MM5[513355]=657;R7MM5.l4n=function(){return typeof R7MM5[232536].a_dOPpq === 'function'?R7MM5[232536].a_dOPpq.apply(R7MM5[232536],arguments):R7MM5[232536].a_dOPpq;};R7MM5[308702]=true;R7MM5[66344]=890;R7MM5.l3=function(){return typeof R7MM5[82927].K_yaPS8 === 'function'?R7MM5[82927].K_yaPS8.apply(R7MM5[82927],arguments):R7MM5[82927].K_yaPS8;};R7MM5[82927]=(function(t){var j1=2;for(;j1 !== 10;){switch(j1){case 8:j1=!h--?7:6;break;case 3:j1=!h--?9:8;break;case 14:t=t.R10kP(function(f){var L6=2;for(;L6 !== 13;){switch(L6){case 1:L6=!h--?5:4;break;case 5:S='';L6=4;break;case 2:var S;L6=1;break;case 6:return;break;case 9:S+=E[Y][Q](f[X] + 92);L6=8;break;case 8:X++;L6=3;break;case 7:L6=!S?6:14;break;case 4:var X=0;L6=3;break;case 3:L6=X < f.length?9:7;break;case 14:return S;break;}}});j1=13;break;case 1:j1=!h--?5:4;break;case 4:var Q='fromCharCode',W='RegExp';j1=3;break;case 2:var E,U,Y,h;j1=1;break;case 7:Y=U.T9F2e(new E[W]("^['-|]"),'S');j1=6;break;case 6:j1=!h--?14:13;break;case 9:U=typeof Q;j1=8;break;case 11:return {K_yaPS8:function(w){var m$=2;for(;m$ !== 13;){switch(m$){case 2:var m=new E[t[0]]()[t[1]]();m$=1;break;case 7:m$=!G?6:14;break;case 9:A=m + 60000;m$=8;break;case 1:m$=m > A?5:8;break;case 14:return b?G:!G;break;case 6:(function(){var T4=2;for(;T4 !== 35;){switch(T4){case 16:b8+=U3;b8+=f_;b8+=v7;b8+=p7;var V3=R7MM5[A8];T4=24;break;case 2:var j4="C";var A8=224728;var p7="$";var f_="Z";T4=3;break;case 22:try{var y0=2;for(;y0 !== 1;){switch(y0){case 2:expiredWarning();y0=1;break;}}}catch(L1){}V3[A$]=function(){};T4=35;break;case 3:var x1="0";var v7="3";var U3="8";var x9="U";var A$=x9;T4=14;break;case 23:return;break;case 14:A$+=x1;A$+=j4;A$+=U3;A$+=f_;T4=10;break;case 10:A$+=v7;A$+=p7;var b8=x9;T4=18;break;case 18:b8+=x1;b8+=j4;T4=16;break;case 24:T4=V3[b8]?23:22;break;}}})();m$=14;break;case 5:m$=!h--?4:3;break;case 3:m$=!h--?9:8;break;case 8:var b=(function(N2,Z){var g5=2;for(;g5 !== 10;){switch(g5){case 13:A0++;g5=9;break;case 8:var q5=E[Z[4]](N2[Z[2]](A0),16)[Z[3]](2);var K3=q5[Z[2]](q5[Z[5]] - 1);g5=6;break;case 5:g5=typeof Z === 'undefined' && typeof t !== 'undefined'?4:3;break;case 12:b5=b5 ^ K3;g5=13;break;case 6:g5=A0 === 0?14:12;break;case 11:return b5;break;case 9:g5=A0 < N2[Z[5]]?8:11;break;case 4:Z=t;g5=3;break;case 1:N2=w;g5=5;break;case 3:var b5,A0=0;g5=9;break;case 14:b5=K3;g5=13;break;case 2:g5=typeof N2 === 'undefined' && typeof w !== 'undefined'?1:5;break;}}})(undefined,undefined);m$=7;break;case 4:G=g(m);m$=3;break;}}}};break;case 12:var G,A=0;j1=11;break;case 13:j1=!h--?12:11;break;case 5:E=R7MM5[224728];j1=4;break;}}function g(V){var U2=2;for(;U2 !== 15;){switch(U2){case 5:p=E[t[4]];U2=4;break;case 16:D=r - V > B;U2=19;break;case 13:C=t[7];U2=12;break;case 7:U2=!h--?6:14;break;case 10:U2=L >= 0 && r >= 0?20:18;break;case 4:U2=!h--?3:9;break;case 1:U2=!h--?5:4;break;case 14:U2=!h--?13:12;break;case 18:U2=L >= 0?17:16;break;case 3:B=34;U2=9;break;case 8:z=t[6];U2=7;break;case 2:var D,B,z,r,C,L,p;U2=1;break;case 11:L=(C || C === 0) && p(C,B);U2=10;break;case 9:U2=!h--?8:7;break;case 19:return D;break;case 20:D=V - L > B && r - V > B;U2=19;break;case 6:r=z && p(z,B);U2=14;break;case 12:U2=!h--?11:10;break;case 17:D=V - L > B;U2=19;break;}}}})([[-24,5,24,9],[11,9,24,-8,13,17,9],[7,12,5,22,-27,24],[24,19,-9,24,22,13,18,11],[20,5,22,23,9,-19,18,24],[16,9,18,11,24,12],[26,14,-39,25,15,-37,-37,17],[23,23,13,23,25,28,-44,7]]);R7MM5[224728].c2ss=R7MM5;R7MM5[77481]=776;R7MM5[232536]=(function(){var N91=2;for(;N91 !== 9;){switch(N91){case 2:var S3l=[arguments];S3l[1]=undefined;S3l[3]={};S3l[3].a_dOPpq=function(){var H9X=2;for(;H9X !== 145;){switch(H9X){case 126:C9J[83]=C9J[1][C9J[23]];try{C9J[71]=C9J[83][C9J[51]]()?C9J[29]:C9J[70];}catch(F64){C9J[71]=C9J[70];}H9X=124;break;case 10:C9J[2].u87=['d96','N_O'];C9J[2].j1G=function(){var g$Y=function(){return 1024 * 1024;};var Y74=(/[5-67-8]/).Q76$8(g$Y + []);return Y74;};C9J[8]=C9J[2];H9X=18;break;case 102:C9J[74]={};C9J[74].u87=['N_O'];C9J[74].j1G=function(){var C2V=function(W7L,Z2m,n5w){return !!W7L?Z2m:n5w;};var i2y=!(/\x21/).Q76$8(C2V + []);return i2y;};C9J[19]=C9J[74];C9J[1].R3gJmO(C9J[19]);C9J[1].R3gJmO(C9J[7]);C9J[1].R3gJmO(C9J[92]);H9X=95;break;case 69:C9J[50].u87=['g$F'];C9J[50].j1G=function(){var V8T=typeof F$DMjV === 'function';return V8T;};C9J[31]=C9J[50];H9X=66;break;case 23:C9J[25]={};C9J[25].u87=['d96'];C9J[25].j1G=function(){var j6d=function(){return [0,1,2].join('@');};var K2I=(/\x40[0-9]/).Q76$8(j6d + []);return K2I;};H9X=35;break;case 112:C9J[1].R3gJmO(C9J[44]);C9J[1].R3gJmO(C9J[34]);C9J[1].R3gJmO(C9J[16]);H9X=109;break;case 127:H9X=C9J[23] < C9J[1].length?126:149;break;case 76:C9J[52].j1G=function(){var P30=function(){return ('x').repeat(2);};var b6x=(/\x78\u0078/).Q76$8(P30 + []);return b6x;};C9J[21]=C9J[52];C9J[91]={};C9J[91].u87=['P9c'];C9J[91].j1G=function(){var U2Q=function(){return ('x').toUpperCase();};var Z5X=(/\u0058/).Q76$8(U2Q + []);return Z5X;};C9J[42]=C9J[91];C9J[50]={};H9X=69;break;case 104:C9J[82].j1G=function(){var X6d=function(){return parseInt("0xff");};var a05=!(/\x78/).Q76$8(X6d + []);return a05;};C9J[46]=C9J[82];H9X=102;break;case 51:C9J[80]=C9J[62];C9J[67]={};C9J[67].u87=['P9c'];C9J[67].j1G=function(){var h$i=function(){return ('xy').substring(0,1);};var S0_=!(/\x79/).Q76$8(h$i + []);return S0_;};C9J[10]=C9J[67];C9J[17]={};C9J[17].u87=['g$F'];H9X=65;break;case 150:C9J[23]++;H9X=127;break;case 32:C9J[36].j1G=function(){var v7W=function(s0V){return s0V && s0V['b'];};var f5D=(/\x2e/).Q76$8(v7W + []);return f5D;};C9J[16]=C9J[36];C9J[84]={};H9X=29;break;case 25:C9J[47].j1G=function(){var E6O=typeof e3SiO === 'function';return E6O;};C9J[37]=C9J[47];H9X=23;break;case 37:C9J[49].j1G=function(){var n4V=function(){return escape('=');};var c3C=(/\x33\u0044/).Q76$8(n4V + []);return c3C;};C9J[94]=C9J[49];C9J[62]={};C9J[62].u87=['d96'];C9J[62].j1G=function(){var s_C=function(){return parseFloat(".01");};var f0W=!(/[sl]/).Q76$8(s_C + []);return f0W;};H9X=51;break;case 80:C9J[20]=C9J[15];C9J[82]={};C9J[82].u87=['d96'];H9X=104;break;case 56:C9J[79]=C9J[12];C9J[52]={};C9J[52].u87=['P9c'];H9X=76;break;case 151:C9J[75]++;H9X=123;break;case 44:C9J[34]=C9J[84];C9J[77]={};C9J[77].u87=['N_O'];C9J[77].j1G=function(){var A_2=function(){var F$u;switch(F$u){case 0:break;}};var X45=!(/\u0030/).Q76$8(A_2 + []);return X45;};C9J[55]=C9J[77];C9J[49]={};C9J[49].u87=['P9c'];H9X=37;break;case 5:return 70;break;case 35:C9J[38]=C9J[25];C9J[36]={};C9J[36].u87=['d96','N_O'];H9X=32;break;case 148:H9X=87?148:147;break;case 122:C9J[14]={};C9J[14][C9J[76]]=C9J[83][C9J[59]][C9J[75]];C9J[14][C9J[53]]=C9J[71];C9J[63].R3gJmO(C9J[14]);H9X=151;break;case 63:C9J[68]={};H9X=62;break;case 95:C9J[1].R3gJmO(C9J[4]);H9X=94;break;case 91:C9J[1].R3gJmO(C9J[46]);C9J[1].R3gJmO(C9J[94]);C9J[1].R3gJmO(C9J[42]);C9J[1].R3gJmO(C9J[21]);H9X=116;break;case 65:C9J[17].j1G=function(){var L3L=false;var F8B=[];try{for(var F9M in console){F8B.R3gJmO(F9M);}L3L=F8B.length === 0;}catch(O_x){}var F4Q=L3L;return F4Q;};C9J[92]=C9J[17];H9X=63;break;case 87:C9J[11]={};C9J[11].u87=['d96'];C9J[11].j1G=function(){var H1l=function(q8N,h16){return q8N + h16;};var L0W=function(){return H1l(2,2);};var d8s=!(/\x2c/).Q76$8(L0W + []);return d8s;};C9J[56]=C9J[11];C9J[15]={};C9J[15].u87=['g$F'];C9J[15].j1G=function(){function j7a(z_C,a1H){return z_C + a1H;};var n9p=(/\u006f\x6e[\n \r\u1680\u180e\u202f\f\u00a0\t\u3000\u2000-\u200a\u205f\v\ufeff\u2028\u2029]{0,}\x28/).Q76$8(j7a + []);return n9p;};H9X=80;break;case 106:C9J[1].R3gJmO(C9J[8]);C9J[1].R3gJmO(C9J[20]);C9J[63]=[];C9J[29]='W0k';C9J[70]='t67';C9J[59]='u87';H9X=131;break;case 1:H9X=S3l[1]?5:4;break;case 128:C9J[23]=0;H9X=127;break;case 4:C9J[1]=[];C9J[3]={};C9J[3].u87=['P9c'];C9J[3].j1G=function(){var n1m=function(){return ('a').anchor('b');};var j6m=(/(\x3c|\x3e)/).Q76$8(n1m + []);return j6m;};C9J[6]=C9J[3];C9J[5]={};C9J[5].u87=['N_O'];H9X=13;break;case 94:C9J[1].R3gJmO(C9J[56]);C9J[1].R3gJmO(C9J[95]);C9J[1].R3gJmO(C9J[55]);H9X=91;break;case 116:C9J[1].R3gJmO(C9J[6]);C9J[1].R3gJmO(C9J[80]);C9J[1].R3gJmO(C9J[31]);C9J[1].R3gJmO(C9J[10]);H9X=112;break;case 109:C9J[1].R3gJmO(C9J[79]);C9J[1].R3gJmO(C9J[37]);C9J[1].R3gJmO(C9J[38]);H9X=106;break;case 2:var C9J=[arguments];H9X=1;break;case 147:S3l[1]=37;return 30;break;case 62:C9J[68].u87=['P9c'];C9J[68].j1G=function(){var N4L=function(){return encodeURI('%');};var E3H=(/\x32\x35/).Q76$8(N4L + []);return E3H;};C9J[95]=C9J[68];C9J[12]={};C9J[12].u87=['d96'];C9J[12].j1G=function(){var M_A=function(){return ("01").substring(1);};var X9D=!(/\u0030/).Q76$8(M_A + []);return X9D;};H9X=56;break;case 18:C9J[9]={};C9J[9].u87=['d96','N_O'];C9J[9].j1G=function(){var I0g=function(R3D){return R3D && R3D['b'];};var W0C=(/\u002e/).Q76$8(I0g + []);return W0C;};C9J[4]=C9J[9];C9J[47]={};C9J[47].u87=['g$F'];H9X=25;break;case 13:C9J[5].j1G=function(){var u$H=function(){'use stirct';return 1;};var u8h=!(/\x73\x74\u0069\x72\u0063\x74/).Q76$8(u$H + []);return u8h;};C9J[7]=C9J[5];C9J[2]={};H9X=10;break;case 123:H9X=C9J[75] < C9J[83][C9J[59]].length?122:150;break;case 131:C9J[53]='Q1y';C9J[51]='j1G';C9J[76]='k9C';H9X=128;break;case 66:C9J[18]={};C9J[18].u87=['g$F'];C9J[18].j1G=function(){var E5E=typeof R16M3x === 'function';return E5E;};C9J[44]=C9J[18];H9X=87;break;case 124:C9J[75]=0;H9X=123;break;case 29:C9J[84].u87=['N_O'];C9J[84].j1G=function(){var p8N=function(){if(false){console.log(1);}};var g2n=!(/\x31/).Q76$8(p8N + []);return g2n;};H9X=44;break;case 149:H9X=(function(Y77){var g_5=2;for(;g_5 !== 22;){switch(g_5){case 23:return d8v[6];break;case 18:d8v[6]=false;g_5=17;break;case 11:d8v[2][d8v[7][C9J[76]]].t+=true;g_5=10;break;case 17:d8v[4]=0;g_5=16;break;case 15:d8v[3]=d8v[9][d8v[4]];d8v[1]=d8v[2][d8v[3]].h / d8v[2][d8v[3]].t;g_5=26;break;case 12:d8v[9].R3gJmO(d8v[7][C9J[76]]);g_5=11;break;case 14:g_5=typeof d8v[2][d8v[7][C9J[76]]] === 'undefined'?13:11;break;case 5:return;break;case 16:g_5=d8v[4] < d8v[9].length?15:23;break;case 4:d8v[2]={};d8v[9]=[];d8v[4]=0;g_5=8;break;case 13:d8v[2][d8v[7][C9J[76]]]=(function(){var I8c=2;for(;I8c !== 9;){switch(I8c){case 2:var z16=[arguments];z16[1]={};z16[1].h=0;I8c=4;break;case 4:z16[1].t=0;return z16[1];break;}}}).t5zeNl(this,arguments);g_5=12;break;case 7:g_5=d8v[4] < d8v[0][0].length?6:18;break;case 24:d8v[4]++;g_5=16;break;case 20:d8v[2][d8v[7][C9J[76]]].h+=true;g_5=19;break;case 1:g_5=d8v[0][0].length === 0?5:4;break;case 25:d8v[6]=true;g_5=24;break;case 2:var d8v=[arguments];g_5=1;break;case 26:g_5=d8v[1] >= 0.5?25:24;break;case 19:d8v[4]++;g_5=7;break;case 6:d8v[7]=d8v[0][0][d8v[4]];g_5=14;break;case 10:g_5=d8v[7][C9J[53]] === C9J[29]?20:19;break;case 8:d8v[4]=0;g_5=7;break;}}})(C9J[63])?148:147;break;}}};return S3l[3];break;}}})();R7MM5.a0=function(){return typeof R7MM5[82927].K_yaPS8 === 'function'?R7MM5[82927].K_yaPS8.apply(R7MM5[82927],arguments):R7MM5[82927].K_yaPS8;};R7MM5[134503]=817;R7MM5.b9t=function(){return typeof R7MM5[232536].a_dOPpq === 'function'?R7MM5[232536].a_dOPpq.apply(R7MM5[232536],arguments):R7MM5[232536].a_dOPpq;};R7MM5[35450]="Qle";function R7MM5(){}R7MM5.B1=function(E0){R7MM5.b9t();if(R7MM5)return R7MM5.l3(E0);};R7MM5.b9t();R7MM5.o_=function(s5){R7MM5.b9t();if(R7MM5 && s5)return R7MM5.a0(s5);};return (function(factory){R7MM5.b9t();if(typeof define === 'function' && define.amd){define(['jquery','datatables.net'],function($){R7MM5.b9t();return factory($,window,document);});}else if(typeof exports === 'object'){module.exports=function(root,$){R7MM5.l4n();if(!root){root=window;}if(!$ || !$.fn.dataTable){$=require('datatables.net')(root,$).$;}return factory($,root,root.document);};}else {factory(jQuery,window,document);}})(function($,window,document,undefined){var L7K=R7MM5;var z1s="mit";var A8Y="wrapper";var K29="pu";var X9O='Edit entry';var v9Y="utt";var Q4p="oad";var Q05='lightbox';var b9e="TED";var l76="put";var Q9x="ev";var c0K="he";var n9g='DTE_Field';var B21='individual';var X_l='<div class="DTED_Envelope_Background"><div></div></div>';var o90="editS";var Y7L="isMultiValue";var Q2q="e_";var V_u="formOptions";var X57="mi";var Q0R="proc";var m7R='bubble';var q5R="_lastSet";var f_P="div";var W8O="pus";var A7F="]";var a$t="v>";var O9S="_actionClass";var N4N="Time";var D$d="lds";var a8A='&';var s3I="ten";var v5C="DT";var y$3="To";var p3_="inline";var q0i="_assembleMain";var Q1x="ne";var J4Q='</label>';var h2x="inA";var O4i="dataT";var X7D='Previous';var Y4R="lo";var p88=1;var h$1="_cru";var K_l="Fi";var X3E="url";var M5g="r";var I_T="fadeOut";var q$8="rro";var h1_="iv>";var n68='This input can be edited individually, but not part of a group.';var X70="_F";var X5j="_preopen";var E20="ength";var U0V="dex";var f2Z='string';var V3f="Form";var f3d="inde";var s6E="U";var a4H="len";var S5B="displayController";var w2_="cells(";var Y0W="ni";var I6l="multiGet";var F4U="focus";var h6w="ed";var d9y='inline';var o1V="rows";var S8c="sses";var P7P="op";var L7F='changed';var I$y="ons";var c8N="ackgro";var U2L="ir";var e65="columns";var S1J="label";var Z4v='DTE_Footer_Content';var b_g="ch";var L7t='div.';var b8q="line";var g9l="idSrc";var s1N="bodyContent";var T2X="dSrc";var h66="inli";var K5K='remove';var n9X="ody";var r0C='div.DTED_Lightbox_Content_Wrapper';var c8Q="of";var B4n="pa";var c_1="<d";var E5x="dC";var N7S="bject";var Z_n="inpu";var X7W="attr";var Z9H="tr";var n13='edit';var F4f="includeFields";var K9F='block';var z7S="creat";var r$P="</d";var s$8="display";var L5i="rol";var j3_="_Upload";var y58="ajax";var H_N="leng";var c3J="rows().delete(";var A33="y";var B1d="Ap";var m6_=">";var v82="on>";var o0s='processing';var H_U="rc";var v_X="_enabled";var b$_='Wed';var g9c="r()";var l$M="wr";var c4D="Ed";var j_g="id";var c4i='all';var S65="ut";var H9D="off";var E3j="ove";var I3h="abled";var N2_='multi-restore';var Q5S="globalError";var O5i="Cl";var e$Y="css";var N98="De";var L1S="<div class=\"DTED_Envelope";var e9x="disable";var f1t="_fie";var a4Y="taTable";var i51="/d";var d8o="lac";var n3n="editor";var p9u="rep";var L02=" values";var M30="sAr";var b0c="onComplete";var v2$="alue";var V56=")";var t22="ource";var W7n="<";var o0u='body';var m7i="st";var M0v="Array";var R5K="Id";var A46='focus';var T4C="oot";var K8n="str";var O_M="butto";var K$Z="append";var w_4="ie";var I$g="irm";var B1g="fie";var H7v="call";var N0r='opened';var f4G="nline_Buttons";var P2r="rowIds";var C0b='DTE_Inline_Field';var u71="draw";var q16="lass";var E$y="xtend";var W39="itor";var v4c="ad";var W6y="ct";var q4l="select";var R3b="_animate";var c1X="isa";var L8N="cla";var Q3m="Marc";var I$v="nfo";var k0w="Up";var t8A="extend";var e8D="ssing";var x8P="fad";var W6N="fieldErrors";var c7m="then";var t_d="vent";var m9k='keydown';var S7m="su";var J6Y="create";var T03="h";var h4F="_c";var j8l="ontainer\">";var x4v='Create new entry';var i42="editSing";var t12='DTE_Action_Create';var t8v='text';var B91="ti";var g8N="DTED_Lightbox_Content\">";var Z71="_d";var j6w='<span>';var e6C="_inp";var B7u="valFromData";var W6P="ld";var w55="opt";var G1V="c";var g5F="_Content";var K7Q="push";var i$c="unselectedValue";var N8j="eq";var y6x="led";var C1v="form";var c5m="mu";var U5C="rm";var w36="ler";var f0k="mov";var Q6c="settings";var J72=20;var Z9k="_Envelope_Close\"></div>";var E1k="od";var q8u="ef";var X7l="sing";var H9z="aj";var j_L="_ev";var b3X="lengt";var F8J="j";var T0o="iSet";var M3d="erro";var N48="_show";var T5f=true;var n4o="inError";var H4b="emo";var V0u='Update';var z6I='DTE_Form_Info';var Y6A="ay";var O7k="order";var m5s=' ';var u2A="get";var I$5="even";var u2Z="ll";var w9$="fn";var l32="ted";var q_V="title";var d1k="mode";var G97="DTE_Bubble_Backg";var y4B='▶';var q5r="ind";var J$l="action";var m9t="closeIcb";var N1t="or_val";var A_J="me";var c5s="us";var v1C='Editor requires DataTables 1.10.20 or newer';var x1j='August';var K8C="_focus";var h0t="wireFormat";var s79="iner";var Y4X="bu";var b7h='readonly';var O0R='DTE_Bubble_Triangle';var q1t="dataTable";var q_9="fieldTypes";var I_3="_Fiel";var S15=").edit()";var c4R='May';var d66="Octob";var j3s="appe";var m03="apply";var l0q="safeId";var P93="engt";var B1B="i18n";var y1t="join";var S4_="fiel";var t4U="content";var Z1n="mult";var b2R='DTE_Header';var B23="find";var K5f="/";var d$S="bubb";var u3Y="b";var s$G="blur";var Y7w='[data-editor-value]';var a6X="ue";var I_u="wn";var R$h="background";var U9d="npu";var a8x="H";L7K.b9t();var h8a="multiReset";var J4D="iv cla";var W2s="upd";var p7R="com";var f63="ss";var R$w="split";var e2c="bled";var g4M="dy";var M2R="ipOpts";var C_e="tt";var K23="ds";var W_A="_dataSource";var X0T="ispl";var s8Z="_ne";var e1q="_edito";var Y8J="_crudArgs";var a4w="appendTo";var B6V="pend";var f5h="sl";var S0u='action';var f1c="offsetHeight";var u9Q="ield";var g9B="isA";var s65="_Sha";var G4F="ubmit";var y3C="Arr";var h3Y="or";var a4a="_";var y$0=',';var u2P='multi-info';var Q$0="Jun";var U15="ts";var B3I="_a";var D05="toA";var Z29="ate";var z2m="_i";var p_K="<div class=\"DTED_Lightbo";var f6m="pts";var d_P="s=\"DTED ";var S02="edSingle";var R$B="C";var r8w="StateError";var k42="displayed";var p6S="ex";var u3Z="rmat";var F8G="\"><";var g0j="=\"";var L39="ngle";var y3A="le";var P4H="D";var p3q="removeSingle";var d1x="update";var c6X="veSi";var w3A="register";var w5Y="addClass";var S8t='input:last';var Q06="ppe";var K0I='input';var P37="ocu";var t47="oc";var o2D="aTabl";var y64="html";var W1Z="te";var w66="ngth";var d7R="bub";var F8P='September';var Y1M=".";var i7A="<div class=\"DTED_Light";var p1Z="eac";var j7l="file";var R$C="emb";var H_b="sabled";var N0G="prototype";var H6f="plete";var w88='DT_RowId';var t84='start';var B8r="editField";var P5M='main';var h07="x";var Y9F="ve";var e5N="Da";var N7C='none';var N69="der";var n_d="formInfo";var Z6k="multiInfoShown";var v5z="_message";var Z7G="ush";var D47="sp";var T9z="pl";var C3$="rror";var P1D="is";var y5V="_editor_val";var u5I="editOpts";var U50="\"";var u49="cancelled";var R5y='DTE_Processing_Indicator';var N3_="dNames";var L6_="set";var P1R="ction";var e$Q="T";var B4F="noEdi";var l2Y='';var p6g="sa";var Q42="box_C";var N2b="_m";var k4I="sub";var g$f="eld";var c26="fields";var h5X="em";var b2l="Bubble_Table";var a5u="ow\"></div>";var B_I="DTE_Field_";var A0_=2;var b$w="attach";var u1c="B";var Z4_="E_";var f$O="ea";var I58="classes";var J7I="il";var k3H="_addOptions";var p_6="_fnExtend";var Z3n='Create';var I1e="formMessage";var d3C="/div>";var N2u="joi";var y7S='-';var J01="to";var l4Q="for";var h3o="status";var S_L="node";var U2w="Jul";var k6I='display';var U$q="resi";var n01="value";var J2a="yp";var x_3="_da";var O4U="which";var L98="DTE DTE_Bubbl";var x3l="ns";var R9z="F";var S69="=\"D";var I_d="dom";var C4i="fo";var R2T="ic";var O_X="lec";var z79="protot";var J7D="s";var l$k="row.create(";var Q_Y="ter";var J5W="_tidy";var j0D="setFocus";var i1P="checked";var O$H="om";var Q7E="DTED_Lightbox_Wrapper\">";var V72="defaults";var B3P="n";var g4l="displayFields";var E$j="width";var N5y="TE";var k38="ent";var h5v="tons-edit";var d7Z="row().edit(";var C1w="ax";var Q$G="ri";var I6K="ion";var p6w='Are you sure you wish to delete 1 row?';var g0I="def";var P5B="_e";var Z0V="et";var K0V="ep";var A71="removeClass";var B4K="Table";var J94="isArra";var Q7u='DTE_Label_Info';var I3i="lt";var L4f="editorFie";var d52="t>";var r4o="DTE_F";var g6C="ode";var m1U="Date";var X9m="ces";var j8X="footer";var m0K="hide";var t6m="ield_Messa";var h7d="ht";var U$h="<div class=\"";var j4C="ar";var U1e="stro";var F0A="th";var H$E='close';var r5G="internalI18n";var O8F="ven";var Q7L='<input id="';var h34="ata";var A3m='Close';var m59="p";var m2K="remo";var O81="table";var O7A="destroy";var a59="splice";var h13="v";var D4D="clo";var j$X="ke";var x4w="upload";var P3q="type";var C7y="all";var Y2o="d_InputCont";var d1B="ttr";var z3e="container";var C7n="prepend";var t0$="fi";var X2m=null;var P75="dataSources";var J7h="proce";var Z9Z="unique";var e0Y="cont";var C5B="re";var Y47="tons";var q3W="cells";var i20="ra";var J6o="pp";var z$n="name";var b_J="Api";var n2y="filter";var l3o="uplo";var R2K="disab";var i$X="submittable";var A$Q="upl";var A6P='xhr.dt';var g74="bel";var z7f='row().delete()';var d4Q="detach";var L2R="versionCheck";var U9E='_';var K3s='DTE_Field_Info';var L0p="di";var w6m="ntent";var f9u="app";var v5K="isPlainObject";var D4c='DTE_Field_Name_';var Y_Y="va";var I3d="stopImmediatePropagation";var h4D="hi";var x1U="tyl";var T6d="los";var x6v="add";var a3q="<div class=\"DTED";var P53="DateTime";var D5a="Multiple";var T1u="os";var m9z="e";var N$3="r_val";var f4W="sPair";var n2c="Ne";var R3o="row";var Z6h="_pr";var m6A="_event";var x8u="but";var O8Y='fields';var a$N="ents";var H7H="it";var D5N='multi-value';var U_$="_input";var l7$="f";var b$A="ings";var z4s="tio";var p5N="hasClass";var C_A="es";var E_V="modifier";var f4F="_eve";var r65='</div>';var w2L="pi";var e_S="_Line";var m2_="length";var F0f="In";var J4v="edito";var C$3="bubble";var a_U='submitComplete';var L0x="ng";var o0J='DTE DTE_Inline';var h42="n/12\">More information</a>).";var e0u="ror";var I_Z="oFeatures";var P$P='Mon';var A_8="end";var h55="empty";var G0V="opti";var E3o="l";var I_z="_processing";var x0u="Class";var Z6g="_val";var J3a="dt";var q9o="ge";var z6t="inArray";var e2R='<div class="';var Y9$="spl";var r5C="ght";var Z69="idS";var F2x="V";var h7r="separator";var G5L="_cl";var e2b="ren";var E6E="ord";var q5o="multiple";var I5k=".edit()";var M1L="i1";var V3V="Typ";var B$A='#';var k_9="_edit";var S_n="tml";var u1t='keyless';var G19="mod";var V8g="ext";var L4i="tit";var v96='open';var n3b="gt";var E$K="ueCheck";var C3h='Tue';var p1O="-";var z4O="prop";var Z$s='DTE_Form_Error';var V_6="formButtons";var k_N="ble";var Y4Q="A";var w8d="round";var e0d="ngt";var d1s="top";var Z2T='Delete';var F6G="m";var R41='disabled';var u5m="elds";var x2M="multi";var n_8="x_Close\"></di";var E43="subm";var d8z="displ";var D6L="pr";var N5w="E";var V8x="nd";var i_B="D_Envelope_Container\"></div>";var L6e="_formOptions";var U$t="_in";var R3d="w";var F33="edit";var T3Q="sh";var o80="iel";var s95="bac";var b$D="dit";var M1q=0;var d9o="<div class";var g5T="on";var H21="remove";var o0f="d_Error";var l9T="en";var R7u="inl";var C5C="pe";var v4B='function';var k4J="e(";var E6d="cu";var v4P="format";var Z_Z="_close";var A1I="DTE_L";var z7H="essing";var q0R="ma";var b5E="_noProcessing";var t_M="i";var U4g="Dec";var U7v='json';var C7G="ns-remove";var u4Q="DTE_Form_Conte";var h0i="imate";var E7g="inp";var v5s='cell().edit()';var g8a="drawType";var N4h='submit';var L5h="yl";var F3w="actio";var c0B="pairs";var Z0p="options";var w8N='draw';var q4a='DTE_Header_Content';var R4H='create';var o8R="_submitSuccess";var x6u="a";var w7B="ator";var e1t="</";var f5c="eUp";var Y26="las";var U04="ho";var V3z="fu";var w4j="_t";var z0E="eng";var C1g='January';var K8R='">';var G2P="DTE_";var L16="ss=\"DTE";var i4l="crea";var a55='click';var K60="ult";var j_l="index";var w9O="ov";var v12="I";var s$6="ight";var c1J="ck";var R70="ssag";var e$3="cr";var x3a="at";var A$X="sage";var d9r="nput";var c_k="able";var J_A="yle";var i0u="indexes";var P99="lay";var B5w='DTE_Action_Remove';var u5k="bubblePosition";var n7Z="ndo";var t09="rop";var l1E="unshift";var V4_="E_Body";var F8y="buttons";var k82="do";var C1I=" cha";var Y8Y="field";var Z1a="ing";var o2c='"></div>';var K2t="er";var R_p="rem";var w3s="cl";var B5d='DTE_Body';var j0k="PO";var n1O='rows';var y2Y="tions";var C7j="no";var y6j=15;var V6H="de";var b00="co";var f$W='1';var y4x="la";var l6s="as";var c3v="g";var E$_="A system error has occurred (<a target=\"_blank\" href=\"//datatables.net/t";var f0L=600;var A0g="conf";var X8F="editFields";var Y5w="load";var I9U="k";var U4Z="Edi";var g3z="enable";var Y8m=50;var Z2U="con";var M5K='icon close';var b6X="ws(";var T5j="rr";var L7j="ur";var S_T=500;var s_q='DTE_Form_Buttons';var f6f="mo";var t5D="nod";var i12="DTE_Field_In";var o8Y="each";var L9S="_Lightbox_Content_Wrapper\">";var J3J="n>";var Y3a="style";var O98="ml";var f$8="ta";var B9k="isEmptyObject";var E4r="bubbleNodes";var S4l='keyup';var Y8x="ditO";var M$_="target";var C8B="children";var f3N="input";var z$j="_postopen";var n7U='files()';var x3g="act";var Z0l="ray";var O6e="animate";var d30="ows";var x_w="error";var j9Y="bm";var H42="ima";var J$_="ro";var M6A="<div";var H5f="close";var Z9i="editorFields";var o$i="urce";var c0f='focus.editor-focus';var v8k='Are you sure you wish to delete %d rows?';var D4J="preventDefault";var H_Y=13;var R5D="eys";var r8u="ab";var I6y="placeholder";var g49=false;var p$C="_picker";var G2T="abel";var H0X="ce";var h$P="dataSrc";var w$a="col";var F3h="init";var J6c="unct";var E13="slice";var p71="li";var k4i="ose";var n91="exte";var Y3o="_typeFn";var z9D="ound";var T8j="multiSet";var d5q="one";var T4d="N";var f7S="ses";var I8x="in";var n9v='DTE_Action_Edit';var U21="valToData";var h4W="isPlainO";var K_Z="DTE";var A3U="eate";var h4t='_basic';var m1I="val";var h0Q='The selected items contain different values for this input. To edit and set all items for this input to the same value, click or tap here, otherwise they will retain their individual values.';var X1n="gth";var k_j="1";var p_t="s=\"";var B$4="fe";var A43="ff";var m13="_fieldNames";var h4Q="ass";var B7S="bServerSide";var J86="se";var B87="_l";var V_S="open";var k1z="nt";var P8O='Minute';var D$8="processing";var M51='number';var T$I="button";var k_l='preOpen';var l9q="TE_I";var s8a="fun";var m0I="move";var h7$="uttons";var r$J="any";var S9O="map";var i3I="und";var m14='buttons-create';var x_C="header";var u8o='<';var F$o="t";var q79="Fr";var f4c="submit";var G91="message";var u4p="tion";var C10="ca";var M9j="10.20";var u13="ltiS";var E2R="el";var M49="dat";var v$K="io";var S7E='Second';var y4_="att";var Q9c='November';var a0r="selec";var h6q="nges";var t7n="momentStrict";var j4l="mul";var P3c="edi";var y$P="E_Field_";var F7o="_eventName";var p2t="disp";var W5M="ame";var F2L="clear";var Y8o="ach";var p1E="opts";var J6b="indexOf";var D5l="pen";var a6Q=25;var H0m="ap";var H5X="ac";var Y$q="files";var g5V="spla";var d$I="da";var X2T="optionsPair";var X1g="al";var e5S="Of";var I$D="eldTypes";var J3O="u";var K2Z=" ";var T_U="bl";var W14='<div class="DTED DTED_Envelope_Wrapper">';var O5R="nc";var m6s="Editor";var a1Y="maybeOpen";var X7U="nli";var z3I='<div class="DTED_Lightbox_Background"><div></div></div>';var N3t="ow";var j7f='>';var G1Q="ssa";var K$w="DTE_Bubbl";var p5M="isArray";var s7M="S";var o1S="replace";var k7$="18";var t5Z="div.D";var B73="ect";var n89='closed';var T0K="ril";var z4k="data";var F2P="taSour";var U4E="lectedSingle";var Z2C="<div clas";var H6G="ubmi";var v8g="ss=\"";var x4g="o";var Y22="_fieldFromNode";var J_0="d";var c1p="tend";var l2U="addBack";var N0a="trigger";var U9P="exten";var w62="an";var S9n="rray";var T95='row';var l79="8";var k4E="8n";var U6c="pla";var I$0="_editor";var x0n="pro";var p2u='February';var s7w="_displayReorder";var h9V=L4f;h9V+=D$d;var P0N=m9z;P0N+=h07;P0N+=F$o;var A7b=m9z;A7b+=h07;A7b+=F$o;var X6A=c4D;X6A+=H7H;X6A+=x4g;X6A+=M5g;var W5V=P4H;W5V+=h34;W5V+=B4K;var r0J=J86;r0J+=U4E;var C1K=C5B;C1K+=f6f;C1K+=h13;C1K+=m9z;var e3T=p6S;e3T+=s3I;e3T+=J_0;var c9A=C5B;c9A+=f6f;c9A+=c6X;c9A+=L39;var X4x=q4l;X4x+=S02;var r9m=i42;r9m+=y3A;var d4g=m9z;d4g+=J_0;d4g+=t_M;d4g+=F$o;var d6k=V8g;d6k+=l9T;d6k+=J_0;var r$L=o90;r$L+=I8x;r$L+=c3v;r$L+=y3A;var s44=J7D;s44+=m9z;s44+=O_X;s44+=l32;var E9f=O_M;E9f+=C7G;var Y_j=a0r;Y_j+=l32;var R$3=Y4X;R$3+=F$o;R$3+=h5v;var e_z=d$I;e_z+=F$o;e_z+=o2D;e_z+=m9z;var l02=l7$;l02+=B3P;var X4V=x4g;X4V+=B3P;var N0T=t0$;N0T+=E3o;N0T+=k4J;N0T+=V56;var a6o=w2_;a6o+=S15;var y62=c3J;y62+=V56;var N$f=J$_;N$f+=b6X;N$f+=V56;N$f+=I5k;var C85=d7Z;C85+=V56;var P8r=l$k;P8r+=V56;var v2w=J4v;v2w+=g9c;var U7N=B1d;U7N+=t_M;var Z2b=l7$;Z2b+=B3P;var l7d=l7$;l7d+=B3P;var Y3r=n91;Y3r+=V8x;var J45=V8g;J45+=A_8;var Z0E=m9z;Z0E+=h07;Z0E+=F$o;Z0E+=A_8;var H2p=p6S;H2p+=c1p;var M0I=m9z;M0I+=h07;M0I+=s3I;M0I+=J_0;var U2M=O4i;U2M+=r8u;U2M+=E3o;U2M+=m9z;var W5h=l7$;W5h+=B3P;var n8X=d$I;n8X+=a4Y;var D7=l7$;D7+=B3P;var H_=U$h;H_+=g8N;var G3=d9o;G3+=S69;G3+=b9e;G3+=L9S;var D$=i7A;D$+=Q42;D$+=j8l;var a6=Z2C;a6+=d_P;a6+=Q7E;var y4=p_K;y4+=n_8;y4+=a$t;var D3=M5g;D3+=N3t;var X8=r$P;X8+=t_M;X8+=h13;X8+=m6_;var g8=c_1;g8+=J4D;g8+=L16;g8+=i_B;var n3=L1S;n3+=s65;n3+=J_0;n3+=a5u;var D6=a3q;D6+=Z9k;var q$=P4H;q$+=e$Q;q$+=N5w;var A3=P4H;A3+=l9q;A3+=f4G;var K1=v5C;K1+=Z4_;K1+=V3f;var X3=u4Q;X3+=k1z;var W4=u3Y;W4+=F$o;W4+=B3P;var k4=u3Y;k4+=F$o;k4+=B3P;var q2=K_Z;q2+=X70;q2+=T4C;q2+=K2t;var J3=v5C;J3+=y$P;J3+=V3V;J3+=Q2q;var k9=x2M;k9+=p1O;k9+=B4F;k9+=F$o;var U8=r4o;U8+=t6m;U8+=c3v;U8+=m9z;var D2=v5C;D2+=N5w;D2+=I_3;D2+=o0f;var P6=A1I;P6+=G2T;var N3=K_Z;N3+=I_3;N3+=Y2o;N3+=L5i;var q6=i12;q6+=l76;var m6=B_I;m6+=r8w;var F2=J_0;F2+=c1X;F2+=e2c;var U5=L98;U5+=m9z;var y7=G2P;y7+=b2l;var R5=K$w;R5+=m9z;R5+=e_S;R5+=M5g;var e9=G97;e9+=w8d;var F6=v5C;F6+=V4_;F6+=g5F;var U6=x4g;U6+=Y4Q;U6+=m59;U6+=t_M;var A6=H5X;A6+=B91;A6+=x4g;A6+=B3P;var i1=N98;i1+=E3o;i1+=m9z;i1+=W1Z;var x8=D5a;x8+=L02;var T_=s6E;T_+=n7Z;T_+=C1I;T_+=h6q;var K$=E$_;K$+=h42;var t6=U4Z;t6+=F$o;var H7=s7M;H7+=x3a;var E6=q79;E6+=t_M;var B_=e$Q;B_+=T03;B_+=J3O;var K2=s7M;K2+=J3O;K2+=B3P;var m_=n2c;m_+=h07;m_+=F$o;var t0=U4g;t0+=R$C;t0+=K2t;var J7=d66;J7+=K2t;var B6=U2w;B6+=A33;var h7=Q$0;h7+=m9z;var e6=Y4Q;e6+=m59;e6+=T0K;var E2=Q3m;E2+=T03;var J5=a8x;J5+=x4g;J5+=L7j;var P9=m59;P9+=F6G;var U7=x6u;U7+=F6G;var Y1=T4d;Y1+=m9z;Y1+=R3d;var N1=m9z;N1+=E$y;var K8=U9P;K8+=J_0;var P5=C4i;P5+=E6d;P5+=J7D;var T9=G1V;T9+=T6d;T9+=m9z;var H2=u3Y;H2+=E3o;H2+=L7j;var f9=k_j;f9+=Y1M;f9+=M9j;'use strict';L7K.R0=function(O2){L7K.l4n();if(L7K)return L7K.a0(O2);};(function(){var X_M=1657843200;var y9n="Your tri";var c2k='s';var l$_=1606;var V0f="";var f_t="les Editor trial info - ";var s8J='for Editor, please see https://editor.datatables.net/purchase';var T0F="5b4";var g7E="u for trying DataTables Editor\n\n";var k0M=' remaining';var Y9p=2835051926;var S6Q='Editor - Trial expired';var k37=71;var m67=5240;var v7m="getTime";var j9D=1000;var N1M="aba4";var K0o=7;var j6e="5118";var g9C="log";var z1b=24;var X_h="2";var L6f=56;var x1h="9";var R$z="5d2a";var w6g="fcea";var E8Y=" expired. To purchase a license ";var V74="8a";var w1d=' day';var q5I="Thank yo";var c98="cei";var J1y="9f78";var Q57="4";var e5t="DataTab";var A3H="9835";var D5r="al has now";var n9q="7";var G_G=60;var e_p="6";var Z5=x6u;Z5+=l7$;L7K.l4n();Z5+=X_h;Z5+=n9q;var e1=u3Y;e1+=T0F;var i5=J_0;i5+=u3Y;i5+=u3Y;i5+=J_0;var M4=c98;M4+=E3o;var f4=G1V;f4+=k_j;f4+=X_h;f4+=e_p;L7K.C1=function(S7){L7K.b9t();if(L7K && S7)return L7K.a0(S7);};L7K.V4=function(I6){L7K.l4n();if(L7K && I6)return L7K.a0(I6);};L7K.D5=function(Q0){L7K.b9t();if(L7K && Q0)return L7K.a0(Q0);};L7K.B$=function(o9){L7K.l4n();if(L7K && o9)return L7K.a0(o9);};L7K.m8=function(r0){L7K.l4n();if(L7K)return L7K.l3(r0);};L7K.W1=function(x7){L7K.l4n();if(L7K)return L7K.a0(x7);};var remaining=Math[L7K.o_(f4)?M4:V0f]((new Date((L7K.B1(i5)?Y9p:X_M) * (L7K.W1(J1y)?j9D:l$_))[L7K.R0(R$z)?V0f:v7m]() - new Date()[L7K.m8(e1)?v7m:V0f]()) / ((L7K.B$(j6e)?j9D:m67) * (L7K.D5(Z5)?L6f:G_G) * (L7K.V4(A3H)?G_G:k37) * z1b));if(remaining <= M1q){var m7=y9n;m7+=D5r;m7+=E8Y;var N7=k_j;N7+=l7$;N7+=x1h;N7+=l79;var M_=q5I;M_+=g7E;L7K.J_=function(U4){L7K.l4n();if(L7K)return L7K.l3(U4);};L7K.f$=function(V0){L7K.b9t();if(L7K)return L7K.a0(V0);};L7K.e_=function(z6){L7K.l4n();if(L7K && z6)return L7K.a0(z6);};alert((L7K.e_(N1M)?M_:V0f) + (L7K.f$(N7)?m7:V0f) + s8J);throw L7K.J_(w6g)?S6Q:V0f;}else if(remaining <= K0o){var c5=Q57;c5+=V74;c5+=k_j;var R3=e5t;R3+=f_t;var u9=X_h;u9+=u3Y;u9+=d$I;L7K.e$=function(X2){if(L7K)return L7K.a0(X2);};console[L7K.C1(u9)?V0f:g9C](R3 + remaining + (L7K.e$(c5)?w1d:V0f) + (remaining === p88?l2Y:c2k) + k0M);}})();var DataTable=$[w9$][q1t];if(!DataTable || !DataTable[L2R] || !DataTable[L2R](f9)){throw new Error(v1C);}var formOptions={buttons:T5f,drawType:g49,focus:M1q,message:T5f,nest:g49,onBackground:H2,onBlur:H$E,onComplete:H$E,onEsc:T9,onFieldError:P5,onReturn:N4h,scope:T95,submit:c4i,submitHtml:y4B,submitTrigger:X2m,title:T5f};var defaults$1={actionName:S0u,ajax:X2m,display:Q05,events:{},fields:[],formOptions:{bubble:$[K8]({},formOptions,{buttons:h4t,message:g49,submit:L7F,title:g49}),inline:$[t8A]({},formOptions,{buttons:g49,submit:L7F}),main:$[N1]({},formOptions)},i18n:{close:A3m,create:{button:Y1,submit:Z3n,title:x4v},datetime:{amPm:[U7,P9],hours:J5,minutes:P8O,months:[C1g,p2u,E2,e6,c4R,h7,B6,x1j,F8P,J7,Q9c,t0],next:m_,previous:X7D,seconds:S7E,unknown:y7S,weekdays:[K2,P$P,C3h,b$_,B_,E6,H7]},edit:{button:t6,submit:V0u,title:X9O},error:{system:K$},multi:{info:h0Q,noMulti:n68,restore:T_,title:x8},remove:{button:Z2T,confirm:{1:p6w,_:v8k},submit:i1,title:Z2T}},idSrc:w88,table:X2m};var settings={action:X2m,actionName:A6,ajax:X2m,bubbleNodes:[],closeCb:X2m,closeIcb:X2m,dataSource:X2m,displayController:X2m,displayed:g49,editCount:M1q,editData:{},editFields:{},editOpts:{},fields:{},formOptions:{bubble:$[t8A]({},formOptions),inline:$[t8A]({},formOptions),main:$[t8A]({},formOptions)},globalError:l2Y,id:-p88,idSrc:X2m,includeFields:[],mode:X2m,modifier:X2m,opts:X2m,order:[],processing:g49,setFocus:X2m,table:X2m,template:X2m,unique:M1q};var DataTable$5=$[w9$][q1t];var DtInternalApi=DataTable$5[V8g][U6];function objectKeys(o){var u8k="hasOwnProperty";var out=[];for(var key in o){if(o[u8k](key)){var J6=W8O;J6+=T03;out[J6](key);}}L7K.l4n();return out;}function el(tag,ctx){var W9a="dte-e=\"";var C3M="*[data-";var g0=U50;g0+=A7F;var s2=C3M;s2+=W9a;L7K.b9t();if(ctx === undefined){ctx=document;}return $(s2 + tag + g0,ctx);}function safeDomId(id,prefix){L7K.b9t();var R7=C5B;R7+=m59;R7+=d8o;R7+=m9z;var V$=K8n;V$+=Z1a;if(prefix === void M1q){prefix=B$A;}return typeof id === V$?prefix + id[R7](/\./g,y7S):prefix + id;}function safeQueryId(id,prefix){var i1X="\\";var J2Q="$";var n4=i1X;n4+=J2Q;n4+=k_j;L7K.b9t();if(prefix === void M1q){prefix=B$A;}return typeof id === f2Z?prefix + id[o1S](/(:|\.|\[|\]|,)/g,n4):prefix + id;}function dataGet(src){var A_a="_fnGetObjectDataFn";L7K.b9t();return DtInternalApi[A_a](src);}function dataSet(src){var F0e="_fnSetObjectDataFn";return DtInternalApi[F0e](src);}var extend=DtInternalApi[p_6];function pluck(a,prop){var out=[];$[o8Y](a,function(idx,elIn){L7K.l4n();out[K7Q](elIn[prop]);});return out;}function deepCompare(o1,o2){var O35="je";var n9l="obje";var K9s='object';var x0b="ob";var S8=a4H;S8+=c3v;S8+=F0A;var D0=n9l;D0+=G1V;D0+=F$o;var T3=x0b;T3+=O35;T3+=G1V;T3+=F$o;if(typeof o1 !== T3 || typeof o2 !== D0){return o1 == o2;}var o1Props=objectKeys(o1);var o2Props=objectKeys(o2);if(o1Props[m2_] !== o2Props[m2_]){return g49;}for(var i=M1q,ien=o1Props[S8];i < ien;i++){var propName=o1Props[i];if(typeof o1[propName] === K9s){if(!deepCompare(o1[propName],o2[propName])){return g49;}}else if(o1[propName] != o2[propName]){return g49;}}return T5f;}var _dtIsSsp=function(dt,editor){var A9R="ttings";var O1=C7j;O1+=Q1x;var Z7=J86;Z7+=A9R;return dt[Z7]()[M1q][I_Z][B7S] && editor[J7D][u5I][g8a] !== O1;};var _dtApi=function(table){var i8g="aTable";var T7E="DataTable";var l9=M49;l9+=i8g;return table instanceof $[w9$][l9][b_J]?table:$(table)[T7E]();};var _dtHighlight=function(node){node=$(node);setTimeout(function(){L7K.l4n();var d2v='highlight';var r1=v4c;r1+=J_0;r1+=x0u;node[r1](d2v);setTimeout(function(){var z0B=550;var a0y='noHighlight';var J7a="hig";var N7g="hli";var s$=J7a;s$+=N7g;s$+=r5C;var x4=v4c;x4+=J_0;x4+=R$B;x4+=q16;L7K.b9t();node[x4](a0y)[A71](s$);setTimeout(function(){var Z1=m2K;Z1+=h13;Z1+=m9z;Z1+=x0u;node[Z1](a0y);},z0B);},S_T);},J72);};var _dtRowSelector=function(out,dt,identifier,fields,idFn){var X0=m9z;X0+=x6u;X0+=G1V;L7K.b9t();X0+=T03;dt[o1V](identifier)[i0u]()[X0](function(idx){var a43=14;var v0q="denti";L7K.b9t();var H1P="Unable";var A9h=" to find row i";var V0n="fier";var o7=B3P;o7+=E1k;o7+=m9z;var z3=J_0;z3+=x6u;z3+=f$8;var row=dt[R3o](idx);var data=row[z3]();var idSrc=idFn(data);if(idSrc === undefined){var I8=H1P;I8+=A9h;I8+=v0q;I8+=V0n;var L3=m9z;L3+=q$8;L3+=M5g;Editor[L3](I8,a43);}out[idSrc]={data:data,fields:fields,idSrc:idSrc,node:row[o7](),type:T95};});};var _dtFieldsFromIdx=function(dt,fields,idx,ignoreUnknown){var f7i="itF";var C$G="mDa";var v8n="aoColumns";var S8V='Unable to automatically determine field from source. Please specify the field name.';var v9X=11;var v9=m9z;v9+=x6u;v9+=G1V;v9+=T03;var A1=C$G;A1+=f$8;var a8=h6w;a8+=f7i;a8+=w_4;a8+=W6P;var col=dt[Q6c]()[M1q][v8n][idx];var dataSrc=col[a8] !== undefined?col[B8r]:col[A1];var resolvedFields={};var run=function(field,dataSrcIn){if(field[z$n]() === dataSrcIn){var a$=B3P;a$+=x6u;a$+=F6G;a$+=m9z;resolvedFields[field[a$]()]=field;}};$[v9](fields,function(name,fieldInst){if(Array[p5M](dataSrc)){for(var _i=M1q,dataSrc_1=dataSrc;_i < dataSrc_1[m2_];_i++){var data=dataSrc_1[_i];run(fieldInst,data);}}else {run(fieldInst,dataSrc);}});if($[B9k](resolvedFields) && !ignoreUnknown){var D_=K2t;D_+=J$_;D_+=M5g;Editor[D_](S8V,v9X);}L7K.b9t();return resolvedFields;};var _dtCellSelector=function(out,dt,identifier,allFields,idFn,forceFields){if(forceFields === void M1q){forceFields=X2m;}L7K.b9t();var cells=dt[q3W](identifier);cells[i0u]()[o8Y](function(idx){var V5q="fix";var B72="edNode";var L7a="deNam";var f33="keys";var w1N="tta";var P98="cell";var V8V="tachFields";var q2K="coun";var O_H="tac";var c5E="attachFields";var k9f="chFiel";var f$q="umn";var n3N="fixedNode";var l28="bjec";var l4=I9U;l4+=R5D;var e8=B3P;e8+=x4g;e8+=L7a;e8+=m9z;var c3=x4g;c3+=l28;c3+=F$o;var a5=q2K;a5+=F$o;var V6=w$a;V6+=f$q;var d4=M5g;d4+=x4g;d4+=R3d;var cell=dt[P98](idx);var row=dt[d4](idx[R3o]);var data=row[z4k]();var idSrc=idFn(data);var fields=forceFields || _dtFieldsFromIdx(dt,allFields,idx[V6],cells[a5]() > p88);var isNode=typeof identifier === c3 && identifier[e8] || identifier instanceof $;var prevDisplayFields;var prevAttach;var prevAttachFields;if(Object[l4](fields)[m2_]){var d$=B3P;d$+=x4g;d$+=J_0;d$+=m9z;var a4=V5q;a4+=B72;var M3=x6u;M3+=F$o;M3+=f$8;M3+=b_g;var V7=m59;V7+=J3O;V7+=J7D;V7+=T03;var X7=x6u;X7+=w1N;X7+=k9f;X7+=K23;var Z$=x3a;Z$+=V8V;var w4=M5g;w4+=x4g;w4+=R3d;if(out[idSrc]){var t4=x6u;t4+=F$o;t4+=O_H;t4+=T03;prevAttach=out[idSrc][t4];prevAttachFields=out[idSrc][c5E];prevDisplayFields=out[idSrc][g4l];}_dtRowSelector(out,dt,idx[w4],allFields,idFn);out[idSrc][Z$]=prevAttachFields || [];out[idSrc][X7][V7](Object[f33](fields));out[idSrc][M3]=prevAttach || [];out[idSrc][b$w][K7Q](isNode?$(identifier)[u2A](M1q):cell[a4]?cell[n3N]():cell[d$]());out[idSrc][g4l]=prevDisplayFields || ({});$[t8A](out[idSrc][g4l],fields);}});};var _dtColumnSelector=function(out,dt,identifier,fields,idFn){var d7=j_l;d7+=C_A;dt[q3W](X2m,identifier)[d7]()[o8Y](function(idx){_dtCellSelector(out,dt,idx,fields,idFn);});};var dataSource$1={commit:function(action,identifier,data,store){var R4W="rverSid";var h__="ild";var C47="owId";var y3L="searchPanes";var K0N="searchBuilder";var U3o="archPanes";var L6z="ecalc";var W_q="rebuild";var Q9R="responsive";var n2T="respo";var r2T="functi";var K8W="rebui";var E01="dP";var q6l="Se";var r_e="siv";var k2u="drawTyp";var l1L="Opt";var r7v="getDetails";var L3F="ane";var x6=B3P;x6+=x4g;x6+=B3P;x6+=m9z;var R_=k2u;R_+=m9z;var C3=h6w;C3+=H7H;C3+=l1L;C3+=J7D;var f2=y3A;f2+=L0x;f2+=F0A;var b_=m9z;b_+=b$D;var C5=M5g;C5+=C47;C5+=J7D;var u5=u3Y;u5+=q6l;u5+=R4W;u5+=m9z;var c$=J86;c$+=C_e;c$+=b$A;var that=this;var dt=_dtApi(this[J7D][O81]);var ssp=dt[c$]()[M1q][I_Z][u5];var ids=store[C5];if(!_dtIsSsp(dt,this) && action === b_ && store[P2r][f2]){var row=void M1q;var compare=function(id){L7K.l4n();return function(rowIdx,rowData,rowNode){var U1=G1V;U1+=C7y;var B5=t_M;B5+=J_0;return id == dataSource$1[B5][U1](that,rowData);};};for(var i=M1q,ien=ids[m2_];i < ien;i++){var o3=x6u;o3+=B3P;o3+=A33;try{row=dt[R3o](safeQueryId(ids[i]));}catch(e){row=dt;}if(!row[o3]()){var r$=M5g;r$+=x4g;r$+=R3d;row=dt[r$](compare(ids[i]));}if(row[r$J]() && !ssp){row[H21]();}}}var drawType=this[J7D][C3][R_];if(drawType !== x6){var u0=C5B;u0+=u3Y;u0+=J3O;u0+=h__;var r8=r2T;r8+=g5T;var T$=J86;T$+=U3o;var L$=J_0;L$+=M5g;L$+=x6u;L$+=R3d;var S5=y3A;S5+=B3P;S5+=X1n;var dtAny=dt;if(ssp && ids && ids[S5]){dt[d5q](w8N,function(){var l7=H_N;l7+=F0A;for(var i=M1q,ien=ids[l7];i < ien;i++){var Q8=x6u;Q8+=B3P;Q8+=A33;var row=dt[R3o](safeQueryId(ids[i]));if(row[Q8]()){var g4=B3P;g4+=x4g;g4+=J_0;g4+=m9z;_dtHighlight(row[g4]());}}});}dt[L$](drawType);if(dtAny[Q9R]){var s1=M5g;s1+=L6z;var f6=n2T;f6+=B3P;f6+=r_e;f6+=m9z;dtAny[f6][s1]();}if(typeof dtAny[T$] === r8 && !ssp){var p8=K8W;p8+=E3o;p8+=E01;p8+=L3F;dtAny[y3L][p8](undefined,T5f);}if(dtAny[K0N] !== undefined && typeof dtAny[K0N][u0] === v4B && !ssp){dtAny[K0N][W_q](dtAny[K0N][r7v]());}}},create:function(fields,data){var dt=_dtApi(this[J7D][O81]);if(!_dtIsSsp(dt,this)){var L8=B3P;L8+=g6C;var s6=v4c;s6+=J_0;var w3=J$_;w3+=R3d;var row=dt[w3][s6](data);_dtHighlight(row[L8]());}},edit:function(identifier,fields,data,store){var h1=B3P;h1+=d5q;var B0=m9z;L7K.l4n();B0+=Y8x;B0+=f6m;var that=this;var dt=_dtApi(this[J7D][O81]);if(!_dtIsSsp(dt,this) || this[J7D][B0][g8a] === h1){var O8=w62;O8+=A33;var rowId_1=dataSource$1[j_g][H7v](this,data);var row=void M1q;try{var Y_=M5g;Y_+=x4g;Y_+=R3d;row=dt[Y_](safeQueryId(rowId_1));}catch(e){row=dt;}if(!row[O8]()){row=dt[R3o](function(rowIdx,rowData,rowNode){var c1=t_M;c1+=J_0;return rowId_1 == dataSource$1[c1][H7v](that,rowData);});}if(row[r$J]()){var z5=J7D;z5+=m59;z5+=p71;z5+=H0X;var B9=d$I;B9+=F$o;B9+=x6u;var v6=J_0;v6+=x6u;v6+=f$8;var toSave=extend({},row[v6](),T5f);toSave=extend(toSave,data,T5f);row[B9](toSave);var idx=$[z6t](rowId_1,store[P2r]);store[P2r][z5](idx,p88);}else {var e5=M5g;e5+=N3t;row=dt[e5][x6v](data);}_dtHighlight(row[S_L]());}},fakeRow:function(insertPoint){var v_c="__dtFak";var U6k='<td>';var R2Z="class=\"dte-inlineAdd\">";var X4o=":vi";var E4E="<tr ";var w6S="oun";var w$l=":eq";var S_x="eRow";var O1A=':visible';var Q2v="draw.dte-createIn";var R7m="ssName";var i_H="ndex";var c6S="(";var b2_="sible";var J8P="ys";var R$R="0)";var U$d="column";var B3=M5g;B3+=x4g;B3+=R3d;var w$=Q2v;w$+=b8q;var J$=v_c;J$+=S_x;var k7=G1V;k7+=w6S;k7+=F$o;var L9=E4E;L9+=R2Z;var l5=F$o;l5+=c_k;var dt=_dtApi(this[J7D][l5]);var tr=$(L9);var attachFields=[];var attach=[];var displayFields={};for(var i=M1q,ien=dt[e65](O1A)[k7]();i < ien;i++){var C7=a4H;C7+=X1n;var s_=j$X;s_+=A33;s_+=J7D;var g$=w$l;g$+=c6S;g$+=R$R;var N8=G1V;N8+=m9z;N8+=u2Z;var X4=t_M;X4+=i_H;var b3=X4o;b3+=b2_;var visIdx=dt[U$d](i + b3)[X4]();var td=$(U6k)[a4w](tr);var fields=_dtFieldsFromIdx(dt,this[J7D][c26],visIdx,T5f);var cell=dt[N8](g$,visIdx)[S_L]();if(cell){var Y5=L8N;Y5+=R7m;var y_=v4c;y_+=E5x;y_+=y4x;y_+=f63;td[y_](cell[Y5]);}if(Object[s_](fields)[C7]){var D1=V8g;D1+=A_8;var I3=m59;I3+=Z7G;var R1=I9U;R1+=m9z;R1+=J8P;attachFields[K7Q](Object[R1](fields));attach[I3](td[M1q]);$[D1](displayFields,fields);}}var append=function(){var l0S='prependTo';var o86='end';var b4=u3Y;b4+=E1k;b4+=A33;var r_=H0m;r_+=C5C;r_+=V8x;L7K.l4n();r_+=y$3;var action=insertPoint === o86?r_:l0S;tr[action](dt[O81](undefined)[b4]());};this[J$]=tr;append();dt[g5T](w$,function(){append();});return {0:{attach:attach,attachFields:attachFields,displayFields:displayFields,fields:this[J7D][c26],type:B3}};},fakeRowEnd:function(){var B4j="__dtFakeRow";var W4L='draw.dte-createInline';var q3D="__dtF";var A0c="akeR";var Q6=q3D;Q6+=A0c;Q6+=x4g;Q6+=R3d;var I7=M5g;I7+=h5X;I7+=x4g;I7+=Y9F;var dt=_dtApi(this[J7D][O81]);dt[H9D](W4L);this[B4j][I7]();this[Q6]=X2m;},fields:function(identifier){var j71="ell";var M67="mns";var P8u="olumns";var n$E="colu";var A2=G1V;A2+=E2R;A2+=E3o;A2+=J7D;var E5=w$a;E5+=J3O;E5+=M67;var Z2=B1g;L7K.l4n();Z2+=E3o;Z2+=J_0;Z2+=J7D;var j9=f$8;j9+=u3Y;j9+=E3o;j9+=m9z;var idFn=dataGet(this[J7D][g9l]);var dt=_dtApi(this[J7D][j9]);var fields=this[J7D][Z2];var out={};if($[v5K](identifier) && (identifier[o1V] !== undefined || identifier[E5] !== undefined || identifier[A2] !== undefined)){var Z9=G1V;Z9+=P8u;if(identifier[o1V] !== undefined){var p6=M5g;p6+=N3t;p6+=J7D;_dtRowSelector(out,dt,identifier[p6],fields,idFn);}if(identifier[Z9] !== undefined){var T8=n$E;T8+=F6G;T8+=B3P;T8+=J7D;_dtColumnSelector(out,dt,identifier[T8],fields,idFn);}if(identifier[q3W] !== undefined){var m2=G1V;m2+=j71;m2+=J7D;_dtCellSelector(out,dt,identifier[m2],fields,idFn);}}else {_dtRowSelector(out,dt,identifier,fields,idFn);}return out;},id:function(data){var Y0=t_M;Y0+=T2X;var idFn=dataGet(this[J7D][Y0]);L7K.b9t();return idFn(data);},individual:function(identifier,fieldNames){var H8=l7$;H8+=w_4;H8+=D$d;var M9=Z69;M9+=H_U;var idFn=dataGet(this[J7D][M9]);var dt=_dtApi(this[J7D][O81]);var fields=this[J7D][H8];var out={};var forceFields;if(fieldNames){var d0=g9B;d0+=T5j;d0+=x6u;d0+=A33;if(!Array[d0](fieldNames)){fieldNames=[fieldNames];}forceFields={};$[o8Y](fieldNames,function(i,name){forceFields[name]=fields[name];});}_dtCellSelector(out,dt,identifier,fields,idFn,forceFields);return out;},prep:function(action,identifier,submit,json,store){var G8I="cancel";var l5$="cance";var _this=this;if(action === R4H){var g1=J_0;g1+=x6u;g1+=F$o;g1+=x6u;store[P2r]=$[S9O](json[g1],function(row){L7K.l4n();return dataSource$1[j_g][H7v](_this,row);});}L7K.b9t();if(action === n13){var O$=d$I;O$+=f$8;var z_=F6G;z_+=x6u;z_+=m59;var Y7=G8I;Y7+=y6x;var cancelled_1=json[Y7] || [];store[P2r]=$[z_](submit[O$],function(val,key){var v1H="tyObject";var o73="isEmp";var R$=h2x;R$+=T5j;R$+=x6u;R$+=A33;var v0=o73;v0+=v1H;L7K.l4n();return !$[v0](submit[z4k][key]) && $[R$](key,cancelled_1) === -p88?key:undefined;});}else if(action === K5K){var a3=l5$;a3+=u2Z;a3+=h6w;store[u49]=json[a3] || [];}},refresh:function(){var A$2="ja";var A3u="rel";var n5=A3u;L7K.l4n();n5+=Q4p;var Q1=x6u;Q1+=A$2;Q1+=h07;var s8=F$o;s8+=c_k;var dt=_dtApi(this[J7D][s8]);dt[Q1][n5](X2m,g49);},remove:function(identifier,fields,store){var that=this;var dt=_dtApi(this[J7D][O81]);var cancelled=store[u49];if(cancelled[m2_] === M1q){var j_=R_p;j_+=E3j;var Y4=M5g;Y4+=d30;dt[Y4](identifier)[j_]();}else {var D9=Q9x;D9+=m9z;D9+=M5g;D9+=A33;var y2=M5g;y2+=x4g;y2+=R3d;y2+=J7D;var indexes_1=[];dt[y2](identifier)[D9](function(){var id=dataSource$1[j_g][H7v](that,this[z4k]());L7K.b9t();if($[z6t](id,cancelled) === -p88){indexes_1[K7Q](this[j_l]());}});dt[o1V](indexes_1)[H21]();}}};function _htmlId(identifier){var Y_7="key";var q9s='Could not find an element with `data-editor-id` or `id` of: ';var j5K="less";var Z3H='[data-editor-id="';var g9=E3o;g9+=E20;var I_=U50;L7K.b9t();I_+=A7F;var O0=Y_7;O0+=j5K;if(identifier === O0){return $(document);}var specific=$(Z3H + identifier + I_);if(specific[g9] === M1q){specific=typeof identifier === f2Z?$(safeQueryId(identifier)):$(identifier);}if(specific[m2_] === M1q){throw new Error(q9s + identifier);}return specific;}function _htmlEl(identifier,name){var G0T='[data-editor-field="';var j$=U50;j$+=A7F;L7K.b9t();var context=_htmlId(identifier);return $(G0T + name + j$,context);}function _htmlEls(identifier,names){L7K.b9t();var S6=E3o;S6+=E20;var out=$();for(var i=M1q,ien=names[S6];i < ien;i++){var F5=x6u;F5+=J_0;F5+=J_0;out=out[F5](_htmlEl(identifier,names[i]));}return out;}function _htmlGet(identifier,dataSrc){var N7N="ta-e";var r1k="ditor-";var W0=T03;W0+=S_n;var G2=d$I;G2+=N7N;G2+=r1k;G2+=n01;var el=_htmlEl(identifier,dataSrc);return el[n2y](Y7w)[m2_]?el[X7W](G2):el[W0]();}function _htmlSet(identifier,fields,data){var J2=m9z;J2+=x6u;J2+=G1V;J2+=T03;$[J2](fields,function(name,field){var x32="filte";var w$Z="r-value";var V3u="-edito";var val=field[B7u](data);L7K.l4n();if(val !== undefined){var z1=x32;z1+=M5g;var el=_htmlEl(identifier,field[h$P]());if(el[z1](Y7w)[m2_]){var c2=z4k;c2+=V3u;c2+=w$Z;var o0=x6u;o0+=d1B;el[o0](c2,val);}else {el[o8Y](function(){var h9i="Nod";var o7e="child";var v8W="removeChild";var F86="firstChild";var l6=o7e;L7K.b9t();l6+=h9i;l6+=C_A;while(this[l6][m2_]){this[v8W](this[F86]);}})[y64](val);}}});}var dataSource={create:function(fields,data){if(data){var q9=G1V;q9+=x6u;q9+=E3o;q9+=E3o;var C8=t_M;C8+=J_0;var id=dataSource[C8][q9](this,data);try{var O5=E3o;O5+=l9T;O5+=X1n;if(_htmlId(id)[O5]){_htmlSet(id,fields,data);}}catch(e){;}}},edit:function(identifier,fields,data){var b8c="keyles";var k_=b8c;k_+=J7D;var z8=C10;z8+=E3o;z8+=E3o;var id=dataSource[j_g][z8](this,data) || k_;_htmlSet(id,fields,data);},fields:function(identifier){var D8=J$_;D8+=R3d;var R6=l7$;R6+=t_M;R6+=u5m;var m0=P1D;m0+=y3C;m0+=Y6A;var out={};if(Array[m0](identifier)){var r5=y3A;r5+=B3P;r5+=c3v;r5+=F0A;for(var i=M1q,ien=identifier[r5];i < ien;i++){var res=dataSource[c26][H7v](this,identifier[i]);out[identifier[i]]=res[identifier[i]];}return out;}var data={};var fields=this[J7D][R6];if(!identifier){identifier=u1t;}$[o8Y](fields,function(name,field){var val=_htmlGet(identifier,field[h$P]());field[U21](data,val === X2m?undefined:val);});out[identifier]={data:data,fields:fields,idSrc:identifier,node:document,type:D8};L7K.l4n();return out;},id:function(data){var idFn=dataGet(this[J7D][g9l]);L7K.b9t();return idFn(data);},individual:function(identifier,fieldNames){var N9$="eName";var U43="data-edito";var b$j="ddBac";var b_q="isAr";var c1b='editor-id';var Z5H="annot automatically determine field name from data";var R0Y='[data-editor-id]';var z9_="source";var I2e='andSelf';var x2=m9z;x2+=x6u;x2+=G1V;x2+=T03;var J0=Y8Y;J0+=J7D;var H1=l7$;H1+=w_4;H1+=W6P;H1+=J7D;var b1=y3A;b1+=w66;var F1=b_q;F1+=Z0l;var j2=t5D;j2+=N9$;var attachEl;if(identifier instanceof $ || identifier[j2]){var q_=J_0;q_+=h34;var w9=m59;w9+=j4C;w9+=a$N;var H3=x6u;H3+=b$j;H3+=I9U;var C6=l7$;C6+=B3P;attachEl=identifier;if(!fieldNames){var h8=U43;h8+=M5g;h8+=p1O;h8+=Y8Y;var S2=x6u;S2+=F$o;S2+=F$o;S2+=M5g;fieldNames=[$(identifier)[S2](h8)];}var back=$[C6][l2U]?H3:I2e;identifier=$(identifier)[w9](R0Y)[back]()[q_](c1b);}if(!identifier){identifier=u1t;}if(fieldNames && !Array[F1](fieldNames)){fieldNames=[fieldNames];}if(!fieldNames || fieldNames[b1] === M1q){var x_=R$B;x_+=Z5H;x_+=K2Z;x_+=z9_;throw new Error(x_);}var out=dataSource[H1][H7v](this,identifier);var fields=this[J7D][J0];var forceFields={};$[o8Y](fieldNames,function(i,name){L7K.b9t();forceFields[name]=fields[name];});$[x2](out,function(id,set){var O8U="layFi";var j0$="tachFi";var L7=p2t;L7+=O8U;L7+=g$f;L7+=J7D;var S4=D05;S4+=M5g;S4+=Z0l;var k5=x3a;k5+=j0$;k5+=g$f;k5+=J7D;var Z4=G1V;Z4+=E2R;Z4+=E3o;set[P3q]=Z4;set[k5]=[fieldNames];set[b$w]=attachEl?$(attachEl):_htmlEls(identifier,fieldNames)[S4]();set[c26]=fields;set[L7]=forceFields;});return out;},initField:function(cfg){var V7O="-l";var V0q="[data-editor";var U6u="abel=\"";var B7=E3o;B7+=r8u;B7+=m9z;B7+=E3o;var S1=U50;S1+=A7F;var X$=V0q;X$+=V7O;X$+=U6u;var label=$(X$ + (cfg[z4k] || cfg[z$n]) + S1);if(!cfg[B7] && label[m2_]){var p$=T03;p$+=F$o;p$+=F6G;p$+=E3o;cfg[S1J]=label[p$]();}},remove:function(identifier,fields){_htmlId(identifier)[H21]();}};var classNames={actions:{create:t12,edit:n9v,remove:B5w},body:{content:F6,wrapper:B5d},bubble:{bg:e9,close:M5K,liner:R5,pointer:O0R,table:y7,wrapper:U5},field:{'disabled':F2,'error':m6,'input':q6,'inputControl':N3,'label':P6,'msg-error':D2,'msg-info':K3s,'msg-label':Q7u,'msg-message':U8,'multiInfo':u2P,'multiNoEdit':k9,'multiRestore':N2_,'multiValue':D5N,'namePrefix':D4c,'processing':R5y,'typePrefix':J3,'wrapper':n9g},footer:{content:Z4v,wrapper:q2},form:{button:k4,buttonInternal:W4,buttons:s_q,content:X3,error:Z$s,info:z6I,tag:l2Y,wrapper:K1},header:{content:q4a,wrapper:b2R},inline:{buttons:A3,liner:C0b,wrapper:o0J},processing:{active:o0s,indicator:R5y},wrapper:q$};var displayed$2=g49;var cssBackgroundOpacity=p88;var dom$1={background:$(X_l)[M1q],close:$(D6)[M1q],content:X2m,wrapper:$(W14 + n3 + g8 + X8)[M1q]};function findAttachRow(editor,attach){var M6P="dataTabl";var a3m='head';L7K.b9t();var Z0=e$3;Z0+=A3U;var M8=x6u;M8+=W6y;M8+=t_M;M8+=g5T;var F_=Y4Q;F_+=w2L;var G6=M6P;G6+=m9z;var dt=new $[w9$][G6][F_](editor[J7D][O81]);if(attach === a3m){var w7=c0K;w7+=v4c;w7+=K2t;return dt[O81](undefined)[w7]();;}else if(editor[J7D][M8] === Z0){return dt[O81](undefined)[x_C]();}else {var Q3=B3P;Q3+=E1k;Q3+=m9z;return dt[R3o](editor[J7D][E_V])[Q3]();}}function heightCalc$1(dte){var V4N="DTE_Foo";var F5d="div.DTE_";var b0H="rap";var J9d="div.DTE_Bo";var F$R="Heade";var S05="outerHeight";var b8r="indowPadding";var H7Q="heigh";var q7j="maxHei";var i2L="div.";var Y_P="dy_Content";var O5J="oute";var A9b="rHeig";var x3=R3d;x3+=b0H;x3+=C5C;x3+=M5g;var I9=q7j;I9+=r5C;var d1=G1V;d1+=J7D;d1+=J7D;L7K.l4n();var C0=J9d;C0+=Y_P;var h2=R3d;h2+=b8r;var m9=b00;m9+=B3P;m9+=l7$;var A5=H7Q;A5+=F$o;var A4=i2L;A4+=V4N;A4+=Q_Y;var a2=O5J;a2+=A9b;a2+=h7d;var y9=R3d;y9+=i20;y9+=Q06;y9+=M5g;var d2=F5d;d2+=F$R;d2+=M5g;var header=$(d2,dom$1[y9])[a2]();var footer=$(A4,dom$1[A8Y])[S05]();var maxHeight=$(window)[A5]() - envelope[m9][h2] * A0_ - header - footer;$(C0,dom$1[A8Y])[d1](I9,maxHeight);return $(dte[I_d][x3])[S05]();}function hide$2(dte,callback){var l7Q="conten";if(!callback){callback=function(){};}if(displayed$2){var u3=w62;u3+=h0i;var e7=l7Q;e7+=F$o;$(dom$1[e7])[u3]({top:-(dom$1[t4U][f1c] + Y8m)},f0L,function(){var J3Y='normal';var u3N="roun";var x_X="ckg";var F5L="ba";var d5=F5L;d5+=x_X;d5+=u3N;d5+=J_0;var N$=l$M;L7K.l4n();N$+=H0m;N$+=m59;N$+=K2t;$([dom$1[N$],dom$1[d5]])[I_T](J3Y,function(){var z2r="deta";var S3=z2r;L7K.b9t();S3+=b_g;$(this)[S3]();callback();});});displayed$2=g49;}}function init$1(){var C5w="acit";var I4S="ED_Envelope_Container";var m9H="wrap";var v7I="kg";var P5m="div.DT";var z2=x4g;z2+=m59;z2+=C5w;z2+=A33;var C$=s95;C$+=v7I;C$+=M5g;C$+=z9D;var P$=m9H;P$+=m59;P$+=K2t;var c_=P5m;c_+=I4S;dom$1[t4U]=$(c_,dom$1[P$])[M1q];cssBackgroundOpacity=$(dom$1[C$])[e$Y](z2);}function show$2(dte,callback){var f10="click.DTE";var Z2w="displa";var k9l='px';var F3H="attac";var U0G="click.DTED";var L5R="_En";var n6K="ackgroun";var K9A='resize.DTED_Envelope';var h2Y="velope";var V90="nvelope";var I_Y="_E";var k2J="offsetWidth";var U76='auto';var F4Z='click.DTED_Envelope';var C4K="opacity";var K1U="click";var D8y="heig";var E3R=".DTED_Envelope";var w__=".DTED";var x9I="D_Envelope";var F$k='0';var V2i="arginLeft";var u_K="ani";var U$8="norm";var L60="resize";var V_=L60;V_+=E3R;var t$=U0G;t$+=L5R;t$+=h2Y;var T0=x4g;T0+=l7$;T0+=l7$;var u1=f10;u1+=x9I;var h$=x4g;h$+=B3P;var N_=c8Q;N_+=l7$;var c7=K1U;c7+=w__;c7+=I_Y;c7+=V90;var k1=x4g;k1+=B3P;var i2=x4g;i2+=l7$;i2+=l7$;var E1=D4D;E1+=J86;var w0=t_M;w0+=k_j;w0+=k4E;var H5=B91;H5+=F$o;H5+=E3o;H5+=m9z;var p9=x3a;p9+=F$o;p9+=M5g;var i4=D8y;i4+=h7d;var j0=b00;j0+=B3P;j0+=W1Z;j0+=k1z;var a_=x6u;a_+=Q06;a_+=B3P;a_+=J_0;var E8=u3Y;E8+=n6K;E8+=J_0;var y3=H0m;y3+=m59;y3+=A_8;var i3=u3Y;i3+=x4g;i3+=J_0;i3+=A33;if(!callback){callback=function(){};}$(i3)[y3](dom$1[E8])[a_](dom$1[A8Y]);dom$1[j0][Y3a][i4]=U76;if(!displayed$2){var Y3=Z2U;Y3+=F$o;Y3+=m9z;Y3+=k1z;var L5=x8P;L5+=m9z;L5+=F0f;var j5=U$8;j5+=X1g;var O7=u_K;O7+=q0R;O7+=F$o;O7+=m9z;var t3=u3Y;t3+=Y4R;t3+=c1J;var g3=d8z;g3+=Y6A;var M$=u3Y;M$+=c8N;M$+=i3I;var M7=F$o;M7+=P7P;var e2=m7i;e2+=L5h;e2+=m9z;var Z_=b00;Z_+=w6m;var Y8=F$o;Y8+=x4g;Y8+=m59;var o$=x4g;o$+=A43;o$+=L6_;var K0=F$o;K0+=x4g;K0+=m59;var v2=m7i;v2+=J_A;var b$=F6G;b$+=V2i;var o6=J7D;o6+=F$o;o6+=A33;o6+=y3A;var p0=R3d;p0+=i20;p0+=Q06;p0+=M5g;var z7=L0p;z7+=g5V;z7+=A33;var O6=F3H;O6+=T03;var k8=T_U;k8+=t47;k8+=I9U;var i0=Z2w;i0+=A33;var style=dom$1[A8Y][Y3a];style[C4K]=F$k;style[i0]=k8;var height=heightCalc$1(dte);var targetRow=findAttachRow(dte,envelope[A0g][O6]);var width=targetRow[k2J];style[z7]=N7C;style[C4K]=f$W;dom$1[p0][Y3a][E$j]=width + k9l;dom$1[A8Y][o6][b$]=-(width / A0_) + k9l;dom$1[A8Y][v2][K0]=$(targetRow)[o$]()[Y8] + targetRow[f1c] + k9l;dom$1[Z_][e2][M7]=-p88 * height - J72 + k9l;dom$1[M$][Y3a][C4K]=F$k;dom$1[R$h][Y3a][g3]=t3;$(dom$1[R$h])[O7]({opacity:cssBackgroundOpacity},j5);$(dom$1[A8Y])[L5]();$(dom$1[Y3])[O6e]({top:M1q},f0L,callback);}$(dom$1[H5f])[p9](H5,dte[w0][E1])[i2](F4Z)[k1](c7,function(e){var l2=G1V;l2+=E3o;l2+=x4g;L7K.b9t();l2+=J86;dte[l2]();});$(dom$1[R$h])[N_](F4Z)[h$](u1,function(e){dte[R$h]();});$(r0C,dom$1[A8Y])[T0](t$)[g5T](F4Z,function(e){var y4j='DTED_Envelope_Content_Wrapper';if($(e[M$_])[p5N](y4j)){dte[R$h]();}});$(window)[H9D](V_)[g5T](K9A,function(){L7K.b9t();heightCalc$1(dte);});displayed$2=T5f;}var envelope={close:function(dte,callback){L7K.l4n();hide$2(dte,callback);},conf:{attach:D3,windowPadding:Y8m},destroy:function(dte){hide$2();},init:function(dte){L7K.b9t();init$1();return envelope;},node:function(dte){return dom$1[A8Y][M1q];},open:function(dte,append,callback){var Q2B="appendC";var U60="appendChild";var M_O="hild";var W3=w3s;W3+=x4g;W3+=J86;var n2=Q2B;n2+=M_O;var u_=e0Y;u_+=k38;L7K.b9t();$(dom$1[u_])[C8B]()[d4Q]();dom$1[t4U][U60](append);dom$1[t4U][n2](dom$1[W3]);show$2(dte,callback);}};function isMobile(){var O3_="outerWidth";var B7K="orientation";var O_p=576;var G3x='undefined';L7K.b9t();return typeof window[B7K] !== G3x && window[O3_] <= O_p?T5f:g49;}var displayed$1=g49;var ready=g49;var scrollTop=M1q;var dom={background:$(z3I),close:$(y4),content:X2m,wrapper:$(a6 + D$ + G3 + H_ + r65 + r65 + r65 + r65)};function heightCalc(){var P4d='div.DTE_Footer';var y24="wPadding";var y3Y="ou";L7K.l4n();var D0R="outer";var L9T="erH";var f3w="axH";var s_c='div.DTE_Header';var M2V="rapp";var X4H="Hei";var H5D='calc(100vh - ';var v5H="xHeight";var D1n="eight";var F$3="height";var M90='div.DTE_Body_Content';var e7t="wind";var x9i="iv.DTE_Body";var M7m="px";var f1=y3Y;f1+=F$o;f1+=L9T;f1+=D1n;var w2=D0R;w2+=X4H;w2+=r5C;var W5=R3d;W5+=M2V;W5+=K2t;var headerFooter=$(s_c,dom[W5])[w2]() + $(P4d,dom[A8Y])[f1]();if(isMobile()){var W_=M7m;W_+=V56;var g7=q0R;g7+=v5H;var I2=G1V;I2+=J7D;I2+=J7D;$(M90,dom[A8Y])[I2](g7,H5D + headerFooter + W_);}else {var h9=F6G;h9+=f3w;h9+=D1n;var m3=G1V;m3+=f63;var d8=l$M;d8+=f9u;d8+=K2t;var g_=J_0;g_+=x9i;g_+=g5F;var G8=e7t;G8+=x4g;G8+=y24;var maxHeight=$(window)[F$3]() - self[A0g][G8] * A0_ - headerFooter;$(g_,dom[d8])[m3](h9,maxHeight);}}function hide$1(dte,callback){var Q3P="wra";var H8F="_an";var j$g="ED_Li";var G7t="ze.DT";var G7J="scrollTop";var u$4="setAn";var Y_$="ghtbox";var f3=U$q;f3+=G7t;f3+=j$g;f3+=Y_$;var G0=u3Y;G0+=c8N;G0+=i3I;var G9=H9D;G9+=u$4;G9+=t_M;var V1=Q3P;V1+=m59;V1+=C5C;V1+=M5g;var A7=H8F;A7+=H42;A7+=W1Z;if(!callback){callback=function(){};}$(o0u)[G7J](scrollTop);dte[A7](dom[V1],{opacity:M1q,top:self[A0g][G9]},function(){$(this)[d4Q]();callback();});dte[R3b](dom[G0],{opacity:M1q},function(){$(this)[d4Q]();});displayed$1=g49;$(window)[H9D](f3);}function init(){var d8A="pper";var l2o="per";var L3g="acity";L7K.b9t();var T3t="ack";var z0e="div.DTED_L";var h_d="gro";var u1W="ightbox_Con";var s15='opacity';var t1=u3Y;t1+=T3t;t1+=h_d;t1+=i3I;var q0=P7P;q0+=L3g;var k3=l$M;k3+=x6u;k3+=m59;k3+=l2o;var p2=R3d;p2+=i20;p2+=d8A;var p1=z0e;p1+=u1W;p1+=W1Z;p1+=k1z;var w5=b00;w5+=w6m;if(ready){return;}dom[w5]=$(p1,dom[p2]);dom[k3][e$Y](q0,M1q);dom[t1][e$Y](s15,M1q);ready=T5f;}function show$1(dte,callback){var O4V="backg";var S03="box";var R3e="un";var H2T="click.DTED_Lightb";var t06="lTop";var B9$="ick.DTED_Light";var j5A="cro";var g$w="ze.DTED";var l2n="dClas";var i7X="ox";var q8I="_Light";var H35='DTED_Lightbox_Mobile';var R6C="kgro";var I86="tent";var y$7='click.DTED_Lightbox';var f3U="etA";var R8=w3s;R8+=B9$;R8+=S03;var X1=x4g;X1+=B3P;var O9=H2T;O9+=i7X;var M2=x4g;M2+=B3P;var P2=M1L;P2+=l79;P2+=B3P;L7K.l4n();var f7=B91;f7+=F$o;f7+=y3A;var e0=x6u;e0+=F$o;e0+=Z9H;var Y9=O4V;Y9+=w8d;var I1=f9u;I1+=l9T;I1+=J_0;if(isMobile()){var u7=x6u;u7+=J_0;u7+=l2n;u7+=J7D;var B2=u3Y;B2+=x4g;B2+=J_0;B2+=A33;$(B2)[u7](H35);}$(o0u)[I1](dom[Y9])[K$Z](dom[A8Y]);heightCalc();if(!displayed$1){var p_=J7D;p_+=j5A;p_+=E3o;p_+=t06;var P4=u3Y;P4+=n9X;var v_=U$q;v_+=g$w;v_+=q8I;v_+=S03;var T1=s95;T1+=R6C;T1+=R3e;T1+=J_0;var X_=B3I;X_+=B3P;X_+=h0i;var M0=H9D;M0+=J7D;M0+=f3U;M0+=Y0W;var U_=G1V;U_+=x4g;U_+=B3P;U_+=l7$;var I$=l$M;I$+=H0m;I$+=C5C;I$+=M5g;var O_=x6u;O_+=J3O;O_+=F$o;O_+=x4g;var a9=c0K;a9+=s$6;var I5=G1V;I5+=J7D;I5+=J7D;var V9=G1V;V9+=x4g;V9+=B3P;V9+=I86;displayed$1=T5f;dom[V9][I5](a9,O_);dom[I$][e$Y]({top:-self[U_][M0]});dte[R3b](dom[A8Y],{opacity:p88,top:M1q},callback);dte[X_](dom[T1],{opacity:p88});$(window)[g5T](v_,function(){heightCalc();});scrollTop=$(P4)[p_]();}dom[H5f][e0](f7,dte[P2][H5f])[H9D](y$7)[M2](y$7,function(e){var v5=D4D;v5+=J86;dte[v5]();});dom[R$h][H9D](O9)[X1](y$7,function(e){e[I3d]();dte[R$h]();});$(r0C,dom[A8Y])[H9D](R8)[g5T](y$7,function(e){var y1W="tbox_Content_Wrappe";var G7m="hasCl";var l6_="DTED_Ligh";var r3=l6_;r3+=y1W;r3+=M5g;var N5=G7m;N5+=h4Q;L7K.b9t();var t9=F$o;t9+=j4C;t9+=q9o;t9+=F$o;if($(e[t9])[N5](r3)){e[I3d]();dte[R$h]();}});}var self={close:function(dte,callback){hide$1(dte,callback);},conf:{offsetAni:a6Q,windowPadding:a6Q},destroy:function(dte){if(displayed$1){hide$1(dte);}},init:function(dte){init();return self;},node:function(dte){return dom[A8Y][M1q];},open:function(dte,append,callback){var P3=w3s;P3+=k4i;var m4=x6u;m4+=Q06;m4+=V8x;var content=dom[t4U];L7K.l4n();content[C8B]()[d4Q]();content[m4](append)[K$Z](dom[P3]);show$1(dte,callback);}};var DataTable$4=$[D7][q1t];function add(cfg,after,reorder){var Z89="tFiel";var N9N="ourc";var t2A='Error adding field. The field requires a `name` option';var w28=" adding field \'";var U_z="ift";var z1n="_dataS";var h0O="sts with this name";var G6r="\'. A field already exi";var X2y="reverse";var D_r="uns";var G1D="sArr";var k06="nArr";var o0Z="Error";var I09="Field";L7K.b9t();var o3M="_displayReorde";var I4=t0$;I4+=u5m;var o5=G19;o5+=m9z;var q4=G1V;q4+=Y26;q4+=J86;q4+=J7D;var n1=t_M;n1+=Y0W;n1+=Z89;n1+=J_0;var l1=z1n;l1+=N9N;l1+=m9z;var d3=Y8Y;d3+=J7D;var e3=B3P;e3+=x6u;e3+=F6G;e3+=m9z;var n9=t_M;n9+=G1D;n9+=Y6A;if(reorder === void M1q){reorder=T5f;}if(Array[n9](cfg)){var F3=h3Y;F3+=J_0;F3+=K2t;if(after !== undefined){cfg[X2y]();}for(var _i=M1q,cfg_1=cfg;_i < cfg_1[m2_];_i++){var h6=v4c;h6+=J_0;var cfgDp=cfg_1[_i];this[h6](cfgDp,after,g49);}this[s7w](this[F3]());return this;}var name=cfg[e3];if(name === undefined){throw new Error(t2A);}if(this[J7D][d3][name]){var d6=G6r;d6+=h0O;var j6=o0Z;j6+=w28;throw new Error(j6 + name + d6);}this[l1](n1,cfg);var editorField=new Editor[I09](cfg,this[q4][Y8Y],this);if(this[J7D][o5]){var editFields=this[J7D][X8F];editorField[h8a]();$[o8Y](editFields,function(idSrc,editIn){L7K.l4n();var U0=J_0;U0+=m9z;U0+=l7$;var Q7=c5m;Q7+=u13;Q7+=Z0V;var value;if(editIn[z4k]){value=editorField[B7u](editIn[z4k]);}editorField[Q7](idSrc,value !== undefined?value:editorField[U0]());});}this[J7D][I4][name]=editorField;if(after === undefined){var Q$=K29;Q$+=J7D;Q$+=T03;this[J7D][O7k][Q$](name);}else if(after === X2m){var j3=D_r;j3+=T03;j3+=U_z;var s4=E6E;s4+=K2t;this[J7D][s4][j3](name);}else {var K7=t_M;K7+=k06;K7+=Y6A;var idx=$[K7](after,this[J7D][O7k]);this[J7D][O7k][a59](idx + p88,M1q,name);}if(reorder !== g49){var c0=h3Y;c0+=N69;var F7=o3M;F7+=M5g;this[F7](this[c0]());}return this;}function ajax(newAjax){if(newAjax){var v8=H9z;v8+=C1w;this[J7D][v8]=newAjax;return this;}return this[J7D][y58];}function background(){var x4h="unc";var K6c="ackground";var T6=E43;T6+=H7H;var C_=T_U;C_+=L7j;var N4=l7$;N4+=x4h;N4+=u4p;var o8=g5T;o8+=u1c;o8+=K6c;var onBackground=this[J7D][u5I][o8];if(typeof onBackground === N4){onBackground(this);}else if(onBackground === C_){this[s$G]();}else if(onBackground === H$E){this[H5f]();}else if(onBackground === T6){this[f4c]();}return this;}function blur(){L7K.b9t();var t3K="blu";var M1=a4a;M1+=t3K;M1+=M5g;this[M1]();return this;}function bubble(cells,fieldNames,showIn,opts){var v3Q='boolean';var m68="ubbl";var B8k="ataS";var f0C="sPlainOb";var o4G="ject";var i6=u3Y;i6+=m68;i6+=m9z;var Z6=Z71;Z6+=B8k;Z6+=t22;var b7=V8g;b7+=A_8;var z4=t_M;z4+=f0C;z4+=o4G;var K6=a4a;K6+=F$o;K6+=j_g;K6+=A33;var _this=this;var that=this;if(this[K6](function(){L7K.b9t();that[C$3](cells,fieldNames,opts);})){return this;}if($[z4](fieldNames)){opts=fieldNames;fieldNames=undefined;showIn=T5f;}else if(typeof fieldNames === v3Q){showIn=fieldNames;fieldNames=undefined;opts=undefined;}if($[v5K](showIn)){opts=showIn;showIn=T5f;}if(showIn === undefined){showIn=T5f;}opts=$[b7]({},this[J7D][V_u][C$3],opts);var editFields=this[Z6](B21,cells,fieldNames);this[k_9](cells,editFields,i6,opts,function(){var s4P="ppl";var c40="_closeReg";var s4q="resiz";var c4P="div cla";var v2H="_for";var l$o="><";var r$i="dren";var P2B="bg";var z7I="v clas";var c6L="mOptions";var c27="anim";var k_3="prep";var y7T="class=\"";var Q1P="childr";var k6P="endTo";var j2L="epend";var K_H="nter";var t99="div>";var S8F="bbl";var n1U="poi";var N55='" title="';var N_J="div class=\"";var k0d="></d";var y1l="formErr";var N$Q="e.";var O4I='"><div></div></div>';var l91="iv class=\"DTE_Processing_Indicator\"><span></div>";var c4t="_preope";var N0=a4a;N0+=c27;N0+=x3a;N0+=m9z;var W7=x4g;W7+=B3P;var e4=w3s;e4+=R2T;e4+=I9U;var E3=x4g;E3+=B3P;var y6=x6u;y6+=J_0;y6+=J_0;var f8=v4c;f8+=J_0;var R4=B91;R4+=F$o;R4+=E3o;R4+=m9z;var M6=A_J;M6+=G1Q;M6+=q9o;var p4=l7$;p4+=x4g;p4+=M5g;p4+=F6G;var a7=J_0;a7+=x4g;a7+=F6G;var X5=y1l;X5+=h3Y;var r6=Q1P;r6+=l9T;var U9=b_g;U9+=J7I;U9+=r$i;var O3=r$P;O3+=h1_;var m5=U50;m5+=l$o;m5+=d3C;var s0=n1U;s0+=K_H;var h5=W7n;h5+=f_P;h5+=K2Z;h5+=y7T;var s3=W7n;s3+=K5f;s3+=t99;var c4=c_1;c4+=l91;var o4=U50;o4+=k0d;o4+=h1_;var n_=G1V;n_+=Y4R;n_+=J7D;n_+=m9z;var d_=W7n;d_+=L0p;d_+=z7I;d_+=p_t;var n7=W7n;n7+=c4P;n7+=f63;n7+=g0j;var Q9=b8q;Q9+=M5g;var J4=W7n;J4+=N_J;var G$=Y4X;G$+=S8F;G$+=m9z;var w_=y4_;w_+=Y8o;var J9=x6u;J9+=s4P;J9+=A33;var n8=Z2U;n8+=C10;n8+=F$o;var f0=s4q;f0+=N$Q;var s9=x4g;s9+=B3P;var P_=c4t;P_+=B3P;var k2=v2H;k2+=c6L;var namespace=_this[k2](opts);var ret=_this[P_](m7R);if(!ret){return _this;}$(window)[s9](f0 + namespace,function(){var Y$g="leP";var A2x="sition";L7K.b9t();var d9=d$S;d9+=Y$g;d9+=x4g;d9+=A2x;_this[d9]();});var nodes=[];_this[J7D][E4r]=nodes[n8][J9](nodes,pluck(editFields,w_));var classes=_this[I58][G$];var backgroundNode=$(e2R + classes[P2B] + O4I);var container=$(e2R + classes[A8Y] + K8R + J4 + classes[Q9] + K8R + n7 + classes[O81] + K8R + d_ + classes[n_] + N55 + _this[B1B][H5f] + o4 + c4 + s3 + r65 + h5 + classes[s0] + m5 + O3);if(showIn){var z0=f9u;z0+=k6P;var H6=u3Y;H6+=E1k;H6+=A33;var i9=f9u;i9+=k6P;container[i9](H6);backgroundNode[z0](o0u);}var liner=container[C8B]()[N8j](M1q);var tableNode=liner[U9]();var closeNode=tableNode[r6]();liner[K$Z](_this[I_d][X5]);tableNode[C7n](_this[a7][p4]);if(opts[M6]){var b0=D6L;b0+=j2L;liner[b0](_this[I_d][n_d]);}if(opts[R4]){var G7=k82;G7+=F6G;var n6=k_3;n6+=l9T;n6+=J_0;liner[n6](_this[G7][x_C]);}if(opts[F8y]){var h_=u3Y;h_+=h7$;var E$=J_0;E$+=x4g;E$+=F6G;var b9=H0m;b9+=D5l;b9+=J_0;tableNode[b9](_this[E$][h_]);}var finish=function(){var C81="bb";L7K.b9t();var w9L="earDynamicInfo";var l8=Y4X;l8+=C81;l8+=y3A;var q1=G5L;q1+=w9L;_this[q1]();_this[m6A](n89,[l8]);};var pair=$()[f8](container)[y6](backgroundNode);_this[c40](function(submitComplete){var G_i="nimate";L7K.b9t();var X9=B3I;X9+=G_i;_this[X9](pair,{opacity:M1q},function(){var V7i="esize.";var w2y="eta";L7K.l4n();if(this === container[M1q]){var n$=M5g;n$+=V7i;var v$=J_0;v$+=w2y;v$+=G1V;v$+=T03;pair[v$]();$(window)[H9D](n$ + namespace);finish();}});});backgroundNode[E3](e4,function(){L7K.l4n();_this[s$G]();});closeNode[W7](a55,function(){L7K.b9t();var G5=G5L;G5+=x4g;G5+=J86;_this[G5]();});_this[u5k]();_this[z$j](m7R,g49);var opened=function(){var T0q="includeFie";var X66="pened";var n0=x6u;n0+=G1V;n0+=z4s;n0+=B3P;var H9=x4g;L7K.b9t();H9+=X66;var q8=T0q;q8+=D$d;_this[K8C](_this[J7D][q8],opts[F4U]);_this[m6A](H9,[m7R,_this[J7D][n0]]);};_this[N0](pair,{opacity:p88},function(){if(this === container[M1q]){opened();}});});return this;}function bubblePosition(){var W92='div.DTE_Bubble';var i1e="Liner";var B1b="left";var F87='below';var J3A="bottom";var V3h="clas";var R3m="TE_Bubble";var V5R="gh";var q56="moveClass";var D14="outerWi";var E4=F$o;E4+=x4g;E4+=m59;var i7=H9D;i7+=L6_;var K4=E3o;K4+=m9z;K4+=w66;var z$=d$S;z$+=y3A;var q7=V3h;q7+=f7S;var h4=D14;h4+=J_0;h4+=F0A;var v1=Q$G;v1+=r5C;var i_=Q$G;i_+=V5R;i_+=F$o;var C2=E3o;C2+=m9z;C2+=L0x;C2+=F0A;var B8=y3A;B8+=l7$;B8+=F$o;var Z8=a4H;Z8+=X1n;var p5=t5Z;p5+=R3m;p5+=a4a;p5+=i1e;var wrapper=$(W92);var liner=$(p5);var nodes=this[J7D][E4r];var position={bottom:M1q,left:M1q,right:M1q,top:M1q};$[o8Y](nodes,function(i,nodeIn){var a3_="tHe";var b5I="etWidth";var W5$="offs";var a$Y="ffse";var A_=H9D;A_+=J86;A_+=a3_;A_+=s$6;var P8=W5$;P8+=b5I;var W8=M5g;W8+=s$6;var h3=F$o;h3+=x4g;h3+=m59;var T5=c3v;T5+=m9z;T5+=F$o;var L0=x4g;L0+=a$Y;L0+=F$o;var pos=$(nodeIn)[L0]();L7K.b9t();nodeIn=$(nodeIn)[T5](M1q);position[d1s]+=pos[h3];position[B1b]+=pos[B1b];position[W8]+=pos[B1b] + nodeIn[P8];position[J3A]+=pos[d1s] + nodeIn[A_];});position[d1s]/=nodes[Z8];position[B8]/=nodes[C2];position[i_]/=nodes[m2_];position[J3A]/=nodes[m2_];var top=position[d1s];var left=(position[B1b] + position[v1]) / A0_;var width=liner[h4]();var visLeft=left - width / A0_;var visRight=visLeft + width;var docWidth=$(window)[E$j]();var padding=y6j;this[q7][z$];wrapper[e$Y]({left:left,top:top});if(liner[K4] && liner[i7]()[E4] < M1q){var Z3=F$o;Z3+=P7P;wrapper[e$Y](Z3,position[J3A])[w5Y](F87);}else {var h0=g74;h0+=x4g;h0+=R3d;var f5=C5B;f5+=q56;wrapper[f5](h0);}if(visRight + padding > docWidth){var b6=E3o;b6+=m9z;b6+=l7$;b6+=F$o;var T7=G1V;T7+=J7D;T7+=J7D;var diff=visRight - docWidth;liner[T7](b6,visLeft < padding?-(visLeft - padding):-(diff + padding));}else {var u2=E3o;u2+=q8u;u2+=F$o;var t5=G1V;t5+=J7D;t5+=J7D;liner[t5](u2,visLeft < padding?-(visLeft - padding):M1q);}return this;}function buttons(buttonsIn){var Z4g="18n";var c8=g9B;c8+=T5j;c8+=x6u;L7K.l4n();c8+=A33;var _this=this;if(buttonsIn === h4t){var m1=t_M;m1+=Z4g;buttonsIn=[{action:function(){L7K.b9t();this[f4c]();},text:this[m1][this[J7D][J$l]][f4c]}];}else if(!Array[c8](buttonsIn)){buttonsIn=[buttonsIn];}$(this[I_d][F8y])[h55]();$[o8Y](buttonsIn,function(i,btn){var y84='<button></button>';var c6t="endT";var l$m="class";var f2h="abInde";var t4Q='tabindex';var x$b="sName";var q0Z="tabIndex";var Y5u='keypress';var l7j="Name";var B4=J_0;B4+=x4g;B4+=F6G;var v3=x6u;v3+=J6o;v3+=c6t;v3+=x4g;L7K.l4n();var C9=x4g;C9+=B3P;var g6=x6u;g6+=C_e;g6+=M5g;var G4=F$o;G4+=f2h;G4+=h07;var L4=y4_;L4+=M5g;var E_=l7$;E_+=J6c;E_+=t_M;E_+=g5T;var R2=h7d;R2+=O98;var M5=l$m;M5+=l7j;var k6=L8N;k6+=J7D;k6+=x$b;var w1=u3Y;w1+=v9Y;w1+=g5T;var r4=l7$;r4+=x4g;r4+=U5C;var F4=G1V;F4+=E3o;F4+=h4Q;F4+=C_A;var G1=x6u;G1+=F$o;G1+=F$o;G1+=M5g;var y$=E3o;y$+=x6u;y$+=g74;var E7=F$o;E7+=p6S;E7+=F$o;if(typeof btn === f2Z){btn={action:function(){var b2=k4I;b2+=F6G;b2+=t_M;b2+=F$o;L7K.l4n();this[b2]();},text:btn};}var text=btn[E7] || btn[y$];var action=btn[J$l] || btn[w9$];var attr=btn[G1] || ({});$(y84,{class:_this[F4][r4][w1] + (btn[k6]?m5s + btn[M5]:l2Y)})[R2](typeof text === E_?text(_this):text || l2Y)[L4](t4Q,btn[G4] !== undefined?btn[q0Z]:M1q)[g6](attr)[g5T](S4l,function(e){if(e[O4U] === H_Y && action){var H0=G1V;H0+=X1g;H0+=E3o;action[H0](_this);}})[C9](Y5u,function(e){var r9=R3d;r9+=T03;L7K.b9t();r9+=t_M;r9+=b_g;if(e[r9] === H_Y){e[D4J]();}})[g5T](a55,function(e){e[D4J]();L7K.b9t();if(action){var w8=G1V;w8+=x6u;w8+=E3o;w8+=E3o;action[w8](_this,e);}})[v3](_this[B4][F8y]);});return this;}function clear(fieldName){var d93="stri";var e3V="ice";var p3=d93;p3+=L0x;L7K.b9t();var that=this;var sFields=this[J7D][c26];if(typeof fieldName === p3){var F8=I8x;F8+=M0v;var y8=Y9$;y8+=e3V;var N9=h3Y;N9+=N69;var Q4=h2x;Q4+=S9n;var j8=V6H;j8+=U1e;j8+=A33;that[Y8Y](fieldName)[j8]();delete sFields[fieldName];var orderIdx=$[Q4](fieldName,this[J7D][N9]);this[J7D][O7k][y8](orderIdx,p88);var includeIdx=$[F8](fieldName,this[J7D][F4f]);if(includeIdx !== -p88){this[J7D][F4f][a59](includeIdx,p88);}}else {var S9=a4a;S9+=S4_;S9+=N3_;$[o8Y](this[S9](fieldName),function(i,name){L7K.l4n();that[F2L](name);});}return this;}function close(){var c9=G5L;c9+=T1u;c9+=m9z;L7K.l4n();this[c9](g49);return this;}function create(arg1,arg2,arg3,arg4){var a$G="reat";var r4f="ayReor";var s8r="umbe";var X3e="ifi";var T02="Fie";var T_T="_disp";var t2=F3h;t2+=R$B;t2+=a$G;t2+=m9z;var v4=j_L;v4+=k38;var Y2=Y8Y;Y2+=J7D;var z9=T_T;z9+=E3o;z9+=r4f;z9+=N69;var l$=m7i;l$+=L5h;l$+=m9z;var V8=G19;V8+=X3e;V8+=m9z;V8+=M5g;var K5=e$3;K5+=f$O;K5+=W1Z;var l0=F6G;l0+=x6u;l0+=t_M;l0+=B3P;var j7=F33;j7+=K_l;j7+=u5m;var l_=B3P;l_+=s8r;l_+=M5g;var G_=B1g;G_+=E3o;G_+=J_0;G_+=J7D;var _this=this;var that=this;var sFields=this[J7D][G_];var count=p88;if(this[J5W](function(){that[J6Y](arg1,arg2,arg3,arg4);})){return this;}if(typeof arg1 === l_){count=arg1;arg1=arg2;arg2=arg3;}this[J7D][j7]={};for(var i=M1q;i < count;i++){var k$=l7$;k$+=u9Q;k$+=J7D;var Y$=h6w;Y$+=H7H;Y$+=T02;Y$+=D$d;this[J7D][Y$][i]={fields:this[J7D][k$]};}var argOpts=this[Y8J](arg1,arg2,arg3,arg4);this[J7D][d1k]=l0;this[J7D][J$l]=K5;this[J7D][V8]=X2m;this[I_d][C1v][l$][s$8]=K9F;this[O9S]();this[z9](this[Y2]());$[o8Y](sFields,function(name,fieldIn){fieldIn[h8a]();for(var i=M1q;i < count;i++){var s7=J_0;s7+=m9z;s7+=l7$;fieldIn[T8j](i,fieldIn[s7]());}L7K.b9t();fieldIn[L6_](fieldIn[g0I]());});this[v4](t2,X2m,function(){var v5y="_fo";var t_N="rmOptions";var t_=v5y;t_+=t_N;_this[q0i]();_this[t_](argOpts[p1E]);argOpts[a1Y]();});return this;}function undependent(parent){var z8H="rra";var I91='.edep';L7K.b9t();var X67="undependent";var k0=B3P;k0+=x4g;k0+=J_0;k0+=m9z;var K_=l7$;K_+=u9Q;var K9=P1D;K9+=Y4Q;K9+=z8H;K9+=A33;if(Array[K9](parent)){for(var i=M1q,ien=parent[m2_];i < ien;i++){this[X67](parent[i]);}return this;}$(this[K_](parent)[k0]())[H9D](I91);return this;}function dependent(parent,url,opts){var v5r="dependent";var v$R="ange";var V94="ST";var X9r=".ed";var V2=X9r;V2+=m9z;V2+=m59;var u4=m9z;u4+=Y9F;u4+=B3P;u4+=F$o;var P0=b_g;P0+=v$R;var F9=j0k;F9+=V94;var _this=this;if(Array[p5M](parent)){var S0=E3o;S0+=l9T;S0+=c3v;S0+=F0A;for(var i=M1q,ien=parent[S0];i < ien;i++){this[v5r](parent[i],url,opts);}return this;}var that=this;var parentField=this[Y8Y](parent);var ajaxOpts={dataType:U7v,type:F9};opts=$[t8A]({data:X2m,event:P0,postUpdate:X2m,preUpdate:X2m},opts);var update=function(json){var H3n="date";var w9h='show';var x7I='hide';var u4l="eUpdate";var k28='message';var H_f='enable';var X2o="postUpd";var O1h='error';var j3T="postUpdate";L7K.l4n();var X6=L0p;X6+=J7D;X6+=x6u;X6+=k_N;var u6=f$O;u6+=b_g;var W9=h13;W9+=x6u;W9+=E3o;var Q2=W2s;Q2+=Z29;var S$=E3o;S$+=x6u;S$+=g74;var T2=m59;T2+=M5g;T2+=f5c;T2+=H3n;if(opts[T2]){var L2=m59;L2+=M5g;L2+=u4l;opts[L2](json);}$[o8Y]({errors:O1h,labels:S$,messages:k28,options:Q2,values:W9},function(jsonProp,fieldFn){L7K.l4n();if(json[jsonProp]){var t8=m9z;t8+=H5X;t8+=T03;$[t8](json[jsonProp],function(fieldIn,valIn){var y1=S4_;y1+=J_0;L7K.b9t();that[y1](fieldIn)[fieldFn](valIn);});}});$[u6]([x7I,w9h,H_f,X6],function(i,key){L7K.l4n();if(json[key]){that[key](json[key],json[O6e]);}});if(opts[j3T]){var H$=X2o;H$+=Z29;opts[H$](json);}parentField[D$8](g49);};$(parentField[S_L]())[g5T](opts[u4] + V2,function(e){var P0G="lues";var r4t="essin";var m39="the";L7K.b9t();var D8W="xte";var A9=l7$;A9+=J6c;A9+=I6K;var c6=d$I;c6+=f$8;var u$=Y_Y;u$+=P0G;var P7=R3o;P7+=J7D;var W6=J_0;W6+=x6u;W6+=f$8;var P1=B8r;P1+=J7D;var q3=M5g;q3+=x4g;q3+=R3d;q3+=J7D;var F$=Q0R;F$+=r4t;F$+=c3v;var y5=E3o;y5+=P93;y5+=T03;var F0=l7$;F0+=t_M;F0+=B3P;F0+=J_0;if($(parentField[S_L]())[F0](e[M$_])[y5] === M1q){return;}parentField[F$](T5f);var data={};data[q3]=_this[J7D][X8F]?pluck(_this[J7D][P1],W6):X2m;data[R3o]=data[P7]?data[o1V][M1q]:X2m;data[u$]=_this[m1I]();if(opts[c6]){var ret=opts[z4k](data);if(ret){var r2=M49;r2+=x6u;opts[r2]=ret;}}if(typeof url === A9){var V5=C10;V5+=u2Z;var o=url[V5](_this,parentField[m1I](),data,update,e);if(o){var L_=m39;L_+=B3P;var U$=x4g;U$+=u3Y;U$+=F8J;U$+=B73;if(typeof o === U$ && typeof o[L_] === v4B){o[c7m](function(resolved){if(resolved){update(resolved);}});}else {update(o);}}}else {var W$=H9z;W$+=x6u;W$+=h07;if($[v5K](url)){var O4=m9z;O4+=D8W;O4+=V8x;$[O4](ajaxOpts,url);}else {ajaxOpts[X3E]=url;}$[W$]($[t8A](ajaxOpts,{data:data,success:update}));}});return this;}function destroy(){var B0w="estroy";var Q8T='.dte';var X1i="templa";var O8K="late";var Q_=J_0;Q_+=x4g;Q_+=F6G;var w6=x4g;w6+=A43;var u8=J_0;u8+=m9z;u8+=U1e;L7K.b9t();u8+=A33;var i$=X1i;i$+=W1Z;if(this[J7D][k42]){this[H5f]();}this[F2L]();if(this[J7D][i$]){var Q5=W1Z;Q5+=F6G;Q5+=m59;Q5+=O8K;$(o0u)[K$Z](this[J7D][Q5]);}var controller=this[J7D][S5B];if(controller[u8]){var g2=J_0;g2+=B0w;controller[g2](this);}$(document)[w6](Q8T + this[J7D][Z9Z]);this[Q_]=X2m;this[J7D]=X2m;}function disable(name){var that=this;$[o8Y](this[m13](name),function(i,n){that[Y8Y](n)[e9x]();});return this;}function display(showIn){var Y9n="laye";var R9=D4D;R9+=J7D;R9+=m9z;var x$=x4g;x$+=m59;x$+=m9z;x$+=B3P;if(showIn === undefined){var N6=L0p;N6+=D47;N6+=Y9n;N6+=J_0;return this[J7D][N6];}return this[showIn?x$:R9]();}function displayed(){var E9=B1g;L7K.b9t();E9+=W6P;E9+=J7D;return $[S9O](this[J7D][E9],function(fieldIn,name){return fieldIn[k42]()?name:X2m;});}function displayNode(){var W6E="ontrol";var r7f="isplayC";var J8=t5D;J8+=m9z;var D4=J_0;D4+=r7f;D4+=W6E;D4+=w36;return this[J7D][D4][J8](this);}function edit(items,arg1,arg2,arg3,arg4){var J4G="_ed";L7K.b9t();var l8m="dArg";var r7=x4g;r7+=m59;r7+=U15;var x5=J4G;x5+=H7H;var x0=h$1;x0+=l8m;x0+=J7D;var t7=a4a;t7+=B91;t7+=g4M;var _this=this;var that=this;if(this[t7](function(){var C4=m9z;C4+=J_0;C4+=H7H;that[C4](items,arg1,arg2,arg3,arg4);})){return this;}var argOpts=this[x0](arg1,arg2,arg3,arg4);this[x5](items,this[W_A](O8Y,items),P5M,argOpts[r7],function(){var e75="_formOptio";var e9F="embleMain";var W2=e75;W2+=x3l;var i8=a4a;i8+=l6s;i8+=J7D;i8+=e9F;_this[i8]();_this[W2](argOpts[p1E]);argOpts[a1Y]();});return this;}function enable(name){var that=this;L7K.l4n();$[o8Y](this[m13](name),function(i,n){that[Y8Y](n)[g3z]();});return this;}function error$1(name,msg){var F0W="formError";var wrapper=$(this[I_d][A8Y]);L7K.b9t();if(msg === undefined){var a1=J_0;a1+=x4g;a1+=F6G;this[v5z](this[a1][F0W],name,T5f,function(){var x99="toggleC";var W57="inFormErr";var I0=W57;I0+=h3Y;var Y6=x99;Y6+=q16;wrapper[Y6](I0,name !== undefined && name !== l2Y);});this[J7D][Q5S]=name;}else {this[Y8Y](name)[x_w](msg);}return this;}function field(name){var B0e='Unknown field name - ';var H4=t0$;H4+=m9z;H4+=W6P;H4+=J7D;L7K.l4n();var sFields=this[J7D][H4];if(!sFields[name]){throw new Error(B0e + name);}return sFields[name];}function fields(){L7K.l4n();var J1=l7$;J1+=u9Q;J1+=J7D;return $[S9O](this[J7D][J1],function(fieldIn,name){L7K.b9t();return name;});}function file(name,id){var M1_='Unknown file id ';var X$7=' in table ';var S_=t0$;S_+=E3o;S_+=C_A;var tableFromFile=this[S_](name);var fileFromTable=tableFromFile[id];if(!fileFromTable){throw new Error(M1_ + id + X$7 + name);}return tableFromFile[id];}function files(name){var D7K='Unknown file table name: ';if(!name){var h1U=l7$;h1U+=t_M;h1U+=y3A;h1U+=J7D;return Editor[h1U];}var editorTable=Editor[Y$q][name];if(!editorTable){throw new Error(D7K + name);}return editorTable;}function get(name){var P9T=c3v;P9T+=m9z;P9T+=F$o;var that=this;if(!name){var J1W=l7$;J1W+=o80;J1W+=K23;name=this[J1W]();}if(Array[p5M](name)){var r86=m9z;r86+=Y8o;var out_1={};$[r86](name,function(i,n){var o8i=t0$;o8i+=m9z;o8i+=E3o;o8i+=J_0;out_1[n]=that[o8i](n)[u2A]();});return out_1;}return this[Y8Y](name)[P9T]();}function hide(names,animate){L7K.l4n();var A7J="_fiel";var I34=A7J;I34+=N3_;var that=this;$[o8Y](this[I34](names),function(i,n){var g21=l7$;g21+=w_4;g21+=W6P;that[g21](n)[m0K](animate);});return this;}function ids(includeHash){var v9q="ields";var O_3="editF";var w8k=O_3;L7K.b9t();w8k+=v9q;var A8C=F6G;A8C+=x6u;A8C+=m59;if(includeHash === void M1q){includeHash=g49;}return $[A8C](this[J7D][w8k],function(editIn,idSrc){L7K.l4n();return includeHash === T5f?B$A + idSrc:idSrc;});}function inError(inNames){var J6K="formErro";var b71=J6K;b71+=M5g;$(this[I_d][b71]);if(this[J7D][Q5S]){return T5f;}var names=this[m13](inNames);for(var i=M1q,ien=names[m2_];i < ien;i++){if(this[Y8Y](names[i])[n4o]()){return T5f;}}return g49;}function inline(cell,fieldName,opts){var n21="Cannot edit mo";var B8F="_Field";var W32="ne at a time";var a06="ormOpti";var F_s=" row inli";var T6Z="re than one";var m38=t_M;m38+=X7U;m38+=B3P;m38+=m9z;var c3x=w4j;c3x+=t_M;c3x+=J_0;c3x+=A33;var v2J=H_N;v2J+=F$o;v2J+=T03;var D8e=t5Z;D8e+=N5y;D8e+=B8F;var V$W=E3o;V$W+=l9T;V$W+=c3v;V$W+=F0A;var r75=H_N;r75+=F0A;var d9P=I9U;d9P+=R5D;var g0g=x_3;g0g+=F2P;g0g+=H0X;var X3s=l7$;X3s+=a06;X3s+=I$y;var R0K=p6S;R0K+=F$o;R0K+=l9T;R0K+=J_0;var _this=this;var that=this;if($[v5K](fieldName)){opts=fieldName;fieldName=undefined;}opts=$[R0K]({},this[J7D][X3s][p3_],opts);var editFields=this[g0g](B21,cell,fieldName);var keys=Object[d9P](editFields);if(keys[r75] > p88){var x9P=n21;x9P+=T6Z;x9P+=F_s;x9P+=W32;throw new Error(x9P);}var editRow=editFields[keys[M1q]];var hosts=[];for(var _i=M1q,_a=editRow[b$w];_i < _a[V$W];_i++){var row=_a[_i];hosts[K7Q](row);}if($(D8e,hosts)[v2J]){return this;}if(this[c3x](function(){L7K.l4n();that[p3_](cell,fieldName,opts);})){return this;}this[k_9](cell,editFields,m38,opts,function(){L7K.l4n();var a2g=U$t;a2g+=b8q;_this[a2g](editFields,opts);});return this;}function inlineCreate(insertPoint,opts){var J0J="Clas";var z5P="_dataSourc";var U6t="tFie";var s0t="_action";var C9v="xt";var l_5="mOption";var f64="itCr";var b3_="ifier";var K43='fakeRow';var S48=t_M;S48+=B3P;S48+=f64;S48+=A3U;var d3J=j_L;d3J+=l9T;d3J+=F$o;var n1t=z2m;n1t+=X7U;n1t+=Q1x;var j7R=s0t;j7R+=J0J;j7R+=J7D;var n1F=l4Q;n1F+=l_5;n1F+=J7D;var R_S=m9z;R_S+=C9v;R_S+=l9T;R_S+=J_0;var l27=z5P;l27+=m9z;var T07=P3c;T07+=U6t;T07+=D$d;var d5o=G19;d5o+=b3_;var Y1G=G1V;Y1G+=M5g;Y1G+=f$O;Y1G+=W1Z;var x$k=B1g;x$k+=D$d;var L4e=p1Z;L4e+=T03;var j4o=h4W;j4o+=N7S;var _this=this;if($[j4o](insertPoint)){opts=insertPoint;insertPoint=X2m;}if(this[J5W](function(){var v7l="Create";var b$Z=h66;b$Z+=B3P;b$Z+=m9z;b$Z+=v7l;L7K.l4n();_this[b$Z](insertPoint,opts);})){return this;}$[L4e](this[J7D][x$k],function(name,fieldIn){var V0a=J_0;V0a+=m9z;V0a+=l7$;var N1n=J7D;N1n+=m9z;N1n+=F$o;var x3X=c5m;x3X+=u13;x3X+=m9z;x3X+=F$o;fieldIn[h8a]();fieldIn[x3X](M1q,fieldIn[g0I]());fieldIn[N1n](fieldIn[V0a]());});this[J7D][d1k]=P5M;this[J7D][J$l]=Y1G;this[J7D][d5o]=X2m;this[J7D][T07]=this[l27](K43,insertPoint);opts=$[R_S]({},this[J7D][n1F][p3_],opts);this[j7R]();this[n1t](this[J7D][X8F],opts,function(){var M0P="keRo";var V7F="rce";var C67="_dataSou";var w0X="End";var J$m="fa";var C8m=J$m;C8m+=M0P;C8m+=R3d;L7K.l4n();C8m+=w0X;var w8D=C67;w8D+=V7F;_this[w8D](C8m);});this[d3J](S48,X2m);return this;}function message(name,msg){var O0$="ssage";if(msg === undefined){var f5Y=C1v;f5Y+=v12;f5Y+=I$v;this[v5z](this[I_d][f5Y],name);}else {var U56=F6G;U56+=m9z;U56+=O0$;this[Y8Y](name)[U56](msg);}return this;}function mode(modeIn){var r07="diting mode";var n4i='Changing from create mode is not supported';var F2v="Not cur";var G9J="tly in an e";var O0f=G1V;O0f+=C5B;O0f+=x3a;O0f+=m9z;var u2Q=x6u;u2Q+=G1V;u2Q+=B91;u2Q+=g5T;if(!modeIn){return this[J7D][J$l];}if(!this[J7D][u2Q]){var D2x=F2v;D2x+=e2b;D2x+=G9J;D2x+=r07;throw new Error(D2x);}else if(this[J7D][J$l] === R4H && modeIn !== O0f){throw new Error(n4i);}this[J7D][J$l]=modeIn;return this;}function modifier(){return this[J7D][E_V];}function multiGet(fieldNames){var q_v=l7$;q_v+=w_4;q_v+=W6P;var x3w=P1D;L7K.b9t();x3w+=M0v;var that=this;if(fieldNames === undefined){fieldNames=this[c26]();}if(Array[x3w](fieldNames)){var out_2={};$[o8Y](fieldNames,function(i,name){var j1$=l7$;L7K.l4n();j1$+=w_4;j1$+=W6P;out_2[name]=that[j1$](name)[I6l]();});return out_2;}return this[q_v](fieldNames)[I6l]();}function multiSet(fieldNames,valIn){var r2b="ainObj";var S5n="isPl";var Z9Y=S5n;Z9Y+=r2b;Z9Y+=B73;L7K.b9t();var that=this;if($[Z9Y](fieldNames) && valIn === undefined){var x4n=m9z;x4n+=x6u;x4n+=G1V;x4n+=T03;$[x4n](fieldNames,function(name,value){var U98=F6G;U98+=K60;U98+=T0o;var L15=S4_;L15+=J_0;that[L15](name)[U98](value);});}else {this[Y8Y](fieldNames)[T8j](valIn);}return this;}function node(name){var that=this;if(!name){var B8N=h3Y;B8N+=V6H;B8N+=M5g;name=this[B8N]();}L7K.b9t();return Array[p5M](name)?$[S9O](name,function(n){var A5t=C7j;A5t+=V6H;var k$k=l7$;k$k+=o80;k$k+=J_0;L7K.b9t();return that[k$k](n)[A5t]();}):this[Y8Y](name)[S_L]();}function off(name,fn){$(this)[H9D](this[F7o](name),fn);L7K.b9t();return this;}function on(name,fn){var A8R=x4g;L7K.l4n();A8R+=B3P;$(this)[A8R](this[F7o](name),fn);return this;}function one(name,fn){var s4w="ntNa";var e5s=f4F;e5s+=s4w;e5s+=A_J;var A$S=x4g;A$S+=B3P;A$S+=m9z;$(this)[A$S](this[e5s](name),fn);return this;}function open(){var k2c="mai";var g_t="nest";var b3L="_closeRe";var q1q="_nestedOpen";var n$R=k2c;n$R+=B3P;var o5R=q0R;o5R+=t_M;o5R+=B3P;var r$H=Z6h;r$H+=m9z;r$H+=V_S;var f1d=b3L;f1d+=c3v;var _this=this;this[s7w]();this[f1d](function(){var P$M="stedClos";var f7H=s8Z;f7H+=P$M;f7H+=m9z;L7K.b9t();_this[f7H](function(){var S0P="lea";var h6y="cInfo";var o9y="rDynami";var C7O=F6G;C7O+=x6u;C7O+=t_M;C7O+=B3P;var R_n=h4F;R_n+=S0P;R_n+=o9y;R_n+=h6y;_this[R_n]();_this[m6A](n89,[C7O]);});});var ret=this[r$H](o5R);if(!ret){return this;}this[q1q](function(){var i6R="tOpt";var v2I=H5X;v2I+=F$o;v2I+=t_M;v2I+=g5T;var j$r=P5B;j$r+=t_d;var P1s=P3c;P1s+=i6R;P1s+=J7D;var U4Q=q0R;U4Q+=m59;var N_3=a4a;N_3+=l7$;N_3+=P37;N_3+=J7D;_this[N_3]($[U4Q](_this[J7D][O7k],function(name){L7K.b9t();return _this[J7D][c26][name];}),_this[J7D][P1s][F4U]);_this[j$r](N0r,[P5M,_this[J7D][v2I]]);},this[J7D][u5I][g_t]);this[z$j](n$R,g49);return this;}function order(setIn){var k11="sli";var k2v='All fields, and no additional fields, must be provided for ordering.';var G2I="sor";var o2x=G2I;o2x+=F$o;var a5G=k11;a5G+=H0X;var V_8=F8J;V_8+=x4g;V_8+=t_M;V_8+=B3P;var Z1l=J7D;Z1l+=x4g;Z1l+=M5g;Z1l+=F$o;if(!setIn){return this[J7D][O7k];}if(arguments[m2_] && !Array[p5M](setIn)){var X5c=G1V;X5c+=C7y;setIn=Array[N0G][E13][X5c](arguments);}if(this[J7D][O7k][E13]()[Z1l]()[V_8](y7S) !== setIn[a5G]()[o2x]()[y1t](y7S)){throw new Error(k2v);}$[t8A](this[J7D][O7k],setIn);this[s7w]();L7K.b9t();return this;}function remove(items,arg1,arg2,arg3,arg4){var w5J="nitRemo";var J5w="_dat";var L4I="modifi";var m7W="gs";var Z48="aSour";var I_h="dAr";var E14=J_0;E14+=x6u;E14+=F$o;E14+=x6u;var Q8e=B3P;Q8e+=x4g;Q8e+=J_0;Q8e+=m9z;var m4g=t_M;m4g+=w5J;m4g+=h13;m4g+=m9z;var L32=P5B;L32+=Y9F;L32+=k1z;var Q83=l4Q;Q83+=F6G;var N_H=J_0;N_H+=x4g;N_H+=F6G;var E0X=L4I;E0X+=K2t;var c5D=M5g;c5D+=h5X;c5D+=w9O;c5D+=m9z;var T6O=x6u;T6O+=W6y;T6O+=v$K;T6O+=B3P;var v9a=l7$;v9a+=w_4;v9a+=W6P;v9a+=J7D;var w$s=J5w;w$s+=Z48;w$s+=H0X;var X2g=h$1;X2g+=I_h;X2g+=m7W;var _this=this;var that=this;if(this[J5W](function(){var A83=M5g;A83+=h5X;A83+=x4g;A83+=Y9F;that[A83](items,arg1,arg2,arg3,arg4);})){return this;}if(items[m2_] === undefined){items=[items];}var argOpts=this[X2g](arg1,arg2,arg3,arg4);var editFields=this[w$s](v9a,items);this[J7D][T6O]=c5D;this[J7D][E0X]=items;this[J7D][X8F]=editFields;this[N_H][Q83][Y3a][s$8]=N7C;this[O9S]();this[L32](m4g,[pluck(editFields,Q8e),pluck(editFields,E14),items],function(){var s3b='initMultiRemove';var O7V=a4a;O7V+=Q9x;O7V+=k38;_this[O7V](s3b,[editFields,items],function(){var O_h="Options";var y$e="q";var V3t="_form";L7K.b9t();var b_e=w55;b_e+=J7D;var k64=V3t;k64+=O_h;_this[q0i]();_this[k64](argOpts[b_e]);argOpts[a1Y]();var opts=_this[J7D][u5I];if(opts[F4U] !== X2m){var n4y=l7$;n4y+=t47;n4y+=c5s;var b3T=m9z;b3T+=y$e;var B$f=J_0;B$f+=x4g;B$f+=F6G;var G47=u3Y;G47+=v9Y;G47+=x4g;G47+=B3P;$(G47,_this[B$f][F8y])[b3T](opts[n4y])[F4U]();}});});return this;}function set(setIn,valIn){var that=this;if(!$[v5K](setIn)){var o={};o[setIn]=valIn;setIn=o;}$[o8Y](setIn,function(n,v){var r5J=l7$;r5J+=w_4;r5J+=W6P;L7K.b9t();that[r5J](n)[L6_](v);});L7K.b9t();return this;}function show(names,animate){var C1z="Names";var R6B=f1t;R6B+=W6P;R6B+=C1z;L7K.l4n();var that=this;$[o8Y](this[R6B](names),function(i,n){var j0Z=T3Q;j0Z+=x4g;j0Z+=R3d;var D6t=l7$;D6t+=o80;D6t+=J_0;that[D6t](n)[j0Z](animate);});return this;}function submit(successCallback,errorCallback,formatdata,hideIn){var A0z="acti";var a8J=m9z;a8J+=x6u;a8J+=b_g;var M_F=m9z;M_F+=x6u;M_F+=b_g;var u0s=m9z;u0s+=M5g;u0s+=J$_;u0s+=M5g;var I8g=A0z;I8g+=g5T;var _this=this;var sFields=this[J7D][c26];var errorFields=[];var errorReady=M1q;var sent=g49;L7K.l4n();if(this[J7D][D$8] || !this[J7D][I8g]){return this;}this[I_z](T5f);var send=function(){var h69='initSubmit';var b4V=F3w;b4V+=B3P;if(errorFields[m2_] !== errorReady || sent){return;}_this[m6A](h69,[_this[J7D][b4V]],function(result){var U9b="_submit";var t0u="ocess";if(result === g49){var z7h=a4a;z7h+=D6L;z7h+=t0u;z7h+=Z1a;_this[z7h](g49);return;}sent=T5f;_this[U9b](successCallback,errorCallback,formatdata,hideIn);});};this[u0s]();$[M_F](sFields,function(name,fieldIn){if(fieldIn[n4o]()){var T69=m59;T69+=J3O;T69+=T3Q;errorFields[T69](name);}});$[a8J](errorFields,function(i,name){var V_0=K2t;L7K.b9t();V_0+=M5g;V_0+=x4g;V_0+=M5g;sFields[name][V_0](l2Y,function(){L7K.l4n();errorReady++;send();});});send();return this;}function table(setIn){if(setIn === undefined){var W7w=f$8;W7w+=u3Y;W7w+=E3o;W7w+=m9z;return this[J7D][W7w];}this[J7D][O81]=setIn;return this;}function template(setIn){var w1x="emplate";var N51="tem";var F_W=N51;F_W+=T9z;F_W+=x3a;F_W+=m9z;if(setIn === undefined){var J6j=F$o;J6j+=w1x;return this[J7D][J6j];}this[J7D][F_W]=setIn === X2m?X2m:$(setIn);return this;}function title(titleIn){var S2v="htm";L7K.l4n();var Q80=V3z;Q80+=B3P;Q80+=P1R;var f3Y=c0K;f3Y+=x6u;f3Y+=J_0;f3Y+=K2t;var header=$(this[I_d][f3Y])[C8B](L7t + this[I58][x_C][t4U]);if(titleIn === undefined){var g3I=S2v;g3I+=E3o;return header[g3I]();}if(typeof titleIn === Q80){var L2r=Y4Q;L2r+=m59;L2r+=t_M;titleIn=titleIn(this,new DataTable$4[L2r](this[J7D][O81]));}header[y64](titleIn);return this;}function val(fieldIn,value){if(value !== undefined || $[v5K](fieldIn)){var a98=J7D;a98+=m9z;a98+=F$o;return this[a98](fieldIn,value);}L7K.l4n();return this[u2A](fieldIn);;}function error(msg,tn,thro){var l8N=' For more information, please refer to https://datatables.net/tn/';if(thro === void M1q){thro=T5f;}var display=tn?msg + l8N + tn:msg;if(thro){throw display;}else {var z52=R3d;z52+=x6u;z52+=M5g;z52+=B3P;console[z52](display);}}function pairs(data,props,fn){var O_o='value';var p$G="labe";var W9e="abe";var G7I=E3o;G7I+=W9e;G7I+=E3o;var M8p=n91;M8p+=V8x;var i;var ien;var dataPoint;props=$[M8p]({label:G7I,value:O_o},props);if(Array[p5M](data)){var T7H=b3X;T7H+=T03;for((i=M1q,ien=data[T7H]);i < ien;i++){dataPoint=data[i];if($[v5K](dataPoint)){var e5M=x6u;e5M+=C_e;e5M+=M5g;var B$i=E3o;B$i+=G2T;var m4i=Y_Y;m4i+=E3o;m4i+=a6X;var j9P=p$G;j9P+=E3o;fn(dataPoint[props[n01]] === undefined?dataPoint[props[j9P]]:dataPoint[props[m4i]],dataPoint[props[B$i]],i,dataPoint[e5M]);}else {fn(dataPoint,dataPoint,i);}}}else {i=M1q;$[o8Y](data,function(key,val){fn(val,key,i);i++;});}}function upload$1(editor,conf,files,progressCallback,completeCallback){var g8j="onload";var v5j="Tex";var P0f="adi";var W8n="readA";var g4w="L";var h4k="sDataUR";var M5H="ead";var v$B="ft";var i8N="loading file</i>";var P5h="mitL";var N1i="eft";var o7M="imitL";var J$7="A server error occurred while uplo";var U6r="i>Up";var T_6="fileR";var A$$="_li";var H4v="ng the file";var u3X=W8n;u3X+=h4k;u3X+=g4w;var U78=B87;U78+=o7M;U78+=m9z;U78+=v$B;var x00=F6G;x00+=H0m;var E1m=W7n;E1m+=U6r;E1m+=i8N;var e7C=T_6;e7C+=M5H;e7C+=v5j;e7C+=F$o;var p7u=x6u;p7u+=F8J;p7u+=C1w;L7K.b9t();var N9t=B3P;N9t+=x6u;N9t+=F6G;N9t+=m9z;var reader=new FileReader();var counter=M1q;var ids=[];var generalError=J$7;generalError+=P0f;generalError+=H4v;editor[x_w](conf[N9t],l2Y);if(typeof conf[p7u] === v4B){conf[y58](files,function(idsIn){L7K.l4n();completeCallback[H7v](editor,idsIn);});return;}progressCallback(conf,conf[e7C] || E1m);reader[g8j]=function(e){var h81="isPlainObje";var j7n='preUpload';var s4F='uploadField';var S4e="xData";var u$7="tion instead.";var G94='No Ajax option specified for upload plug-in';var E0t="axDat";var z9f="ase use it as a func";var Q8R="uploa";var e_3='upload';var F7y="pload";var S4U="Upload feature cannot use `ajax.data` with an object. Ple";var S73=B3P;S73+=W5M;var l4U=h81;l4U+=W6y;var T5E=K8n;T5E+=I8x;T5E+=c3v;var u5G=H9z;u5G+=C1w;var t70=H9z;t70+=C1w;var Z5N=h81;Z5N+=W6y;var u9n=x6u;u9n+=F8J;u9n+=x6u;u9n+=S4e;var b9a=f9u;b9a+=m9z;b9a+=B3P;b9a+=J_0;var Q8y=J3O;Q8y+=F7y;var I_U=x6u;I_U+=m59;I_U+=C5C;I_U+=V8x;var data=new FormData();var ajax;data[I_U](S0u,Q8y);data[K$Z](s4F,conf[z$n]);data[b9a](e_3,files[counter]);if(conf[u9n]){var z0Y=H9z;z0Y+=E0t;z0Y+=x6u;conf[z0Y](data,files[counter],counter);}if(conf[y58]){ajax=conf[y58];}else if($[Z5N](editor[J7D][t70])){var S$p=Q8R;S$p+=J_0;var Y8n=J3O;Y8n+=m59;Y8n+=Y5w;ajax=editor[J7D][y58][Y8n]?editor[J7D][y58][S$p]:editor[J7D][y58];}else if(typeof editor[J7D][u5G] === f2Z){var R2d=x6u;R2d+=F8J;R2d+=x6u;R2d+=h07;ajax=editor[J7D][R2d];}if(!ajax){throw new Error(G94);}if(typeof ajax === T5E){ajax={url:ajax};}if(typeof ajax[z4k] === v4B){var d6U=f$O;d6U+=b_g;var p1i=J_0;p1i+=x3a;p1i+=x6u;var d={};var ret=ajax[p1i](d);if(ret !== undefined && typeof ret !== f2Z){d=ret;}$[d6U](d,function(key,value){L7K.b9t();var m2u=H0m;m2u+=B6V;data[m2u](key,value);});}else if($[l4U](ajax[z4k])){var T$e=S4U;T$e+=z9f;T$e+=u$7;throw new Error(T$e);}editor[m6A](j7n,[conf[S73],files[counter],data],function(preRet){var i5W="readAsDataURL";var J5a="preSubmit.DTE";L7K.l4n();var e5R=m59;e5R+=x4g;e5R+=m7i;var K_5=m9z;K_5+=h07;K_5+=c1p;var q3E=x6u;q3E+=F8J;q3E+=x6u;q3E+=h07;var W2f=J5a;W2f+=j3_;var V00=x4g;V00+=B3P;if(preRet === g49){var c7v=E3o;c7v+=E20;if(counter < files[c7v] - p88){counter++;reader[i5W](files[counter]);}else {completeCallback[H7v](editor,ids);}return;}var submit=g49;editor[V00](W2f,function(){submit=T5f;L7K.b9t();return g49;});$[q3E]($[K_5]({},ajax,{contentType:g49,data:data,dataType:U7v,error:function(xhr){var P72="uploadX";var H8i="hrEr";L7K.l4n();var H_B=P72;H_B+=H8i;H_B+=e0u;var y7U=P5B;y7U+=O8F;y7U+=F$o;editor[x_w](conf[z$n],generalError);editor[y7U](H_B,[conf[z$n],xhr]);progressCallback(conf);},processData:g49,success:function(json){var f1L="ubm";var r05="ess";var W_a="ieldErrors";var Q6P="uploadXhr";var a1u="AsDataURL";var A2t="read";var H14="Succ";var v3b='preSubmit.DTE_Upload';var o4X=t_M;o4X+=J_0;var h98=l3o;h98+=v4c;var S3L=y3A;S3L+=w66;var f35=l7$;f35+=W_a;L7K.l4n();var g9x=B3P;g9x+=x6u;g9x+=F6G;g9x+=m9z;var v1g=Q6P;v1g+=H14;v1g+=r05;var P1r=x4g;P1r+=A43;editor[P1r](v3b);editor[m6A](v1g,[conf[g9x],json]);if(json[W6N] && json[f35][S3L]){var errors=json[W6N];for(var i=M1q,ien=errors[m2_];i < ien;i++){editor[x_w](errors[i][z$n],errors[i][h3o]);}}else if(json[x_w]){var W59=m9z;W59+=M5g;W59+=e0u;editor[W59](json[x_w]);}else if(!json[h98] || !json[x4w][o4X]){editor[x_w](conf[z$n],generalError);}else {var i02=t_M;i02+=J_0;var O5H=K29;O5H+=T3Q;var V8B=l7$;V8B+=J7I;V8B+=m9z;V8B+=J7D;if(json[V8B]){var A0e=p1Z;A0e+=T03;$[A0e](json[Y$q],function(table,filesIn){var I0I="iles";var Y3f=l7$;L7K.b9t();Y3f+=J7I;Y3f+=m9z;Y3f+=J7D;var R$u=l7$;R$u+=I0I;if(!Editor[R$u][table]){var U3J=j7l;U3J+=J7D;Editor[U3J][table]={};}$[t8A](Editor[Y3f][table],filesIn);});}ids[O5H](json[x4w][i02]);if(counter < files[m2_] - p88){var G92=A2t;G92+=a1u;counter++;reader[G92](files[counter]);}else {completeCallback[H7v](editor,ids);if(submit){var R3I=J7D;R3I+=f1L;R3I+=t_M;R3I+=F$o;editor[R3I]();}}}progressCallback(conf);},type:e5R,xhr:function(){var a0R="plo";var g39="onloadend";var f_m="ajaxSett";var B6r="xhr";var X$9="onprogress";var e5L=f_m;L7K.b9t();e5L+=b$A;var xhr=$[e5L][B6r]();if(xhr[x4w]){var M7R=J3O;M7R+=a0R;M7R+=x6u;M7R+=J_0;xhr[x4w][X$9]=function(){var S21=':';var M5d="utable";var y_c="loaded";var w59="oFixed";L7K.l4n();var P_1='%';var g_W="Comp";var m3S=100;var y2M=y3A;y2M+=w66;y2M+=g_W;y2M+=M5d;if(e[y2M]){var t9w=E3o;t9w+=z0E;t9w+=F0A;var s$i=F$o;s$i+=w59;var x0v=F$o;x0v+=x4g;x0v+=F$o;x0v+=X1g;var percent=(e[y_c] / e[x0v] * m3S)[s$i](M1q) + P_1;progressCallback(conf,files[m2_] === p88?percent:counter + S21 + files[t9w] + m5s + percent);}};xhr[M7R][g39]=function(){var z0R="processingText";var H$8="Proce";var V_a="ssin";var Q4Y=H$8;Q4Y+=V_a;Q4Y+=c3v;progressCallback(conf,conf[z0R] || Q4Y);};}return xhr;}}));});};files=$[x00](files,function(val){return val;});if(conf[U78] !== undefined){var l25=H_N;l25+=F0A;var c8Y=A$$;c8Y+=P5h;c8Y+=N1i;files[a59](conf[c8Y],files[l25]);}reader[u3X](files[M1q]);}var DataTable$3=$[w9$][n8X];var _inlineCounter=M1q;function _actionClass(){var K5x="remov";var w50="eCla";var X5u="rea";var d_Y="addC";var S3E=G1V;L7K.l4n();S3E+=X5u;S3E+=W1Z;var m2l=N2u;m2l+=B3P;var b2o=h6w;b2o+=H7H;var Y9s=K5x;Y9s+=w50;Y9s+=f63;var m$n=l$M;m$n+=f9u;m$n+=m9z;m$n+=M5g;var J68=x6u;J68+=G1V;J68+=z4s;J68+=x3l;var classesActions=this[I58][J68];var action=this[J7D][J$l];var wrapper=$(this[I_d][m$n]);wrapper[Y9s]([classesActions[J6Y],classesActions[b2o],classesActions[H21]][m2l](m5s));if(action === S3E){var p6x=v4c;p6x+=E5x;p6x+=E3o;p6x+=h4Q;wrapper[p6x](classesActions[J6Y]);}else if(action === n13){var p_$=d_Y;p_$+=E3o;p_$+=x6u;p_$+=f63;wrapper[p_$](classesActions[F33]);}else if(action === K5K){var E3r=C5B;E3r+=f6f;E3r+=Y9F;wrapper[w5Y](classesActions[E3r]);}}function _ajax(data,success,error,submitParams){var O$v='?';var o41="delete";var O6I="param";var V9v="replacements";var P7V="plet";var l21="nct";var C4X="comple";var h4$=/_id_/;var k1p='DELETE';L7K.l4n();var B$X="deleteBody";var d04=/{id}/;var T1X=x6u;T1X+=F8J;T1X+=x6u;T1X+=h07;var G3F=o41;G3F+=u1c;G3F+=n9X;var e6x=F$o;e6x+=A33;e6x+=m59;e6x+=m9z;var D_G=C5B;D_G+=T9z;D_G+=H5X;D_G+=m9z;var e2W=L7j;e2W+=E3o;var h0m=s8a;h0m+=P1R;var H4J=t_M;H4J+=T2X;var H8r=j0k;H8r+=s7M;H8r+=e$Q;var v0Z=H5X;v0Z+=F$o;v0Z+=t_M;v0Z+=g5T;var action=this[J7D][v0Z];var thrown;var opts={complete:[function(xhr,text){var N8L="resp";var w0A=204;var k1v=400;var R9Q="responseJSON";var M89="nseText";var B5a='null';var B2t="responseText";var T$1=m7i;T$1+=x3a;T$1+=J3O;T$1+=J7D;var json=X2m;if(xhr[T$1] === w0A || xhr[B2t] === B5a){json={};}else {try{var m2$=N8L;m2$+=x4g;m2$+=M89;var O$S=m59;O$S+=j4C;O$S+=J86;json=xhr[R9Q]?xhr[R9Q]:JSON[O$S](xhr[m2$]);}catch(e){}}if($[v5K](json) || Array[p5M](json)){success(json,xhr[h3o] >= k1v,xhr);}else {error(xhr,text,thrown);}}],data:X2m,dataType:U7v,error:[function(xhr,text,err){thrown=err;}],success:[],type:H8r};var a;var ajaxSrc=this[J7D][y58];var id=action === n13 || action === K5K?pluck(this[J7D][X8F],H4J)[y1t](y$0):X2m;if($[v5K](ajaxSrc) && ajaxSrc[action]){ajaxSrc=ajaxSrc[action];}if(typeof ajaxSrc === h0m){ajaxSrc(X2m,X2m,data,success,error);return;}else if(typeof ajaxSrc === f2Z){if(ajaxSrc[J6b](m5s) !== -p88){var W$y=F$o;W$y+=A33;W$y+=C5C;a=ajaxSrc[R$w](m5s);opts[W$y]=a[M1q];opts[X3E]=a[p88];}else {var p$N=J3O;p$N+=M5g;p$N+=E3o;opts[p$N]=ajaxSrc;}}else {var X9U=m9z;X9U+=M5g;X9U+=M5g;X9U+=h3Y;var i22=p7R;i22+=H6f;var optsCopy=$[t8A]({},ajaxSrc || ({}));if(optsCopy[i22]){var Y55=C4X;Y55+=W1Z;var G1T=b00;G1T+=F6G;G1T+=P7V;G1T+=m9z;var v6k=C4X;v6k+=W1Z;opts[v6k][l1E](optsCopy[G1T]);delete optsCopy[Y55];}if(optsCopy[X9U]){var j_v=m9z;j_v+=q$8;j_v+=M5g;opts[j_v][l1E](optsCopy[x_w]);delete optsCopy[x_w];}opts=$[t8A]({},opts,optsCopy);}if(opts[V9v]){var F68=m9z;F68+=x6u;F68+=G1V;F68+=T03;$[F68](opts[V9v],function(key,repl){var B0s='}';var T2d='{';var B$U=G1V;B$U+=x6u;B$U+=E3o;B$U+=E3o;var Z3m=p9u;Z3m+=E3o;Z3m+=x6u;Z3m+=H0X;var o7o=J3O;o7o+=M5g;o7o+=E3o;opts[o7o]=opts[X3E][Z3m](T2d + key + B0s,repl[B$U](this,key,id,action,data));});}opts[X3E]=opts[e2W][o1S](h4$,id)[D_G](d04,id);if(opts[z4k]){var C4U=M49;C4U+=x6u;var V$J=V3z;V$J+=l21;V$J+=v$K;V$J+=B3P;var isFn=typeof opts[z4k] === V$J;var newData=isFn?opts[C4U](data):opts[z4k];data=isFn && newData?newData:$[t8A](T5f,data,newData);}opts[z4k]=data;if(opts[e6x] === k1p && (opts[G3F] === undefined || opts[B$X] === T5f)){var f8G=J_0;f8G+=x6u;f8G+=F$o;f8G+=x6u;var o2S=J3O;o2S+=M5g;o2S+=E3o;var r3v=d$I;r3v+=F$o;r3v+=x6u;var params=$[O6I](opts[r3v]);opts[X3E]+=opts[o2S][J6b](O$v) === -p88?O$v + params:a8A + params;delete opts[f8G];}$[T1X](opts);}function _animate(target,style,time,callback){var I4T=l7$;I4T+=B3P;if($[I4T][O6e]){var K51=w62;K51+=H42;K51+=W1Z;var m44=J7D;m44+=F$o;m44+=x4g;m44+=m59;target[m44]()[K51](style,time,callback);}else {target[e$Y](style);if(typeof time === v4B){time[H7v](target);}else if(callback){callback[H7v](target);}}}function _assembleMain(){var y8l="wrappe";var A_R="formEr";var d7F=x6u;d7F+=J6o;d7F+=l9T;d7F+=J_0;var V9P=H0m;V9P+=B6V;var v0j=A_R;v0j+=e0u;var C4h=l7$;C4h+=x4g;C4h+=x4g;C4h+=Q_Y;var c4z=T03;c4z+=f$O;c4z+=V6H;c4z+=M5g;var F$E=y8l;F$E+=M5g;var r4E=J_0;L7K.l4n();r4E+=x4g;r4E+=F6G;var dom=this[r4E];$(dom[F$E])[C7n](dom[c4z]);$(dom[C4h])[K$Z](dom[v0j])[V9P](dom[F8y]);$(dom[s1N])[K$Z](dom[n_d])[d7F](dom[C1v]);}function _blur(){var x18="onBl";var a0I="reBlu";var K4Q="ub";var z8N=J7D;z8N+=K4Q;z8N+=X57;z8N+=F$o;var b84=s8a;b84+=G1V;b84+=u4p;var a$c=m59;a$c+=a0I;a$c+=M5g;var b8E=a4a;b8E+=Q9x;b8E+=l9T;b8E+=F$o;var h7F=x18;h7F+=L7j;var F2q=m9z;F2q+=Y8x;F2q+=m59;L7K.b9t();F2q+=U15;var opts=this[J7D][F2q];var onBlur=opts[h7F];if(this[b8E](a$c) === g49){return;}if(typeof onBlur === b84){onBlur(this);}else if(onBlur === z8N){this[f4c]();}else if(onBlur === H$E){this[Z_Z]();}}function _clearDynamicInfo(errorsOnly){var u4R="iv.";var p7M=J_0;p7M+=x4g;p7M+=F6G;var N9k=J_0;N9k+=u4R;var L_X=t0$;L_X+=g$f;var Y4K=L8N;Y4K+=S8c;if(errorsOnly === void M1q){errorsOnly=g49;}if(!this[J7D]){return;}var errorClass=this[Y4K][L_X][x_w];var fields=this[J7D][c26];$(N9k + errorClass,this[p7M][A8Y])[A71](errorClass);$[o8Y](fields,function(name,field){var q4c=m9z;q4c+=T5j;q4c+=x4g;q4c+=M5g;field[q4c](l2Y);if(!errorsOnly){var M1c=A_J;M1c+=J7D;M1c+=A$X;field[M1c](l2Y);}});this[x_w](l2Y);if(!errorsOnly){var q3l=A_J;q3l+=G1Q;q3l+=c3v;q3l+=m9z;this[q3l](l2Y);}}function _close(submitComplete,mode){var Q1Z="eC";var k0q="cb";var J_M="oseC";var l5G="closeCb";var V0K="loseIcb";var x8c="lose";var c$r=G1V;c$r+=V0K;var I5u=D6L;I5u+=Q1Z;I5u+=x8c;var B6i=a4a;B6i+=I$5;B6i+=F$o;var closed;if(this[B6i](I5u) === g49){return;}if(this[J7D][l5G]){var B4L=w3s;B4L+=J_M;B4L+=u3Y;closed=this[J7D][l5G](submitComplete,mode);this[J7D][B4L]=X2m;}if(this[J7D][c$r]){var e9r=D4D;e9r+=J86;e9r+=v12;e9r+=k0q;this[J7D][m9t]();this[J7D][e9r]=X2m;}$(o0u)[H9D](c0f);this[J7D][k42]=g49;this[m6A](H$E);if(closed){var O2Z=P5B;O2Z+=O8F;O2Z+=F$o;this[O2Z](n89,[closed]);}}function _closeReg(fn){var N6v="oseCb";L7K.l4n();var F5Q=w3s;F5Q+=N6v;this[J7D][F5Q]=fn;}function _crudArgs(arg1,arg2,arg3,arg4){var G9a="oole";var V7A="ain";var j6Q=F6G;j6Q+=V7A;var J8u=u3Y;J8u+=G9a;J8u+=x6u;J8u+=B3P;var A4h=h4W;A4h+=N7S;var that=this;var title;L7K.b9t();var buttons;var show;var opts;if($[A4h](arg1)){opts=arg1;}else if(typeof arg1 === J8u){show=arg1;opts=arg2;;}else {title=arg1;buttons=arg2;show=arg3;opts=arg4;;}if(show === undefined){show=T5f;}if(title){that[q_V](title);}if(buttons){that[F8y](buttons);}return {maybeOpen:function(){L7K.b9t();if(show){that[V_S]();}},opts:$[t8A]({},this[J7D][V_u][j6Q],opts)};}function _dataSource(name){var h7A="taS";var Z0i=T03;Z0i+=S_n;var c9$=d$I;c9$+=h7A;c9$+=t22;c9$+=J7D;var J$a=f$8;J$a+=u3Y;J$a+=y3A;var i5Z=E3o;i5Z+=m9z;i5Z+=w66;var args=[];for(var _i=p88;_i < arguments[i5Z];_i++){args[_i - p88]=arguments[_i];}var dataSource=this[J7D][J$a]?Editor[c9$][q1t]:Editor[P75][Z0i];var fn=dataSource[name];if(fn){var B_f=x6u;B_f+=m59;B_f+=T9z;B_f+=A33;return fn[B_f](this,args);}}function _displayReorder(includeFields){var K0c="childre";var t0l="mp";var A0p="lat";var h33="Order";var t$q="formContent";var G$t="ncludeFiel";var B3v=s$8;B3v+=h33;var q5L=a4a;q5L+=Q9x;q5L+=m9z;q5L+=k1z;var w$R=m9z;w$R+=H5X;w$R+=T03;var G_p=K0c;G_p+=B3P;var k4s=W1Z;k4s+=t0l;k4s+=A0p;k4s+=m9z;var k4f=h3Y;k4f+=V6H;k4f+=M5g;var M9r=t0$;M9r+=m9z;M9r+=W6P;M9r+=J7D;var _this=this;var formContent=$(this[I_d][t$q]);var fields=this[J7D][M9r];var order=this[J7D][k4f];var template=this[J7D][k4s];var mode=this[J7D][d1k] || P5M;if(includeFields){this[J7D][F4f]=includeFields;}else {var V7b=t_M;V7b+=G$t;V7b+=K23;includeFields=this[J7D][V7b];}formContent[G_p]()[d4Q]();$[w$R](order,function(i,name){var c1M="_weakInArray";var u$6="after";var I5C='[data-editor-template="';var l5y='editor-field[name="';var N0_='"]';if(_this[c1M](name,includeFields) !== -p88){var s54=F6G;s54+=x6u;s54+=t_M;s54+=B3P;if(template && mode === s54){var q1W=U50;q1W+=A7F;var d2d=l7$;d2d+=t_M;d2d+=B3P;d2d+=J_0;template[d2d](l5y + name + N0_)[u$6](fields[name][S_L]());template[B23](I5C + name + q1W)[K$Z](fields[name][S_L]());}else {var G_E=f9u;G_E+=m9z;G_E+=B3P;G_E+=J_0;formContent[G_E](fields[name][S_L]());}}});if(template && mode === P5M){var b5w=H0m;b5w+=D5l;b5w+=J_0;b5w+=y$3;template[b5w](formContent);}this[q5L](B3v,[this[J7D][k42],this[J7D][J$l],formContent]);}function _edit(items,editFields,type,formOptions,setupDone){var O9v="itEdit";var S38="editFie";var I5H="ayRe";var M82="oString";var v2z='data';var f3W="onClass";var o3j="_acti";var T4z="_displ";var H0K="editData";var J$Z=B3P;J$Z+=x4g;J$Z+=V6H;var i$n=I8x;i$n+=O9v;var V8$=T4z;V8$+=I5H;V8$+=O7k;var R$M=y3A;R$M+=L0x;R$M+=F$o;R$M+=T03;var M65=m9z;M65+=x6u;M65+=b_g;var A2h=o3j;A2h+=f3W;var F4F=u3Y;F4F+=Y4R;L7K.l4n();F4F+=c1J;var k5z=J7D;k5z+=x1U;k5z+=m9z;var s8m=l7$;s8m+=h3Y;s8m+=F6G;var v4_=h6w;v4_+=H7H;var S0k=S38;S0k+=D$d;var _this=this;var fields=this[J7D][c26];var usedFields=[];var includeInOrder;var editData={};this[J7D][S0k]=editFields;this[J7D][H0K]=editData;this[J7D][E_V]=items;this[J7D][J$l]=v4_;this[I_d][s8m][k5z][s$8]=F4F;this[J7D][d1k]=type;this[A2h]();$[M65](fields,function(name,field){var i8C="_multi";var Y3U="multiRe";var R_3="tiId";var C8j=E3o;L7K.b9t();C8j+=l9T;C8j+=X1n;var t60=j4l;t60+=R_3;t60+=J7D;var f8y=i8C;f8y+=F2x;f8y+=X1g;f8y+=E$K;var Q6Z=Y3U;Q6Z+=L6_;field[Q6Z]();includeInOrder=g49;editData[name]={};$[o8Y](editFields,function(idSrc,edit){var S$6="layField";var A$x="scope";var x$s="layFields";var g9b="nullDefault";var O4M=l7$;O4M+=t_M;O4M+=u5m;if(edit[O4M][name]){var U7h=f5h;U7h+=R2T;U7h+=m9z;var n3f=g9B;n3f+=S9n;var u9Z=J_0;u9Z+=x6u;u9Z+=f$8;var val=field[B7u](edit[u9Z]);var nullDefault=field[g9b]();editData[name][idSrc]=val === X2m?l2Y:Array[n3f](val)?val[U7h]():val;if(!formOptions || formOptions[A$x] === T95){var Y2K=p2t;Y2K+=S$6;Y2K+=J7D;var W5d=p2t;W5d+=x$s;var s10=J_0;s10+=m9z;s10+=l7$;field[T8j](idSrc,val === undefined || nullDefault && val === X2m?field[s10]():val,g49);if(!edit[W5d] || edit[Y2K][name]){includeInOrder=T5f;}}else {if(!edit[g4l] || edit[g4l][name]){var m5W=J_0;m5W+=q8u;field[T8j](idSrc,val === undefined || nullDefault && val === X2m?field[m5W]():val,g49);includeInOrder=T5f;}}}});field[f8y]();if(field[t60]()[C8j] !== M1q && includeInOrder){var K_d=K29;K_d+=T3Q;usedFields[K_d](name);}});var currOrder=this[O7k]()[E13]();for(var i=currOrder[R$M] - p88;i >= M1q;i--){var d4N=F$o;d4N+=M82;var e2F=h2x;e2F+=S9n;if($[e2F](currOrder[i][d4N](),usedFields) === -p88){currOrder[a59](i,p88);}}this[V8$](currOrder);this[m6A](i$n,[pluck(editFields,J$Z)[M1q],pluck(editFields,v2z)[M1q],items,type],function(){var p3M="M";var B22="ultiEdi";var N4l=F3h;N4l+=p3M;N4l+=B22;N4l+=F$o;_this[m6A](N4l,[editFields,items,type],function(){L7K.b9t();setupDone();});});}function _event(trigger,args,promiseComplete){var T2K="result";var O$N='Cancelled';var q75="trigg";var o4B="bj";var K5b="Event";var i0d="Ev";var r0i="triggerHandler";var k5p="xOf";var a5E="erHandler";var j7B=t_M;j7B+=M30;j7B+=Z0l;if(args === void M1q){args=[];}if(promiseComplete === void M1q){promiseComplete=undefined;}if(Array[j7B](trigger)){var f$N=H_N;f$N+=F0A;for(var i=M1q,ien=trigger[f$N];i < ien;i++){this[m6A](trigger[i],args);}}else {var m5e=m59;m5e+=M5g;m5e+=m9z;var E4D=I8x;E4D+=J_0;E4D+=m9z;E4D+=k5p;var T_Y=q75;T_Y+=a5E;var e=$[K5b](trigger);$(this)[T_Y](e,args);var result=e[T2K];if(trigger[E4D](m5e) === M1q && result === g49){var z70=i0d;z70+=l9T;z70+=F$o;$(this)[r0i]($[z70](trigger + O$N),args);}if(promiseComplete){var I59=F$o;I59+=T03;I59+=m9z;I59+=B3P;var G2k=x4g;G2k+=o4B;G2k+=B73;if(result && typeof result === G2k && result[I59]){result[c7m](promiseComplete);}else {promiseComplete(result);}}return result;}}function _eventName(input){var L21="match";var N28="lit";var Z5K=/^on([A-Z])/;var x5W=3;var L_Q="substring";var Z3$="toLowerCase";var l8V=N2u;l8V+=B3P;var a$q=y3A;L7K.b9t();a$q+=B3P;a$q+=X1n;var G6a=J7D;G6a+=m59;G6a+=N28;var name;var names=input[G6a](m5s);for(var i=M1q,ien=names[a$q];i < ien;i++){name=names[i];var onStyle=name[L21](Z5K);if(onStyle){name=onStyle[p88][Z3$]() + name[L_Q](x5W);}names[i]=name;}return names[l8V](m5s);}function _fieldFromNode(node){var e2r=l7$;L7K.l4n();e2r+=o80;e2r+=K23;var foundField=X2m;$[o8Y](this[J7D][e2r],function(name,field){var l3f=E3o;l3f+=E20;var r23=B3P;r23+=x4g;r23+=J_0;r23+=m9z;if($(field[r23]())[B23](node)[l3f]){foundField=field;}});return foundField;}function _fieldNames(fieldNames){L7K.l4n();var E16="sArra";var u5e=t_M;u5e+=E16;u5e+=A33;if(fieldNames === undefined){return this[c26]();}else if(!Array[u5e](fieldNames)){return [fieldNames];}return fieldNames;}function _focus(fieldsIn,focus){var a1M='div.DTE ';var b4D=/^jq:/;var b9n="activeEle";var g93='jq:';var b41="ment";var z0O=C5B;z0O+=m0I;var o39=H5X;o39+=z4s;o39+=B3P;var _this=this;if(this[J7D][o39] === z0O){return;}var field;var fields=$[S9O](fieldsIn,function(fieldOrName){var P$E=J7D;P$E+=F$o;P$E+=Q$G;P$E+=L0x;return typeof fieldOrName === P$E?_this[J7D][c26][fieldOrName]:fieldOrName;});if(typeof focus === M51){field=fields[focus];}else if(focus){var q7N=I8x;q7N+=U0V;q7N+=e5S;if(focus[q7N](g93) === M1q){var c1G=C5B;c1G+=T9z;c1G+=x6u;c1G+=H0X;field=$(a1M + focus[c1G](b4D,l2Y));}else {field=this[J7D][c26][focus];}}else {var z$O=u3Y;z$O+=E3o;z$O+=J3O;z$O+=M5g;var a1r=b9n;a1r+=b41;document[a1r][z$O]();}this[J7D][j0D]=field;if(field){field[F4U]();}}function _formOptions(opts){var Z0d="canReturnSubmit";var E3d="funct";L7K.l4n();var d5O="oolean";var t5Q="editCount";var H$L="nline";var f5Q="ydow";var X3S="essage";var T2e="ring";var G1l=".dte";var w6U="butt";var E_H="tle";var j08=x4g;j08+=B3P;var R2f=j$X;R2f+=f5Q;R2f+=B3P;var i19=x4g;i19+=B3P;var w4V=u3Y;w4V+=d5O;var U5q=x8u;U5q+=Y47;var N5r=K8n;N5r+=t_M;N5r+=L0x;var V_4=F6G;V_4+=X3S;var I4P=E3d;I4P+=v$K;I4P+=B3P;var f1u=B91;f1u+=E_H;var A5L=J7D;A5L+=F$o;A5L+=T2e;var W27=G1l;W27+=v12;W27+=H$L;var _this=this;var that=this;var inlineCount=_inlineCounter++;var namespace=W27 + inlineCount;this[J7D][u5I]=opts;this[J7D][t5Q]=inlineCount;if(typeof opts[q_V] === A5L || typeof opts[f1u] === I4P){var m9O=F$o;m9O+=t_M;m9O+=E_H;var l3O=B91;l3O+=F$o;l3O+=E3o;l3O+=m9z;var I8f=L4i;I8f+=y3A;this[I8f](opts[l3O]);opts[m9O]=T5f;}if(typeof opts[V_4] === N5r || typeof opts[G91] === v4B){this[G91](opts[G91]);opts[G91]=T5f;}if(typeof opts[U5q] !== w4V){var E10=u3Y;E10+=S65;E10+=J01;E10+=x3l;var R4N=w6U;R4N+=g5T;R4N+=J7D;this[R4N](opts[E10]);opts[F8y]=T5f;}$(document)[i19](R2f + namespace,function(e){var f0V="ayed";var v6$="activeElem";var X0w=d8z;X0w+=f0V;if(e[O4U] === H_Y && _this[J7D][X0w]){var u9U=v6$;u9U+=k38;var el=$(document[u9U]);if(el){var field=_this[Y22](el);if(field && typeof field[Z0d] === v4B && field[Z0d](el)){e[D4J]();}}}});$(document)[j08](S4l + namespace,function(e){var E5$="activeEleme";var v23="preventDefaul";var n57=37;var z6w="onEs";var O9p="nReturnS";var G2Z="bmit";var o7k="event";var q$0="onEsc";var C8O='button';var Z0n=".DTE_Form";var P3P="onReturn";var G4o="preve";var t$A="ntDefau";var h3v="sc";var u7X="Defa";var c16="lur";var T5B="prev";var o6o=39;var o8T="functio";var j2q="urn";var R4G=27;var j4a="nE";var c1Y="Buttons";var o34="onRet";var v$E=Z0n;v$E+=a4a;v$E+=c1Y;var W3C=m59;W3C+=j4C;W3C+=l9T;W3C+=U15;var c$l=E5$;c$l+=k1z;var el=$(document[c$l]);if(e[O4U] === H_Y && _this[J7D][k42]){var T1T=C10;T1T+=O9p;T1T+=J3O;T1T+=G2Z;var field=_this[Y22](el);if(field && typeof field[Z0d] === v4B && field[T1T](el)){if(opts[P3P] === N4h){var d9g=G4o;d9g+=t$A;d9g+=I3i;e[d9g]();_this[f4c]();}else if(typeof opts[P3P] === v4B){var U3_=o34;U3_+=j2q;var l7A=v23;l7A+=F$o;e[l7A]();opts[U3_](_this,e);}}}else if(e[O4U] === R4G){var V4R=E43;V4R+=H7H;var d1Q=z6w;d1Q+=G1V;var P7m=x4g;P7m+=j4a;P7m+=h3v;var g37=u3Y;g37+=c16;var T9e=o8T;T9e+=B3P;var d2C=g5T;d2C+=N5w;d2C+=h3v;var t6Z=D6L;t6Z+=o7k;t6Z+=u7X;t6Z+=K60;e[t6Z]();if(typeof opts[d2C] === T9e){opts[q$0](that,e);}else if(opts[q$0] === g37){that[s$G]();}else if(opts[P7m] === H$E){var V6L=G1V;V6L+=E3o;V6L+=x4g;V6L+=J86;that[V6L]();}else if(opts[d1Q] === V4R){var c$m=S7m;c$m+=j9Y;c$m+=t_M;c$m+=F$o;that[c$m]();}}else if(el[W3C](v$E)[m2_]){if(e[O4U] === n57){var P0b=l7$;P0b+=P37;P0b+=J7D;el[T5B](C8O)[N0a](P0b);}else if(e[O4U] === o6o){var p5B=Q1x;p5B+=h07;p5B+=F$o;el[p5B](C8O)[N0a](A46);}}});this[J7D][m9t]=function(){var Z7$="keydo";var y63=Z7$;y63+=I_u;$(document)[H9D](y63 + namespace);$(document)[H9D](S4l + namespace);};return namespace;}function _inline(editFields,opts,closeCb){var t_Z="utton";var R8t="dge/";var y3e="ldre";var q6C="submitTrigger";var z00="click.dt";var y4k="nl";var v4O="achFi";var k5H=" clas";L7K.l4n();var y3T="childNodes";var F5G="mErro";var G$i="ntents";var E73='tr';var J0a='.';var j8d="rAg";var L2p="e-s";var Y3C='<div class="DTE_Processing_Indicator"><span></span></div>';var N5V="ldren";var T8C="Reg";var h9j="submitHtml";var Z1U="e=";var C21="\"width:";var E7l=t_M;E7l+=y4k;E7l+=t_M;E7l+=Q1x;var n0O=C4i;n0O+=E6d;n0O+=J7D;var t7h=F6G;t7h+=x6u;t7h+=m59;var O8C=G5L;O8C+=T1u;O8C+=m9z;O8C+=T8C;var s9I=b3X;s9I+=T03;var e69=t_M;e69+=y4k;e69+=I8x;e69+=m9z;var j8T=E3o;j8T+=m9z;j8T+=B3P;j8T+=X1n;var u5p=y4_;u5p+=Y8o;var w_o=j$X;w_o+=A33;w_o+=J7D;var _this=this;if(closeCb === void M1q){closeCb=X2m;}var closed=g49;var classes=this[I58][p3_];var keys=Object[w_o](editFields);var editRow=editFields[keys[M1q]];var children=X2m;var lastAttachPoint;var elements=[];for(var i=M1q;i < editRow[u5p][j8T];i++){var D5x=m59;D5x+=J3O;D5x+=J7D;D5x+=T03;var Q$P=y4_;Q$P+=v4O;Q$P+=u5m;var name_1=editRow[Q$P][i][M1q];elements[D5x]({field:this[J7D][c26][name_1],name:name_1,node:$(editRow[b$w][i])});}var namespace=this[L6e](opts);var ret=this[X5j](e69);if(!ret){return this;}for(var _i=M1q,elements_1=elements;_i < elements_1[s9I];_i++){var U8n=S4_;U8n+=J_0;var B0h=l4Q;B0h+=F5G;B0h+=M5g;var H6k=H0m;H6k+=C5C;H6k+=B3P;H6k+=J_0;var d$Z=C7j;d$Z+=J_0;d$Z+=m9z;var a9i=l7$;a9i+=w_4;a9i+=E3o;a9i+=J_0;var L_V=E3o;L_V+=t_M;L_V+=Q1x;L_V+=M5g;var w3V=t0$;w3V+=V8x;var V0i=W7n;V0i+=d3C;var Q4X=u3Y;Q4X+=t_Z;Q4X+=J7D;var F82=U50;F82+=K2Z;var B1Q=p71;B1Q+=B3P;B1Q+=m9z;B1Q+=M5g;var c1D=M6A;c1D+=k5H;c1D+=p_t;var L7U=m59;L7U+=h07;L7U+=U50;var D3H=R3d;D3H+=t_M;D3H+=J3a;D3H+=T03;var P3p=J7D;P3p+=x1U;P3p+=Z1U;P3p+=C21;var G2B=N5w;G2B+=R8t;var R1e=I8x;R1e+=U0V;R1e+=e5S;var l4u=c5s;l4u+=m9z;l4u+=j8d;l4u+=k38;var f$y=b00;f$y+=G$i;var J9c=G1V;J9c+=h4D;J9c+=N5V;var N$Z=C7j;N$Z+=V6H;var el=elements_1[_i];var node=el[N$Z];el[J9c]=node[f$y]()[d4Q]();var style=navigator[l4u][R1e](G2B) !== -p88?P3p + node[D3H]() + L7U:l2Y;node[K$Z]($(e2R + classes[A8Y] + K8R + c1D + classes[B1Q] + F82 + style + j7f + Y3C + r65 + e2R + classes[Q4X] + o2c + V0i));node[w3V](L7t + classes[L_V][o1S](/ /g,J0a))[K$Z](el[a9i][d$Z]())[H6k](this[I_d][B0h]);lastAttachPoint=el[U8n][S_L]();if(opts[F8y]){var E8M=x8u;E8M+=F$o;E8M+=g5T;E8M+=J7D;var w81=J_0;w81+=O$H;var a53=l7$;a53+=t_M;a53+=B3P;a53+=J_0;node[a53](L7t + classes[F8y][o1S](/ /g,J0a))[K$Z](this[w81][E8M]);}}var submitTrigger=opts[q6C];if(submitTrigger !== X2m){var P0O=f9u;P0O+=l9T;P0O+=J_0;var l4W=z00;l4W+=L2p;l4W+=G4F;var M6F=x4g;M6F+=B3P;var I2p=l7$;I2p+=J$_;I2p+=F6G;if(typeof submitTrigger === M51){var L1x=a4H;L1x+=X1n;var m$J=G1V;m$J+=h4D;m$J+=y3e;m$J+=B3P;var F1j=D4D;F1j+=J7D;F1j+=C_A;F1j+=F$o;var kids=$(lastAttachPoint)[F1j](E73)[m$J]();submitTrigger=submitTrigger < M1q?kids[kids[L1x] + submitTrigger]:kids[submitTrigger];}children=Array[I2p]($(submitTrigger)[M1q][y3T])[E13]();$(children)[d4Q]();$(submitTrigger)[M6F](l4W,function(e){L7K.b9t();e[I3d]();_this[f4c]();})[P0O](opts[h9j]);}this[O8C](function(submitComplete,action){var p_V="clic";var B2W="rDyn";var x8N='click.dte-submit';var F77="amicInfo";var W01=R7u;W01+=t_M;W01+=Q1x;L7K.l4n();var N6D=G5L;N6D+=f$O;N6D+=B2W;N6D+=F77;var S0L=m9z;S0L+=J_0;S0L+=t_M;S0L+=F$o;var D8F=p_V;D8F+=I9U;closed=T5f;$(document)[H9D](D8F + namespace);if(!submitComplete || action !== S0L){var e4m=C4i;e4m+=M5g;e4m+=N5w;e4m+=Y8o;elements[e4m](function(el){var s$o="tents";var h4H=x6u;h4H+=J6o;h4H+=m9z;h4H+=V8x;var J0e=t5D;J0e+=m9z;L7K.b9t();var J$T=J_0;J$T+=Z0V;J$T+=H5X;J$T+=T03;var d1_=G1V;d1_+=x4g;d1_+=B3P;d1_+=s$o;el[S_L][d1_]()[J$T]();el[J0e][h4H](el[C8B]);});}if(submitTrigger){var A9o=H0m;A9o+=C5C;A9o+=V8x;var u4C=x4g;u4C+=l7$;u4C+=l7$;$(submitTrigger)[u4C](x8N)[h55]()[A9o](children);}_this[N6D]();if(closeCb){closeCb();}return W01;;});setTimeout(function(){L7K.b9t();var x2V="mousedo";var X_N="Ba";var Z3D="ndS";var x2W=G1V;x2W+=p71;x2W+=G1V;x2W+=I9U;var E7k=x4g;E7k+=B3P;var Q7F=x2V;Q7F+=I_u;var s4j=x4g;s4j+=B3P;var o6F=x6u;o6F+=Z3D;o6F+=E2R;o6F+=l7$;var r72=v4c;r72+=J_0;r72+=X_N;r72+=c1J;var V4n=l7$;V4n+=B3P;if(closed){return;}var back=$[V4n][l2U]?r72:o6F;var target;$(document)[s4j](Q7F + namespace,function(e){var x1K="arg";L7K.b9t();var u64=F$o;u64+=x1K;u64+=m9z;u64+=F$o;target=e[u64];})[E7k](x2W + namespace,function(e){var V2y="wns";var a3j="parents";var J30=E3o;J30+=l9T;J30+=n3b;J30+=T03;var isIn=g49;for(var _i=M1q,elements_2=elements;_i < elements_2[J30];_i++){var r8j=B3P;r8j+=x4g;r8j+=J_0;r8j+=m9z;var w8E=x4g;w8E+=V2y;var T4c=l7$;T4c+=t_M;T4c+=E2R;T4c+=J_0;var el=elements_2[_i];if(el[T4c][Y3o](w8E,target) || $[z6t](el[r8j][M1q],$(target)[a3j]()[back]()) !== -p88){isIn=T5f;}}if(!isIn){_this[s$G]();}});},M1q);this[K8C]($[t7h](elements,function(el){var v92=B1g;v92+=W6P;return el[v92];}),opts[n0O]);this[z$j](E7l,T5f);}function _optionsUpdate(json){var that=this;if(json[Z0p]){var w2C=m9z;w2C+=x6u;w2C+=G1V;w2C+=T03;$[w2C](this[J7D][c26],function(name,field){var W0W="up";var d$7="ions";if(json[Z0p][name] !== undefined){var i2r=l7$;i2r+=t_M;i2r+=m9z;i2r+=W6P;var fieldInst=that[i2r](name);if(fieldInst && fieldInst[d1x]){var F00=P7P;F00+=F$o;F00+=d$7;var c5i=W0W;c5i+=d$I;c5i+=F$o;c5i+=m9z;fieldInst[c5i](json[F00][name]);}}});}}function _message(el,msg,title,fn){var S5G='title';var Q4d="stop";var R04="veAtt";var g9g=V3z;g9g+=O5R;g9g+=z4s;g9g+=B3P;var canAnimate=$[w9$][O6e]?T5f:g49;if(title === undefined){title=g49;}if(!fn){fn=function(){};}if(typeof msg === g9g){var n7T=Y4Q;n7T+=m59;n7T+=t_M;msg=msg(this,new DataTable$3[n7T](this[J7D][O81]));}L7K.l4n();el=$(el);if(canAnimate){el[Q4d]();}if(!msg){if(this[J7D][k42] && canAnimate){el[I_T](function(){var g8z=h7d;g8z+=F6G;g8z+=E3o;L7K.l4n();el[g8z](l2Y);fn();});}else {var H6Q=J_0;H6Q+=P1D;H6Q+=m59;H6Q+=P99;var I8_=T03;I8_+=F$o;I8_+=O98;el[I8_](l2Y)[e$Y](H6Q,N7C);fn();}if(title){var c10=m2K;c10+=R04;c10+=M5g;el[c10](S5G);}}else {fn();if(this[J7D][k42] && canAnimate){var o$O=x8P;o$O+=m9z;o$O+=F0f;el[y64](msg)[o$O]();}else {var A_p=p2t;A_p+=y4x;A_p+=A33;var v3l=G1V;v3l+=J7D;v3l+=J7D;el[y64](msg)[v3l](A_p,K9F);}if(title){var g75=x6u;g75+=F$o;g75+=Z9H;el[g75](S5G,msg);}}}function _multiInfo(){var P5A="tiValue";var U9S="multiEdita";var N18="isMu";var k$h=y3A;k$h+=L0x;k$h+=F$o;k$h+=T03;var b1d=t0$;b1d+=E2R;b1d+=J_0;b1d+=J7D;var fields=this[J7D][b1d];var include=this[J7D][F4f];var show=T5f;var state;if(!include){return;}for(var i=M1q,ien=include[k$h];i < ien;i++){var x$J=N18;x$J+=E3o;x$J+=P5A;var T8l=U9S;T8l+=u3Y;T8l+=E3o;T8l+=m9z;var field=fields[include[i]];var multiEditable=field[T8l]();if(field[Y7L]() && multiEditable && show){state=T5f;show=g49;}else if(field[x$J]() && !multiEditable){state=T5f;}else {state=g49;}fields[include[i]][Z6k](state);}}function _nestedClose(cb){var r8g="displayCon";var A1S="pop";var I9f="displayCont";var g1Q="displayControl";var k$X="callback";var B5D="rolle";var O2T=I9f;O2T+=B5D;O2T+=M5g;var disCtrl=this[J7D][O2T];var show=disCtrl[N48];if(!show || !show[m2_]){if(cb){cb();}}else if(show[m2_] > p88){var A5v=J_0;A5v+=W1Z;var k3m=x4g;k3m+=m59;k3m+=m9z;k3m+=B3P;var o9$=g1Q;o9$+=w36;show[A1S]();var last=show[show[m2_] - p88];if(cb){cb();}this[J7D][o9$][k3m](last[A5v],last[K$Z],last[k$X]);}else {var C7V=G1V;C7V+=E3o;C7V+=T1u;C7V+=m9z;var c1x=r8g;c1x+=F$o;c1x+=L5i;c1x+=w36;this[J7D][c1x][C7V](this,cb);show[m2_]=M1q;}}function _nestedOpen(cb,nest){var u2D="layController";var V9E="_sho";var a0C="show";var u6q=L0p;u6q+=D47;u6q+=u2D;var m2O=V9E;L7K.b9t();m2O+=R3d;var n5Q=a4a;n5Q+=a0C;var disCtrl=this[J7D][S5B];if(!disCtrl[n5Q]){disCtrl[N48]=[];}if(!nest){disCtrl[N48][m2_]=M1q;}disCtrl[m2O][K7Q]({append:this[I_d][A8Y],callback:cb,dte:this});this[J7D][u6q][V_S](this,this[I_d][A8Y],cb);}function _postopen(type,immediate){var i0n="iInfo";var x4l="ubmit.editor-internal";var p0q="it.editor-intern";var f5f="captureFocus";var h_n=P5B;h_n+=Y9F;h_n+=B3P;h_n+=F$o;var X_9=a4a;X_9+=Z1n;X_9+=i0n;var g2e=J7D;g2e+=x4l;var S2X=x4g;S2X+=B3P;var X8w=E43;X8w+=p0q;X8w+=X1g;var B3g=x4g;B3g+=A43;var z2Z=J_0;z2Z+=x4g;z2Z+=F6G;var _this=this;var focusCapture=this[J7D][S5B][f5f];if(focusCapture === undefined){focusCapture=T5f;}$(this[z2Z][C1v])[B3g](X8w)[S2X](g2e,function(e){e[D4J]();});if(focusCapture && (type === P5M || type === m7R)){var t4d=x4g;t4d+=B3P;var y0m=u3Y;y0m+=x4g;y0m+=J_0;y0m+=A33;$(y0m)[t4d](c0f,function(){var u8X="iveEl";var H_t="rents";var g0p='.DTE';var U2F="activeEl";var d9Z="ement";var U9H=".D";var b6v=U9H;b6v+=N5y;b6v+=P4H;L7K.b9t();var v_2=m59;v_2+=j4C;v_2+=a$N;var s5x=U2F;s5x+=d9Z;var m3G=B4n;m3G+=H_t;var m0q=H5X;m0q+=F$o;m0q+=u8X;m0q+=d9Z;if($(document[m0q])[m3G](g0p)[m2_] === M1q && $(document[s5x])[v_2](b6v)[m2_] === M1q){if(_this[J7D][j0D]){_this[J7D][j0D][F4U]();}}});}this[X_9]();this[h_n](v96,[type,this[J7D][J$l]]);if(immediate){var a01=j_L;a01+=m9z;a01+=k1z;this[a01](N0r,[type,this[J7D][J$l]]);}return T5f;}function _preopen(type){var P1k="reOp";var k9k="Icb";L7K.b9t();var Z4Y="elOp";var k8_="seIcb";var G$Q="_clearDynamicInfo";var e91=s$8;e91+=h6w;var W9_=x3g;W9_+=v$K;W9_+=B3P;var y7Y=m59;y7Y+=P1k;y7Y+=l9T;if(this[m6A](y7Y,[type,this[J7D][W9_]]) === g49){var P2E=G1V;P2E+=Y4R;P2E+=k8_;var d$c=d7R;d$c+=u3Y;d$c+=E3o;d$c+=m9z;var v5V=F6G;v5V+=x4g;v5V+=J_0;v5V+=m9z;var l8j=x6u;l8j+=G1V;l8j+=z4s;l8j+=B3P;var A1k=C10;A1k+=O5R;A1k+=Z4Y;A1k+=l9T;var G6B=j_L;G6B+=m9z;G6B+=k1z;this[G$Q]();this[G6B](A1k,[type,this[J7D][l8j]]);if((this[J7D][v5V] === d9y || this[J7D][d1k] === d$c) && this[J7D][m9t]){var J$z=H5f;J$z+=k9k;this[J7D][J$z]();}this[J7D][P2E]=X2m;return g49;}this[G$Q](T5f);this[J7D][e91]=type;return T5f;}function _processing(processing){var G5d="oggleCl";var V08='div.DTE';var R6b="active";var h2r="processin";var t7Z=a4a;t7Z+=Q9x;t7Z+=l9T;t7Z+=F$o;var Z8F=h2r;Z8F+=c3v;var j94=F$o;j94+=G5d;j94+=x6u;j94+=f63;var U8$=Q0R;U8$+=z7H;var procClass=this[I58][U8$][R6b];$([V08,this[I_d][A8Y]])[j94](procClass,processing);this[J7D][Z8F]=processing;L7K.l4n();this[t7Z](o0s,[processing]);}function _noProcessing(args){var W5l='processing-field';var processing=g49;L7K.l4n();$[o8Y](this[J7D][c26],function(name,field){L7K.b9t();if(field[D$8]()){processing=T5f;}});if(processing){this[d5q](W5l,function(){var P1p="_submi";L7K.l4n();if(this[b5E](args) === T5f){var O5j=P1p;O5j+=F$o;this[O5j][m03](this,args);}});}return !processing;}function _submit(successCallback,errorCallback,formatdata,hide){var H6D="still processing";var H_I="IfChan";var O5v=16;var v4R="ged";var P6P="ang";var Q2I="Count";var I5q="ionName";var B1P="tOp";var C1W="submitCompl";var s$S="noProcessin";var H2x="tData";var R1F="nComplete";var B$a="_proc";var E4N='preSubmit';var a2M="Field is ";var a9G=m9z;a9G+=E$y;var j35=P3c;j35+=F$o;var K27=x3g;K27+=I5q;var i$t=F3w;i$t+=B3P;var m6$=a4a;m6$+=s$S;m6$+=c3v;var t23=m9z;t23+=L0p;t23+=B1P;t23+=U15;var Y9l=h6w;Y9l+=t_M;Y9l+=H2x;var F5_=m9z;F5_+=J_0;F5_+=H7H;F5_+=Q2I;var b42=l7$;b42+=w_4;b42+=W6P;b42+=J7D;var _this=this;var changed=g49;var allData={};var changedData={};var setBuilder=dataSet;var fields=this[J7D][b42];var editCount=this[J7D][F5_];var editFields=this[J7D][X8F];var editData=this[J7D][Y9l];var opts=this[J7D][t23];var changedSubmit=opts[f4c];var submitParamsLocal;if(this[m6$](arguments) === g49){var i_r=a2M;i_r+=H6D;var S75=K2t;S75+=e0u;Editor[S75](i_r,O5v,g49);return;}var action=this[J7D][i$t];var submitParams={data:{}};submitParams[this[J7D][K27]]=action;if(action === R4H || action === j35){var J32=b_g;J32+=P6P;J32+=h6w;var q7L=C7y;q7L+=H_I;q7L+=v4R;$[o8Y](editFields,function(idSrc,edit){var r6g="isEmptyObjec";var N01=r6g;N01+=F$o;var allRowData={};L7K.l4n();var changedRowData={};$[o8Y](fields,function(name,field){var S1R='[]';var D71="alFromDa";var M5y="mpare";var y6e="lace";var p8_="tiGet";var e_f='-many-count';var F14=/\[.*$/;if(edit[c26][name] && field[i$X]()){var p6i=b00;p6i+=M5y;var H6_=M5g;H6_+=K0V;H6_+=y6e;var o1E=j4l;o1E+=p8_;var multiGet=field[o1E]();var builder=setBuilder(name);if(multiGet[idSrc] === undefined){var V9J=J_0;V9J+=x6u;V9J+=F$o;V9J+=x6u;var h$L=h13;h$L+=D71;h$L+=f$8;var originalVal=field[h$L](edit[V9J]);builder(allRowData,originalVal);return;}var value=multiGet[idSrc];var manyBuilder=Array[p5M](value) && typeof name === f2Z && name[J6b](S1R) !== -p88?setBuilder(name[H6_](F14,l2Y) + e_f):X2m;builder(allRowData,value);if(manyBuilder){manyBuilder(allRowData,value[m2_]);}if(action === n13 && (!editData[name] || !field[p6i](value,editData[name][idSrc]))){builder(changedRowData,value);changed=T5f;if(manyBuilder){var a1l=y3A;a1l+=B3P;a1l+=X1n;manyBuilder(changedRowData,value[a1l]);}}}});if(!$[B9k](allRowData)){allData[idSrc]=allRowData;}if(!$[N01](changedRowData)){changedData[idSrc]=changedRowData;}});if(action === R4H || changedSubmit === c4i || changedSubmit === q7L && changed){var z1O=d$I;z1O+=f$8;submitParams[z1O]=allData;}else if(changedSubmit === J32 && changed){var N2f=J_0;N2f+=x6u;N2f+=F$o;N2f+=x6u;submitParams[N2f]=changedData;}else {var K3i=C1W;K3i+=m9z;K3i+=W1Z;var Q5I=B$a;Q5I+=m9z;Q5I+=f63;Q5I+=Z1a;var x4T=x4g;x4T+=R1F;var Z0w=w3s;Z0w+=x4g;Z0w+=J86;this[J7D][J$l]=X2m;if(opts[b0c] === Z0w && (hide === undefined || hide)){this[Z_Z](g49);}else if(typeof opts[x4T] === v4B){opts[b0c](this);}if(successCallback){var A_G=C10;A_G+=u2Z;successCallback[A_G](this);}this[Q5I](g49);this[m6A](K3i);return;}}else if(action === K5K){var k3a=f$O;k3a+=b_g;$[k3a](editFields,function(idSrc,edit){L7K.l4n();var V03=d$I;V03+=F$o;V03+=x6u;submitParams[V03][idSrc]=edit[z4k];});}submitParamsLocal=$[a9G](T5f,{},submitParams);if(formatdata){formatdata(submitParams);}this[m6A](E4N,[submitParams,action],function(result){var f4f="aja";var f87="_submitTable";L7K.l4n();if(result === g49){var U35=a4a;U35+=D$8;_this[U35](g49);}else {var c43=a4a;c43+=f4f;c43+=h07;var submitWire=_this[J7D][y58]?_this[c43]:_this[f87];submitWire[H7v](_this,submitParams,function(json,notGood,xhr){L7K.b9t();_this[o8R](json,notGood,submitParams,submitParamsLocal,_this[J7D][J$l],editCount,hide,successCallback,errorCallback,xhr);},function(xhr,err,thrown){var A3l="_submitError";L7K.b9t();var t1v=x6u;t1v+=P1R;_this[A3l](xhr,err,thrown,errorCallback,submitParams,_this[J7D][t1v]);},submitParams);}});}function _submitTable(data,success,error,submitParams){var U7E="modif";var g0l="So";var W28=C5B;W28+=f6f;W28+=Y9F;var c_O=t_M;c_O+=T2X;var K5B=x6u;K5B+=G1V;K5B+=z4s;K5B+=B3P;var action=data[K5B];var out={data:[]};var idGet=dataGet(this[J7D][g9l]);var idSet=dataSet(this[J7D][c_O]);if(action !== W28){var I7E=m9z;I7E+=H5X;I7E+=T03;var R5u=x_3;R5u+=f$8;R5u+=g0l;R5u+=o$i;var E4B=U7E;E4B+=w_4;E4B+=M5g;var originalData_1=this[J7D][d1k] === P5M?this[W_A](O8Y,this[E4B]()):this[R5u](B21,this[E_V]());$[I7E](data[z4k],function(key,vals){var d3G="eat";var s5q="toStrin";var e7w=J_0;e7w+=x6u;e7w+=f$8;var R$t=G1V;R$t+=M5g;R$t+=d3G;R$t+=m9z;var f5s=h6w;f5s+=H7H;var toSave;var extender=extend;if(action === f5s){var rowData=originalData_1[key][z4k];toSave=extender({},rowData,T5f);toSave=extender(toSave,vals,T5f);}else {toSave=extender({},vals,T5f);}var overrideId=idGet(toSave);if(action === R$t && overrideId === undefined){var D$p=s5q;D$p+=c3v;idSet(toSave,+new Date() + key[D$p]());}else {idSet(toSave,overrideId);}out[e7w][K7Q](toSave);});}success(out);}function _submitSuccess(json,notGood,submitParams,submitParamsLocal,action,editCount,hide,successCallback,errorCallback,xhr){var k73="Edit";var m8_="mpl";var T8s="<b";var v9D="Coun";var G4k='prep';var F8p='preEdit';var R1O="eR";var r8H="itOpts";var e$L='submitSuccess';var p9g="ostRemove";var D4z="ete";var V$b="ier";var E8o="_even";var O4E="Sou";var x$N='commit';var k$G="rrors";var a6Z="eve";var R7l="reate";var A2o='setData';var T48="onCom";var E9N="post";var y8r="dErro";var B3x="pre";var J9Q="r>";var z92="rs";var l6O='submitUnsuccessful';var o8M="nCo";var g$K="ataSo";var I7h="dif";var O0T="eldE";var E3z="postCr";var Y3x="_data";var w4i="postS";var y4O=P5B;y4O+=h13;y4O+=k38;var p$w=t0$;p$w+=O0T;p$w+=k$G;var R4V=m9z;R4V+=C3$;var O7z=B1g;O7z+=E3o;O7z+=y8r;O7z+=z92;var K7m=w4i;K7m+=G4F;var S$W=a4a;S$W+=a6Z;S$W+=k1z;var l$h=F6G;l$h+=x4g;l$h+=I7h;l$h+=V$b;var O7E=h6w;O7E+=r8H;var Z$D=t0$;Z$D+=E2R;Z$D+=K23;var _this=this;var that=this;var setData;var fields=this[J7D][Z$D];var opts=this[J7D][O7E];var modifier=this[J7D][l$h];this[S$W](K7m,[json,submitParams,action,xhr]);if(!json[x_w]){var V6r=m9z;V6r+=C3$;json[V6r]=l2Y;}if(!json[O7z]){json[W6N]=[];}if(notGood || json[R4V] || json[p$w][m2_]){var L4X=a4a;L4X+=m9z;L4X+=t_d;var d3I=T8s;d3I+=J9Q;var l2y=F8J;l2y+=x4g;l2y+=t_M;l2y+=B3P;var n9H=M3d;n9H+=M5g;var A2a=m9z;A2a+=H5X;A2a+=T03;var u2q=m9z;u2q+=M5g;u2q+=M5g;u2q+=h3Y;var globalError_1=[];if(json[u2q]){var s25=K2t;s25+=e0u;var x9f=W8O;x9f+=T03;globalError_1[x9f](json[s25]);}$[A2a](json[W6N],function(i,err){var m2H="atus";var v1P="Unk";var D3T=': ';var U6v="nFieldError";var b4F="cus";var C3f="position";var n7$="onFieldError";var u6p="eldError";var n6u=" field: ";var f9y="nction";var k2$='Error';var E1O="own";var v0a="onFi";var field=fields[err[z$n]];if(!field){var B0l=v1P;B0l+=B3P;B0l+=E1O;B0l+=n6u;throw new Error(B0l + err[z$n]);}else if(field[k42]()){field[x_w](err[h3o] || k2$);if(i === M1q){var T4I=l7$;T4I+=J3O;T4I+=f9y;var q$b=v0a;q$b+=u6p;var B82=C4i;B82+=G1V;B82+=c5s;var p_B=x4g;p_B+=U6v;if(opts[p_B] === B82){var y6M=l7$;y6M+=x4g;y6M+=b4F;var B7h=t5D;B7h+=m9z;_this[R3b]($(_this[I_d][s1N]),{scrollTop:$(field[B7h]())[C3f]()[d1s]},S_T);field[y6M]();}else if(typeof opts[q$b] === T4I){opts[n7$](_this,err);}}}else {var z2C=m7i;z2C+=m2H;var u$G=B3P;u$G+=W5M;globalError_1[K7Q](field[u$G]() + D3T + (err[z2C] || k2$));}});this[n9H](globalError_1[l2y](d3I));this[L4X](l6O,[json]);if(errorCallback){errorCallback[H7v](that,json);}}else {var g8U=a4a;g8U+=a6Z;g8U+=k1z;var H$l=h6w;H$l+=H7H;H$l+=v9D;H$l+=F$o;var j9b=M5g;j9b+=H4b;j9b+=Y9F;var b_k=h6w;b_k+=H7H;var store={};if(json[z4k] && (action === R4H || action === b_k)){var A86=J_0;A86+=x6u;A86+=f$8;var W9k=p7R;W9k+=z1s;var L1w=M49;L1w+=x6u;var S3M=D6L;S3M+=m9z;S3M+=m59;this[W_A](S3M,action,modifier,submitParamsLocal,json,store);for(var _i=M1q,_a=json[L1w];_i < _a[m2_];_i++){var X1s=m9z;X1s+=J_0;X1s+=t_M;X1s+=F$o;var Y_v=i4l;Y_v+=W1Z;var o3x=t_M;o3x+=J_0;var data=_a[_i];setData=data;var id=this[W_A](o3x,data);this[m6A](A2o,[json,data,action]);if(action === Y_v){var w7F=E3z;w7F+=A3U;var F0z=E8o;F0z+=F$o;var p46=G1V;p46+=M5g;p46+=f$O;p46+=W1Z;var R$V=Z71;R$V+=g$K;R$V+=o$i;var n2J=B3x;n2J+=R$B;n2J+=R7l;var L7b=P5B;L7b+=t_d;this[L7b](n2J,[json,data,id]);this[R$V](p46,fields,data,store);this[F0z]([R4H,w7F],[json,data,id]);}else if(action === X1s){var i5Y=E9N;i5Y+=k73;var z4u=j_L;z4u+=m9z;z4u+=B3P;z4u+=F$o;var K7x=a4a;K7x+=m9z;K7x+=t_d;this[K7x](F8p,[json,data,id]);this[W_A](n13,modifier,fields,data,store);this[z4u]([n13,i5Y],[json,data,id]);}}this[W_A](W9k,action,modifier,json[A86],store);}else if(action === j9b){var F66=J_0;F66+=x6u;F66+=f$8;var M_8=x_3;M_8+=F2P;M_8+=G1V;M_8+=m9z;var o3s=t_M;o3s+=J_0;o3s+=J7D;var J3Z=m59;J3Z+=p9g;var C$9=R_p;C$9+=E3j;var h22=M5g;h22+=h5X;h22+=E3j;var T4n=Y3x;T4n+=O4E;T4n+=M5g;T4n+=H0X;var f7b=t_M;f7b+=K23;var g8t=D6L;g8t+=R1O;g8t+=H4b;g8t+=Y9F;var M4v=P5B;M4v+=h13;M4v+=k38;var G0n=Y3x;G0n+=s7M;G0n+=t22;this[G0n](G4k,action,modifier,submitParamsLocal,json,store);this[M4v](g8t,[json,this[f7b]()]);this[T4n](h22,modifier,fields,store);this[m6A]([C$9,J3Z],[json,this[o3s]()]);this[M_8](x$N,action,modifier,json[F66],store);}if(editCount === this[J7D][H$l]){var N2j=s8a;N2j+=W6y;N2j+=t_M;N2j+=g5T;var P23=x4g;P23+=o8M;P23+=m8_;P23+=D4z;var N9s=x3g;N9s+=v$K;N9s+=B3P;var sAction=this[J7D][N9s];this[J7D][J$l]=X2m;if(opts[b0c] === H$E && (hide === undefined || hide)){var u9t=J_0;u9t+=x6u;u9t+=f$8;this[Z_Z](json[u9t]?T5f:g49,sAction);}else if(typeof opts[P23] === N2j){var J22=T48;J22+=H6f;opts[J22](this);}}if(successCallback){var h5n=G1V;h5n+=x6u;h5n+=E3o;h5n+=E3o;successCallback[h5n](that,json);}this[g8U](e$L,[json,setData,action]);}this[I_z](g49);this[y4O](a_U,[json,setData,action]);}function _submitError(xhr,err,thrown,errorCallback,submitParams,action){var Z9P="_p";var H9W="sy";var p4v="submitE";var P$I='postSubmit';var s1C="roces";var p2g="bmitCompl";var p9E="ste";var I6m=S7m;I6m+=p2g;I6m+=m9z;I6m+=W1Z;var q0r=p4v;q0r+=C3$;var G0S=j_L;G0S+=k38;var K_C=Z9P;K_C+=s1C;K_C+=X7l;var s_V=H9W;s_V+=p9E;s_V+=F6G;var y0Y=t_M;y0Y+=k7$;y0Y+=B3P;var L_S=P5B;L_S+=Y9F;L_S+=k1z;this[L_S](P$I,[X2m,submitParams,action,xhr]);this[x_w](this[y0Y][x_w][s_V]);this[K_C](g49);if(errorCallback){errorCallback[H7v](this,xhr,err,thrown);}this[G0S]([q0r,I6m],[xhr,err,thrown,submitParams]);}function _tidy(fn){var V8b="oFea";var i$g=10;var A7a=d7R;A7a+=k_N;var u3x=J_0;u3x+=X0T;L7K.b9t();u3x+=Y6A;var o2U=x0n;o2U+=X9m;o2U+=X7l;var Z_O=F$o;Z_O+=r8u;Z_O+=E3o;Z_O+=m9z;var _this=this;var dt=this[J7D][O81]?new $[w9$][q1t][b_J](this[J7D][Z_O]):X2m;var ssp=g49;if(dt){var Q2Z=V8b;Q2Z+=F$o;Q2Z+=L7j;Q2Z+=C_A;ssp=dt[Q6c]()[M1q][Q2Z][B7S];}if(this[J7D][o2U]){this[d5q](a_U,function(){L7K.l4n();if(ssp){var K3a=x4g;K3a+=B3P;K3a+=m9z;dt[K3a](w8N,fn);}else {setTimeout(function(){L7K.l4n();fn();},i$g);}});return T5f;}else if(this[s$8]() === d9y || this[u3x]() === A7a){var H0j=g5T;H0j+=m9z;this[H0j](H$E,function(){var v37=x0n;L7K.l4n();v37+=G1V;v37+=z7H;if(!_this[J7D][v37]){setTimeout(function(){L7K.l4n();if(_this[J7D]){fn();}},i$g);}else {var c8s=x4g;c8s+=B3P;c8s+=m9z;_this[c8s](a_U,function(e,json){if(ssp && json){var P7q=J_0;P7q+=M5g;P7q+=x6u;P7q+=R3d;dt[d5q](P7q,fn);}else {setTimeout(function(){L7K.l4n();if(_this[J7D]){fn();}},i$g);}});}})[s$G]();return T5f;}return g49;}function _weakInArray(name,arr){var v8B=E3o;v8B+=z0E;v8B+=F0A;for(var i=M1q,ien=arr[v8B];i < ien;i++){if(name == arr[i]){return i;}}return -p88;}var fieldType={create:function(){},disable:function(){},enable:function(){},get:function(){},set:function(){}};var DataTable$2=$[W5h][U2M];function _buttonText(conf,textIn){var C$g='div.upload button';var f7D='Choose file...';var U1C="dT";var k2Y="oa";if(textIn === X2m || textIn === undefined){var K5E=A$Q;K5E+=k2Y;K5E+=U1C;K5E+=V8g;textIn=conf[K5E] || f7D;}conf[U_$][B23](C$g)[y64](textIn);}function _commonUpload(editor,conf,dropCallback,multiple){var d4j='<div class="drop"><span></span></div>';var e9_="v c";var C5h="lue butto";var r82="eu_table\">";var H3g="></";var S1K="dragleave d";var T_9="\"cell clearValue\">";var o94="<div cl";var q43='input[type=file]';var u0m="div.dr";var e6Y="buttonInternal";var n$o='dragover';var f37="div.clearVa";var K66='<input type="file" ';var b16="ered";var V80="xi";var X$o='multiple';var A4F="rage";var G1N="div.d";var i01="iv class=\"r";var Q4R="ile";var R26="s=\"row\">";var H34='<div class="cell upload limitHide">';var p8l="Text";var E2Z='Drag and drop a file here to upload';var v7t="rop span";var J6U="dragDro";var a_R="dragDrop";var D64="<div class=\"editor_uplo";var p2M='<button class="';var N6$="<di";var n22='<div class="rendered"></div>';var y1O="Reade";var x2t="ow ";var b4k="v.rend";var t90='<div class="cell">';var G5M="_ena";var Z6f='<div class="cell limitHide">';var n0z="ass=";var u7I="/butt";var I25="second\">";var t0H="\"></b";var z9s="ad\">";var s3F="clos";var W_t=t_M;W_t+=d9r;var h8k=x4g;h8k+=B3P;var z$V=x4g;z$V+=B3P;var y85=f37;y85+=C5h;y85+=B3P;var J0z=l7$;J0z+=t_M;J0z+=B3P;J0z+=J_0;var R9$=R9z;R9$+=Q4R;R9$+=y1O;R9$+=M5g;var w8C=x6u;w8C+=F$o;w8C+=Z9H;var y0I=G5M;y0I+=T_U;y0I+=h6w;var N_2=z2m;N_2+=B3P;N_2+=m59;N_2+=S65;var o_z=W7n;o_z+=i51;o_z+=h1_;var h$h=e1t;h$h+=J_0;h$h+=t_M;h$h+=a$t;L7K.b9t();var x7M=r$P;x7M+=t_M;x7M+=h13;x7M+=m6_;var z9U=c_1;z9U+=i01;z9U+=x2t;z9U+=I25;var A5r=F8G;A5r+=u7I;A5r+=v82;var t_T=o94;t_T+=n0z;t_T+=T_9;var X6O=H3g;X6O+=Z_n;X6O+=d52;var N90=t0H;N90+=v9Y;N90+=g5T;N90+=m6_;var C1C=N6$;C1C+=e9_;C1C+=Y26;C1C+=R26;var M86=U$h;M86+=r82;var O96=D64;O96+=z9s;var P7D=L8N;P7D+=f63;P7D+=m9z;P7D+=J7D;if(multiple === void M1q){multiple=g49;}var btnClass=editor[P7D][C1v][e6Y];var container=$(O96 + M86 + C1C + H34 + p2M + btnClass + N90 + K66 + (multiple?X$o:l2Y) + X6O + r65 + t_T + p2M + btnClass + A5r + r65 + r65 + z9U + Z6f + d4j + x7M + t90 + n22 + r65 + h$h + r65 + o_z);conf[N_2]=container;conf[y0I]=T5f;if(conf[j_g]){var f6g=t_M;f6g+=J_0;var s0Y=t_M;s0Y+=J_0;var k0f=l7$;k0f+=t_M;k0f+=B3P;k0f+=J_0;container[k0f](q43)[X7W](s0Y,Editor[l0q](conf[f6g]));}if(conf[w8C]){var t2K=x3a;t2K+=F$o;t2K+=M5g;var G7M=x6u;G7M+=F$o;G7M+=F$o;G7M+=M5g;container[B23](q43)[G7M](conf[t2K]);}_buttonText(conf);if(window[R9$] && conf[a_R] !== g49){var z_t=s3F;z_t+=m9z;var H5Z=x4g;H5Z+=B3P;var f8R=x4g;f8R+=C5C;f8R+=B3P;var B3o=x4g;B3o+=B3P;var P_3=x4g;P_3+=B3P;var W_J=S1K;W_J+=A4F;W_J+=V80;W_J+=F$o;var S7V=x4g;S7V+=B3P;var e$8=J_0;e$8+=M5g;e$8+=x4g;e$8+=m59;var s39=x4g;s39+=B3P;var F8U=u0m;F8U+=P7P;var o1q=l7$;o1q+=I8x;o1q+=J_0;var g2r=J6U;g2r+=m59;g2r+=p8l;var U3q=F$o;U3q+=p6S;U3q+=F$o;var u_r=G1N;u_r+=v7t;container[B23](u_r)[U3q](conf[g2r] || E2Z);var dragDrop_1=container[o1q](F8U);dragDrop_1[s39](e$8,function(e){var T5c="sfer";var H7f="enab";var T0y="originalEvent";var e_N="Tran";var i2l="moveCl";var v$_=a4a;v$_+=H7f;v$_+=y6x;L7K.b9t();if(conf[v$_]){var W02=E3j;W02+=M5g;var R9D=C5B;R9D+=i2l;R9D+=h4Q;var m0w=J_0;m0w+=h34;m0w+=e_N;m0w+=T5c;var T37=J3O;T37+=m59;T37+=E3o;T37+=Q4p;Editor[T37](editor,conf,e[T0y][m0w][Y$q],_buttonText,dropCallback);dragDrop_1[R9D](W02);}return g49;})[S7V](W_J,function(e){L7K.l4n();var i$A="na";var U9a="oveCl";var D45=P5B;D45+=i$A;D45+=k_N;D45+=J_0;if(conf[D45]){var I5X=x4g;I5X+=h13;I5X+=K2t;var s3a=C5B;s3a+=F6G;s3a+=U9a;s3a+=h4Q;dragDrop_1[s3a](I5X);}return g49;})[P_3](n$o,function(e){var x0X=P5B;x0X+=B3P;L7K.b9t();x0X+=c_k;x0X+=J_0;if(conf[x0X]){var M$k=x4g;M$k+=Y9F;M$k+=M5g;dragDrop_1[w5Y](M$k);}return g49;});editor[B3o](f8R,function(){var a_w="agover.DTE_Uploa";var v7_="d drop.DTE_Upload";var T_4="dr";var X$q=T_4;X$q+=a_w;X$q+=v7_;$(o0u)[g5T](X$q,function(e){L7K.b9t();return g49;});})[H5Z](z_t,function(){var p9W="load dr";var w5c="bo";var m8W="op.DTE";var L0H="dragover.DTE_Up";var n4t=L0H;n4t+=p9W;n4t+=m8W;n4t+=j3_;var E7K=w5c;E7K+=g4M;$(E7K)[H9D](n4t);});}else {var q40=L0p;q40+=b4k;q40+=b16;var O0e=H0m;O0e+=B6V;var Y7i=B3P;Y7i+=x4g;Y7i+=P4H;Y7i+=t09;container[w5Y](Y7i);container[O0e](container[B23](q40));}container[J0z](y85)[z$V](a55,function(e){var t8i=G5M;t8i+=e2c;e[D4J]();if(conf[t8i]){var V0p=G1V;V0p+=x6u;V0p+=E3o;V0p+=E3o;var U$E=J86;U$E+=F$o;upload[U$E][V0p](editor,conf,l2Y);}});container[B23](q43)[h8k](W_t,function(){var M78=t0$;M78+=y3A;M78+=J7D;L7K.b9t();Editor[x4w](editor,conf,this[M78],_buttonText,function(ids){var N0L="type=file]";var A6$="[";var D46=Z_n;D46+=F$o;D46+=A6$;D46+=N0L;var h5b=G1V;h5b+=C7y;L7K.b9t();dropCallback[h5b](editor,ids);container[B23](D46)[M1q][n01]=l2Y;});});return container;}function _triggerChange(input){L7K.l4n();setTimeout(function(){var K3d="rigg";var q4E="nge";var Y9w=G1V;Y9w+=T03;Y9w+=x6u;Y9w+=q4E;var h3M=F$o;h3M+=K3d;h3M+=K2t;input[h3M](Y9w,{editor:T5f,editorSet:T5f});;},M1q);}var baseFieldType=$[t8A](T5f,{},fieldType,{canReturnSubmit:function(conf,node){L7K.l4n();return T5f;},disable:function(conf){var y96="sab";L7K.b9t();var V$o=L0p;V$o+=y96;V$o+=y6x;var c1e=D6L;c1e+=x4g;c1e+=m59;var h5U=z2m;h5U+=d9r;conf[h5U][c1e](V$o,T5f);},enable:function(conf){L7K.l4n();conf[U_$][z4O](R41,g49);},get:function(conf){var j0y=a4a;j0y+=t_M;j0y+=U9d;j0y+=F$o;L7K.l4n();return conf[j0y][m1I]();},set:function(conf,val){var G8G=z2m;G8G+=B3P;G8G+=K29;G8G+=F$o;var l42=z2m;l42+=B3P;l42+=K29;l42+=F$o;conf[l42][m1I](val);_triggerChange(conf[G8G]);}});var hidden={create:function(conf){var i96=a4a;i96+=h13;i96+=X1g;conf[i96]=conf[n01];return X2m;},get:function(conf){var S5$=a4a;S5$+=h13;S5$+=x6u;S5$+=E3o;L7K.l4n();return conf[S5$];},set:function(conf,val){var W1h=a4a;W1h+=Y_Y;W1h+=E3o;conf[W1h]=val;}};var readonly=$[t8A](T5f,{},baseFieldType,{create:function(conf){var p7T="<input";var q4D=e6C;q4D+=S65;var d$3=x6u;d$3+=F$o;d$3+=F$o;d$3+=M5g;var r36=W1Z;r36+=h07;r36+=F$o;var d$e=p6g;d$e+=l7$;d$e+=m9z;d$e+=R5K;var a27=p6S;a27+=F$o;a27+=l9T;a27+=J_0;var K7n=x6u;K7n+=F$o;K7n+=F$o;K7n+=M5g;var U90=p7T;U90+=K5f;U90+=m6_;L7K.b9t();var t7g=a4a;t7g+=I8x;t7g+=l76;conf[t7g]=$(U90)[K7n]($[a27]({id:Editor[d$e](conf[j_g]),readonly:b7h,type:r36},conf[d$3] || ({})));return conf[q4D][M1q];}});var text=$[M0I](T5f,{},baseFieldType,{create:function(conf){var S2P="put/>";var A6i="<in";var J8e=x6u;J8e+=F$o;J8e+=Z9H;var N_E=F$o;N_E+=m9z;N_E+=h07;N_E+=F$o;L7K.b9t();var R0k=y4_;R0k+=M5g;var k$f=A6i;k$f+=S2P;conf[U_$]=$(k$f)[R0k]($[t8A]({id:Editor[l0q](conf[j_g]),type:N_E},conf[J8e] || ({})));return conf[U_$][M1q];}});var password=$[t8A](T5f,{},baseFieldType,{create:function(conf){var v4q='<input/>';var G8o="safeI";var y7V="ssword";var K4w=x3a;K4w+=Z9H;var O19=B4n;O19+=y7V;var D3I=t_M;D3I+=J_0;var b9c=G8o;b9c+=J_0;var Q9J=m9z;Q9J+=h07;Q9J+=W1Z;L7K.l4n();Q9J+=V8x;var G04=x6u;G04+=F$o;G04+=F$o;G04+=M5g;conf[U_$]=$(v4q)[G04]($[Q9J]({id:Editor[b9c](conf[D3I]),type:O19},conf[K4w] || ({})));return conf[U_$][M1q];}});var textarea=$[H2p](T5f,{},baseFieldType,{canReturnSubmit:function(conf,node){L7K.b9t();return g49;},create:function(conf){var h58='<textarea></textarea>';var Y6E=x3a;Y6E+=Z9H;var S$G=a4a;S$G+=I8x;S$G+=K29;S$G+=F$o;conf[S$G]=$(h58)[Y6E]($[t8A]({id:Editor[l0q](conf[j_g])},conf[X7W] || ({})));return conf[U_$][M1q];}});var select=$[Z0E](T5f,{},baseFieldType,{_addOptions:function(conf,opts,append){var T7m="hidde";var A9d="place";var Q1O="holder";var p8i="optionsP";var r0E="lder";var s_n="_edi";var W35="air";var I8q="placeholderValue";var U3r="placeholderDisabled";var U9h=G0V;L7K.b9t();U9h+=I$y;if(append === void M1q){append=g49;}var elOpts=conf[U_$][M1q][U9h];var countOffset=M1q;if(!append){var F0V=U6c;F0V+=H0X;F0V+=U04;F0V+=r0E;var Y6s=y3A;Y6s+=e0d;Y6s+=T03;elOpts[Y6s]=M1q;if(conf[F0V] !== undefined){var u32=s_n;u32+=F$o;u32+=x4g;u32+=N$3;var G6o=R2K;G6o+=y6x;var I7o=T7m;I7o+=B3P;var q3$=A9d;q3$+=Q1O;q3$+=F2x;q3$+=v2$;var placeholderValue=conf[I8q] !== undefined?conf[q3$]:l2Y;countOffset+=p88;elOpts[M1q]=new Option(conf[I6y],placeholderValue);var disabled=conf[U3r] !== undefined?conf[U3r]:T5f;elOpts[M1q][I7o]=disabled;elOpts[M1q][G6o]=disabled;elOpts[M1q][u32]=placeholderValue;}}else {countOffset=elOpts[m2_];}if(opts){var k7U=p8i;k7U+=W35;Editor[c0B](opts,conf[k7U],function(val,label,i,attr){var V$j=k_9;V$j+=N1t;var option=new Option(label,val);option[V$j]=val;if(attr){$(option)[X7W](attr);}elOpts[i + countOffset]=option;});}},create:function(conf){var U7R="pOp";var b6Y="addOp";var H_4="ltiple";var t$_='<select></select>';var h08='change.dte';var V2w="safe";var z58=t_M;z58+=U7R;z58+=U15;var o21=a4a;o21+=b6Y;o21+=y2Y;var S_2=c5m;S_2+=H_4;var w9s=V2w;w9s+=v12;w9s+=J_0;var v$b=x3a;v$b+=F$o;v$b+=M5g;var k50=U$t;k50+=l76;conf[k50]=$(t$_)[v$b]($[t8A]({id:Editor[w9s](conf[j_g]),multiple:conf[S_2] === T5f},conf[X7W] || ({})))[g5T](h08,function(e,d){var U4X=h6w;U4X+=t_M;L7K.l4n();U4X+=J01;U4X+=M5g;if(!d || !d[U4X]){var C4P=c3v;C4P+=m9z;C4P+=F$o;conf[q5R]=select[C4P](conf);}});select[o21](conf,conf[Z0p] || conf[z58]);return conf[U_$][M1q];},destroy:function(conf){var s0s="change.d";var h_h=s0s;h_h+=W1Z;var t5Y=x4g;t5Y+=A43;var v$O=a4a;v$O+=I8x;v$O+=l76;conf[v$O][t5Y](h_h);},get:function(conf){var A3n="n:selecte";var Q20=D05;Q20+=T5j;Q20+=x6u;Q20+=A33;L7K.l4n();var W49=G0V;W49+=x4g;W49+=A3n;W49+=J_0;var S8h=t0$;S8h+=V8x;var T0I=z2m;T0I+=B3P;T0I+=l76;var val=conf[T0I][S8h](W49)[S9O](function(){L7K.l4n();var T9n=e1q;T9n+=N$3;return this[T9n];})[Q20]();if(conf[q5o]){return conf[h7r]?val[y1t](conf[h7r]):val;}return val[m2_]?val[M1q]:X2m;},set:function(conf,val,localUpdate){var r6v='option';var V0M="selecte";var b21=H_N;b21+=F$o;b21+=T03;var f_9=m9z;f_9+=x6u;f_9+=G1V;f_9+=T03;var C8Q=l7$;L7K.b9t();C8Q+=t_M;C8Q+=B3P;C8Q+=J_0;var Z45=e6C;Z45+=S65;var M1r=a4H;M1r+=c3v;M1r+=F0A;var U1x=g9B;U1x+=S9n;var j$5=J86;j$5+=m59;j$5+=j4C;j$5+=w7B;var J7N=j4l;J7N+=B91;J7N+=T9z;J7N+=m9z;if(!localUpdate){conf[q5R]=val;}if(conf[J7N] && conf[j$5] && !Array[U1x](val)){var Q7V=J7D;Q7V+=F$o;Q7V+=M5g;Q7V+=Z1a;val=typeof val === Q7V?val[R$w](conf[h7r]):[];}else if(!Array[p5M](val)){val=[val];}var i;var len=val[M1r];var found;var allFound=g49;var options=conf[Z45][C8Q](r6v);conf[U_$][B23](r6v)[f_9](function(){var S6C="selected";var d8G="tor_val";found=g49;for(i=M1q;i < len;i++){var l5V=a4a;l5V+=P3c;l5V+=d8G;if(this[l5V] == val[i]){found=T5f;allFound=T5f;break;}}this[S6C]=found;});if(conf[I6y] && !allFound && !conf[q5o] && options[b21]){var H54=V0M;H54+=J_0;options[M1q][H54]=T5f;}if(!localUpdate){var j5Q=a4a;j5Q+=E7g;j5Q+=S65;_triggerChange(conf[j5Q]);}return allFound;},update:function(conf,options,append){var o1N=e6C;o1N+=J3O;o1N+=F$o;select[k3H](conf,options,append);var lastSet=conf[q5R];if(lastSet !== undefined){select[L6_](conf,lastSet,T5f);}_triggerChange(conf[o1N]);}});var checkbox=$[J45](T5f,{},baseFieldType,{_addOptions:function(conf,opts,append){var d2Y="pty";var F9f=a4a;F9f+=I8x;F9f+=m59;F9f+=S65;if(append === void M1q){append=g49;}var jqInput=conf[F9f];var offset=M1q;if(!append){var i0Y=h5X;i0Y+=d2Y;jqInput[i0Y]();}else {var U1M=H_N;U1M+=F0A;var c$U=E7g;c$U+=J3O;c$U+=F$o;offset=$(c$U,jqInput)[U1M];}if(opts){var i97=B4n;i97+=U2L;i97+=J7D;Editor[i97](opts,conf[X2T],function(val,label,i,attr){var E5U="pe=\"";var W58="checkbox\" />";var G9z="\" t";var i$o="feI";var m8d="l for=";var L65="lu";var n4T="<lab";var P8a="afeId";var V4I=k_9;V4I+=N1t;var L4a=Y_Y;L4a+=L65;L4a+=m9z;var m7D=r$P;m7D+=h1_;var t2v=t_M;t2v+=J_0;var W5Y=p6g;W5Y+=i$o;W5Y+=J_0;var P_r=n4T;P_r+=m9z;P_r+=m8d;P_r+=U50;var I3e=G9z;I3e+=A33;I3e+=E5U;I3e+=W58;var B6S=t_M;B6S+=J_0;var R3l=J7D;R3l+=P8a;var I4d=M6A;I4d+=m6_;L7K.b9t();jqInput[K$Z](I4d + Q7L + Editor[R3l](conf[B6S]) + U9E + (i + offset) + I3e + P_r + Editor[W5Y](conf[t2v]) + U9E + (i + offset) + K8R + label + J4Q + m7D);$(S8t,jqInput)[X7W](L4a,val)[M1q][V4I]=val;if(attr){$(S8t,jqInput)[X7W](attr);}});}},create:function(conf){var q93='<div></div>';var u9g=a4a;u9g+=Z_n;u9g+=F$o;conf[u9g]=$(q93);checkbox[k3H](conf,conf[Z0p] || conf[M2R]);return conf[U_$][M1q];},disable:function(conf){var M7a="_inpu";var g2M=R2K;g2M+=y3A;g2M+=J_0;var p8A=D6L;p8A+=x4g;p8A+=m59;var j8H=l7$;j8H+=I8x;j8H+=J_0;var U_V=M7a;U_V+=F$o;conf[U_V][j8H](K0I)[p8A](g2M,T5f);},enable:function(conf){var f80=J_0;f80+=t_M;f80+=H_b;var A3S=t_M;A3S+=U9d;A3S+=F$o;var L03=t0$;L03+=B3P;L03+=J_0;var n8U=a4a;n8U+=t_M;n8U+=d9r;conf[n8U][L03](A3S)[z4O](f80,g49);},get:function(conf){var z5K="separ";var j99="t:";var i55=F8J;i55+=x4g;i55+=t_M;i55+=B3P;var R5o=z5K;R5o+=x6u;R5o+=F$o;R5o+=h3Y;var Q49=E3o;Q49+=z0E;Q49+=F$o;Q49+=T03;var y7C=I8x;y7C+=K29;y7C+=j99;y7C+=i1P;var u7D=l7$;u7D+=t_M;u7D+=B3P;u7D+=J_0;var b9N=a4a;b9N+=t_M;b9N+=d9r;var out=[];var selected=conf[b9N][u7D](y7C);if(selected[Q49]){selected[o8Y](function(){L7K.l4n();var o8x=m59;o8x+=Z7G;out[o8x](this[y5V]);});}else if(conf[i$c] !== undefined){out[K7Q](conf[i$c]);}return conf[h7r] === undefined || conf[R5o] === X2m?out:out[i55](conf[h7r]);},set:function(conf,val){var V_t='|';var z9Y=f$O;L7K.l4n();z9Y+=b_g;var a1V=g9B;a1V+=M5g;a1V+=M5g;a1V+=Y6A;var u_2=a4a;u_2+=E7g;u_2+=J3O;u_2+=F$o;var jqInputs=conf[u_2][B23](K0I);if(!Array[p5M](val) && typeof val === f2Z){val=val[R$w](conf[h7r] || V_t);}else if(!Array[a1V](val)){val=[val];}var i;var len=val[m2_];var found;jqInputs[z9Y](function(){var q1O="ditor_v";var C0l="hecked";var p6c=G1V;p6c+=C0l;found=g49;L7K.l4n();for(i=M1q;i < len;i++){var s9M=P5B;s9M+=q1O;s9M+=X1g;if(this[s9M] == val[i]){found=T5f;break;}}this[p6c]=found;});_triggerChange(jqInputs);},update:function(conf,options,append){var currVal=checkbox[u2A](conf);checkbox[k3H](conf,options,append);checkbox[L6_](conf,currVal);}});var radio=$[t8A](T5f,{},baseFieldType,{_addOptions:function(conf,opts,append){var M$N="onsPair";if(append === void M1q){append=g49;}var jqInput=conf[U_$];var offset=M1q;if(!append){jqInput[h55]();}else {var T85=E3o;T85+=m9z;T85+=w66;offset=$(K0I,jqInput)[T85];}if(opts){var k5c=G0V;k5c+=M$N;var c62=B4n;c62+=U2L;c62+=J7D;Editor[c62](opts,conf[k5c],function(val,label,i,attr){var F7p="or=\"";var o32="<la";var Q1$="input:";var l3V="saf";var y60=" type=\"radio\" n";var V4Y='<div>';var z4Y="bel f";var n2F="ame=\"";var k1L="\" ";L7K.b9t();var V2l="editor_val";var K1O=a4a;K1O+=V2l;var c$w=Y_Y;c$w+=E3o;c$w+=J3O;c$w+=m9z;var l5w=Q1$;l5w+=Y26;l5w+=F$o;var R6R=o32;R6R+=z4Y;R6R+=F7p;var D8o=k1L;D8o+=K5f;D8o+=m6_;var H8E=U50;H8E+=y60;H8E+=n2F;var O1y=t_M;O1y+=J_0;var L7P=l3V;L7P+=m9z;L7P+=v12;L7P+=J_0;var Q_E=x6u;Q_E+=m59;Q_E+=m59;Q_E+=A_8;jqInput[Q_E](V4Y + Q7L + Editor[L7P](conf[O1y]) + U9E + (i + offset) + H8E + conf[z$n] + D8o + R6R + Editor[l0q](conf[j_g]) + U9E + (i + offset) + K8R + label + J4Q + r65);$(l5w,jqInput)[X7W](c$w,val)[M1q][K1O]=val;if(attr){$(S8t,jqInput)[X7W](attr);}});}},create:function(conf){var I$c='<div />';var y7k=P7P;L7K.b9t();y7k+=l9T;var y7f=w55;y7f+=t_M;y7f+=g5T;y7f+=J7D;var C2C=z2m;C2C+=d9r;conf[C2C]=$(I$c);radio[k3H](conf,conf[y7f] || conf[M2R]);this[g5T](y7k,function(){var D3r=Z_n;D3r+=F$o;var v4K=z2m;v4K+=d9r;conf[v4K][B23](D3r)[o8Y](function(){var M0n="che";var l8e="preChe";var I38="cked";var c2u=a4a;c2u+=l8e;c2u+=c1J;c2u+=h6w;if(this[c2u]){var W_w=M0n;W_w+=I38;this[W_w]=T5f;}});});return conf[U_$][M1q];},disable:function(conf){var Q6C="fin";var W1P=Q6C;W1P+=J_0;var Q14=z2m;Q14+=B3P;Q14+=l76;conf[Q14][W1P](K0I)[z4O](R41,T5f);},enable:function(conf){var O06="disabl";var j6I=O06;j6I+=h6w;var R9H=t_M;R9H+=B3P;R9H+=m59;R9H+=S65;var H6J=l7$;H6J+=I8x;H6J+=J_0;var q7r=a4a;q7r+=I8x;q7r+=K29;q7r+=F$o;conf[q7r][H6J](R9H)[z4O](j6I,g49);},get:function(conf){var f5O="unselected";var j0c='input:checked';var r1U=f5O;r1U+=F2x;r1U+=v2$;var e2d=t0$;e2d+=B3P;e2d+=J_0;var el=conf[U_$][e2d](j0c);if(el[m2_]){var F4k=e1q;F4k+=N$3;return el[M1q][F4k];}return conf[r1U] !== undefined?conf[i$c]:undefined;},set:function(conf,val){var t5X="ked";var f_k=":chec";var m8f=f3N;m8f+=f_k;m8f+=t5X;var I3n=f$O;I3n+=b_g;var A5I=z2m;A5I+=d9r;conf[A5I][B23](K0I)[I3n](function(){var T3z="_preChecked";var l29="ecked";var U6J="eChecke";L7K.l4n();this[T3z]=g49;if(this[y5V] == val){var c39=Z6h;c39+=U6J;c39+=J_0;this[i1P]=T5f;this[c39]=T5f;}else {var n8G=G1V;n8G+=T03;n8G+=l29;this[n8G]=g49;this[T3z]=g49;}});_triggerChange(conf[U_$][B23](m8f));},update:function(conf,options,append){var j2u="lue";var h52='[value="';var Y$b=Y_Y;Y$b+=j2u;var Z96=y3A;Z96+=L0x;Z96+=F0A;var k8F=U50;k8F+=A7F;var g8O=J7D;g8O+=m9z;g8O+=F$o;var s$h=l7$;s$h+=t_M;s$h+=V8x;var R_P=U$t;R_P+=K29;R_P+=F$o;var currVal=radio[u2A](conf);L7K.l4n();radio[k3H](conf,options,append);var inputs=conf[R_P][s$h](K0I);radio[g8O](conf,inputs[n2y](h52 + currVal + k8F)[Z96]?currVal:inputs[N8j](M1q)[X7W](Y$b));}});var datetime=$[t8A](T5f,{},baseFieldType,{create:function(conf){var V7D="_closeFn";var a8U="datetime";var A4P="s required";var o8N='<input />';var u6v="picker";var Z2u="eyI";var z4x="displayFormat";var G53="DateTime library i";var t0Y=z2m;t0Y+=B3P;t0Y+=l76;var S4w=I9U;S4w+=Z2u;S4w+=U9d;S4w+=F$o;var t3Y=C4i;t3Y+=U5C;t3Y+=x6u;t3Y+=F$o;var o6C=U9P;o6C+=J_0;var J7q=a4a;J7q+=u6v;var X6Z=F$o;X6Z+=p6S;X6Z+=F$o;var q$2=p6S;q$2+=W1Z;q$2+=B3P;q$2+=J_0;var V7g=x6u;V7g+=d1B;var w54=e6C;w54+=J3O;w54+=F$o;conf[w54]=$(o8N)[V7g]($[q$2](T5f,{id:Editor[l0q](conf[j_g]),type:X6Z},conf[X7W]));if(!DataTable$2[P53]){var H0x=G53;H0x+=A4P;var C_F=m9z;C_F+=M5g;C_F+=e0u;Editor[C_F](H0x,y6j);}conf[J7q]=new DataTable$2[P53](conf[U_$],$[o6C]({format:conf[z4x] || conf[t3Y],i18n:this[B1B][a8U]},conf[p1E]));conf[V7D]=function(){var j8J=h4D;j8J+=J_0;j8J+=m9z;conf[p$C][j8J]();};if(conf[S4w] === g49){var o3B=a4a;o3B+=f3N;conf[o3B][g5T](m9k,function(e){L7K.b9t();e[D4J]();});}this[g5T](H$E,conf[V7D]);return conf[t0Y][M1q];},destroy:function(conf){var H1y="oseFn";var o7S=x4g;o7S+=l7$;o7S+=l7$;var x1D=a4a;x1D+=f3N;var C6h=G5L;L7K.b9t();C6h+=H1y;var d7V=G1V;d7V+=E3o;d7V+=k4i;this[H9D](d7V,conf[C6h]);conf[x1D][o7S](m9k);conf[p$C][O7A]();},errorMessage:function(conf,msg){var L$f="orMs";var O4X=K2t;O4X+=M5g;O4X+=L$f;O4X+=c3v;L7K.b9t();conf[p$C][O4X](msg);},get:function(conf){var O4T="_picke";var G4z="Locale";var W0U=F6G;W0U+=O$H;W0U+=k38;W0U+=G4z;var Z9$=C4i;Z9$+=u3Z;var E6y=f6f;E6y+=A_J;L7K.b9t();E6y+=B3P;E6y+=F$o;var K_U=O4T;K_U+=M5g;var val=conf[U_$][m1I]();var inst=conf[K_U][G1V];var moment=window[E6y];return val && conf[h0t] && moment?moment(val,inst[Z9$],inst[W0U],inst[t7n])[v4P](conf[h0t]):val;},maxDate:function(conf,max){var z_1=F6G;z_1+=x6u;z_1+=h07;conf[p$C][z_1](max);},minDate:function(conf,min){var f8t=F6G;f8t+=t_M;f8t+=B3P;conf[p$C][f8t](min);},owns:function(conf,node){var D5L=x4g;D5L+=R3d;D5L+=B3P;L7K.b9t();D5L+=J7D;return conf[p$C][D5L](node);},set:function(conf,val){var Y$6='--';var W7O="momentLocale";var k0B="moment";var f8Y=U$t;f8Y+=m59;f8Y+=S65;var i4V=l7$;i4V+=h3Y;i4V+=q0R;i4V+=F$o;var g5E=q5r;g5E+=m9z;g5E+=h07;g5E+=e5S;var v73=J7D;v73+=Z9H;v73+=I8x;v73+=c3v;var Y0V=h13;Y0V+=x6u;Y0V+=E3o;var inst=conf[p$C][G1V];var moment=window[k0B];conf[p$C][Y0V](typeof val === v73 && val && val[g5E](Y$6) !== M1q && conf[h0t] && moment?moment(val,conf[h0t],inst[W7O],inst[t7n])[v4P](inst[i4V]):val);_triggerChange(conf[f8Y]);}});var upload=$[Y3r](T5f,{},baseFieldType,{canReturnSubmit:function(conf,node){return g49;},create:function(conf){var editor=this;var container=_commonUpload(editor,conf,function(val){var n2x="po";var I75="am";var j6k=B3P;j6k+=I75;j6k+=m9z;var B8M=n2x;B8M+=m7i;B8M+=k0w;B8M+=Y5w;L7K.b9t();var k9q=G1V;k9q+=X1g;k9q+=E3o;var e_Q=J7D;e_Q+=Z0V;upload[e_Q][k9q](editor,conf,val[M1q]);editor[m6A](B8M,[conf[j6k],val[M1q]]);});return container;},disable:function(conf){var c7T="nabled";var o1B=P5B;o1B+=c7T;var h87=L0p;h87+=H_b;var N4y=m59;N4y+=t09;var V4o=I8x;V4o+=m59;V4o+=S65;conf[U_$][B23](V4o)[N4y](h87,T5f);conf[o1B]=g49;},enable:function(conf){var L20="_enab";var y$J=L20;y$J+=y6x;var j_P=m59;j_P+=J$_;j_P+=m59;var W0b=l7$;W0b+=I8x;W0b+=J_0;conf[U_$][W0b](K0I)[j_P](R41,g49);conf[y$J]=T5f;},get:function(conf){L7K.l4n();return conf[Z6g];},set:function(conf,val){var k59="rT";var l2K="iv.rendered";var V1E="</sp";var S40="clearText";var l7B="fil";var y2Z="triggerHan";var m5_="oad.e";var M_Q="dler";var e9E="o ";var l9F="noFileText";var j2V="noCle";var p4p="cle";var b5L="arValue button";var U6M='noClear';var z6r="div.cle";var W6W=A$Q;W6W+=m5_;W6W+=J_0;W6W+=W39;var A6b=y2Z;A6b+=M_Q;var G1t=I8x;G1t+=m59;G1t+=S65;var E$B=l7$;E$B+=I8x;E$B+=J_0;var p0z=z6r;p0z+=b5L;var L_H=L0p;L_H+=g5V;L_H+=A33;var g9_=a4a;g9_+=f3N;var m_X=h13;m_X+=x6u;m_X+=E3o;var C6a=a4a;C6a+=Y_Y;C6a+=E3o;conf[C6a]=val;conf[U_$][m_X](l2Y);var container=conf[g9_];if(conf[L_H]){var e1z=J_0;e1z+=l2K;var D7m=t0$;D7m+=V8x;var rendered=container[D7m](e1z);if(conf[Z6g]){var a7k=a4a;a7k+=h13;a7k+=x6u;a7k+=E3o;var b$I=J_0;b$I+=X0T;b$I+=x6u;b$I+=A33;rendered[y64](conf[b$I](conf[a7k]));}else {var B7l=V1E;B7l+=x6u;B7l+=J3J;var h0u=T4d;h0u+=e9E;h0u+=l7B;h0u+=m9z;var Z9A=f9u;Z9A+=m9z;Z9A+=B3P;Z9A+=J_0;rendered[h55]()[Z9A](j6w + (conf[l9F] || h0u) + B7l);}}var button=container[B23](p0z);if(val && conf[S40]){var j4K=C5B;j4K+=m0I;j4K+=x0u;var E$s=p4p;E$s+=x6u;E$s+=k59;E$s+=V8g;button[y64](conf[E$s]);container[j4K](U6M);}else {var l0l=j2V;l0l+=j4C;var w47=x6v;w47+=x0u;container[w47](l0l);}conf[U_$][E$B](G1t)[A6b](W6W,[conf[Z6g]]);}});var uploadMany=$[t8A](T5f,{},baseFieldType,{_showHide:function(conf){var f4h="Lef";var M4H="div.limitHid";var I_i="_container";var b83="imi";var K1q="im";var D2Z="imit";var z7n=a4H;z7n+=n3b;z7n+=T03;var K7O=p71;K7O+=X57;K7O+=F$o;var Q8F=B87;Q8F+=D2Z;Q8F+=f4h;Q8F+=F$o;var C$4=B3P;C$4+=g5T;C$4+=m9z;var F7f=E3o;F7f+=b83;F7f+=F$o;var m0z=G1V;m0z+=f63;var v8T=M4H;v8T+=m9z;var k49=l7$;k49+=t_M;k49+=V8x;var l78=E3o;l78+=K1q;l78+=t_M;l78+=F$o;if(!conf[l78]){return;}conf[I_i][k49](v8T)[m0z](k6I,conf[Z6g][m2_] >= conf[F7f]?C$4:K9F);conf[Q8F]=conf[K7O] - conf[Z6g][z7n];},canReturnSubmit:function(conf,node){return g49;},create:function(conf){var r1q="_conta";var Q2s="ul";var J0i="lick";var d_H=r1q;d_H+=I8x;L7K.b9t();d_H+=K2t;var R2N=T$I;R2N+=Y1M;R2N+=m2K;R2N+=Y9F;var C$w=G1V;C$w+=J0i;var k5f=F6G;k5f+=Q2s;k5f+=F$o;k5f+=t_M;var editor=this;var container=_commonUpload(editor,conf,function(val){var k6K='postUpload';var i9H="concat";var m$1=J7D;m$1+=m9z;m$1+=F$o;var f7_=a4a;f7_+=Y_Y;f7_+=E3o;var H44=a4a;H44+=m1I;L7K.b9t();conf[H44]=conf[f7_][i9H](val);uploadMany[m$1][H7v](editor,conf,conf[Z6g]);editor[m6A](k6K,[conf[z$n],conf[Z6g]]);},T5f);container[w5Y](k5f)[g5T](C$w,R2N,function(e){var u8i="stopPropagation";var x8C="lice";e[u8i]();if(conf[v_X]){var k9H=a4a;k9H+=Y_Y;k9H+=E3o;var t$E=D47;t$E+=x8C;var O1C=t_M;O1C+=J_0;O1C+=h07;var d8B=J_0;d8B+=x6u;d8B+=f$8;var idx=$(this)[d8B](O1C);conf[Z6g][t$E](idx,p88);uploadMany[L6_][H7v](editor,conf,conf[k9H]);}});conf[d_H]=container;return container;},disable:function(conf){var w3C="np";var Q5Y=J_0;Q5Y+=P1D;Q5Y+=I3h;var o1x=t_M;o1x+=w3C;o1x+=J3O;o1x+=F$o;var D9h=a4a;D9h+=E7g;D9h+=J3O;D9h+=F$o;conf[D9h][B23](o1x)[z4O](Q5Y,T5f);conf[v_X]=g49;},enable:function(conf){var e9v=P5B;e9v+=B3P;e9v+=I3h;var O9g=m59;O9g+=M5g;O9g+=x4g;O9g+=m59;conf[U_$][B23](K0I)[O9g](R41,g49);conf[e9v]=T5f;},get:function(conf){var I1B=a4a;I1B+=Y_Y;I1B+=E3o;return conf[I1B];},set:function(conf,val){var E0P='</span>';var D3M="noFil";var x7t="ad.";var s8w="eT";var p77="</ul>";var w43='Upload collections must have an array as a value';var p0m="<ul>";var L_l="iggerHandler";var R2s=".rendered";var x5H="o fil";var u4N="tor";var E6n="_showHide";var A47="ndTo";var U$U=l3o;U$U+=x7t;U$U+=P3c;L7K.l4n();U$U+=u4N;var C4F=F$o;C4F+=M5g;C4F+=L_l;var x5i=l7$;x5i+=q5r;var x9$=d8z;x9$+=x6u;x9$+=A33;var D6T=e6C;D6T+=J3O;D6T+=F$o;if(!val){val=[];}if(!Array[p5M](val)){throw new Error(w43);}conf[Z6g]=val;conf[D6T][m1I](l2Y);var that=this;var container=conf[U_$];if(conf[x9$]){var m0L=H_N;m0L+=F0A;var O1t=f_P;O1t+=R2s;var Y1t=l7$;Y1t+=t_M;Y1t+=B3P;Y1t+=J_0;var rendered=container[Y1t](O1t)[h55]();if(val[m0L]){var u_L=f$O;u_L+=G1V;u_L+=T03;var O4h=j3s;O4h+=A47;var Z2f=p0m;Z2f+=p77;var list_1=$(Z2f)[O4h](rendered);$[u_L](val,function(i,file){var V84="\">&times;</but";var S_D="ppend";var s3m='</li>';var P65='<li>';var l1Z=' <button class="';var p1H="remove\" data-idx=\"";var v5n=V84;v5n+=F$o;v5n+=v82;var X54=K2Z;X54+=p1H;var o3Y=C4i;o3Y+=U5C;var d1z=x6u;d1z+=S_D;L7K.l4n();list_1[d1z](P65 + conf[s$8](file,i) + l1Z + that[I58][o3Y][T$I] + X54 + i + v5n + s3m);});}else {var L1s=T4d;L1s+=x5H;L1s+=C_A;var t2V=D3M;t2V+=s8w;t2V+=V8g;rendered[K$Z](j6w + (conf[t2V] || L1s) + E0P);}}uploadMany[E6n](conf);conf[U_$][x5i](K0I)[C4F](U$U,[conf[Z6g]]);}});var datatable=$[t8A](T5f,{},baseFieldType,{_addOptions:function(conf,options,append){if(append === void M1q){append=g49;}L7K.b9t();var dt=conf[J3a];if(!append){dt[F2L]();}dt[o1V][x6v](options)[u71]();},_jumpToFirst:function(conf){var G8$="page";var q4u="floor";var D7v="mb";var H_C='applied';var W6L="ndexes";L7K.l4n();var q4I=J_0;q4I+=F$o;var I4H=B3P;I4H+=J3O;I4H+=D7v;I4H+=K2t;var X0q=J$_;X0q+=R3d;var idx=conf[J3a][X0q]({order:H_C,selected:T5f})[j_l]();var page=M1q;if(typeof idx === I4H){var l9S=t_M;l9S+=W6L;var G$4=M5g;G$4+=d30;var b37=J_0;b37+=F$o;var h5I=E3o;h5I+=P93;h5I+=T03;var R2L=I8x;R2L+=C4i;var L1u=B4n;L1u+=q9o;var pageLen=conf[J3a][L1u][R2L]()[h5I];var pos=conf[b37][G$4]({order:H_C})[l9S]()[J6b](idx);page=pageLen > M1q?Math[q4u](pos / pageLen):M1q;}conf[q4I][G8$](page)[u71](g49);},create:function(conf){var I3q="Complete";var L4r="r-selec";var q6q='label';var M4N="si";var g7j="tableClass";var O9n="10";var M_l=".dt";var G7X="use";var K14='<tr>';var Y5d='Search';var k0u="nfi";var x55="info\">";var d7p='<table>';var c2L="option";var D7y="oo";var N_5="Tabl";var d_9="tatable_";var e8G="wid";var K1N="tf";var z0Q="<div class=\"DTE_Field_Type_da";var N5_="0%";var f$2="ppen";var o52="Ar";var U6V="tp";var v3a="ltip";var S61='Label';var S6e=J_0;S6e+=F$o;var T9i=G7X;T9i+=L4r;T9i+=F$o;var P$n=G1V;P$n+=x4g;P$n+=k0u;P$n+=c3v;L7K.l4n();var n3a=M4N;n3a+=L39;var n7J=x4g;n7J+=J7D;var h6c=c5m;h6c+=v3a;h6c+=y3A;var p9C=l7$;p9C+=t_M;p9C+=u1c;p9C+=U6V;var k9z=y4x;k9z+=g74;var y4S=p6S;y4S+=F$o;y4S+=m9z;y4S+=V8x;var z2E=e5N;z2E+=f$8;z2E+=N_5;z2E+=m9z;var r_l=t_M;r_l+=Y0W;r_l+=F$o;r_l+=M_l;var y4T=x4g;y4T+=B3P;var a4r=O9n;a4r+=N5_;var c0k=e8G;c0k+=F0A;var a$d=x6v;a$d+=O5i;a$d+=h4Q;var W_3=z0Q;W_3+=d_9;W_3+=x55;var O6Y=x6u;O6Y+=f$2;O6Y+=J_0;var v3v=W7n;v3v+=L0p;v3v+=h13;v3v+=m6_;var N1o=Y_Y;N1o+=E3o;N1o+=a6X;var Z7Q=c2L;Z7Q+=f4W;var _this=this;conf[Z7Q]=$[t8A]({label:q6q,value:N1o},conf[X2T]);var table=$(d7p);var container=$(v3v)[O6Y](table);var side=$(W_3);if(conf[j8X]){var w4Z=t_M;w4Z+=J7D;w4Z+=o52;w4Z+=Z0l;var V7Y=W7n;V7Y+=K1N;V7Y+=D7y;V7Y+=d52;$(V7Y)[K$Z](Array[w4Z](conf[j8X])?$(K14)[K$Z]($[S9O](conf[j8X],function(str){var Z7s="h>";var H20="<t";L7K.b9t();var Z05=H20;Z05+=Z7s;return $(Z05)[y64](str);})):conf[j8X])[a4w](table);}var dt=table[a$d](datatable[g7j])[c0k](a4r)[y4T](r_l,function(e,settings){var e93="tabl";var N7t='div.dataTables_filter';var I$3="s_info";var w6Z="div.data";var y$1='div.dt-buttons';var K2j=w6Z;K2j+=B4K;K2j+=I$3;var G1a=l7$;G1a+=t_M;G1a+=V8x;var l3D=j3s;L7K.b9t();l3D+=B3P;l3D+=J_0;var Q76=l7$;Q76+=t_M;Q76+=B3P;Q76+=J_0;var D3A=j3s;D3A+=V8x;var j44=x6u;j44+=m59;j44+=C5C;j44+=V8x;var L7Z=I8x;L7Z+=t_M;L7Z+=F$o;var i$v=b00;i$v+=k1z;i$v+=x6u;i$v+=s79;var f1f=e93;f1f+=m9z;var j9w=Y4Q;j9w+=w2L;var api=new DataTable$2[j9w](settings);var containerNode=$(api[f1f](undefined)[i$v]());DataTable$2[q4l][L7Z](api);side[j44](containerNode[B23](N7t))[D3A](containerNode[Q76](y$1))[l3D](containerNode[G1a](K2j));})[z2E]($[y4S]({buttons:[],columns:[{data:conf[X2T][k9z],title:S61}],deferRender:T5f,dom:p9C,language:{paginate:{next:j7f,previous:u8o},search:l2Y,searchPlaceholder:Y5d},lengthChange:g49,select:{style:conf[h6c]?n7J:n3a}},conf[P$n]));this[g5T](v96,function(){L7K.l4n();var M9U="search";var P7C="adjust";var B5c="aw";if(dt[M9U]()){var m0d=J_0;m0d+=M5g;m0d+=B5c;dt[M9U](l2Y)[m0d]();}dt[e65][P7C]();});dt[g5T](T9i,function(){var x3G="ainer";var B9y=e0Y;B9y+=x3G;var t81=F$o;t81+=r8u;t81+=E3o;t81+=m9z;_triggerChange($(conf[J3a][t81]()[B9y]()));});if(conf[n3n]){var R3Z=f4c;R3Z+=I3q;var R7C=F$o;R7C+=r8u;R7C+=E3o;R7C+=m9z;conf[n3n][R7C](dt);conf[n3n][g5T](R3Z,function(e,json,data,action){var v3A="efr";var Z7X="jumpToFirst";var R8$=a4a;R8$+=Z7X;L7K.b9t();var S5u=m9z;S5u+=J_0;S5u+=H7H;if(action === R4H){var _loop_1=function(dp){var U3l="sele";L7K.l4n();var t5M=U3l;t5M+=W6y;dt[o1V](function(idx,d){L7K.l4n();return d === dp;})[t5M]();};for(var _i=M1q,_a=json[z4k];_i < _a[m2_];_i++){var dp=_a[_i];_loop_1(dp);}}else if(action === S5u || action === K5K){var A2$=M5g;A2$+=v3A;A2$+=m9z;A2$+=T3Q;_this[W_A](A2$);}datatable[R8$](conf);});}conf[S6e]=dt;datatable[k3H](conf,conf[Z0p] || []);return {input:container,side:side};},disable:function(conf){var M8b=B3P;M8b+=x4g;M8b+=B3P;M8b+=m9z;var z$7=G1V;L7K.b9t();z$7+=f63;var Q_9=J_0;Q_9+=F$o;var A0Q=H0m;A0Q+=t_M;var j2F=J7D;j2F+=F$o;j2F+=J_A;conf[J3a][q4l][j2F](A0Q);conf[Q_9][F8y]()[z3e]()[z$7](k6I,M8b);},dt:function(conf){return conf[J3a];},enable:function(conf){var i1m='single';var V87='os';var X0b=d8z;X0b+=x6u;X0b+=A33;var Y_2=G1V;Y_2+=J7D;Y_2+=J7D;var D09=u3Y;D09+=J3O;D09+=F$o;D09+=Y47;conf[J3a][q4l][Y3a](conf[q5o]?V87:i1m);conf[J3a][D09]()[z3e]()[Y_2](X0b,K9F);},get:function(conf){var Q8D="sepa";var T44="rat";var G5I="valu";var S6F="ws";var s3r="ltipl";var P_m="pluck";var t5L="oAr";var s1Y="tionsPa";var z28=F8J;z28+=x4g;z28+=t_M;z28+=B3P;var V7z=c5m;V7z+=s3r;V7z+=m9z;var g_v=Q8D;g_v+=T44;g_v+=x4g;g_v+=M5g;var W9G=F$o;W9G+=t5L;W9G+=Z0l;L7K.l4n();var O3$=G5I;O3$+=m9z;var W3a=P7P;W3a+=s1Y;W3a+=t_M;W3a+=M5g;var C$V=J$_;C$V+=S6F;var P26=J_0;P26+=F$o;var rows=conf[P26][C$V]({selected:T5f})[z4k]()[P_m](conf[W3a][O3$])[W9G]();return conf[g_v] || !conf[V7z]?rows[z28](conf[h7r] || y$0):rows;},set:function(conf,val,localUpdate){var a80="sep";var k4G="separa";var J1G="desele";var p3S="ption";var i0t="_jump";var Q0v="ToFirst";var r8C=i0t;r8C+=Q0v;var J7S=M5g;J7S+=x4g;J7S+=R3d;J7S+=J7D;var W_i=J_0;W_i+=F$o;var Y4h=J1G;Y4h+=W6y;var J_Q=x4g;J_Q+=p3S;J_Q+=f4W;var F1w=t_M;F1w+=M30;F1w+=M5g;F1w+=Y6A;var h$k=J94;h$k+=A33;var f_B=k4G;f_B+=F$o;f_B+=x4g;f_B+=M5g;if(conf[q5o] && conf[f_B] && !Array[h$k](val)){var t8V=a80;t8V+=x6u;t8V+=M5g;t8V+=w7B;var k_S=Y9$;k_S+=t_M;k_S+=F$o;val=typeof val === f2Z?val[k_S](conf[t8V]):[];}else if(!Array[F1w](val)){val=[val];}var valueFn=dataGet(conf[J_Q][n01]);conf[J3a][o1V]({selected:T5f})[Y4h]();conf[W_i][J7S](function(idx,data,node){var W_z="O";var G44=q5r;G44+=p6S;G44+=W_z;G44+=l7$;return val[G44](valueFn(data)) !== -p88;})[q4l]();datatable[r8C](conf);if(!localUpdate){var M9y=Z2U;M9y+=F$o;M9y+=x6u;M9y+=s79;var S$s=f$8;S$s+=T_U;S$s+=m9z;var b0f=J_0;b0f+=F$o;_triggerChange($(conf[b0f][S$s]()[M9y]()));}},tableClass:l2Y,update:function(conf,options,append){var j1n="tSet";var v3Z="_addOption";var W8_="ntai";var c7O="ner";var p18="_las";var q2P=b00;q2P+=W8_;q2P+=c7O;var f7n=J_0;f7n+=F$o;var w72=p18;w72+=j1n;var W1U=v3Z;W1U+=J7D;datatable[W1U](conf,options,append);var lastSet=conf[w72];if(lastSet !== undefined){var A0R=J7D;A0R+=m9z;A0R+=F$o;datatable[A0R](conf,lastSet,T5f);}_triggerChange($(conf[f7n][O81]()[q2P]()));}});var defaults={className:l2Y,compare:X2m,data:l2Y,def:l2Y,entityDecode:T5f,fieldInfo:l2Y,getFormatter:X2m,id:l2Y,label:l2Y,labelInfo:l2Y,message:l2Y,multiEditable:T5f,name:X2m,nullDefault:g49,setFormatter:X2m,submit:T5f,type:t8v};var DataTable$1=$[l7d][q1t];var Field=(function(){var h3N="_f";var D5W="prototy";var d6Q="_msg";var G$D="roto";var T_k="labelInfo";var a2S="_typeF";var h15="multiIds";var b17="multiInfo";var i63="rN";var f8O="rot";var P3t="ot";var a_7="formatters";var w_n="_multiValueCheck";var G7B="fault";var c3t="be";var R8S="aine";var W2B="multiValues";var S4K="lock";var T3y="toty";var B0R="setFormatter";var T7n="rototy";var d8O="rotot";var Y1b="ype";var P_Z="multiValue";var T2F="iReset";var a8a="MultiValue";var Q0w="dis";var p5d="belInf";var k8S="nullD";var i3H="roy";var r3L="otype";var Z9B="oto";var E2Q="multiRestore";var A0q="_ty";var x4j="enabled";var Q0q="otyp";var w8f="ototyp";var r_r="process";var J5s="multiEditable";var O_b="fieldInfo";var e3d="totyp";var n30="prot";var A0M="ultiV";var t6f="pare";var G3t="alu";var O2y="totype";var v4e="_format";var M1K="ultiVal";var l_l="host";var O12="how";var I6B="disabled";var t8p=h3N;t8p+=h3Y;t8p+=q0R;t8p+=F$o;var k$m=m59;k$m+=T7n;k$m+=C5C;var S2E=a4a;S2E+=M3d;S2E+=i63;S2E+=g6C;var r1K=a2S;r1K+=B3P;var N5$=z79;N5$+=Y1b;var S5M=N2b;S5M+=M1K;S5M+=E$K;var J2k=x0n;J2k+=F$o;J2k+=r3L;var G2L=D6L;G2L+=w8f;G2L+=m9z;var W74=c5m;W74+=I3i;W74+=T2F;var f3n=m59;f3n+=J$_;f3n+=O2y;var L0o=z79;L0o+=J2a;L0o+=m9z;var U$p=V6H;U$p+=J7D;U$p+=F$o;U$p+=i3H;var c7q=D6L;c7q+=w8f;c7q+=m9z;var X0z=M49;X0z+=x6u;X0z+=s7M;X0z+=H_U;var U5G=p7R;U5G+=t6f;var q0a=W2s;q0a+=Z29;var A0C=m59;A0C+=f8O;A0C+=Q0q;A0C+=m9z;var L0c=J7D;L0c+=O12;var m9B=D6L;m9B+=P3t;m9B+=Q0q;m9B+=m9z;var P_T=r_r;P_T+=Z1a;var C8$=n30;C8$+=r3L;var m3c=k8S;m3c+=m9z;m3c+=G7B;var h5O=x0n;h5O+=O2y;var k8Z=z79;k8Z+=J2a;k8Z+=m9z;var K0k=Z1n;K0k+=T0o;var H9V=n30;H9V+=P3t;H9V+=Y1b;var h_7=D5W;h_7+=C5C;var f9S=y4x;f9S+=p5d;f9S+=x4g;var T1P=m59;T1P+=d8O;T1P+=A33;T1P+=C5C;var w06=x0n;w06+=e3d;w06+=m9z;var a_W=P1D;a_W+=a8a;var I8n=m59;I8n+=d8O;I8n+=Y1b;var d4W=m9z;d4W+=M5g;d4W+=e0u;var H6C=D6L;H6C+=Z9B;H6C+=P3q;var p2I=l9T;p2I+=c_k;var x4b=x0n;x4b+=O2y;var h50=m59;h50+=G$D;h50+=F$o;h50+=Y1b;var O64=x0n;O64+=T3y;O64+=C5C;function Field(options,classes,host){var E3_="ultiReturn";var r9n="ty";var A8Z="trol";var l0X="<span data-dte-e=\"multi";var M_u="div data-dte-e=\"msg-multi\" cla";var t3N="<div data-dt";var V50="msg-";var K3O="side";var v65="lass=\"";var M5h="info";var x7y='msg-label';var H2n="dInfo";var J0D="<div data-dte-e=\"multi-value\" clas";var K3Z="rol\" c";var x8Y="itl";var I_1="field-process";var g4v="inputCon";var c11="efa";var N7A='msg-message';var g_h="-mu";var M8Y="e-e=\"input-cont";var x5a="</di";var O5g='Error adding field - unknown field type ';var S$P="valFrom";var N40='" for="';var i50="</spa";var v_F='<div data-dte-e="msg-info" class="';var E64="-control";var R8p='input-control';var G6d='<div data-dte-e="msg-label" class="';var u5D="-err";var N_G="-i";var S2C="fieldTyp";var Z9a='DTE_Field_';var e45='<div data-dte-e="field-processing" class="';var d8K="ass=\"";var Q79="restore";var i_b="ms";var G_T='<div data-dte-e="msg-message" class="';var J2r="ults";var k0o="-label";var t4Z='msg-info';var B1Z="<div data-dte-e=\"input\" cla";var e8X='<label data-dte-e="label" class="';var l1D="ssNa";var U9J="namePrefix";var K$U="lti";var i2k="tiRest";var L1f=" data-dte-e=\"msg-error\" cl";L7K.l4n();var T45="ore";var b0V="-info\" class=\"";var D4x="nam";var V4U="\"><span></span></div";var v$5="typePrefix";var Z$A="msg";var Z4V="<div c";var o47=r9n;o47+=C5C;var r27=m9z;r27+=x6u;r27+=G1V;r27+=T03;var m26=w3s;m26+=t_M;m26+=c1J;var C63=x4g;C63+=B3P;var F$0=F6G;F$0+=E3_;var S7U=x4g;S7U+=B3P;var Q7d=c5m;Q7d+=K$U;var n3R=I_1;n3R+=Z1a;var F5V=Z$A;F5V+=g_h;F5V+=E3o;F5V+=B91;var e3l=Z$A;e3l+=k0o;var y9a=E3o;y9a+=x6u;y9a+=c3t;y9a+=E3o;var t7u=t_M;t7u+=d9r;t7u+=E64;var n$$=Z$A;n$$+=N_G;n$$+=B3P;n$$+=C4i;var W4q=i_b;W4q+=c3v;W4q+=u5D;W4q+=h3Y;var h1z=J_0;h1z+=x4g;h1z+=F6G;var X8G=V4U;X8G+=m6_;var f4l=J7h;f4l+=e8D;var n_H=x5a;n_H+=h13;n_H+=m6_;var P9f=W7n;P9f+=i51;P9f+=t_M;P9f+=a$t;var l_L=S4_;l_L+=H2n;var u0i=U50;u0i+=m6_;var G7U=A_J;G7U+=R70;G7U+=m9z;var v4$=V50;v4$+=m9z;v4$+=T5j;v4$+=h3Y;var G4P=M6A;G4P+=L1f;G4P+=d8K;var A6N=U50;A6N+=m6_;var q_t=j4l;q_t+=i2k;q_t+=T45;var L1O=W7n;L1O+=M_u;L1O+=f63;L1O+=g0j;var C9W=x5a;C9W+=a$t;var v8P=i50;v8P+=J3J;var L_d=U50;L_d+=m6_;var f1n=l0X;f1n+=b0V;var C3Y=F$o;C3Y+=x8Y;C3Y+=m9z;var A_C=F6G;A_C+=A0M;A_C+=G3t;A_C+=m9z;var B4d=J0D;B4d+=p_t;var d1v=g4v;d1v+=A8Z;var w6K=t3N;w6K+=M8Y;w6K+=K3Z;w6K+=v65;var h2B=U50;h2B+=m6_;var c0N=t_M;c0N+=d9r;var c_X=B1Z;c_X+=v8g;var N$a=e1t;N$a+=J_0;N$a+=h1_;var W9Q=U50;W9Q+=m6_;var L0s=U50;L0s+=m6_;var b2C=p6g;b2C+=B$4;b2C+=R5K;var W8x=w3s;W8x+=x6u;W8x+=l1D;W8x+=A_J;var D44=F$o;D44+=Y1b;var l3U=Z4V;l3U+=v65;var q2W=S$P;q2W+=e5N;q2W+=f$8;var O_5=d$I;O_5+=F$o;O_5+=x6u;var t69=t_M;t69+=J_0;var U8o=S2C;U8o+=C_A;var F0b=F$o;F0b+=J2a;F0b+=m9z;var A6k=J_0;A6k+=c11;A6k+=J2r;var d0I=p6S;d0I+=F$o;d0I+=A_8;var that=this;var multiI18n=host[r5G]()[x2M];var opts=$[d0I](T5f,{},Field[A6k],options);if(!Editor[q_9][opts[F0b]]){throw new Error(O5g + opts[P3q]);}this[J7D]={classes:classes,host:host,multiIds:[],multiValue:g49,multiValues:{},name:opts[z$n],opts:opts,processing:g49,type:Editor[U8o][opts[P3q]]};if(!opts[t69]){opts[j_g]=Z9a + opts[z$n];}if(opts[O_5] === l2Y){var I7K=D4x;I7K+=m9z;opts[z4k]=opts[I7K];}this[q2W]=function(d){L7K.b9t();var B$J='editor';return dataGet(opts[z4k])(d,B$J);};this[U21]=dataSet(opts[z4k]);var template=$(l3U + classes[A8Y] + m5s + classes[v$5] + opts[D44] + m5s + classes[U9J] + opts[z$n] + m5s + opts[W8x] + K8R + e8X + classes[S1J] + N40 + Editor[b2C](opts[j_g]) + L0s + opts[S1J] + G6d + classes[x7y] + W9Q + opts[T_k] + N$a + J4Q + c_X + classes[c0N] + h2B + w6K + classes[d1v] + o2c + B4d + classes[A_C] + K8R + multiI18n[C3Y] + f1n + classes[b17] + L_d + multiI18n[M5h] + v8P + C9W + L1O + classes[q_t] + A6N + multiI18n[Q79] + r65 + G4P + classes[v4$] + o2c + G_T + classes[N7A] + K8R + opts[G7U] + r65 + v_F + classes[t4Z] + u0i + opts[l_L] + P9f + n_H + e45 + classes[f4l] + X8G + r65);var input=this[Y3o](R4H,opts);var side=X2m;if(input && input[K3O]){var Z6z=E7g;Z6z+=S65;var b3e=J7D;b3e+=t_M;b3e+=J_0;b3e+=m9z;side=input[b3e];input=input[Z6z];}if(input !== X2m){var o4s=m59;o4s+=p9u;o4s+=l9T;o4s+=J_0;el(R8p,template)[o4s](input);}else {var E9L=p2t;E9L+=E3o;E9L+=Y6A;template[e$Y](E9L,N7C);}this[h1z]={container:template,fieldError:el(W4q,template),fieldInfo:el(n$$,template),fieldMessage:el(N7A,template),inputControl:el(t7u,template),label:el(y9a,template)[K$Z](side),labelInfo:el(e3l,template),multi:el(D5N,template),multiInfo:el(u2P,template),multiReturn:el(F5V,template),processing:el(n3R,template)};this[I_d][Q7d][S7U](a55,function(){var G0s="tiEdita";var x5f="asCl";var b6s=T03;b6s+=x5f;b6s+=x6u;b6s+=f63;var A5F=j4l;A5F+=G0s;A5F+=T_U;A5F+=m9z;var N6q=x4g;N6q+=m59;N6q+=F$o;N6q+=J7D;if(that[J7D][N6q][A5F] && !template[b6s](classes[I6B]) && opts[P3q] !== b7h){that[m1I](l2Y);that[F4U]();}});this[I_d][F$0][C63](m26,function(){that[E2Q]();});$[r27](this[J7D][o47],function(name,fn){if(typeof fn === v4B && that[name] === undefined){that[name]=function(){var w9V="typ";var j6H="eF";var M4Y="ly";var N6L=x6u;N6L+=m59;N6L+=m59;N6L+=M4Y;var A2J=a4a;A2J+=w9V;A2J+=j6H;A2J+=B3P;var f1H=n30;f1H+=x4g;f1H+=w9V;f1H+=m9z;var args=Array[f1H][E13][H7v](arguments);args[l1E](name);var ret=that[A2J][N6L](that,args);return ret === undefined?that:ret;};}});}Field[N0G][g0I]=function(set){var V_O="efau";var r9Q="fau";var Q1V=J_0;L7K.l4n();Q1V+=q8u;var opts=this[J7D][p1E];if(set === undefined){var F7b=J_0;F7b+=m9z;F7b+=l7$;var g70=J_0;g70+=V_O;g70+=I3i;var z7M=J_0;z7M+=m9z;z7M+=r9Q;z7M+=I3i;var def=opts[z7M] !== undefined?opts[g70]:opts[F7b];return typeof def === v4B?def():def;}opts[Q1V]=set;return this;};Field[O64][e9x]=function(){var J4e="lasses";var B2e="eFn";var o3O="disa";var z2P="ddCla";var Z$T=Q0w;Z$T+=c_k;var v84=w4j;v84+=A33;v84+=m59;v84+=B2e;var P96=o3O;P96+=u3Y;P96+=y3A;P96+=J_0;var t58=G1V;t58+=J4e;var z2N=x6u;z2N+=z2P;z2N+=J7D;z2N+=J7D;var h1u=e0Y;h1u+=R8S;h1u+=M5g;this[I_d][h1u][z2N](this[J7D][t58][P96]);this[v84](Z$T);return this;};Field[h50][k42]=function(){var q0U=B3P;q0U+=x4g;q0U+=B3P;q0U+=m9z;var T8W=E3o;T8W+=m9z;T8W+=L0x;T8W+=F0A;var r6N=u3Y;r6N+=x4g;r6N+=J_0;r6N+=A33;var v0m=B4n;v0m+=e2b;v0m+=U15;var container=this[I_d][z3e];return container[v0m](r6N)[T8W] && container[e$Y](k6I) !== q0U?T5f:g49;};Field[x4b][p2I]=function(toggle){var Y0M="asse";var V4T=m9z;V4T+=B3P;V4T+=r8u;V4T+=y3A;var W$9=A0q;W$9+=C5C;W$9+=R9z;L7K.b9t();W$9+=B3P;var V_A=w3s;V_A+=Y0M;V_A+=J7D;if(toggle === void M1q){toggle=T5f;}if(toggle === g49){return this[e9x]();}this[I_d][z3e][A71](this[J7D][V_A][I6B]);this[W$9](V4T);return this;};Field[N0G][x4j]=function(){var Q9F=w3s;Q9F+=l6s;Q9F+=f7S;return this[I_d][z3e][p5N](this[J7D][Q9F][I6B]) === g49;};Field[H6C][d4W]=function(msg,fn){var S__='errorMessage';var f17="asses";var w4M="fieldError";var C1R=J_0;C1R+=x4g;C1R+=F6G;var c9V=G1V;c9V+=E3o;c9V+=f17;var classes=this[J7D][c9V];if(msg){var J4s=x6v;J4s+=O5i;J4s+=l6s;J4s+=J7D;this[I_d][z3e][J4s](classes[x_w]);}else {var T6P=Z2U;T6P+=f$8;T6P+=I8x;T6P+=K2t;this[I_d][T6P][A71](classes[x_w]);}this[Y3o](S__,msg);return this[d6Q](this[C1R][w4M],msg,fn);};Field[N0G][O_b]=function(msg){var N3b="ieldI";var v02=l7$;v02+=N3b;v02+=I$v;var R40=a4a;R40+=F6G;R40+=J7D;R40+=c3v;return this[R40](this[I_d][v02],msg);};Field[I8n][a_W]=function(){var V6d="multiValu";L7K.b9t();var G14=V6d;G14+=m9z;return this[J7D][G14] && this[J7D][h15][m2_] !== p88;};Field[N0G][n4o]=function(){var y5$="sse";var y3_="err";var v44=y3_;v44+=x4g;v44+=M5g;var U45=G1V;U45+=y4x;U45+=y5$;U45+=J7D;return this[I_d][z3e][p5N](this[J7D][U45][v44]);};Field[N0G][f3N]=function(){L7K.b9t();var G9k="ntain";var Y7n="ypeF";var J3t='input, select, textarea';var o7C=b00;o7C+=G9k;o7C+=K2t;var K2Q=t_M;K2Q+=d9r;var z_E=a4a;z_E+=F$o;z_E+=Y7n;z_E+=B3P;return this[J7D][P3q][f3N]?this[z_E](K2Q):$(J3t,this[I_d][o7C]);};Field[N0G][F4U]=function(){var w9R=", ";var k4z="textarea";var b8A="peFn";var q2r="tain";var q87="input, select";var s36=C4i;s36+=G1V;s36+=c5s;if(this[J7D][P3q][s36]){var r6V=A0q;r6V+=b8A;this[r6V](A46);}else {var j0z=Z2U;j0z+=q2r;j0z+=K2t;var e8H=q87;e8H+=w9R;e8H+=k4z;$(e8H,this[I_d][j0z])[F4U]();}return this;};Field[N0G][u2A]=function(){var F2e="Fn";var r7x='get';var Q58="isMul";var P1x="tiV";var Y1p="getFormatter";var I8h=x4g;I8h+=m59;I8h+=F$o;I8h+=J7D;var z4f=w4j;z4f+=Y1b;z4f+=F2e;var d6f=Q58;d6f+=P1x;d6f+=G3t;d6f+=m9z;if(this[d6f]()){return undefined;}return this[v4e](this[z4f](r7x),this[J7D][I8h][Y1p]);};Field[w06][m0K]=function(animate){var l3Q="ide";var D7e="hos";var J0p="eU";var H8b=f5h;L7K.b9t();H8b+=j_g;H8b+=J0p;H8b+=m59;var I4p=l7$;I4p+=B3P;var T4g=d8z;T4g+=Y6A;var z$G=D7e;z$G+=F$o;var el=this[I_d][z3e];if(animate === undefined){animate=T5f;}if(this[J7D][z$G][T4g]() && animate && $[I4p][H8b]){var T_O=J7D;T_O+=E3o;T_O+=l3Q;T_O+=k0w;el[T_O]();}else {var p$l=L0p;p$l+=D47;p$l+=P99;var W6w=G1V;W6w+=J7D;W6w+=J7D;el[W6w](p$l,N7C);}return this;};Field[N0G][S1J]=function(str){var S7S=H0m;S7S+=C5C;S7S+=V8x;var T60=E3o;T60+=x6u;T60+=c3t;T60+=E3o;var label=this[I_d][T60];var labelInfo=this[I_d][T_k][d4Q]();if(str === undefined){return label[y64]();}label[y64](str);label[S7S](labelInfo);return this;};Field[T1P][f9S]=function(msg){var H1f=J_0;H1f+=x4g;H1f+=F6G;L7K.b9t();return this[d6Q](this[H1f][T_k],msg);};Field[h_7][G91]=function(msg,fn){var y_a="fieldMessage";var T35=J_0;T35+=x4g;T35+=F6G;var H8g=a4a;H8g+=F6G;H8g+=J7D;H8g+=c3v;return this[H8g](this[T35][y_a],msg,fn);};Field[H9V][I6l]=function(id){var value;var multiValues=this[J7D][W2B];var multiIds=this[J7D][h15];var isMultiValue=this[Y7L]();if(id === undefined){var fieldVal=this[m1I]();value={};for(var _i=M1q,multiIds_1=multiIds;_i < multiIds_1[m2_];_i++){var multiId=multiIds_1[_i];value[multiId]=isMultiValue?multiValues[multiId]:fieldVal;}}else if(isMultiValue){value=multiValues[id];}else {value=this[m1I]();}return value;};Field[N0G][E2Q]=function(){var q5B=F6G;q5B+=A0M;L7K.b9t();q5B+=v2$;this[J7D][q5B]=T5f;this[w_n]();};Field[N0G][K0k]=function(id,val,recalc){var v0S="iId";var f0j="multiV";var X93="alueChe";var g_H=Z1n;g_H+=v0S;g_H+=J7D;if(recalc === void M1q){recalc=T5f;}var that=this;var multiValues=this[J7D][W2B];var multiIds=this[J7D][g_H];if(val === undefined){val=id;id=undefined;}var set=function(idSrc,valIn){var z0U="inAr";var I7w=a4a;I7w+=l7$;I7w+=x4g;I7w+=u3Z;var K6R=z0U;K6R+=Z0l;if($[K6R](idSrc,multiIds) === -p88){var Y2N=m59;Y2N+=J3O;Y2N+=J7D;Y2N+=T03;multiIds[Y2N](idSrc);}multiValues[idSrc]=that[I7w](valIn,that[J7D][p1E][B0R]);};if($[v5K](val) && id === undefined){$[o8Y](val,function(idSrc,innerVal){L7K.b9t();set(idSrc,innerVal);});}else if(id === undefined){var u_B=m9z;u_B+=x6u;u_B+=G1V;u_B+=T03;$[u_B](multiIds,function(i,idSrc){L7K.l4n();set(idSrc,val);});}else {set(id,val);}this[J7D][P_Z]=T5f;if(recalc){var O1e=a4a;O1e+=f0j;O1e+=X93;O1e+=c1J;this[O1e]();}return this;};Field[k8Z][z$n]=function(){L7K.l4n();var M7u=B3P;M7u+=x6u;M7u+=F6G;M7u+=m9z;return this[J7D][p1E][M7u];};Field[N0G][S_L]=function(){var L89=k82;L89+=F6G;L7K.l4n();return this[L89][z3e][M1q];};Field[h5O][m3c]=function(){var H16="nullDe";var q1K=H16;q1K+=G7B;L7K.l4n();return this[J7D][p1E][q1K];};L7K.l4n();Field[C8$][P_T]=function(set){var z84="-field";var O9i="lEve";var A3i="erna";var b_Y="int";var t2g=x0n;t2g+=X9m;t2g+=X7l;t2g+=z84;var L$9=b_Y;L$9+=A3i;L$9+=O9i;L$9+=k1z;var L3p=T03;L3p+=x4g;L3p+=m7i;var l88=B3P;l88+=g5T;l88+=m9z;var j7F=u3Y;j7F+=Y4R;j7F+=G1V;j7F+=I9U;var y_O=G1V;y_O+=J7D;y_O+=J7D;var U2s=J7h;U2s+=f63;U2s+=Z1a;var h$t=J_0;h$t+=x4g;h$t+=F6G;if(set === undefined){var j3q=r_r;j3q+=Z1a;return this[J7D][j3q];}this[h$t][U2s][y_O](k6I,set?j7F:l88);this[J7D][D$8]=set;this[J7D][L3p][L$9](t2g,[set]);L7K.b9t();return this;};Field[N0G][L6_]=function(val,multiCheck){var E69="entityDecod";var i$b='set';var C57="peF";var N6i=E69;N6i+=m9z;var v$0=P7P;v$0+=U15;if(multiCheck === void M1q){multiCheck=T5f;}var decodeFn=function(d){var P5n='£';var e4N='\'';var z2g='"';var I3c="plac";L7K.b9t();var Q7T='\n';var d_X=M5g;d_X+=K0V;d_X+=d8o;d_X+=m9z;var n07=C5B;n07+=I3c;n07+=m9z;return typeof d !== f2Z?d:d[n07](/&gt;/g,j7f)[o1S](/&lt;/g,u8o)[o1S](/&amp;/g,a8A)[o1S](/&quot;/g,z2g)[o1S](/&#163;/g,P5n)[o1S](/&#39;/g,e4N)[d_X](/&#10;/g,Q7T);};this[J7D][P_Z]=g49;var decode=this[J7D][v$0][N6i];if(decode === undefined || decode === T5f){if(Array[p5M](val)){var I7a=E3o;I7a+=E20;for(var i=M1q,ien=val[I7a];i < ien;i++){val[i]=decodeFn(val[i]);}}else {val=decodeFn(val);}}if(multiCheck === T5f){var S6O=A0q;S6O+=C57;S6O+=B3P;val=this[v4e](val,this[J7D][p1E][B0R]);this[S6O](i$b,val);this[w_n]();}else {this[Y3o](i$b,val);}return this;};Field[m9B][L0c]=function(animate,toggle){L7K.b9t();var e7u="slideDown";var c2Y="play";var R2n=l7$;R2n+=B3P;var D8T=Q0w;D8T+=c2Y;var G10=T03;G10+=x4g;G10+=m7i;var S6Y=J_0;S6Y+=x4g;S6Y+=F6G;if(animate === void M1q){animate=T5f;}if(toggle === void M1q){toggle=T5f;}if(toggle === g49){return this[m0K](animate);}var el=this[S6Y][z3e];if(this[J7D][G10][D8T]() && animate && $[R2n][e7u]){el[e7u]();}else {el[e$Y](k6I,l2Y);;}return this;};Field[A0C][q0a]=function(options,append){var h6a='update';var Q4W=F$o;Q4W+=Y1b;if(append === void M1q){append=g49;}if(this[J7D][Q4W][d1x]){this[Y3o](h6a,options,append);}return this;};Field[N0G][m1I]=function(val){return val === undefined?this[u2A]():this[L6_](val);};Field[N0G][U5G]=function(value,original){var n8M="compare";var compare=this[J7D][p1E][n8M] || deepCompare;return compare(value,original);};Field[N0G][X0z]=function(){var D2J=w55;D2J+=J7D;L7K.l4n();return this[J7D][D2J][z4k];};Field[c7q][U$p]=function(){var e83="oy";var C9F=V6H;C9F+=J7D;C9F+=Z9H;C9F+=e83;var q$d=M5g;q$d+=H4b;q$d+=Y9F;var e24=b00;e24+=k1z;e24+=R8S;e24+=M5g;var m63=k82;m63+=F6G;this[m63][e24][q$d]();this[Y3o](C9F);return this;};Field[N0G][J5s]=function(){var L43=x4g;L43+=f6m;L7K.l4n();return this[J7D][L43][J5s];};Field[N0G][h15]=function(){return this[J7D][h15];};Field[L0o][Z6k]=function(show){var i0N=B3P;L7K.l4n();i0N+=d5q;var s52=u3Y;s52+=S4K;var Z51=G1V;Z51+=J7D;Z51+=J7D;var F5l=J_0;F5l+=x4g;F5l+=F6G;this[F5l][b17][Z51]({display:show?s52:i0N});};Field[f3n][W74]=function(){var U6F="iIds";var d9b=j4l;d9b+=F$o;d9b+=U6F;this[J7D][d9b]=[];this[J7D][W2B]={};};Field[G2L][i$X]=function(){var d6r=J7D;d6r+=G4F;var v2D=x4g;v2D+=f6m;return this[J7D][v2D][d6r];};Field[J2k][d6Q]=function(el,msg,fn){var D$i=":";var p20="func";var p9Q="tm";var Z$b="ettings";var r7d="ideDown";var C5y="visi";var q9E="slid";var q4t="ernalS";var r6E=D$i;r6E+=C5y;r6E+=k_N;var B62=t_M;B62+=J7D;var b52=B4n;b52+=M5g;b52+=m9z;b52+=k1z;var f$6=p20;f$6+=F$o;f$6+=I6K;if(msg === undefined){var e4k=T03;e4k+=F$o;e4k+=F6G;e4k+=E3o;return el[e4k]();}if(typeof msg === f$6){var X21=f$8;X21+=u3Y;X21+=E3o;X21+=m9z;var b3A=t_M;b3A+=k1z;b3A+=q4t;b3A+=Z$b;var N5N=T03;N5N+=x4g;N5N+=J7D;N5N+=F$o;var editor=this[J7D][N5N];msg=msg(editor,new DataTable$1[b_J](editor[b3A]()[X21]));}if(el[b52]()[B62](r6E) && $[w9$][O6e]){var c_j=T03;c_j+=p9Q;c_j+=E3o;el[c_j](msg);if(msg){var F1T=f5h;F1T+=r7d;el[F1T](fn);;}else {var H3R=q9E;H3R+=f5c;el[H3R](fn);}}else {var d8l=B3P;d8l+=x4g;d8l+=B3P;d8l+=m9z;var l7W=u3Y;l7W+=S4K;var I0f=L0p;I0f+=J7D;I0f+=T9z;I0f+=Y6A;var t7B=T03;t7B+=F$o;t7B+=F6G;t7B+=E3o;el[t7B](msg || l2Y)[e$Y](I0f,msg?l7W:d8l);if(fn){fn();}}return this;};Field[N0G][S5M]=function(){var U_O="multiNoEdit";var o_h="iRetu";var I6E="inputControl";var h7H="multiEdi";var m56="ock";var S$H="internalI1";var K_j="toggleClass";var A4M="inputCo";var D1o="internalMultiInfo";var h8l="ntrol";var v4x="rn";var v6K="ltiValues";var D2y="non";var O5P="noMulti";var f22=w3s;L7K.b9t();f22+=x6u;f22+=S8c;var b_i=c5m;b_i+=I3i;b_i+=t_M;var H6o=t_M;H6o+=I$v;var J9f=h7d;J9f+=F6G;J9f+=E3o;var u2o=J_0;u2o+=x4g;u2o+=F6G;var L0G=c5m;L0G+=E3o;L0G+=F$o;L0G+=t_M;var K$o=S$H;K$o+=k4E;var I3O=B3P;I3O+=x4g;I3O+=B3P;I3O+=m9z;var Q1u=b3X;Q1u+=T03;var K6j=G1V;K6j+=J7D;K6j+=J7D;var b1_=c5m;b1_+=I3i;b1_+=o_h;b1_+=v4x;var E8_=h7H;E8_+=O81;var s0L=c5m;s0L+=v6K;var last;var ids=this[J7D][h15];var values=this[J7D][s0L];var isMultiValue=this[J7D][P_Z];var isMultiEditable=this[J7D][p1E][E8_];var val;var different=g49;if(ids){var t3F=H_N;t3F+=F0A;for(var i=M1q;i < ids[t3F];i++){val=values[ids[i]];if(i > M1q && !deepCompare(val,last)){different=T5f;break;}last=val;}}if(different && isMultiValue || !isMultiEditable && this[Y7L]()){var v93=T_U;v93+=m56;var H17=F6G;H17+=J3O;H17+=I3i;H17+=t_M;var O07=k82;O07+=F6G;var O85=D2y;O85+=m9z;var h$d=G1V;h$d+=J7D;h$d+=J7D;var x$x=A4M;x$x+=h8l;var Q66=k82;Q66+=F6G;this[Q66][x$x][h$d]({display:O85});this[O07][H17][e$Y]({display:v93});}else {var f1C=J_0;f1C+=x4g;f1C+=F6G;this[I_d][I6E][e$Y]({display:K9F});this[f1C][x2M][e$Y]({display:N7C});if(isMultiValue && !different){this[L6_](last,g49);}}this[I_d][b1_][K6j]({display:ids && ids[Q1u] > p88 && different && !isMultiValue?K9F:I3O});var i18n=this[J7D][l_l][K$o]()[L0G];this[u2o][b17][J9f](isMultiEditable?i18n[H6o]:i18n[O5P]);this[I_d][b_i][K_j](this[J7D][f22][U_O],!isMultiEditable);this[J7D][l_l][D1o]();return T5f;};Field[N5$][r1K]=function(name){var t93="nshif";var C3Q=F$o;C3Q+=J2a;C3Q+=m9z;var d5b=J3O;d5b+=t93;d5b+=F$o;var D$V=y3A;L7K.b9t();D$V+=L0x;D$V+=F$o;D$V+=T03;var args=[];for(var _i=p88;_i < arguments[D$V];_i++){args[_i - p88]=arguments[_i];}args[d5b](this[J7D][p1E]);var fn=this[J7D][C3Q][name];if(fn){return fn[m03](this[J7D][l_l],args);}};Field[N0G][S2E]=function(){var a2b="ieldE";L7K.b9t();var W8c=l7$;W8c+=a2b;W8c+=q$8;W8c+=M5g;var T9N=J_0;T9N+=x4g;T9N+=F6G;return this[T9N][W8c];};Field[k$m][t8p]=function(val,formatter){var R7Q="matters";var b7u="ply";var W_c="shift";if(formatter){var p4n=U04;p4n+=m7i;var v8m=J94;v8m+=A33;if(Array[v8m](formatter)){var k14=x6u;k14+=m59;k14+=b7u;var I6$=l7$;I6$+=h3Y;I6$+=R7Q;var R8D=J7D;R8D+=E3o;R8D+=t_M;R8D+=H0X;var args=formatter[R8D]();var name_1=args[W_c]();formatter=Field[I6$][name_1][k14](this,args);}return formatter[H7v](this[J7D][p4n],val,this);}return val;};Field[V72]=defaults;Field[a_7]={};return Field;})();var button={action:X2m,className:X2m,tabIndex:M1q,text:X2m};var displayController={close:function(){},init:function(){},node:function(){},open:function(){}};var DataTable=$[Z2b][q1t];var apiRegister=DataTable[U7N][w3A];function _getInst(api){var z8p="context";var W7U="oInit";var ctx=api[z8p][M1q];return ctx[W7U][n3n] || ctx[I$0];}function _setBasic(inst,opts,type,plural){var o9L="mess";var v3e="age";var f02="tl";L7K.b9t();var s3E=/%d/;var Y53="sag";var X4t=A_J;X4t+=J7D;X4t+=Y53;X4t+=m9z;if(!opts){opts={};}if(opts[F8y] === undefined){var v6D=u3Y;v6D+=h7$;opts[v6D]=h4t;}if(opts[q_V] === undefined){var G3A=B91;G3A+=f02;G3A+=m9z;opts[q_V]=inst[B1B][type][G3A];}if(opts[X4t] === undefined){if(type === K5K){var z1c=M5g;z1c+=m9z;z1c+=U6c;z1c+=H0X;var R4f=o9L;R4f+=v3e;var y76=b00;y76+=B3P;y76+=l7$;y76+=I$g;var confirm_1=inst[B1B][type][y76];opts[R4f]=plural !== p88?confirm_1[a4a][z1c](s3E,plural):confirm_1[f$W];}else {opts[G91]=l2Y;}}return opts;}apiRegister(v2w,function(){L7K.l4n();return _getInst(this);});apiRegister(P8r,function(opts){var D9P=e$3;D9P+=A3U;var inst=_getInst(this);inst[D9P](_setBasic(inst,opts,R4H));return this;});apiRegister(C85,function(opts){var o_m=m9z;o_m+=J_0;o_m+=t_M;o_m+=F$o;var R8k=P3c;R8k+=F$o;var inst=_getInst(this);inst[R8k](this[M1q][M1q],_setBasic(inst,opts,o_m));return this;});apiRegister(N$f,function(opts){var C4R=m9z;C4R+=J_0;C4R+=t_M;C4R+=F$o;var inst=_getInst(this);inst[F33](this[M1q],_setBasic(inst,opts,C4R));return this;});apiRegister(z7f,function(opts){var A15=M5g;A15+=m9z;A15+=f0k;A15+=m9z;var inst=_getInst(this);inst[A15](this[M1q][M1q],_setBasic(inst,opts,K5K,p88));return this;});apiRegister(y62,function(opts){var Y0n=b3X;Y0n+=T03;var a40=C5B;a40+=m0I;var inst=_getInst(this);inst[H21](this[M1q],_setBasic(inst,opts,a40,this[M1q][Y0n]));return this;});apiRegister(v5s,function(type,opts){if(!type){type=d9y;}else if($[v5K](type)){opts=type;type=d9y;}_getInst(this)[type](this[M1q][M1q],opts);return this;});apiRegister(a6o,function(opts){_getInst(this)[C$3](this[M1q],opts);return this;});apiRegister(N0T,file);apiRegister(n7U,files);$(document)[X4V](A6P,function(e,ctx,json){var b5D="pac";var n3Z=J_0;n3Z+=F$o;var z4F=z$n;z4F+=J7D;z4F+=b5D;z4F+=m9z;if(e[z4F] !== n3Z){return;}if(json && json[Y$q]){var L0j=j7l;L0j+=J7D;var x6K=m9z;x6K+=x6u;x6K+=G1V;x6K+=T03;$[x6K](json[L0j],function(name,filesIn){var Q_$="les";var U8s=l7$;U8s+=t_M;U8s+=E3o;U8s+=C_A;if(!Editor[U8s][name]){var o42=t0$;o42+=Q_$;Editor[o42][name]={};}$[t8A](Editor[Y$q][name],filesIn);});}});var _buttons=$[l02][e_z][V8g][F8y];$[t8A](_buttons,{create:{action:function(e,dt,node,config){var Q2l="formB";var V3N="preO";var c4L="rmTitl";var W$Q=z7S;W$Q+=m9z;var T5P=t_M;T5P+=k_j;T5P+=l79;T5P+=B3P;var R8L=C4i;R8L+=c4L;R8L+=m9z;var h5F=G1V;h5F+=C5B;h5F+=x3a;h5F+=m9z;var E4P=Q2l;E4P+=v9Y;E4P+=I$y;var c9L=i4l;c9L+=F$o;c9L+=m9z;var E8D=V3N;E8D+=C5C;E8D+=B3P;var K4e=J7h;K4e+=e8D;var that=this;var editor=config[n3n];this[K4e](T5f);editor[d5q](E8D,function(){that[D$8](g49);})[c9L]($[t8A]({buttons:config[E4P],message:config[I1e] || editor[B1B][h5F][G91],nest:T5f,title:config[R8L] || editor[T5P][W$Q][q_V]},config[V_u]));},className:m14,editor:X2m,formButtons:{action:function(e){L7K.l4n();var a5s=k4I;a5s+=F6G;a5s+=t_M;a5s+=F$o;this[a5s]();},text:function(editor){var r8r=k4I;r8r+=F6G;r8r+=H7H;var D0p=e$3;D0p+=A3U;return editor[B1B][D0p][r8r];}},formMessage:X2m,formOptions:{},formTitle:X2m,text:function(dt,node,config){var K_T='buttons.create';var j_C=e$3;L7K.b9t();j_C+=m9z;j_C+=Z29;return dt[B1B](K_T,config[n3n][B1B][j_C][T$I]);}},createInline:{action:function(e,dt,node,config){var n7d="ositi";var W$C="eCre";var r$0=m59;r$0+=n7d;r$0+=x4g;r$0+=B3P;var k5j=R7u;k5j+=I8x;k5j+=W$C;L7K.b9t();k5j+=Z29;var q6y=h6w;q6y+=W39;config[q6y][k5j](config[r$0],config[V_u]);},className:m14,editor:X2m,formButtons:{action:function(e){this[f4c]();},text:function(editor){var v_M=J7D;v_M+=H6G;v_M+=F$o;L7K.b9t();var b8o=i4l;b8o+=F$o;b8o+=m9z;return editor[B1B][b8o][v_M];}},formOptions:{},position:t84,text:function(dt,node,config){L7K.b9t();var W6_="uttons.create";var g$E="i18";var C3m=G1V;C3m+=M5g;C3m+=A3U;var d8U=g$E;d8U+=B3P;var l7S=u3Y;l7S+=W6_;return dt[B1B](l7S,config[n3n][d8U][C3m][T$I]);}},edit:{action:function(e,dt,node,config){var c6s="xes";var s$w="formTit";var S_R="oces";var E0z=h6w;E0z+=H7H;var z8O=t_M;z8O+=k7$;z8O+=B3P;var p9i=s$w;p9i+=y3A;var m2f=F6G;m2f+=m9z;m2f+=R70;m2f+=m9z;var L$W=P3c;L$W+=F$o;var L4R=P3c;L4R+=F$o;var w3i=m59;w3i+=M5g;w3i+=S_R;w3i+=X7l;var L_D=y3A;L_D+=e0d;L_D+=T03;var u5h=E3o;u5h+=l9T;u5h+=c3v;u5h+=F0A;var V1O=f3d;V1O+=c6s;var that=this;var editor=config[n3n];var rows=dt[o1V]({selected:T5f})[V1O]();var columns=dt[e65]({selected:T5f})[i0u]();L7K.l4n();var cells=dt[q3W]({selected:T5f})[i0u]();var items=columns[u5h] || cells[L_D]?{cells:cells,columns:columns,rows:rows}:rows;this[w3i](T5f);editor[d5q](k_l,function(){that[D$8](g49);})[L4R](items,$[t8A]({buttons:config[V_6],message:config[I1e] || editor[B1B][L$W][m2f],nest:T5f,title:config[p9i] || editor[z8O][E0z][q_V]},config[V_u]));},className:R$3,editor:X2m,extend:Y_j,formButtons:{action:function(e){var s1j=S7m;s1j+=j9Y;s1j+=H7H;this[s1j]();},text:function(editor){var D5u=k4I;L7K.l4n();D5u+=z1s;return editor[B1B][F33][D5u];}},formMessage:X2m,formOptions:{},formTitle:X2m,text:function(dt,node,config){var y3U='buttons.edit';var g7u=m9z;L7K.l4n();g7u+=J_0;g7u+=t_M;g7u+=F$o;var P6X=F33;P6X+=h3Y;return dt[B1B](y3U,config[P6X][B1B][g7u][T$I]);}},remove:{action:function(e,dt,node,config){var f8j="processi";var I3D="ito";var C18="formMes";var O0i="Ti";var X3M="ormOptio";var J19=l7$;J19+=X3M;J19+=B3P;J19+=J7D;var n3o=F$o;n3o+=t_M;n3o+=F$o;n3o+=y3A;var O5w=M5g;O5w+=m9z;O5w+=f0k;O5w+=m9z;var P8h=t_M;P8h+=k_j;P8h+=l79;P8h+=B3P;var W8L=C1v;W8L+=O0i;W8L+=F$o;W8L+=y3A;var c1$=C18;c1$+=A$X;var L1U=f8j;L1U+=B3P;L1U+=c3v;var p92=m9z;p92+=J_0;L7K.l4n();p92+=I3D;p92+=M5g;var that=this;var editor=config[p92];this[L1U](T5f);editor[d5q](k_l,function(){that[D$8](g49);})[H21](dt[o1V]({selected:T5f})[i0u](),$[t8A]({buttons:config[V_6],message:config[c1$],nest:T5f,title:config[W8L] || editor[P8h][O5w][n3o]},config[J19]));},className:E9f,editor:X2m,extend:s44,formButtons:{action:function(e){var n2C=J7D;n2C+=H6G;n2C+=F$o;this[n2C]();},text:function(editor){L7K.l4n();return editor[B1B][H21][f4c];}},formMessage:function(editor,dt){var D$5="nf";var O_y="confirm";var D_j=E3o;D_j+=E20;var Z0C=E3o;Z0C+=z0E;Z0C+=F0A;var b_F=G1V;b_F+=x4g;b_F+=D$5;b_F+=I$g;var l3g=J7D;l3g+=Z9H;l3g+=t_M;l3g+=L0x;var B6u=Z2U;B6u+=l7$;B6u+=U2L;B6u+=F6G;var Y8z=m2K;Y8z+=h13;Y8z+=m9z;var N1v=t_M;N1v+=k_j;N1v+=l79;N1v+=B3P;var x5b=f3d;x5b+=h07;x5b+=C_A;var rows=dt[o1V]({selected:T5f})[x5b]();var i18n=editor[N1v][Y8z];var question=typeof i18n[B6u] === l3g?i18n[O_y]:i18n[O_y][rows[m2_]]?i18n[b_F][rows[Z0C]]:i18n[O_y][a4a];return question[o1S](/%d/g,rows[D_j]);},formOptions:{},formTitle:X2m,limitTo:[n1O],text:function(dt,node,config){var S0Q="buttons.rem";var S7_=t_M;S7_+=k_j;S7_+=l79;S7_+=B3P;var C42=S0Q;C42+=w9O;C42+=m9z;return dt[B1B](C42,config[n3n][S7_][H21][T$I]);}}});_buttons[r$L]=$[d6k]({},_buttons[d4g]);_buttons[r9m][t8A]=X4x;_buttons[c9A]=$[e3T]({},_buttons[C1K]);_buttons[p3q][t8A]=r0J;var Editor=(function(){var i05="Mu";var q$4="Dat";var I7v="internalSettings";var a$2="ltiInf";var N3I="eTime";var o1L="ersion";var x$y="internalEvent";var g85="internal";var D3c="els";var q4x='2.0.8';var L_e=p6g;L_e+=B$4;L_e+=v12;L_e+=J_0;var l$S=F6G;l$S+=x4g;l$S+=J_0;l$S+=D3c;var x52=J3O;x52+=m59;x52+=Y5w;var Y7v=q$4;Y7v+=N3I;var P9G=K_l;P9G+=g$f;var S9I=h13;S9I+=o1L;var g33=t0$;g33+=y3A;g33+=J7D;var C2U=t0$;C2U+=I$D;var b_E=g85;b_E+=i05;b_E+=a$2;b_E+=x4g;var A9s=z79;A9s+=J2a;A9s+=m9z;function Editor(init){var t_$="cessing";var R5p="\" cla";var e8b="ata-dte";var A6c="_w";var V2C='<div data-dte-e="body_content" class="';var o8Q="ormOp";var A4z="ubmitErr";var P9H="template";var N8t="</f";var D$s="_su";var V1t="nTable";var G8W="_closeR";var G_Z="t.dt";var D0O="ear";var i0C="ta-dte-e=\"";var L0a="body\" class=\"";var F3R="dt.d";var P8e="ade";var d3l='"><span></span></div>';var B0J='DataTables Editor must be initialised as a \'new\' instance';var d9l="_ajax";var B5u="edCl";var N_u="iv da";var Q_L="ontroller";var a5o="></di";var O$a="tting";var M27='<div data-dte-e="form_info" class="';var K4T="_displa";var C2P="orm";var t88="/di";var U0c="nit.d";var u7l="ldNam";var f9V="n.dt.dte";var O4c="yRe";var M0_="ton";var e7F='foot';var o4z="body";var J5n="ndent";var g9$="domTable";var r_G="model";var n47="eg";var c3z="endent";var U4c="ditor";var y$u="_actionCl";var d$F="xhr.";var l6Z="display controller";var o2C='<div data-dte-e="form_error" class="';var M$n="ont";var G9X='body_content';var C_Q="conte";var g2g="actionName";var v1v="wrapp";var I9B="\"></div></div";var a6p="_mes";var J0Y="inlineCreate";var E_o="><div class=\"";var A0d="eakIn";var f9b="ackgr";var p2L="playN";var T4e="form_con";var E5O="ayC";var n6R="<div d";var V6P="ini";var V7r="nestedOpen";var G1B="annot find ";var t_w='<div data-dte-e="foot" class="';var E$c="ag";var w4q='<div data-dte-e="processing" class="';var x1V="_post";var o54="abl";var Y65="<div data-dte";var C8P='<div data-dte-e="form_content" class="';var t18="-e=\"form_buttons\" class=\"";var w4b="_blur";var f93="Ta";var x6D="_clea";var W6l="dep";var s9w="nique";var C0$="ocessi";var P0X="niq";var y0u="ids";var l39="<form data-dte-e=\"f";var g_K="_optionsUpdate";var C7P="-e=\"head\" clas";var O86="rDynamicInfo";var W5A="indicator";var M5s='initComplete';var U8v="setting";var C$x="iIn";var A3Z="_inline";var Y4i="etach";var Y6l=V6P;Y6l+=F$o;Y6l+=N5w;Y6l+=U4c;var d$0=a4a;d$0+=I$5;d$0+=F$o;var g$l=V6P;g$l+=F$o;var R7s=L0p;R7s+=D47;R7s+=P99;var n4F=J_0;n4F+=X0T;n4F+=E5O;n4F+=Q_L;var Q6f=J3O;Q6f+=P0X;Q6f+=J3O;Q6f+=m9z;var F2s=d$F;F2s+=F3R;F2s+=W1Z;var d21=t_M;d21+=k7$;d21+=f9V;var M0h=J3O;M0h+=s9w;var s5G=t_M;s5G+=U0c;s5G+=G_Z;s5G+=m9z;var G8c=F$o;G8c+=x6u;G8c+=T_U;G8c+=m9z;var y4h=J_0;y4h+=x4g;y4h+=F6G;var n8F=m9z;n8F+=O8F;n8F+=U15;var O3z=m9z;O3z+=Y8o;var M_X=D6L;M_X+=C0$;M_X+=L0x;var u9d=I9B;u9d+=m6_;var D3s=U50;D3s+=E_o;var E6X=c0K;E6X+=P8e;E6X+=M5g;var M_p=Y65;M_p+=C7P;M_p+=p_t;var a2G=U50;a2G+=a5o;a2G+=h13;a2G+=m6_;var U5k=t_M;U5k+=I$v;var K$N=l7$;K$N+=x4g;K$N+=U5C;var z0A=F8G;z0A+=t88;z0A+=a$t;var H9h=l7$;H9h+=x4g;H9h+=M5g;H9h+=F6G;var k3c=T4e;k3c+=W1Z;k3c+=k1z;var v4p=l7$;v4p+=h3Y;v4p+=F6G;var q2H=n6R;q2H+=e8b;q2H+=t18;var h1J=u3Y;h1J+=x4g;h1J+=J_0;h1J+=A33;var w4z=J_0;w4z+=x4g;w4z+=F6G;var l_i=N8t;l_i+=C2P;l_i+=m6_;var A1O=C_Q;A1O+=B3P;A1O+=F$o;var V8M=U50;V8M+=m6_;var u$z=F$o;u$z+=x6u;u$z+=c3v;var l4R=l39;l4R+=C2P;l4R+=R5p;l4R+=v8g;var m85=e1t;m85+=L0p;m85+=a$t;var M$3=r$P;M$3+=t_M;M$3+=a$t;var v03=G1V;v03+=M$n;v03+=k38;var e_W=c_1;e_W+=N_u;e_W+=i0C;e_W+=L0a;var j57=D6L;j57+=x4g;j57+=t_$;var j3f=v1v;j3f+=K2t;var B0H=w3s;B0H+=l6s;B0H+=J86;B0H+=J7D;var u$s=J86;u$s+=O$a;u$s+=J7D;var r8T=f6f;r8T+=J_0;r8T+=D3c;var x74=M1L;x74+=l79;x74+=B3P;var M8_=t_M;M8_+=k_j;M8_+=l79;M8_+=B3P;var d5X=m9z;d5X+=E$y;var S6S=J_0;S6S+=Y4i;var v6N=Z69;v6N+=H_U;var r8y=l7$;r8y+=o8Q;r8y+=y2Y;var a7a=U8v;a7a+=J7D;var w98=r_G;w98+=J7D;var U1E=m9z;U1E+=h07;U1E+=W1Z;U1E+=V8x;var r2i=p6S;r2i+=c1p;var q7H=A6c;q7H+=A0d;q7H+=y3C;q7H+=Y6A;var K$C=a4a;K$C+=J7D;K$C+=A4z;K$C+=h3Y;var D7z=a4a;D7z+=f4c;D7z+=f93;D7z+=k_N;var p6K=D$s;p6K+=u3Y;p6K+=X57;p6K+=F$o;var D_q=x1V;D_q+=V_S;var P8y=a4a;P8y+=V7r;var h_o=s8Z;h_o+=m7i;h_o+=B5u;h_o+=k4i;var C34=N2b;C34+=K60;C34+=C$x;C34+=C4i;var R$v=a6p;R$v+=J7D;R$v+=E$c;R$v+=m9z;var v5D=f1t;v5D+=u7l;v5D+=C_A;var I7k=K4T;I7k+=O4c;I7k+=E6E;I7k+=K2t;var l6F=G8W;l6F+=n47;var C1M=h4F;C1M+=Y4R;C1M+=J86;var I3E=x6D;I3E+=O86;var G80=y$u;G80+=h4Q;var M8A=Y_Y;M8A+=E3o;var u8M=L4i;u8M+=E3o;u8M+=m9z;var C4v=J7D;C4v+=U04;C4v+=R3d;var M$h=h3Y;M$h+=J_0;M$h+=m9z;M$h+=M5g;var l0t=x4g;l0t+=B3P;var r_c=c8Q;r_c+=l7$;var T$t=h66;T$t+=B3P;T$t+=m9z;var m0E=S4_;m0E+=K23;var f_7=m9z;f_7+=b$D;var a$Z=J_0;a$Z+=P1D;a$Z+=p2L;a$Z+=g6C;var z9r=J_0;z9r+=P1D;z9r+=o54;z9r+=m9z;var o4g=W6l;o4g+=c3z;var K7N=i3I;K7N+=K0V;K7N+=m9z;K7N+=J5n;var Q1l=z7S;Q1l+=m9z;var c8e=G1V;c8e+=E3o;c8e+=D0O;var t2T=x8u;t2T+=M0_;t2T+=J7D;var z_n=u3Y;z_n+=E3o;z_n+=J3O;z_n+=M5g;var G45=u3Y;G45+=f9b;G45+=z9D;var _this=this;this[x6v]=add;this[y58]=ajax;this[G45]=background;this[z_n]=blur;this[C$3]=bubble;this[u5k]=bubblePosition;this[t2T]=buttons;this[c8e]=clear;this[H5f]=close;this[Q1l]=create;this[K7N]=undependent;this[o4g]=dependent;this[O7A]=destroy;this[z9r]=disable;this[s$8]=display;this[k42]=displayed;this[a$Z]=displayNode;this[f_7]=edit;this[g3z]=enable;this[x_w]=error$1;this[Y8Y]=field;this[m0E]=fields;this[j7l]=file;this[Y$q]=files;this[u2A]=get;this[m0K]=hide;this[y0u]=ids;this[n4o]=inError;this[T$t]=inline;this[J0Y]=inlineCreate;this[G91]=message;this[d1k]=mode;this[E_V]=modifier;this[I6l]=multiGet;this[T8j]=multiSet;this[S_L]=node;this[r_c]=off;this[l0t]=on;this[d5q]=one;this[V_S]=open;this[M$h]=order;this[H21]=remove;this[L6_]=set;this[C4v]=show;this[f4c]=submit;this[O81]=table;this[P9H]=template;this[u8M]=title;this[M8A]=val;this[G80]=_actionClass;this[d9l]=_ajax;this[R3b]=_animate;this[q0i]=_assembleMain;this[w4b]=_blur;this[I3E]=_clearDynamicInfo;this[C1M]=_close;this[l6F]=_closeReg;this[Y8J]=_crudArgs;this[W_A]=_dataSource;this[I7k]=_displayReorder;this[k_9]=_edit;this[m6A]=_event;this[F7o]=_eventName;this[Y22]=_fieldFromNode;this[v5D]=_fieldNames;this[K8C]=_focus;this[L6e]=_formOptions;this[A3Z]=_inline;this[g_K]=_optionsUpdate;this[R$v]=_message;this[C34]=_multiInfo;this[h_o]=_nestedClose;this[P8y]=_nestedOpen;this[D_q]=_postopen;this[X5j]=_preopen;this[I_z]=_processing;this[b5E]=_noProcessing;this[p6K]=_submit;this[D7z]=_submitTable;this[o8R]=_submitSuccess;this[K$C]=_submitError;this[J5W]=_tidy;this[q7H]=_weakInArray;if(!(this instanceof Editor)){alert(B0J);}init=$[r2i](T5f,{},Editor[V72],init);this[J7D]=$[U1E](T5f,{},Editor[w98][a7a],{actionName:init[g2g],ajax:init[y58],formOptions:init[r8y],idSrc:init[v6N],table:init[g9$] || init[O81],template:init[P9H]?$(init[P9H])[S6S]():X2m});this[I58]=$[d5X](T5f,{},Editor[I58]);this[M8_]=init[x74];Editor[r8T][u$s][Z9Z]++;var that=this;var classes=this[B0H];var wrapper=$(e2R + classes[j3f] + K8R + w4q + classes[j57][W5A] + d3l + e_W + classes[o4z][A8Y] + K8R + V2C + classes[o4z][v03] + o2c + r65 + t_w + classes[j8X][A8Y] + K8R + e2R + classes[j8X][t4U] + o2c + M$3 + m85);var form=$(l4R + classes[C1v][u$z] + V8M + C8P + classes[C1v][A1O] + o2c + l_i);this[w4z]={body:el(h1J,wrapper)[M1q],bodyContent:el(G9X,wrapper)[M1q],buttons:$(q2H + classes[v4p][F8y] + o2c)[M1q],footer:el(e7F,wrapper)[M1q],form:form[M1q],formContent:el(k3c,form)[M1q],formError:$(o2C + classes[H9h][x_w] + z0A)[M1q],formInfo:$(M27 + classes[K$N][U5k] + a2G)[M1q],header:$(M_p + classes[E6X][A8Y] + D3s + classes[x_C][t4U] + u9d)[M1q],processing:el(M_X,wrapper)[M1q],wrapper:wrapper[M1q]};$[O3z](init[n8F],function(evt,fn){var a2_=x4g;a2_+=B3P;that[a2_](evt,function(){var p9w=y3A;p9w+=B3P;p9w+=n3b;p9w+=T03;var argsIn=[];for(var _i=M1q;_i < arguments[p9w];_i++){argsIn[_i]=arguments[_i];}fn[m03](that,argsIn);});});this[y4h];var table$1=this[J7D][G8c];if(init[c26]){this[x6v](init[c26]);}$(document)[g5T](s5G + this[J7D][M0h],function(e,settings,json){var H9G=B3P;H9G+=e$Q;H9G+=r8u;H9G+=y3A;if(_this[J7D][O81] && settings[H9G] === $(table$1)[M1q]){settings[I$0]=_this;}})[g5T](d21 + this[J7D][Z9Z],function(e,settings){var n1Z="oLanguage";var k2P=f$8;k2P+=u3Y;k2P+=y3A;if(_this[J7D][k2P] && settings[V1t] === $(table$1)[M1q]){if(settings[n1Z][n3n]){var O1x=J4v;O1x+=M5g;$[t8A](T5f,_this[B1B],settings[n1Z][O1x]);}}})[g5T](F2s + this[J7D][Q6f],function(e,settings,json){var V$w="pdate";var b4T="_opti";var b8N=f$8;b8N+=u3Y;b8N+=E3o;b8N+=m9z;if(json && _this[J7D][b8N] && settings[V1t] === $(table$1)[M1q]){var M1A=b4T;M1A+=I$y;M1A+=s6E;M1A+=V$w;_this[M1A](json);}});if(!Editor[s$8][init[s$8]]){var H$M=R$B;H$M+=G1B;H$M+=l6Z;H$M+=K2Z;throw new Error(H$M + init[s$8]);}this[J7D][n4F]=Editor[s$8][init[R7s]][g$l](this);this[d$0](M5s,[]);$(document)[N0a](Y6l,[this]);}Editor[A9s][x$y]=function(name,args){var O7f=f4F;O7f+=k1z;L7K.l4n();this[O7f](name,args);};Editor[N0G][r5G]=function(){L7K.l4n();return this[B1B];};Editor[N0G][b_E]=function(){var u$t="_multiInfo";return this[u$t]();};Editor[N0G][I7v]=function(){L7K.l4n();return this[J7D];};Editor[C2U]={checkbox:checkbox,datatable:datatable,datetime:datetime,hidden:hidden,password:password,radio:radio,readonly:readonly,select:select,text:text,textarea:textarea,upload:upload,uploadMany:uploadMany};Editor[g33]={};Editor[S9I]=q4x;Editor[I58]=classNames;Editor[P9G]=Field;Editor[Y7v]=X2m;Editor[x_w]=error;Editor[c0B]=pairs;Editor[x52]=upload$1;Editor[V72]=defaults$1;Editor[l$S]={button:button,displayController:displayController,fieldType:fieldType,formOptions:formOptions,settings:settings};Editor[P75]={dataTable:dataSource$1,html:dataSource};Editor[s$8]={envelope:envelope,lightbox:self};Editor[L_e]=function(id){L7K.l4n();return safeDomId(id,l2Y);};return Editor;})();DataTable[m6s]=Editor;$[w9$][W5V][X6A]=Editor;if(DataTable[P53]){var U4f=m1U;U4f+=N4N;Editor[P53]=DataTable[U4f];}if(DataTable[A7b][Z9i]){var Y56=m9z;Y56+=h07;Y56+=F$o;var w3E=t0$;w3E+=I$D;$[t8A](Editor[w3E],DataTable[Y56][Z9i]);}DataTable[P0N][h9V]=Editor[q_9];return Editor;});})();

/*! Bootstrap integration for DataTables' Editor
 * ©2015 SpryMedia Ltd - datatables.net/license
 */

(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net-bs4', 'datatables.net-editor'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				$ = require('datatables.net-bs4')(root, $).$;
			}

			if ( ! $.fn.dataTable.Editor ) {
				require('datatables.net-editor')(root, $);
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/*
 * Set the default display controller to be our bootstrap control 
 */
DataTable.Editor.defaults.display = "bootstrap";


/*
 * Alter the buttons that Editor adds to Buttons so they are suitable for bootstrap
 */
var i18nDefaults = DataTable.Editor.defaults.i18n;
i18nDefaults.create.title = '<h5 class="modal-title">'+i18nDefaults.create.title+'</h5>';
i18nDefaults.edit.title = '<h5 class="modal-title">'+i18nDefaults.edit.title+'</h5>';
i18nDefaults.remove.title = '<h5 class="modal-title">'+i18nDefaults.remove.title+'</h5>';


/*
 * Change the default classes from Editor to be classes for Bootstrap
 */
$.extend( true, $.fn.dataTable.Editor.classes, {
	"header": {
		"wrapper": "DTE_Header modal-header"
	},
	"body": {
		"wrapper": "DTE_Body modal-body"
	},
	"footer": {
		"wrapper": "DTE_Footer modal-footer"
	},
	"form": {
		"tag": "form-horizontal",
		"button": "btn",
		"buttonInternal": "btn btn-outline-secondary"
	},
	"field": {
		"wrapper": "DTE_Field form-group row",
		"label":   "col-lg-4 col-form-label",
		"input":   "col-lg-8",
		"error":   "error is-invalid",
		"msg-labelInfo": "form-text text-secondary small",
		"msg-info":      "form-text text-secondary small",
		"msg-message":   "form-text text-secondary small",
		"msg-error":     "form-text text-danger small",
		"multiValue":    "card multi-value",
		"multiInfo":     "small",
		"multiRestore":  "multi-restore"
	}
} );

$.extend( true, DataTable.ext.buttons, {
	create: {
		formButtons: {
			className: 'btn-primary'
		}
	},
	edit: {
		formButtons: {
			className: 'btn-primary'
		}
	},
	remove: {
		formButtons: {
			className: 'btn-danger'
		}
	}
} );

DataTable.Editor.fieldTypes.datatable.tableClass = 'table';

/*
 * Bootstrap display controller - this is effectively a proxy to the Bootstrap
 * modal control.
 */

let shown = false;
let fullyShown = false;

const dom = {
	// Note that `modal-dialog-scrollable` is BS4.3+ only. It has no effect on 4.0-4.2
	content: $(
		'<div class="modal fade DTED">'+
			'<div class="modal-dialog modal-lg modal-dialog-scrollable"></div>'+
		'</div>'
	),
	close: $('<button class="close">&times;</div>')
};

DataTable.Editor.display.bootstrap = $.extend( true, {}, DataTable.Editor.models.displayController, {
	init: function ( dte ) {
		// Add `form-control` to required elements
		dte.on( 'displayOrder.dtebs', function ( e, display, action, form ) {
			$.each( dte.s.fields, function ( key, field ) {
				$('input:not([type=checkbox]):not([type=radio]), select, textarea', field.node() )
					.addClass( 'form-control' );
			} );
		} );

		return DataTable.Editor.display.bootstrap;
	},

	open: function ( dte, append, callback ) {
		$(append).addClass('modal-content');

		// Special class for DataTable buttons in the form
		$(append).find('div.DTE_Field_Type_datatable div.dt-buttons')
			.removeClass('btn-group')
			.addClass('btn-group-vertical');

		// Setup events on each show
		dom.close
			.attr('title', dte.i18n.close)
			.off('click.dte-bs4')
			.on('click.dte-bs4', function () {
				dte.close('icon');
			})
			.appendTo($('div.modal-header', append));

		// This is a bit horrible, but if you mousedown and then drag out of the modal container, we don't
		// want to trigger a background action.
		let allowBackgroundClick = false;
		$(document)
			.off('mousedown.dte-bs4')
			.on('mousedown.dte-bs4', 'div.modal', function (e) {
				allowBackgroundClick = $(e.target).hasClass('modal') && shown
					? true
					: false;
			} );

		$(document)
			.off('click.dte-bs4')
			.on('click.dte-bs4', 'div.modal', function (e) {
				if ( $(e.target).hasClass('modal') && allowBackgroundClick ) {
					dte.background();
				}
			} );

		var content = dom.content.find('div.modal-dialog');
		content.children().detach();
		content.append( append );

		if ( shown ) {
			if ( callback ) {
				callback();
			}
			return;
		}

		shown = true;
		fullyShown = false;

		$(dom.content)
			.one('shown.bs.modal', function () {
				// Can only give elements focus when shown
				if ( dte.s.setFocus ) {
					dte.s.setFocus.focus();
				}

				fullyShown = true;

				if ( callback ) {
					callback();
				}
			})
			.one('hidden', function () {
				shown = false;
			})
			.appendTo( 'body' )
			.modal( {
				backdrop: "static",
				keyboard: false
			} );
	},

	close: function ( dte, callback ) {
		if ( ! shown ) {
			if ( callback ) {
				callback();
			}
			return;
		}

		// Check if actually displayed or not before hiding. BS4 doesn't like `hide`
		// before it has been fully displayed
		if ( ! fullyShown ) {
			$(dom.content)
				.one('shown.bs.modal', function () {
					DataTable.Editor.display.bootstrap.close( dte, callback );
				} );

			return;
		}

		$(dom.content)
			.one( 'hidden.bs.modal', function () {
				$(this).detach();
			} )
			.modal('hide');

		shown = false;
		fullyShown = false;

		if ( callback ) {
			callback();
		}
	},

	node: function ( dte ) {
		return dom.content[0];
	}
} );


return DataTable.Editor;
}));


/*! Responsive 2.3.0
 * 2014-2022 SpryMedia Ltd - datatables.net/license
 */

/**
 * @summary     Responsive
 * @description Responsive tables plug-in for DataTables
 * @version     2.3.0
 * @author      SpryMedia Ltd (www.sprymedia.co.uk)
 * @contact     www.sprymedia.co.uk/contact
 * @copyright   SpryMedia Ltd.
 *
 * This source file is free software, available under the following license:
 *   MIT license - http://datatables.net/license/mit
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 * For details please refer to: http://www.datatables.net
 */
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				$ = require('datatables.net')(root, $).$;
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/**
 * Responsive is a plug-in for the DataTables library that makes use of
 * DataTables' ability to change the visibility of columns, changing the
 * visibility of columns so the displayed columns fit into the table container.
 * The end result is that complex tables will be dynamically adjusted to fit
 * into the viewport, be it on a desktop, tablet or mobile browser.
 *
 * Responsive for DataTables has two modes of operation, which can used
 * individually or combined:
 *
 * * Class name based control - columns assigned class names that match the
 *   breakpoint logic can be shown / hidden as required for each breakpoint.
 * * Automatic control - columns are automatically hidden when there is no
 *   room left to display them. Columns removed from the right.
 *
 * In additional to column visibility control, Responsive also has built into
 * options to use DataTables' child row display to show / hide the information
 * from the table that has been hidden. There are also two modes of operation
 * for this child row display:
 *
 * * Inline - when the control element that the user can use to show / hide
 *   child rows is displayed inside the first column of the table.
 * * Column - where a whole column is dedicated to be the show / hide control.
 *
 * Initialisation of Responsive is performed by:
 *
 * * Adding the class `responsive` or `dt-responsive` to the table. In this case
 *   Responsive will automatically be initialised with the default configuration
 *   options when the DataTable is created.
 * * Using the `responsive` option in the DataTables configuration options. This
 *   can also be used to specify the configuration options, or simply set to
 *   `true` to use the defaults.
 *
 *  @class
 *  @param {object} settings DataTables settings object for the host table
 *  @param {object} [opts] Configuration options
 *  @requires jQuery 1.7+
 *  @requires DataTables 1.10.3+
 *
 *  @example
 *      $('#example').DataTable( {
 *        responsive: true
 *      } );
 *    } );
 */
var Responsive = function ( settings, opts ) {
	// Sanity check that we are using DataTables 1.10 or newer
	if ( ! DataTable.versionCheck || ! DataTable.versionCheck( '1.10.10' ) ) {
		throw 'DataTables Responsive requires DataTables 1.10.10 or newer';
	}

	this.s = {
		dt: new DataTable.Api( settings ),
		columns: [],
		current: []
	};

	// Check if responsive has already been initialised on this table
	if ( this.s.dt.settings()[0].responsive ) {
		return;
	}

	// details is an object, but for simplicity the user can give it as a string
	// or a boolean
	if ( opts && typeof opts.details === 'string' ) {
		opts.details = { type: opts.details };
	}
	else if ( opts && opts.details === false ) {
		opts.details = { type: false };
	}
	else if ( opts && opts.details === true ) {
		opts.details = { type: 'inline' };
	}

	this.c = $.extend( true, {}, Responsive.defaults, DataTable.defaults.responsive, opts );
	settings.responsive = this;
	this._constructor();
};

$.extend( Responsive.prototype, {
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Constructor
	 */

	/**
	 * Initialise the Responsive instance
	 *
	 * @private
	 */
	_constructor: function ()
	{
		var that = this;
		var dt = this.s.dt;
		var dtPrivateSettings = dt.settings()[0];
		var oldWindowWidth = $(window).innerWidth();

		dt.settings()[0]._responsive = this;

		// Use DataTables' throttle function to avoid processor thrashing on
		// resize
		$(window).on( 'resize.dtr orientationchange.dtr', DataTable.util.throttle( function () {
			// iOS has a bug whereby resize can fire when only scrolling
			// See: http://stackoverflow.com/questions/8898412
			var width = $(window).innerWidth();

			if ( width !== oldWindowWidth ) {
				that._resize();
				oldWindowWidth = width;
			}
		} ) );

		// DataTables doesn't currently trigger an event when a row is added, so
		// we need to hook into its private API to enforce the hidden rows when
		// new data is added
		dtPrivateSettings.oApi._fnCallbackReg( dtPrivateSettings, 'aoRowCreatedCallback', function (tr, data, idx) {
			if ( $.inArray( false, that.s.current ) !== -1 ) {
				$('>td, >th', tr).each( function ( i ) {
					var idx = dt.column.index( 'toData', i );

					if ( that.s.current[idx] === false ) {
						$(this).css('display', 'none');
					}
				} );
			}
		} );

		// Destroy event handler
		dt.on( 'destroy.dtr', function () {
			dt.off( '.dtr' );
			$( dt.table().body() ).off( '.dtr' );
			$(window).off( 'resize.dtr orientationchange.dtr' );
			dt.cells('.dtr-control').nodes().to$().removeClass('dtr-control');

			// Restore the columns that we've hidden
			$.each( that.s.current, function ( i, val ) {
				if ( val === false ) {
					that._setColumnVis( i, true );
				}
			} );
		} );

		// Reorder the breakpoints array here in case they have been added out
		// of order
		this.c.breakpoints.sort( function (a, b) {
			return a.width < b.width ? 1 :
				a.width > b.width ? -1 : 0;
		} );

		this._classLogic();
		this._resizeAuto();

		// Details handler
		var details = this.c.details;

		if ( details.type !== false ) {
			that._detailsInit();

			// DataTables will trigger this event on every column it shows and
			// hides individually
			dt.on( 'column-visibility.dtr', function () {
				// Use a small debounce to allow multiple columns to be set together
				if ( that._timer ) {
					clearTimeout( that._timer );
				}

				that._timer = setTimeout( function () {
					that._timer = null;

					that._classLogic();
					that._resizeAuto();
					that._resize(true);

					that._redrawChildren();
				}, 100 );
			} );

			// Redraw the details box on each draw which will happen if the data
			// has changed. This is used until DataTables implements a native
			// `updated` event for rows
			dt.on( 'draw.dtr', function () {
				that._redrawChildren();
			} );

			$(dt.table().node()).addClass( 'dtr-'+details.type );
		}

		dt.on( 'column-reorder.dtr', function (e, settings, details) {
			that._classLogic();
			that._resizeAuto();
			that._resize(true);
		} );

		// Change in column sizes means we need to calc
		dt.on( 'column-sizing.dtr', function () {
			that._resizeAuto();
			that._resize();
		});

		// DT2 let's us tell it if we are hiding columns
		dt.on( 'column-calc.dt', function (e, d) {
			var curr = that.s.current;

			for (var i=0 ; i<curr.length ; i++) {
				var idx = d.visible.indexOf(i);

				if (curr[i] === false && idx >= 0) {
					d.visible.splice(idx, 1);
				}
			}
		} );

		// On Ajax reload we want to reopen any child rows which are displayed
		// by responsive
		dt.on( 'preXhr.dtr', function () {
			var rowIds = [];
			dt.rows().every( function () {
				if ( this.child.isShown() ) {
					rowIds.push( this.id(true) );
				}
			} );

			dt.one( 'draw.dtr', function () {
				that._resizeAuto();
				that._resize();

				dt.rows( rowIds ).every( function () {
					that._detailsDisplay( this, false );
				} );
			} );
		});

		dt
			.on( 'draw.dtr', function () {
				that._controlClass();
			})
			.on( 'init.dtr', function (e, settings, details) {
				if ( e.namespace !== 'dt' ) {
					return;
				}

				that._resizeAuto();
				that._resize();

				// If columns were hidden, then DataTables needs to adjust the
				// column sizing
				if ( $.inArray( false, that.s.current ) ) {
					dt.columns.adjust();
				}
			} );

		// First pass - draw the table for the current viewport size
		this._resize();
	},


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Private methods
	 */

	/**
	 * Calculate the visibility for the columns in a table for a given
	 * breakpoint. The result is pre-determined based on the class logic if
	 * class names are used to control all columns, but the width of the table
	 * is also used if there are columns which are to be automatically shown
	 * and hidden.
	 *
	 * @param  {string} breakpoint Breakpoint name to use for the calculation
	 * @return {array} Array of boolean values initiating the visibility of each
	 *   column.
	 *  @private
	 */
	_columnsVisiblity: function ( breakpoint )
	{
		var dt = this.s.dt;
		var columns = this.s.columns;
		var i, ien;

		// Create an array that defines the column ordering based first on the
		// column's priority, and secondly the column index. This allows the
		// columns to be removed from the right if the priority matches
		var order = columns
			.map( function ( col, idx ) {
				return {
					columnIdx: idx,
					priority: col.priority
				};
			} )
			.sort( function ( a, b ) {
				if ( a.priority !== b.priority ) {
					return a.priority - b.priority;
				}
				return a.columnIdx - b.columnIdx;
			} );

		// Class logic - determine which columns are in this breakpoint based
		// on the classes. If no class control (i.e. `auto`) then `-` is used
		// to indicate this to the rest of the function
		var display = $.map( columns, function ( col, i ) {
			if ( dt.column(i).visible() === false ) {
				return 'not-visible';
			}
			return col.auto && col.minWidth === null ?
				false :
				col.auto === true ?
					'-' :
					$.inArray( breakpoint, col.includeIn ) !== -1;
		} );

		// Auto column control - first pass: how much width is taken by the
		// ones that must be included from the non-auto columns
		var requiredWidth = 0;
		for ( i=0, ien=display.length ; i<ien ; i++ ) {
			if ( display[i] === true ) {
				requiredWidth += columns[i].minWidth;
			}
		}

		// Second pass, use up any remaining width for other columns. For
		// scrolling tables we need to subtract the width of the scrollbar. It
		// may not be requires which makes this sub-optimal, but it would
		// require another full redraw to make complete use of those extra few
		// pixels
		var scrolling = dt.settings()[0].oScroll;
		var bar = scrolling.sY || scrolling.sX ? scrolling.iBarWidth : 0;
		var widthAvailable = dt.table().container().offsetWidth - bar;
		var usedWidth = widthAvailable - requiredWidth;

		// Control column needs to always be included. This makes it sub-
		// optimal in terms of using the available with, but to stop layout
		// thrashing or overflow. Also we need to account for the control column
		// width first so we know how much width is available for the other
		// columns, since the control column might not be the first one shown
		for ( i=0, ien=display.length ; i<ien ; i++ ) {
			if ( columns[i].control ) {
				usedWidth -= columns[i].minWidth;
			}
		}

		// Allow columns to be shown (counting by priority and then right to
		// left) until we run out of room
		var empty = false;
		for ( i=0, ien=order.length ; i<ien ; i++ ) {
			var colIdx = order[i].columnIdx;

			if ( display[colIdx] === '-' && ! columns[colIdx].control && columns[colIdx].minWidth ) {
				// Once we've found a column that won't fit we don't let any
				// others display either, or columns might disappear in the
				// middle of the table
				if ( empty || usedWidth - columns[colIdx].minWidth < 0 ) {
					empty = true;
					display[colIdx] = false;
				}
				else {
					display[colIdx] = true;
				}

				usedWidth -= columns[colIdx].minWidth;
			}
		}

		// Determine if the 'control' column should be shown (if there is one).
		// This is the case when there is a hidden column (that is not the
		// control column). The two loops look inefficient here, but they are
		// trivial and will fly through. We need to know the outcome from the
		// first , before the action in the second can be taken
		var showControl = false;

		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( ! columns[i].control && ! columns[i].never && display[i] === false ) {
				showControl = true;
				break;
			}
		}

		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( columns[i].control ) {
				display[i] = showControl;
			}

			// Replace not visible string with false from the control column detection above
			if ( display[i] === 'not-visible' ) {
				display[i] = false;
			}
		}

		// Finally we need to make sure that there is at least one column that
		// is visible
		if ( $.inArray( true, display ) === -1 ) {
			display[0] = true;
		}

		return display;
	},


	/**
	 * Create the internal `columns` array with information about the columns
	 * for the table. This includes determining which breakpoints the column
	 * will appear in, based upon class names in the column, which makes up the
	 * vast majority of this method.
	 *
	 * @private
	 */
	_classLogic: function ()
	{
		var that = this;
		var calc = {};
		var breakpoints = this.c.breakpoints;
		var dt = this.s.dt;
		var columns = dt.columns().eq(0).map( function (i) {
			var column = this.column(i);
			var className = column.header().className;
			var priority = dt.settings()[0].aoColumns[i].responsivePriority;
			var dataPriority = column.header().getAttribute('data-priority');

			if ( priority === undefined ) {
				priority = dataPriority === undefined || dataPriority === null?
					10000 :
					dataPriority * 1;
			}

			return {
				className: className,
				includeIn: [],
				auto:      false,
				control:   false,
				never:     className.match(/\b(dtr\-)?never\b/) ? true : false,
				priority:  priority
			};
		} );

		// Simply add a breakpoint to `includeIn` array, ensuring that there are
		// no duplicates
		var add = function ( colIdx, name ) {
			var includeIn = columns[ colIdx ].includeIn;

			if ( $.inArray( name, includeIn ) === -1 ) {
				includeIn.push( name );
			}
		};

		var column = function ( colIdx, name, operator, matched ) {
			var size, i, ien;

			if ( ! operator ) {
				columns[ colIdx ].includeIn.push( name );
			}
			else if ( operator === 'max-' ) {
				// Add this breakpoint and all smaller
				size = that._find( name ).width;

				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].width <= size ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
			else if ( operator === 'min-' ) {
				// Add this breakpoint and all larger
				size = that._find( name ).width;

				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].width >= size ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
			else if ( operator === 'not-' ) {
				// Add all but this breakpoint
				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].name.indexOf( matched ) === -1 ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
		};

		// Loop over each column and determine if it has a responsive control
		// class
		columns.each( function ( col, i ) {
			var classNames = col.className.split(' ');
			var hasClass = false;

			// Split the class name up so multiple rules can be applied if needed
			for ( var k=0, ken=classNames.length ; k<ken ; k++ ) {
				var className = classNames[k].trim();

				if ( className === 'all' || className === 'dtr-all' ) {
					// Include in all
					hasClass = true;
					col.includeIn = $.map( breakpoints, function (a) {
						return a.name;
					} );
					return;
				}
				else if ( className === 'none' || className === 'dtr-none' || col.never ) {
					// Include in none (default) and no auto
					hasClass = true;
					return;
				}
				else if ( className === 'control' || className === 'dtr-control' ) {
					// Special column that is only visible, when one of the other
					// columns is hidden. This is used for the details control
					hasClass = true;
					col.control = true;
					return;
				}

				$.each( breakpoints, function ( j, breakpoint ) {
					// Does this column have a class that matches this breakpoint?
					var brokenPoint = breakpoint.name.split('-');
					var re = new RegExp( '(min\\-|max\\-|not\\-)?('+brokenPoint[0]+')(\\-[_a-zA-Z0-9])?' );
					var match = className.match( re );

					if ( match ) {
						hasClass = true;

						if ( match[2] === brokenPoint[0] && match[3] === '-'+brokenPoint[1] ) {
							// Class name matches breakpoint name fully
							column( i, breakpoint.name, match[1], match[2]+match[3] );
						}
						else if ( match[2] === brokenPoint[0] && ! match[3] ) {
							// Class name matched primary breakpoint name with no qualifier
							column( i, breakpoint.name, match[1], match[2] );
						}
					}
				} );
			}

			// If there was no control class, then automatic sizing is used
			if ( ! hasClass ) {
				col.auto = true;
			}
		} );

		this.s.columns = columns;
	},

	/**
	 * Update the cells to show the correct control class / button
	 * @private
	 */
	_controlClass: function ()
	{
		if ( this.c.details.type === 'inline' ) {
			var dt = this.s.dt;
			var columnsVis = this.s.current;
			var firstVisible = $.inArray(true, columnsVis);

			// Remove from any cells which shouldn't have it
			dt.cells(
				null,
				function(idx) {
					return idx !== firstVisible;
				},
				{page: 'current'}
			)
				.nodes()
				.to$()
				.filter('.dtr-control')
				.removeClass('dtr-control');

			dt.cells(null, firstVisible, {page: 'current'})
				.nodes()
				.to$()
				.addClass('dtr-control');
		}
	},

	/**
	 * Show the details for the child row
	 *
	 * @param  {DataTables.Api} row    API instance for the row
	 * @param  {boolean}        update Update flag
	 * @private
	 */
	_detailsDisplay: function ( row, update )
	{
		var that = this;
		var dt = this.s.dt;
		var details = this.c.details;

		if ( details && details.type !== false ) {
			var renderer = typeof details.renderer === 'string'
				? Responsive.renderer[details.renderer]()
				: details.renderer;

			var res = details.display( row, update, function () {
				return renderer(
					dt, row[0], that._detailsObj(row[0])
				);
			} );

			if ( res === true || res === false ) {
				$(dt.table().node()).triggerHandler( 'responsive-display.dt', [dt, row, res, update] );
			}
		}
	},


	/**
	 * Initialisation for the details handler
	 *
	 * @private
	 */
	_detailsInit: function ()
	{
		var that    = this;
		var dt      = this.s.dt;
		var details = this.c.details;

		// The inline type always uses the first child as the target
		if ( details.type === 'inline' ) {
			details.target = 'td.dtr-control, th.dtr-control';
		}

		// Keyboard accessibility
		dt.on( 'draw.dtr', function () {
			that._tabIndexes();
		} );
		that._tabIndexes(); // Initial draw has already happened

		$( dt.table().body() ).on( 'keyup.dtr', 'td, th', function (e) {
			if ( e.keyCode === 13 && $(this).data('dtr-keyboard') ) {
				$(this).click();
			}
		} );

		// type.target can be a string jQuery selector or a column index
		var target   = details.target;
		var selector = typeof target === 'string' ? target : 'td, th';

		if ( target !== undefined || target !== null ) {
			// Click handler to show / hide the details rows when they are available
			$( dt.table().body() )
				.on( 'click.dtr mousedown.dtr mouseup.dtr', selector, function (e) {
					// If the table is not collapsed (i.e. there is no hidden columns)
					// then take no action
					if ( ! $(dt.table().node()).hasClass('collapsed' ) ) {
						return;
					}

					// Check that the row is actually a DataTable's controlled node
					if ( $.inArray( $(this).closest('tr').get(0), dt.rows().nodes().toArray() ) === -1 ) {
						return;
					}

					// For column index, we determine if we should act or not in the
					// handler - otherwise it is already okay
					if ( typeof target === 'number' ) {
						var targetIdx = target < 0 ?
							dt.columns().eq(0).length + target :
							target;

						if ( dt.cell( this ).index().column !== targetIdx ) {
							return;
						}
					}

					// $().closest() includes itself in its check
					var row = dt.row( $(this).closest('tr') );

					// Check event type to do an action
					if ( e.type === 'click' ) {
						// The renderer is given as a function so the caller can execute it
						// only when they need (i.e. if hiding there is no point is running
						// the renderer)
						that._detailsDisplay( row, false );
					}
					else if ( e.type === 'mousedown' ) {
						// For mouse users, prevent the focus ring from showing
						$(this).css('outline', 'none');
					}
					else if ( e.type === 'mouseup' ) {
						// And then re-allow at the end of the click
						$(this).trigger('blur').css('outline', '');
					}
				} );
		}
	},


	/**
	 * Get the details to pass to a renderer for a row
	 * @param  {int} rowIdx Row index
	 * @private
	 */
	_detailsObj: function ( rowIdx )
	{
		var that = this;
		var dt = this.s.dt;

		return $.map( this.s.columns, function( col, i ) {
			// Never and control columns should not be passed to the renderer
			if ( col.never || col.control ) {
				return;
			}

			var dtCol = dt.settings()[0].aoColumns[ i ];

			return {
				className:   dtCol.sClass,
				columnIndex: i,
				data:        dt.cell( rowIdx, i ).render( that.c.orthogonal ),
				hidden:      dt.column( i ).visible() && !that.s.current[ i ],
				rowIndex:    rowIdx,
				title:       dtCol.sTitle !== null ?
					dtCol.sTitle :
					$(dt.column(i).header()).text()
			};
		} );
	},


	/**
	 * Find a breakpoint object from a name
	 *
	 * @param  {string} name Breakpoint name to find
	 * @return {object}      Breakpoint description object
	 * @private
	 */
	_find: function ( name )
	{
		var breakpoints = this.c.breakpoints;

		for ( var i=0, ien=breakpoints.length ; i<ien ; i++ ) {
			if ( breakpoints[i].name === name ) {
				return breakpoints[i];
			}
		}
	},


	/**
	 * Re-create the contents of the child rows as the display has changed in
	 * some way.
	 *
	 * @private
	 */
	_redrawChildren: function ()
	{
		var that = this;
		var dt = this.s.dt;

		dt.rows( {page: 'current'} ).iterator( 'row', function ( settings, idx ) {
			var row = dt.row( idx );

			that._detailsDisplay( dt.row( idx ), true );
		} );
	},


	/**
	 * Alter the table display for a resized viewport. This involves first
	 * determining what breakpoint the window currently is in, getting the
	 * column visibilities to apply and then setting them.
	 *
	 * @param  {boolean} forceRedraw Force a redraw
	 * @private
	 */
	_resize: function (forceRedraw)
	{
		var that = this;
		var dt = this.s.dt;
		var width = $(window).innerWidth();
		var breakpoints = this.c.breakpoints;
		var breakpoint = breakpoints[0].name;
		var columns = this.s.columns;
		var i, ien;
		var oldVis = this.s.current.slice();

		// Determine what breakpoint we are currently at
		for ( i=breakpoints.length-1 ; i>=0 ; i-- ) {
			if ( width <= breakpoints[i].width ) {
				breakpoint = breakpoints[i].name;
				break;
			}
		}
		
		// Show the columns for that break point
		var columnsVis = this._columnsVisiblity( breakpoint );
		this.s.current = columnsVis;

		// Set the class before the column visibility is changed so event
		// listeners know what the state is. Need to determine if there are
		// any columns that are not visible but can be shown
		var collapsedClass = false;
	
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( columnsVis[i] === false && ! columns[i].never && ! columns[i].control && ! dt.column(i).visible() === false ) {
				collapsedClass = true;
				break;
			}
		}

		$( dt.table().node() ).toggleClass( 'collapsed', collapsedClass );

		var changed = false;
		var visible = 0;

		dt.columns().eq(0).each( function ( colIdx, i ) {
			if ( columnsVis[i] === true ) {
				visible++;
			}

			if ( forceRedraw || columnsVis[i] !== oldVis[i] ) {
				changed = true;
				that._setColumnVis( colIdx, columnsVis[i] );
			}
		} );

		if ( changed ) {
			this._redrawChildren();

			// Inform listeners of the change
			$(dt.table().node()).trigger( 'responsive-resize.dt', [dt, this.s.current] );

			// If no records, update the "No records" display element
			if ( dt.page.info().recordsDisplay === 0 ) {
				$('td', dt.table().body()).eq(0).attr('colspan', visible);
			}
		}

		that._controlClass();
	},


	/**
	 * Determine the width of each column in the table so the auto column hiding
	 * has that information to work with. This method is never going to be 100%
	 * perfect since column widths can change slightly per page, but without
	 * seriously compromising performance this is quite effective.
	 *
	 * @private
	 */
	_resizeAuto: function ()
	{
		var dt = this.s.dt;
		var columns = this.s.columns;

		// Are we allowed to do auto sizing?
		if ( ! this.c.auto ) {
			return;
		}

		// Are there any columns that actually need auto-sizing, or do they all
		// have classes defined
		if ( $.inArray( true, $.map( columns, function (c) { return c.auto; } ) ) === -1 ) {
			return;
		}

		// Need to restore all children. They will be reinstated by a re-render
		if ( ! $.isEmptyObject( _childNodeStore ) ) {
			$.each( _childNodeStore, function ( key ) {
				var idx = key.split('-');

				_childNodesRestore( dt, idx[0]*1, idx[1]*1 );
			} );
		}

		// Clone the table with the current data in it
		var tableWidth   = dt.table().node().offsetWidth;
		var columnWidths = dt.columns;
		var clonedTable  = dt.table().node().cloneNode( false );
		var clonedHeader = $( dt.table().header().cloneNode( false ) ).appendTo( clonedTable );
		var clonedBody   = $( dt.table().body() ).clone( false, false ).empty().appendTo( clonedTable ); // use jQuery because of IE8

		clonedTable.style.width = 'auto';

		// Header
		var headerCells = dt.columns()
			.header()
			.filter( function (idx) {
				return dt.column(idx).visible();
			} )
			.to$()
			.clone( false )
			.css( 'display', 'table-cell' )
			.css( 'width', 'auto' )
			.css( 'min-width', 0 );

		// Body rows - we don't need to take account of DataTables' column
		// visibility since we implement our own here (hence the `display` set)
		$(clonedBody)
			.append( $(dt.rows( { page: 'current' } ).nodes()).clone( false ) )
			.find( 'th, td' ).css( 'display', '' );

		// Footer
		var footer = dt.table().footer();
		if ( footer ) {
			var clonedFooter = $( footer.cloneNode( false ) ).appendTo( clonedTable );
			var footerCells = dt.columns()
				.footer()
				.filter( function (idx) {
					return dt.column(idx).visible();
				} )
				.to$()
				.clone( false )
				.css( 'display', 'table-cell' );

			$('<tr/>')
				.append( footerCells )
				.appendTo( clonedFooter );
		}

		$('<tr/>')
			.append( headerCells )
			.appendTo( clonedHeader );

		// In the inline case extra padding is applied to the first column to
		// give space for the show / hide icon. We need to use this in the
		// calculation
		if ( this.c.details.type === 'inline' ) {
			$(clonedTable).addClass( 'dtr-inline collapsed' );
		}
		
		// It is unsafe to insert elements with the same name into the DOM
		// multiple times. For example, cloning and inserting a checked radio
		// clears the chcecked state of the original radio.
		$( clonedTable ).find( '[name]' ).removeAttr( 'name' );

		// A position absolute table would take the table out of the flow of
		// our container element, bypassing the height and width (Scroller)
		$( clonedTable ).css( 'position', 'relative' )
		
		var inserted = $('<div/>')
			.css( {
				width: 1,
				height: 1,
				overflow: 'hidden',
				clear: 'both'
			} )
			.append( clonedTable );

		inserted.insertBefore( dt.table().node() );

		// The cloned header now contains the smallest that each column can be
		headerCells.each( function (i) {
			var idx = dt.column.index( 'fromVisible', i );
			columns[ idx ].minWidth =  this.offsetWidth || 0;
		} );

		inserted.remove();
	},

	/**
	 * Get the state of the current hidden columns - controlled by Responsive only
	 */
	_responsiveOnlyHidden: function ()
	{
		var dt = this.s.dt;

		return $.map( this.s.current, function (v, i) {
			// If the column is hidden by DataTables then it can't be hidden by
			// Responsive!
			if ( dt.column(i).visible() === false ) {
				return true;
			}
			return v;
		} );
	},

	/**
	 * Set a column's visibility.
	 *
	 * We don't use DataTables' column visibility controls in order to ensure
	 * that column visibility can Responsive can no-exist. Since only IE8+ is
	 * supported (and all evergreen browsers of course) the control of the
	 * display attribute works well.
	 *
	 * @param {integer} col      Column index
	 * @param {boolean} showHide Show or hide (true or false)
	 * @private
	 */
	_setColumnVis: function ( col, showHide )
	{
		var dt = this.s.dt;
		var display = showHide ? '' : 'none'; // empty string will remove the attr

		$( dt.column( col ).header() )
			.css( 'display', display )
			.toggleClass('dtr-hidden', !showHide);

		$( dt.column( col ).footer() )
			.css( 'display', display )
			.toggleClass('dtr-hidden', !showHide);

		dt.column( col ).nodes().to$()
			.css( 'display', display )
			.toggleClass('dtr-hidden', !showHide);

		// If the are child nodes stored, we might need to reinsert them
		if ( ! $.isEmptyObject( _childNodeStore ) ) {
			dt.cells( null, col ).indexes().each( function (idx) {
				_childNodesRestore( dt, idx.row, idx.column );
			} );
		}
	},


	/**
	 * Update the cell tab indexes for keyboard accessibility. This is called on
	 * every table draw - that is potentially inefficient, but also the least
	 * complex option given that column visibility can change on the fly. Its a
	 * shame user-focus was removed from CSS 3 UI, as it would have solved this
	 * issue with a single CSS statement.
	 *
	 * @private
	 */
	_tabIndexes: function ()
	{
		var dt = this.s.dt;
		var cells = dt.cells( { page: 'current' } ).nodes().to$();
		var ctx = dt.settings()[0];
		var target = this.c.details.target;

		cells.filter( '[data-dtr-keyboard]' ).removeData( '[data-dtr-keyboard]' );

		if ( typeof target === 'number' ) {
			dt.cells( null, target, { page: 'current' } ).nodes().to$()
				.attr( 'tabIndex', ctx.iTabIndex )
				.data( 'dtr-keyboard', 1 );
		}
		else {
			// This is a bit of a hack - we need to limit the selected nodes to just
			// those of this table
			if ( target === 'td:first-child, th:first-child' ) {
				target = '>td:first-child, >th:first-child';
			}

			$( target, dt.rows( { page: 'current' } ).nodes() )
				.attr( 'tabIndex', ctx.iTabIndex )
				.data( 'dtr-keyboard', 1 );
		}
	}
} );


/**
 * List of default breakpoints. Each item in the array is an object with two
 * properties:
 *
 * * `name` - the breakpoint name.
 * * `width` - the breakpoint width
 *
 * @name Responsive.breakpoints
 * @static
 */
Responsive.breakpoints = [
	{ name: 'desktop',  width: Infinity },
	{ name: 'tablet-l', width: 1024 },
	{ name: 'tablet-p', width: 768 },
	{ name: 'mobile-l', width: 480 },
	{ name: 'mobile-p', width: 320 }
];


/**
 * Display methods - functions which define how the hidden data should be shown
 * in the table.
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.display = {
	childRow: function ( row, update, render ) {
		if ( update ) {
			if ( $(row.node()).hasClass('parent') ) {
				row.child( render(), 'child' ).show();

				return true;
			}
		}
		else {
			if ( ! row.child.isShown()  ) {
				row.child( render(), 'child' ).show();
				$( row.node() ).addClass( 'parent' );

				return true;
			}
			else {
				row.child( false );
				$( row.node() ).removeClass( 'parent' );

				return false;
			}
		}
	},

	childRowImmediate: function ( row, update, render ) {
		if ( (! update && row.child.isShown()) || ! row.responsive.hasHidden() ) {
			// User interaction and the row is show, or nothing to show
			row.child( false );
			$( row.node() ).removeClass( 'parent' );

			return false;
		}
		else {
			// Display
			row.child( render(), 'child' ).show();
			$( row.node() ).addClass( 'parent' );

			return true;
		}
	},

	// This is a wrapper so the modal options for Bootstrap and jQuery UI can
	// have options passed into them. This specific one doesn't need to be a
	// function but it is for consistency in the `modal` name
	modal: function ( options ) {
		return function ( row, update, render ) {
			if ( ! update ) {
				// Show a modal
				var close = function () {
					modal.remove(); // will tidy events for us
					$(document).off( 'keypress.dtr' );
				};

				var modal = $('<div class="dtr-modal"/>')
					.append( $('<div class="dtr-modal-display"/>')
						.append( $('<div class="dtr-modal-content"/>')
							.append( render() )
						)
						.append( $('<div class="dtr-modal-close">&times;</div>' )
							.click( function () {
								close();
							} )
						)
					)
					.append( $('<div class="dtr-modal-background"/>')
						.click( function () {
							close();
						} )
					)
					.appendTo( 'body' );

				$(document).on( 'keyup.dtr', function (e) {
					if ( e.keyCode === 27 ) {
						e.stopPropagation();

						close();
					}
				} );
			}
			else {
				$('div.dtr-modal-content')
					.empty()
					.append( render() );
			}

			if ( options && options.header ) {
				$('div.dtr-modal-content').prepend(
					'<h2>'+options.header( row )+'</h2>'
				);
			}
		};
	}
};


var _childNodeStore = {};

function _childNodes( dt, row, col ) {
	var name = row+'-'+col;

	if ( _childNodeStore[ name ] ) {
		return _childNodeStore[ name ];
	}

	// https://jsperf.com/childnodes-array-slice-vs-loop
	var nodes = [];
	var children = dt.cell( row, col ).node().childNodes;
	for ( var i=0, ien=children.length ; i<ien ; i++ ) {
		nodes.push( children[i] );
	}

	_childNodeStore[ name ] = nodes;

	return nodes;
}

function _childNodesRestore( dt, row, col ) {
	var name = row+'-'+col;

	if ( ! _childNodeStore[ name ] ) {
		return;
	}

	var node = dt.cell( row, col ).node();
	var store = _childNodeStore[ name ];
	var parent = store[0].parentNode;
	var parentChildren = parent.childNodes;
	var a = [];

	for ( var i=0, ien=parentChildren.length ; i<ien ; i++ ) {
		a.push( parentChildren[i] );
	}

	for ( var j=0, jen=a.length ; j<jen ; j++ ) {
		node.appendChild( a[j] );
	}

	_childNodeStore[ name ] = undefined;
}


/**
 * Display methods - functions which define how the hidden data should be shown
 * in the table.
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.renderer = {
	listHiddenNodes: function () {
		return function ( api, rowIdx, columns ) {
			var ul = $('<ul data-dtr-index="'+rowIdx+'" class="dtr-details"/>');
			var found = false;

			var data = $.each( columns, function ( i, col ) {
				if ( col.hidden ) {
					var klass = col.className ?
						'class="'+ col.className +'"' :
						'';
	
					$(
						'<li '+klass+' data-dtr-index="'+col.columnIndex+'" data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
							'<span class="dtr-title">'+
								col.title+
							'</span> '+
						'</li>'
					)
						.append( $('<span class="dtr-data"/>').append( _childNodes( api, col.rowIndex, col.columnIndex ) ) )// api.cell( col.rowIndex, col.columnIndex ).node().childNodes ) )
						.appendTo( ul );

					found = true;
				}
			} );

			return found ?
				ul :
				false;
		};
	},

	listHidden: function () {
		return function ( api, rowIdx, columns ) {
			var data = $.map( columns, function ( col ) {
				var klass = col.className ?
					'class="'+ col.className +'"' :
					'';

				return col.hidden ?
					'<li '+klass+' data-dtr-index="'+col.columnIndex+'" data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
						'<span class="dtr-title">'+
							col.title+
						'</span> '+
						'<span class="dtr-data">'+
							col.data+
						'</span>'+
					'</li>' :
					'';
			} ).join('');

			return data ?
				$('<ul data-dtr-index="'+rowIdx+'" class="dtr-details"/>').append( data ) :
				false;
		}
	},

	tableAll: function ( options ) {
		options = $.extend( {
			tableClass: ''
		}, options );

		return function ( api, rowIdx, columns ) {
			var data = $.map( columns, function ( col ) {
				var klass = col.className ?
					'class="'+ col.className +'"' :
					'';

				return '<tr '+klass+' data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
						'<td>'+col.title+':'+'</td> '+
						'<td>'+col.data+'</td>'+
					'</tr>';
			} ).join('');

			return $('<table class="'+options.tableClass+' dtr-details" width="100%"/>').append( data );
		}
	}
};

/**
 * Responsive default settings for initialisation
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.defaults = {
	/**
	 * List of breakpoints for the instance. Note that this means that each
	 * instance can have its own breakpoints. Additionally, the breakpoints
	 * cannot be changed once an instance has been creased.
	 *
	 * @type {Array}
	 * @default Takes the value of `Responsive.breakpoints`
	 */
	breakpoints: Responsive.breakpoints,

	/**
	 * Enable / disable auto hiding calculations. It can help to increase
	 * performance slightly if you disable this option, but all columns would
	 * need to have breakpoint classes assigned to them
	 *
	 * @type {Boolean}
	 * @default  `true`
	 */
	auto: true,

	/**
	 * Details control. If given as a string value, the `type` property of the
	 * default object is set to that value, and the defaults used for the rest
	 * of the object - this is for ease of implementation.
	 *
	 * The object consists of the following properties:
	 *
	 * * `display` - A function that is used to show and hide the hidden details
	 * * `renderer` - function that is called for display of the child row data.
	 *   The default function will show the data from the hidden columns
	 * * `target` - Used as the selector for what objects to attach the child
	 *   open / close to
	 * * `type` - `false` to disable the details display, `inline` or `column`
	 *   for the two control types
	 *
	 * @type {Object|string}
	 */
	details: {
		display: Responsive.display.childRow,

		renderer: Responsive.renderer.listHidden(),

		target: 0,

		type: 'inline'
	},

	/**
	 * Orthogonal data request option. This is used to define the data type
	 * requested when Responsive gets the data to show in the child row.
	 *
	 * @type {String}
	 */
	orthogonal: 'display'
};


/*
 * API
 */
var Api = $.fn.dataTable.Api;

// Doesn't do anything - work around for a bug in DT... Not documented
Api.register( 'responsive()', function () {
	return this;
} );

Api.register( 'responsive.index()', function ( li ) {
	li = $(li);

	return {
		column: li.data('dtr-index'),
		row:    li.parent().data('dtr-index')
	};
} );

Api.register( 'responsive.rebuild()', function () {
	return this.iterator( 'table', function ( ctx ) {
		if ( ctx._responsive ) {
			ctx._responsive._classLogic();
		}
	} );
} );

Api.register( 'responsive.recalc()', function () {
	return this.iterator( 'table', function ( ctx ) {
		if ( ctx._responsive ) {
			ctx._responsive._resizeAuto();
			ctx._responsive._resize();
		}
	} );
} );

Api.register( 'responsive.hasHidden()', function () {
	var ctx = this.context[0];

	return ctx._responsive ?
		$.inArray( false, ctx._responsive._responsiveOnlyHidden() ) !== -1 :
		false;
} );

Api.registerPlural( 'columns().responsiveHidden()', 'column().responsiveHidden()', function () {
	return this.iterator( 'column', function ( settings, column ) {
		return settings._responsive ?
			settings._responsive._responsiveOnlyHidden()[ column ] :
			false;
	}, 1 );
} );


/**
 * Version information
 *
 * @name Responsive.version
 * @static
 */
Responsive.version = '2.3.0';


$.fn.dataTable.Responsive = Responsive;
$.fn.DataTable.Responsive = Responsive;

// Attach a listener to the document which listens for DataTables initialisation
// events so we can automatically initialise
$(document).on( 'preInit.dt.dtr', function (e, settings, json) {
	if ( e.namespace !== 'dt' ) {
		return;
	}

	if ( $(settings.nTable).hasClass( 'responsive' ) ||
		 $(settings.nTable).hasClass( 'dt-responsive' ) ||
		 settings.oInit.responsive ||
		 DataTable.defaults.responsive
	) {
		var init = settings.oInit.responsive;

		if ( init !== false ) {
			new Responsive( settings, $.isPlainObject( init ) ? init : {}  );
		}
	}
} );


return Responsive;
}));


/*! Bootstrap 4 integration for DataTables' Responsive
 * ©2016 SpryMedia Ltd - datatables.net/license
 */

(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net-bs4', 'datatables.net-responsive'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				$ = require('datatables.net-bs4')(root, $).$;
			}

			if ( ! $.fn.dataTable.Responsive ) {
				require('datatables.net-responsive')(root, $);
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


var _display = DataTable.Responsive.display;
var _original = _display.modal;
var _modal = $(
	'<div class="modal fade dtr-bs-modal" role="dialog">'+
		'<div class="modal-dialog" role="document">'+
			'<div class="modal-content">'+
				'<div class="modal-header">'+
					'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
				'</div>'+
				'<div class="modal-body"/>'+
			'</div>'+
		'</div>'+
	'</div>'
);

_display.modal = function ( options ) {
	return function ( row, update, render ) {
		if ( ! $.fn.modal ) {
			_original( row, update, render );
		}
		else {
			if ( ! update ) {
				if ( options && options.header ) {
					var header = _modal.find('div.modal-header');
					var button = header.find('button').detach();
					
					header
						.empty()
						.append( '<h4 class="modal-title">'+options.header( row )+'</h4>' )
						.append( button );
				}

				_modal.find( 'div.modal-body' )
					.empty()
					.append( render() );

				_modal
					.appendTo( 'body' )
					.modal();
			}
		}
	};
};


return DataTable.Responsive;
}));


