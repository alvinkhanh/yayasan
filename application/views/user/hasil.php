<style>
    img {
        width: 100px;
    }

    #myTable {
        width: inherit !important;
    }
</style>
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.2.4/css/fixedHeader.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        PPA
                    </div>
                    <div class="card-body">
                        <table id="myTable" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <!-- <th>Foto</th> -->
                                    <th>Nama Murid</th>
                                    <th>Nis</th>
                                    <th>Kelas</th>
                                    <th>Tahun Ajaran</th>
                                    <th>SK1</th>
                                    <th>SK2</th>
                                    <th>SK3</th>
                                    <th>SK4</th>
                                    <th>SK5</th>
                                    <th>Total</th>
                                    <th>Hasil</th>
                                </tr>
                            </thead>
                            <tbody id="show_data">
                                <?php
                                foreach ($ppa as $i => $p) {
                                ?>
                                    <tr>
                                        <!-- <td>
                                            <img src="<?php echo base_url(); ?>assets/foto/user/<?php echo $p->image ?>" class="brand-image elevation-3">
                                        </td> -->
                                        <td><?php echo $p->nama_murid ?></td>
                                        <td><?php echo $p->nis ?></td>
                                        <td><?php echo $p->kelas ?></td>
                                        <td><?php echo $p->tahun_ajaran ?></td>
                                        <td><?php echo $p->SK1 ?></td>
                                        <td><?php echo $p->SK2 ?></td>
                                        <td><?php echo $p->SK3 ?></td>
                                        <td><?php echo $p->SK4 ?></td>
                                        <td><?php echo $p->SK5 ?></td>
                                        <td><?php echo $p->total ?></td>
                                        <td>
                                            <?php if ($p->total <= 0.25) : ?>
                                                BSB
                                            <?php elseif ($p->total >= 0.25 && $p->total <= 0.50) : ?>
                                                BSH
                                            <?php elseif ($p->total >= 0.50 && $p->total <= 0.75) : ?>
                                                MB
                                            <?php elseif ($p->total >= 0.75 && $p->total <= 1.00) : ?>
                                                BB
                                            <?php else : ?>
                                                Error
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>


<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#myTable').DataTable({
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
</script>