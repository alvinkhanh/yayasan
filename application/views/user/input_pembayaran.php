<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-light">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Custom Filter : </h3>
                            </div>
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="periode" class="col-sm control-label" style="float: left">Periode</label>
                                                <select class="form-control" id="Periode">
                                                    <option value="" selected>Pilih Periode Pemasukan Infaq</option>
                                                    <?php
                                                    foreach ($periode as $pr) {
                                                    ?>
                                                        <option value="<?php echo $pr->periode ?>"><?php echo $pr->periode ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nama_siswa" class="col-sm control-label" style="float: left">Nama Siswa</label>
                                                <select class="form-control" id="nama_siswa">
                                                    <option value="" selected>Pilih Nama Siswa</option>
                                                    <?php
                                                    foreach ($siswa as $sis) {
                                                    ?>
                                                        <option value="<?php echo $sis->nama_murid ?>"><?php echo $sis->nama_murid ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nama_guru" class="col-sm control-label" style="float: left">Nama Guru</label>
                                                <select class="form-control" id="nama_guru">
                                                    <option value="" selected>Pilih Nama Guru</option>
                                                    <?php
                                                    foreach ($guru as $gr) {
                                                    ?>
                                                        <option value="<?php echo $gr->nama_guru ?>"><?php echo $gr->nama_guru ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label for="LastName" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4" style="float: right; text-align-last: right;">
                                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                            <button type="button" id="btn-reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>

                        <h3 class="card-title"><i class="fa fa-list text-blue"></i> Data Pembayaran Siswa</h3>

                        <div class="text-right">
                            <button type="button" class="btn btn-sm btn-outline-primary" onclick="add_input_pembayaran()" title="Add Data"><i class="fas fa-plus"></i> Lakukan Pembayaran</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="table_kriteria" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Nama Guru</th>
                                    <th>Jam Mengajar</th>
                                    <th>January</th>
                                    <th>February</th>
                                    <th>Maret</th>
                                    <th>April</th>
                                    <th>Mei</th>
                                    <th>Juni</th>
                                    <th>July</th>
                                    <th>Agustus</th>
                                    <th>September</th>
                                    <th>Oktober</th>
                                    <th>Nobember</th>
                                    <th>Desember</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {

        //datatables
        table = $("#table_kriteria").DataTable({
            "dom": 'Bfrtip',
            "buttons": [
                'pageLength', 'copy', 'excel', 'pdf'
            ],
            // "lengthMenu": [
            //     [10, 25, 50, -1],
            //     [10, 25, 50, "All"]
            // ],
            "scrollX": true,
            "responsive": false,
            "autoWidth": true,
            "language": {
                "sEmptyTable": "Belum Ada Data Pembayaran"
            },
            "searching": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('input_pembayaran/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.periode = $('#periode').val();
                    data.nama_siswa = $('#nama_siswa').val();
                    data.nama_guru = $('#nama_guru').val();
                }
            }, //Set column definition initialisation properties.
        });
        $('#btn-filter').click(function() { //button filter event click
            table.ajax.reload(); //just reload table
        });

        $('#btn-reset').click(function() { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });

    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });


    //delete
    function delinput_pembayaran(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?php echo site_url('input_pembayaran/delete'); ?>",
                    type: "POST",
                    data: "id=" + id,
                    cache: false,
                    dataType: 'json',
                    success: function(respone) {
                        if (respone.status == true) {
                            reload_table();
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'Delete Error!!.'
                            });
                        }
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }



    function add_input_pembayaran() {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal({
            backdrop: 'static',
            keyboard: false
        }); // show bootstrap modal
        $('.modal-title').text('Lakukan Pembayaran'); // Set Title to Bootstrap modal title
    }

    // function edit_input_pembayaran(id) {
    //     save_method = 'update';
    //     $('#form')[0].reset(); // reset form on modals
    //     $('.form-group').removeClass('has-error'); // clear error class
    //     $('.help-block').empty(); // clear error string

    //     //Ajax Load data from ajax
    //     $.ajax({
    //         url: "<?php echo site_url('input_pembayaran/edit_kriteria') ?>/" + id,
    //         type: "GET",
    //         dataType: "JSON",
    //         success: function(data) {

    //             $('[name="id"]').val(data.id_kriteria);
    //             $('[name="nama_kriteria"]').val(data.nama_kriteria);
    //             $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
    //             $('.modal-title').text('Edit Kriteria'); // Set title to Bootstrap modal title

    //         },
    //         error: function(jqXHR, textStatus, errorThrown) {
    //             alert('Error get data from ajax');
    //         }
    //     });
    // }

    function save() {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        if (save_method == 'add') {
            url = "<?php echo site_url('input_pembayaran/insert') ?>"; //arahin ke kategori insert
        } else {
            url = "<?php echo site_url('input_pembayaran/update') ?>"; //arahin ke kategori update
        }

        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    Toast.fire({
                        icon: 'success',
                        title: 'Success!!.'
                    });
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').addClass('is-invalid');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]).addClass('invalid-feedback');
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled', false); //set button enable 


            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled', false); //set button enable 

            }
        });
    }
</script>



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id" />
                    <div class="card-body">
                        <div class="form-group row ">
                            <label for="nama" class="col-sm-3 col-form-label">Nama Murid</label>
                            <div class="col-sm-9 kosong">
                                <select class="form-control" id="id_siswa" name="id_siswa">
                                    <option value="" selected disabled>Pilih Nama Murid</option>
                                    <?php
                                    foreach ($murid as $m) { ?>
                                        <option value="<?php echo $m->id_murid ?>"><?php echo $m->nama_murid ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="nama" class="col-sm-3 col-form-label">Total Bayar</label>
                            <div class="col-sm-9 kosong">
                                <input type="number" class="form-control" id="pembayaran" name="pembayaran" placeholder="">
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->