<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-light">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Custom Filter : </h3>
                            </div>
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="periode" class="col-sm control-label" style="float: left">Periode</label>
                                                <select class="form-control" id="periode">
                                                    <option value="" selected>Pilih Periode Pengeluaran Yayasan</option>
                                                    <?php
                                                    foreach ($periode as $pr) {
                                                    ?>
                                                        <option value="<?php echo $pr->periode ?>"><?php echo $pr->periode ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="periode" class="col-sm control-label" style="float: left">Bulan</label>
                                                <select class="form-control" id="bulan">
                                                    <!-- <option value="" selected>Pilih Bulan Pengeluaran Yayasan</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="nama_guru" class="col-sm control-label" style="float: left">Nama Pelapor</label>
                                                <select class="form-control" id="user">
                                                    <option value="" selected>Pilih Nama Pelapor</option>
                                                    <?php
                                                    foreach ($user as $us) {
                                                    ?>
                                                        <option value="<?php echo $us->full_name ?>"><?php echo $us->full_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label for="LastName" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4" style="float: right; text-align-last: right;">
                                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                            <button type="button" id="btn-reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>

                        <h3 class="card-title"><i class="fa fa-list text-blue"></i> Data Pengeluaran Yayasan</h3>

                        <div class="text-right">
                            <button type="button" class="btn btn-sm btn-outline-primary" onclick="add_input_pengeluaran()" title="Add Data"><i class="fas fa-plus"></i> Input Pengeluaran</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="table_kriteria" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Keterangan</th>
                                    <th>Keluar</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {

        //datatables
        table = $("#table_kriteria").DataTable({
            "dom": 'Bfrtip',
            "buttons": [
                'pageLength', 'copy', 'excel', 'pdf'
            ],
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            "scrollX": false,
            "responsive": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Belum Ada Data Pembayaran"
            },
            "searching": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('input_pengeluaran/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.periode = $('#periode').val();
                    data.bulan = $('#bulan').val();
                    data.full_name = $('#user').val();

                }
            }, //Set column definition initialisation properties.
        });
        $('#btn-filter').click(function() { //button filter event click
            table.ajax.reload(); //just reload table
        });

        $('#btn-reset').click(function() { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });

    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $("#periode").change(function() {

        // variabel dari nilai combo box kendaraan
        var periode = $("#periode").val();

        // Menggunakan ajax untuk mengirim dan dan menerima data dari server
        $.ajax({
            url: "<?php echo site_url('input_pengeluaran/get_data'); ?>",
            method: "POST",
            data: {
                periode: periode
            },
            async: false,
            dataType: 'json',
            success: function(data) {
                var html = '';
                var i;
                html += "<option value='' disabled selected> Pilih Bulan yang akan di tampilkan</option>";
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].bulan + '>' + data[i].bulan + '</option>';
                }
                $('#bulan').html(html);

            }
        });
    });

    //delete
    function delinput_pengeluaran(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?php echo site_url('input_pengeluaran/delete'); ?>",
                    type: "POST",
                    data: "id=" + id,
                    cache: false,
                    dataType: 'json',
                    success: function(respone) {
                        if (respone.status == true) {
                            reload_table();
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'Delete Error!!.'
                            });
                        }
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }



    function add_input_pengeluaran() {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal({
            backdrop: 'static',
            keyboard: false
        }); // show bootstrap modal
        $('.modal-title').text('Masukan Pengeluaran'); // Set Title to Bootstrap modal title
    }


    function save() {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        if (save_method == 'add') {
            url = "<?php echo site_url('input_pengeluaran/insert') ?>"; //arahin ke kategori insert
        } else {
            url = "<?php echo site_url('input_pengeluaran/update') ?>"; //arahin ke kategori update
        }

        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data) {

                if (data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    Toast.fire({
                        icon: 'success',
                        title: 'Success!!.'
                    });
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').addClass('is-invalid');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]).addClass('invalid-feedback');
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled', false); //set button enable 


            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled', false); //set button enable 

            }
        });
    }
</script>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id" />
                    <div class="card-body">
                        <div class="form-group row ">
                            <label for="keterangan" class="col-sm-3 col-form-label">Pilih Keterangan Pengeluaran</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="keterangan" name="keterangan">
                                    <option value="" selected disabled>Pilih Keterangan</option>
                                    <?php
                                    foreach ($keterangan as $m) { ?>
                                        <option value="<?php echo $m->id_keterangan ?>"><?php echo $m->nama_keterangan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="pengeluaran" class="col-sm-3 col-form-label">Total Pengeluaran</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="pengeluaran" name="pengeluaran" placeholder="Masukan Nominal Pengeluaran" value="0">
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->