<!-- Main content -->
<section>
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <!-- ./col -->
        <div class="col-lg-3 ">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>Rp. <?php echo $pemasukan ?></h3>
                    <p>Pemasukan Bulan Ini</p>
                </div>
                <div class="icon">
                    <i class="fas fa-money-bill-wave"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 ">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>Rp. <?php echo $past_saldo ?></h3>
                    <p>Saldo Bulan Sebelumnya</p>
                </div>
                <div class="icon">
                    <i class="far fa-money-bill-alt"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 ">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>Rp. <?php echo $pengeluaran ?></h3>
                    <p>Pengeluaran Bulan ini</p>
                </div>
                <div class="icon">
                    <i class="fas fa-money-bill-alt"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>Rp. <?php echo $now_saldo ?></h3>
                    <p>Saldo Bulan Ini</p>
                </div>
                <div class="icon">
                    <i class="far fa-money-bill-alt"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-light">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Custom Filter : </h3>
                            </div>
                            <div class="panel-body">
                                <form id="form-filter" class="form-horizontal">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="periode" class="col-sm control-label" style="float: left">Periode</label>
                                                <select class="form-control" id="periode">
                                                    <option value="" selected>Pilih Periode Pengeluaran Yayasan</option>
                                                    <?php
                                                    foreach ($periode as $pr) {
                                                    ?>
                                                        <option value="<?php echo $pr->periode ?>"><?php echo $pr->periode ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="periode" class="col-sm control-label" style="float: left">Bulan</label>
                                                <select class="form-control" id="bulan">
                                                    <!-- <option value="" selected>Pilih Bulan Pengeluaran Yayasan</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label for="LastName" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4" style="float: right; text-align-last: right;">
                                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                            <button type="button" id="btn-reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>

                        <h3 class="card-title"><i class="fa fa-list text-blue"></i> Data Pengeluaran Yayasan</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="table_kriteria" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr class="bg-info">
                                    <th>No</th>
                                    <th>Keterangan</th>
                                    <th>Pemasukan</th>
                                    <th>Pengeluaran</th>
                                    <th>Saldo</th>
                                    <th>Tanggal</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {

        //datatables
        table = $("#table_kriteria").DataTable({
            "dom": 'Bfrtip',
            "buttons": [
                'pageLength', 'copy', 'excel', 'pdf'
            ],
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            "scrollX": false,
            "responsive": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Belum Ada Data Pembayaran"
            },
            "searching": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan_keuangan/ajax_list') ?>",
                "type": "POST",
                "data": function(data) {
                    data.periode = $('#periode').val();
                    data.bulan = $('#bulan').val();
                    data.full_name = $('#user').val();

                }
            }, //Set column definition initialisation properties.
        });
        $('#btn-filter').click(function() { //button filter event click
            table.ajax.reload(); //just reload table
        });

        $('#btn-reset').click(function() { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });

    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $("#periode").change(function() {

        // variabel dari nilai combo box kendaraan
        var periode = $("#periode").val();

        // Menggunakan ajax untuk mengirim dan dan menerima data dari server
        $.ajax({
            url: "<?php echo site_url('laporan_keuangan/get_data'); ?>",
            method: "POST",
            data: {
                periode: periode
            },
            async: false,
            dataType: 'json',
            success: function(data) {
                var html = '';
                var i;
                html += "<option value='' selected> Pilih Bulan yang akan di tampilkan</option>";
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].bulan + '>' + data[i].bulan + '</option>';
                }
                $('#bulan').html(html);

            }
        });
    });
</script>