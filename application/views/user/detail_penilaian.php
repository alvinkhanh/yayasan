<style>
    img {
        width: 100px;
    }
</style>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Penilaian Murid
                    </div>
                    <div class="card-body">
                        <form action="<?php echo site_url('ppa/save'); ?>" method="POST">
                            <?php
                            foreach ($murid as $mur) {
                            ?>
                                <div class="form-group">
                                    <h4>Nama : <?php echo $mur->nama_murid ?></h4>
                                </div>
                                <div class="form-group">
                                    <h4>Nis : <?php echo $mur->nis ?></h4>
                                </div>
                                <div class="form-group">
                                    <h4>Kelas : <?php echo $mur->kelas ?></h4>
                                </div>
                                <div class="form-group">
                                    <h4>Jenis Kelamin : <?php echo $mur->jenis_kelamin  ?></h4>
                                </div>
                                <input type="text" class="form-control" name="id_siswa" id="id_siswa" value="<?php echo $mur->id_murid ?>" placeholder="Kode Sub Kriteria" hidden>
                            <?php } ?>
                            <br>
                            <?php
                            $no = 1;
                            $no2 = 1;
                            foreach ($penilaian as $pen) {
                            ?>
                                <div class="form-group">
                                    <h4><?php echo $pen->kode ?></h4>
                                    <div class="col-sm-9 kosong">
                                        <?php echo $pen->deskripsi_kode ?>
                                    </div>
                                    <select class="form-control" name="SK<?php echo $no++ ?>" id="SK<?php echo $no2++ ?>">
                                        <option disabled selected value="">Pilih</option>
                                        <option value="4">Berkembang Sangat Baik</option>
                                        <option value="3">Berkembang Sesuai Harapan</option>
                                        <option value="2">Mulai Berkembang</option>
                                        <option value="1">Belum Berkembang</option>
                                    </select>
                                </div>
                            <?php } ?>
                            <button type="submit" name="submit" value="submit" class="btn btn-primary save">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>



<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        setInterval(function() {
            table.ajax.reload(null, false); // user paging is not reset on reload
        }, 10000);
    });
</script>