<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-lg-3 col-4">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3><?php echo $siswa_sudah_bayar ?></h3>
        <p>Siswa yang sudah Bayar Bulan Ini</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-4">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3><?php echo $jumlah_semua ?></h3>

        <p>Jumlah Keseluruhan Siswa</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-4">
    <!-- small box -->
    <div class="small-box bg-warning">
      <div class="inner">
        <h3><?php echo $jumlah_guru ?></h3>

        <p>Jumlah Keseluruhan Guru</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
    </div>
  </div>
  <!-- ./col -->
  <!-- ./col -->
  <div class="col-lg-3 col-4">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>Rp. <?php echo $pemasukan_bulan_ini ?></h3>

        <p>Jumlah Pemasukan Bulan ini</p>
      </div>
      <div class="icon">
        <i class="fas fa-money-bill-wave"></i>
      </div>
    </div>
  </div>
  <!-- ./col -->

</div>
<!-- /.row -->

<div class="container">
  <div class="row">
    <!-- table 1 -->
    <div class="col-6">
      <div class="card">
        <div class="card-header bg-light">
          <h3 class="card-title"><i class="fa fa-list text-blue"></i> Data Murid Belum Bayar Bulan Ini</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="tbl_murid_blm_bayar" class="table table-bordered table-striped table-hover">
            <thead class="thead-dark">
              <tr class="bg-info">
                <th>No</th>
                <th>Nama Murid</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <!--table 2 -->
    <div class="col-6">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header bg-light">
              <h3 class="card-title"><i class="fa fa-list text-blue"></i> List Murid Sudah Bayar Bulan Ini</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="container-fluid">
                <table id="tbl_sub" class="table table-bordered table-striped table-hover">
                  <thead class="thead-dark">
                    <tr class="bg-info">
                      <th>No</th>
                      <th>Nama Murid</th>
                      <th>Nominal Pembayaran</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->




        </div>
        <!-- /.col -->
      </div>

    </div>

  </div>
</div>

<!-- table 1 -->
<script type="text/javascript">
  var save_method; //for save method string
  var table;

  $(document).ready(function() {

    table = $("#tbl_murid_blm_bayar").DataTable({
      "responsive": true,
      "autoWidth": false,
      "language": {
        "sEmptyTable": "Semua Murid Telah Melakukan Pembayaran Bulan Ini"
      },
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('dashboard/ajax_list') ?>",
        "type": "POST"
      },
      //Set column definition initialisation properties.

    });
    $("input").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });
    $("textarea").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });
    $("select").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });
  });

  function reload_table() {
    table.ajax.reload(null, false); //reload datatable ajax 
  }


  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
</script>

<!-- table 2 -->
<script type="text/javascript">
  var table2;

  $(document).ready(function() {

    //datatables
    table2 = $("#tbl_sub").DataTable({
      "responsive": true,
      "searching": false,
      "autoWidth": false,
      "language": {
        "sEmptyTable": "Data section Belum Ada"
      },
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('Dashboard/ajax_list2') ?>",
        "type": "POST"
      },
      //Set column definition initialisation properties.
      "columnDefs": [{
          "targets": [-1], //last column
          "orderable": false, //set not orderable
        },

      ],
    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });
    $("textarea").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });
    $("select").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
      $(this).removeClass('is-invalid');
    });

  });

  function reload_table() {
    table2.ajax.reload(null, false); //reload datatable ajax 
  }
</script>