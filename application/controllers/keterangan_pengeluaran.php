<?php
defined('BASEPATH') or exit('No direct script access allowed');

class keterangan_pengeluaran extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_keterangan_pengeluaran'));
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('landing');
        } else {
            $idlevel  = $this->session->userdata['id_level'];
            $sub_menu = $this->db->select('a.id_level');
            $sub_menu = $this->db->join('tbl_submenu b', 'a.id_submenu=b.id_submenu');
            $sub_menu = $this->db->join('tbl_userlevel c', 'a.id_level=c.id_level');
            $sub_menu = $this->db->where('a.id_level', $idlevel);
            $sub_menu = $this->db->where('a.view_level', 'Y');
            $sub_menu = $this->db->where('a.id_submenu', '28');
            $sub_menu = $this->db->get('tbl_akses_submenu a');
            $results = $sub_menu->row();
            if ($results->id_level == $idlevel) {
                $this->load->helper('url');
                $this->template->load('layoutbackend', 'admin/keterangan_pengeluaran');
            } else {
                redirect('errors');
            }
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_keterangan_pengeluaran->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $no; //array 0
            $row[] = $pel->nama_keterangan; //array 1
            $row[] = $pel->updated_at; //array 2
            $row[] = $pel->id_keterangan; //array 3
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_keterangan_pengeluaran->count_all(),
            "recordsFiltered" => $this->Mod_keterangan_pengeluaran->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // $this->_validate();
        $nama_keterangan = $this->input->post('nama_keterangan');
        $id_user = $this->session->userdata['id_user'];
        $save  = array(
            'nama_keterangan' => $nama_keterangan,
            'id_user' => $id_user,
        );
        $this->Mod_keterangan_pengeluaran->insert_section("tbl_keterangan", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        // $this->_validate();
        $nama_keterangan = $this->input->post('nama_keterangan');
        $id_user = $this->session->userdata['id_user'];
        $id      = $this->input->post('id');
        $save  = array(
            'nama_keterangan' => $nama_keterangan,
            'id_user' => $id_user,
        );
        $this->Mod_keterangan_pengeluaran->update_section($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_sub($id)
    {
        $data = $this->Mod_keterangan_pengeluaran->get_section($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_keterangan_pengeluaran->delete_section($id, 'tbl_keterangan_pengeluaran');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('kode') == '') {
            $data['inputerror'][] = 'kode';
            $data['error_string'][] = 'Kode Sub Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($this->input->post('deskripsi_kode') == '') {
            $data['inputerror'][] = 'deskripsi_kode';
            $data['error_string'][] = 'Deskripsi Kode Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
