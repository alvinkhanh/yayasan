<?php
defined('BASEPATH') or exit('No direct script access allowed');


class input_pengeluaran extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_input_pengeluaran'));
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('landing');
        } else {
            $idlevel  = $this->session->userdata['id_level'];
            $sub_menu = $this->db->select('a.id_level');
            $sub_menu = $this->db->join('tbl_submenu b', 'a.id_submenu=b.id_submenu');
            $sub_menu = $this->db->join('tbl_userlevel c', 'a.id_level=c.id_level');
            $sub_menu = $this->db->where('a.id_level', $idlevel);
            $sub_menu = $this->db->where('a.view_level', 'Y');
            $sub_menu = $this->db->where('a.id_submenu', '27');
            $sub_menu = $this->db->get('tbl_akses_submenu a');
            $results = $sub_menu->row();
            if ($results->id_level == $idlevel) {
                $this->load->helper('url');
                $data['user'] = $this->Mod_input_pengeluaran->user();
                $data['bulan'] = $this->Mod_input_pengeluaran->bulan();
                $data['keterangan'] = $this->Mod_input_pengeluaran->keterangan();
                $data['periode'] = $this->Mod_input_pengeluaran->periode();
                $this->template->load('layoutbackend', 'user/input_pengeluaran', $data);
            } else {
                redirect('errors');
            }
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_input_pengeluaran->get_datatables();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $no; //array 0
            $row[] = $pel->created_at . ' Wib'; //array 1
            $row[] = $pel->nama_keterangan; //array 2
            $row[] = $pel->pengeluaran; //array 2
            $row[] = $pel->full_name; //array 4
            $row[] = $pel->id_pengeluaran; //array 5
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_input_pengeluaran->count_all(),
            "recordsFiltered" => $this->Mod_input_pengeluaran->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function get_data()
    {
        $periode = $this->input->post('periode');
        $data = $this->Mod_input_pengeluaran->data_bulan($periode);
        echo json_encode($data);
    }



    public function insert()
    {
        // $this->_validate();
        $keterangan     = $this->input->post('keterangan');
        $pengeluaran    = $this->input->post('pengeluaran');
        $bulan          = date('m');
        $id_user        = $this->session->userdata['id_user'];
        $category       = 'B';

        $save  = array(
            'id_keterangan' => $keterangan,
            'id_user'       => $id_user,
            'pengeluaran'   => $pengeluaran,
            'bulan'         => $bulan,
        );
        $this->Mod_input_pengeluaran->insert_pembayaran("tbl_pengeluaran", $save);

        $insertid = $this->db->insert_id();
        $save2  = array(
            'id_peng_pem'   => $insertid,
            'id_user'       => $id_user,
            'category'      => $category,
        );
        $this->Mod_input_pengeluaran->insert_pembayaran("tbl_kegiatan", $save2);

        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('kode') == '') {
            $data['inputerror'][] = 'kode';
            $data['error_string'][] = 'Kode Sub Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($this->input->post('deskripsi_kode') == '') {
            $data['inputerror'][] = 'deskripsi_kode';
            $data['error_string'][] = 'Deskripsi Kode Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
