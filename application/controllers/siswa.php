<?php
defined('BASEPATH') or exit('No direct script access allowed');

class siswa extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_siswa'));
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('landing');
        } else {
            $idlevel  = $this->session->userdata['id_level'];
            $sub_menu = $this->db->select('a.id_level');
            $sub_menu = $this->db->join('tbl_submenu b', 'a.id_submenu=b.id_submenu');
            $sub_menu = $this->db->join('tbl_userlevel c', 'a.id_level=c.id_level');
            $sub_menu = $this->db->where('a.id_level', $idlevel);
            $sub_menu = $this->db->where('a.view_level', 'Y');
            $sub_menu = $this->db->where('a.id_submenu', '25');
            $sub_menu = $this->db->get('tbl_akses_submenu a');
            $results = $sub_menu->row();
            if ($results->id_level == $idlevel) {
                $this->load->helper('url');
                $data['guru'] = $this->Mod_siswa->guru();
                $this->template->load('layoutbackend', 'admin/siswa', $data);
            } else {
                redirect('errors');
            }
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_siswa->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $aktif = '<button type="button" class="btn btn-success">Masih Bersekolah</button>';
        $keluar = '<button type="button" class="btn btn-danger">Sudah Keluar</button>';
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->image;
            $row[] = $pel->nama_murid; //array 1
            $row[] = $pel->nis; //array 2
            $row[] = $pel->kelas; //array 3
            $row[] = $pel->tanggal_lahir; //array 4
            $row[] = $pel->jenis_kelamin; //array 5
            $row[] = $pel->tahun_ajaran; //array 6
            $row[] = $pel->nama_guru; //array 8
            if ($pel->status_aktif = 'Y') {
                $row[] = $aktif; //array 7
            } else {
                $row[] = $keluar; //array 7
            }
            $row[] = $pel->id_murid; //array 9
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_siswa->count_all(),
            "recordsFiltered" => $this->Mod_siswa->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $username = $this->input->post('nis');
        $cek = $this->Mod_siswa->cekUsername($username);
        $murid = $this->input->post('nama_murid');
        $nis = $this->input->post('nis');
        $kelas = $this->input->post('kelas');
        $ttl = $this->input->post('ttl');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tahun_ajaran = $this->input->post('tahun_ajaran');
        $guru = $this->input->post('guru');
        $status_aktif = $this->input->post('status_aktif');

        if ($cek->num_rows() > 0) {
            echo json_encode(array("error" => "nis Sudah Ada!!"));
        } else {
            $nama = slug($this->input->post('nama_murid'));
            $config['upload_path']   = './assets/foto/user/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png'; //mencegah upload backdor
            $config['max_size']      = '1000';
            $config['max_width']     = '2000';
            $config['max_height']    = '1024';
            $config['file_name']     = $nama;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('imagefile')) {
                $gambar = $this->upload->data();

                $save  = array(
                    'nama_murid' => $murid,
                    'nis' => $nis,
                    'kelas'  => $kelas,
                    'tanggal_lahir'  => $ttl,
                    'jenis_kelamin'  => $jenis_kelamin,
                    'tahun_ajaran'  => $tahun_ajaran,
                    'status_aktif' => $status_aktif,
                    'guru'  => $guru,
                    'image' => $gambar['file_name']
                );

                $this->Mod_siswa->insert_murid("tbl_murid", $save);
                echo json_encode(array("status" => TRUE));
            } else { //Apabila tidak ada gambar yang di upload
                $save  = array(
                    'nama_murid' => $murid,
                    'nis' => $nis,
                    'kelas'  => $kelas,
                    'tanggal_lahir'  => $ttl,
                    'jenis_kelamin'  => $jenis_kelamin,
                    'tahun_ajaran'  => $tahun_ajaran,
                    'guru'  => $guru,
                    'status_aktif' => $status_aktif,
                );
                $this->Mod_siswa->insert_murid("tbl_murid", $save);
                echo json_encode(array("status" => TRUE));
            }
        }
    }


    public function update()
    {
        $this->_validate();
        $id = $this->input->post('id_murid');
        $murid = $this->input->post('nama_murid');
        $nis = $this->input->post('nis');
        $status_aktif = $this->input->post('status_aktif');
        $kelas = $this->input->post('kelas');
        $ttl = $this->input->post('ttl');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $guru = $this->input->post('guru');
        $tahun_ajaran = $this->input->post('tahun_ajaran');

        $save = array(
            'nama_murid' => $murid,
            'nis' => $nis,
            'kelas' => $kelas,
            'tanggal_lahir' => $ttl,
            'jenis_kelamin' => $jenis_kelamin,
            'tahun_ajaran' => $tahun_ajaran,
            'status_aktif' => $status_aktif,
            'guru' => $guru
        );

        if (!empty($_FILES['imagefile']['name'])) {
            $nama = slug($this->input->post('username'));
            $config['upload_path'] = './assets/foto/user/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png'; //mencegah upload backdor
            $config['max_size'] = '1000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1024';
            $config['file_name'] = $nama;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('imagefile')) {
                $existingImage = $this->Mod_siswa->getImage($id)->row_array();

                if (!empty($existingImage['image'])) {
                    $imagePath = './assets/foto/user/' . $existingImage['image'];
                    unlink($imagePath);
                }

                $gambar = $this->upload->data();
                $save['image'] = $gambar['file_name'];
            }
        }

        $this->Mod_siswa->update_murid($id, $save);
        echo json_encode(array("status" => true));
    }



    public function edit_murid($id)
    {
        $data = $this->Mod_siswa->get_murid($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_siswa->delete_murid($id, 'tbl_murid');
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_murid') == '') {
            $data['inputerror'][] = 'nama_murid';
            $data['error_string'][] = 'Nama murid Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($this->input->post('nis') == '') {
            $data['inputerror'][] = 'nis';
            $data['error_string'][] = 'nis Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('kelas') == '') {
            $data['inputerror'][] = 'kelas';
            $data['error_string'][] = 'Kelas Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('ttl') == '') {
            $data['inputerror'][] = 'ttl';
            $data['error_string'][] = 'Tempat & tanggal lahir Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('jenis_kelamin') == '') {
            $data['inputerror'][] = 'jenis_kelamin';
            $data['error_string'][] = 'Jenis Kelamin Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('tahun_ajaran') == '') {
            $data['inputerror'][] = 'tahun_ajaran';
            $data['error_string'][] = 'Tahun Ajaran Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('guru') == '') {
            $data['inputerror'][] = 'guru';
            $data['error_string'][] = 'Guru Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('status_aktif') == '') {
            $data['inputerror'][] = 'status_aktif';
            $data['error_string'][] = 'Status Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
