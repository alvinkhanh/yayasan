<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_dashboard'));
        $this->load->model(array('Mod_dashboard2'));
        $this->load->library('fungsi');
        $this->load->library('user_agent');
        $this->load->helper('myfunction_helper');
        // backButtonHandle();
    }

    function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('login');
        } else {
            $data['jumlah'] = $this->Mod_dashboard->jumlah_siswa();
            $data['jumlah_semua'] = $this->Mod_dashboard->jumlah_semua_siswa();
            $data['jumlah_guru'] = $this->Mod_dashboard->jumlah_guru();
            $data['siswa_sudah_bayar'] = $this->Mod_dashboard->siswa_sudah_bayar();
            $data['pemasukan_bulan_ini'] = $this->Mod_dashboard->pemasukan_bulan_ini();
            $this->template->load('layoutbackend', 'dashboard/dashboard_data', $data);
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_dashboard->get_datatables();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $no; //array 0
            $row[] = $pel->nama_murid; //array 1
            $row[] = $pel->id_murid; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_dashboard->count_all(),
            "recordsFiltered" => $this->Mod_dashboard->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list2()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_dashboard2->get_datatables();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $no; //array 0
            $row[] = $pel->nama_murid; //array 1
            $row[] = $pel->pembayaran; //array 1
            $row[] = $pel->id_murid; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_dashboard2->count_all(),
            "recordsFiltered" => $this->Mod_dashboard2->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}
/* End of file Controllername.php */
