<?php
defined('BASEPATH') or exit('No direct script access allowed');

class guru extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_guru'));
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('landing');
        } else {
            $idlevel  = $this->session->userdata['id_level'];
            $sub_menu = $this->db->select('a.id_level');
            $sub_menu = $this->db->join('tbl_submenu b', 'a.id_submenu=b.id_submenu');
            $sub_menu = $this->db->join('tbl_userlevel c', 'a.id_level=c.id_level');
            $sub_menu = $this->db->where('a.id_level', $idlevel);
            $sub_menu = $this->db->where('a.view_level', 'Y');
            $sub_menu = $this->db->where('a.id_submenu', '25');
            $sub_menu = $this->db->get('tbl_akses_submenu a');
            $results = $sub_menu->row();
            if ($results->id_level == $idlevel) {
                $this->load->helper('url');
                $data['guru'] = $this->Mod_guru->guru();
                $this->template->load('layoutbackend', 'admin/guru', $data);
            } else {
                redirect('errors');
            }
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_guru->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $mengajar = '<button type="button" class="btn btn-success">Masih Mengejar</button>';
        $keluar = '<button type="button" class="btn btn-danger">Sudah Keluar</button>';
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->gambar; //array 0 
            $row[] = $pel->nama_guru; //array 1
            $row[] = $pel->ttl; //array 2
            $row[] = $pel->jenkel; //array 3
            $row[] = $pel->jam_mengajar; //array 4
            if ($pel->status == 'Y') {
                $row[] = $mengajar; //array 5
            } else {
                $row[] = $keluar; //array 5
            }
            $row[] = $pel->id_guru; //array 6
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_guru->count_all(),
            "recordsFiltered" => $this->Mod_guru->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // $this->_validate();
        $guru = $this->input->post('nama_guru');
        $ttl = $this->input->post('ttl');
        $jenkel = $this->input->post('jenis_kelamin');
        $jam_mengajar = $this->input->post('jam_mengajar');
        $status = $this->input->post('status');
        $id_user = $this->session->userdata('id_user'); // Changed array access to function call

        $nama = slug($guru);
        $config['upload_path']   = './assets/foto/guru/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        // $config['max_size'] = 1000; // Removed quotes around numeric values
        // $config['max_width'] = 2000;
        // $config['max_height'] = 1024;
        $config['file_name'] = $nama;

        $this->upload->initialize($config);

        if ($this->upload->do_upload('imagefile')) {
            $gambar = $this->upload->data();

            $save = array(
                'nama_guru' => $guru,
                'ttl' => $ttl,
                'jenkel' => $jenkel,
                'jam_mengajar' => $jam_mengajar,
                'status' => $status,
                'gambar' => $gambar['file_name'],
                'id_user' => $id_user,
            );

            $this->Mod_guru->insert_guru("tbl_guru", $save);
            echo json_encode(array("status" => true)); // Changed TRUE to true
        } else {
            // Apabila tidak ada gambar yang di upload
            $save = array(
                'nama_guru' => $guru,
                'ttl' => $ttl,
                'jenkel' => $jenkel,
                'jam_mengajar' => $jam_mengajar,
                'status' => $status,
                'id_user' => $id_user,
            );
            $this->Mod_guru->insert_guru("tbl_guru", $save);
            echo json_encode(array("status" => true)); // Changed TRUE to true
        }
    }

    public function update()
    {
        // $this->_validate();
        $id = $this->input->post('id_guru');
        $guru = $this->input->post('nama_guru');
        $ttl = $this->input->post('ttl');
        $jenkel = $this->input->post('jenis_kelamin');
        $jam_mengajar = $this->input->post('jam_mengajar');
        $status = $this->input->post('status');

        $save = array(
            'nama_guru' => $guru,
            'ttl' => $ttl,
            'jenkel' => $jenkel,
            'jam_mengajar' => $jam_mengajar,
            'status' => $status
        );

        if (!empty($_FILES['imagefile']['name'])) {
            $nama = slug($guru);
            $config['upload_path'] = './assets/foto/guru';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1024';
            $config['file_name'] = $nama;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('imagefile')) {
                $existingImage = $this->Mod_guru->getImage($id)->row_array();

                if (!empty($existingImage['gambar'])) {
                    $imagePath = './assets/foto/guru/' . $existingImage['gambar'];
                    unlink($imagePath);
                }

                $gambar = $this->upload->data();
                $save['gambar'] = $gambar['file_name'];
            }
        }

        $this->Mod_guru->update_guru($id, $save);
        echo json_encode(array("status" => true));
    }




    public function edit_guru($id)
    {
        $data = $this->Mod_guru->get_guru($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_guru->delete_guru($id, 'tbl_guru');
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_guru') == '') {
            $data['inputerror'][] = 'nama_guru';
            $data['error_string'][] = 'Nama guru Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('ttl') == '') {
            $data['inputerror'][] = 'ttl';
            $data['error_string'][] = 'Tempat & tanggal lahir Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('jenkel') == '') {
            $data['inputerror'][] = 'jenkel';
            $data['error_string'][] = 'Jenis Kelamin Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('jam_mengajar') == '') {
            $data['inputerror'][] = 'jam_mengajar';
            $data['error_string'][] = 'Jam Mengajar Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }
        if ($this->input->post('status') == '') {
            $data['inputerror'][] = 'status';
            $data['error_string'][] = 'Status Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
