<?php
defined('BASEPATH') or exit('No direct script access allowed');


class hasil extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_hasil'));
    }

    public function index()
    {
        $this->load->helper('url');
        $data['ppa'] = $this->Mod_hasil->hasil();
        $this->template->load('layoutbackend', 'user/hasil', $data);
    }
}
