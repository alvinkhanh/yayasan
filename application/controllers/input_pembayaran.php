<?php
defined('BASEPATH') or exit('No direct script access allowed');


class input_pembayaran extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_input_pembayaran'));
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('landing');
        } else {
            $idlevel  = $this->session->userdata['id_level'];
            $sub_menu = $this->db->select('a.id_level');
            $sub_menu = $this->db->join('tbl_submenu b', 'a.id_submenu=b.id_submenu');
            $sub_menu = $this->db->join('tbl_userlevel c', 'a.id_level=c.id_level');
            $sub_menu = $this->db->where('a.id_level', $idlevel);
            $sub_menu = $this->db->where('a.view_level', 'Y');
            $sub_menu = $this->db->where('a.id_submenu', '25');
            $sub_menu = $this->db->get('tbl_akses_submenu a');
            $results = $sub_menu->row();
            if ($results->id_level == $idlevel) {
                $this->load->helper('url');
                $data['guru'] = $this->Mod_input_pembayaran->guru();
                $data['siswa'] = $this->Mod_input_pembayaran->siswa();
                $data['murid'] = $this->Mod_input_pembayaran->murid();
                $data['periode'] = $this->Mod_input_pembayaran->periode();
                $this->template->load('layoutbackend', 'user/input_pembayaran', $data);
            } else {
                redirect('errors');
            }
        }
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_input_pembayaran->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $months = array('january', 'february', 'maret', 'april', 'mei', 'juni', 'july', 'agustus', 'september', 'oktober', 'november', 'desember');

        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $no; //array 0
            $row[] = $pel->nama_murid; //array 1
            $row[] = $pel->nama_guru; //array 2
            $row[] = $pel->jam_mengajar . ' Wib'; //array 2

            foreach ($months as $month) {
                $value = $pel->$month ?? 0;
                $row[] = 'Rp' . number_format($value, 2); // add 'Rp' and format the value
            }

            $row[] = $pel->full_name; //array 4
            $row[] = $pel->id_pembayaran; //array 5
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_input_pembayaran->count_all(),
            "recordsFiltered" => $this->Mod_input_pembayaran->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function insert()
    {
        // $this->_validate();
        $id_siswa       = $this->input->post('id_siswa');
        $pembayaran     = $this->input->post('pembayaran');
        $bulan          = date('m');
        $id_user        = $this->session->userdata['id_user'];
        $category       = 'A';

        $save  = array(
            'id_siswa'      => $id_siswa,
            'id_user'       => $id_user,
            'pembayaran'    => $pembayaran,
            'bulan'         => $bulan,
        );
        $this->Mod_input_pembayaran->insert_pembayaran("tbl_pembayaran", $save);

        $insertid = $this->db->insert_id();
        $save2  = array(
            'id_peng_pem'   => $insertid,
            'id_user'       => $id_user,
            'category'      => $category,

        );
        $this->Mod_input_pembayaran->insert_pembayaran("tbl_kegiatan", $save2);

        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('kode') == '') {
            $data['inputerror'][] = 'kode';
            $data['error_string'][] = 'Kode Sub Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($this->input->post('deskripsi_kode') == '') {
            $data['inputerror'][] = 'deskripsi_kode';
            $data['error_string'][] = 'Deskripsi Kode Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
