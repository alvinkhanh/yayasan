<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_ppa extends CI_Model
{
    public function ppa()
    {
        $id_user = $this->session->userdata('id_user');
        $this->db->select('a.*, b.nama_status');
        $this->db->join('tbl_status b', 'a.status=b.id_status');
        // $this->db->where('a.guru', $id_user);
        $this->db->where('a.status_aktif', 'Y');
        $this->db->order_by('id_murid DESC');
        return $this->db->get("tbl_murid a")
            ->result();
    }

    public function murid($id_murid)
    {
        $id_user = $this->session->userdata('id_user');
        $this->db->select('a.*, b.nama_status, c.id_ppa, c.SK1, c.SK2, c.SK3, c.SK4, c.SK5, c.total, d.guru, e.full_name');
        $this->db->join('tbl_status b', 'a.status=b.id_status');
        $this->db->join('tbl_ppa c', 'a.id_murid=c.id_siswa');
        $this->db->join('tbl_murid d', 'a.id_murid=d.id_murid');
        $this->db->join('tbl_user e', 'd.guru=e.id_user');
        // $this->db->where('a.guru', $id_user);
        $this->db->where('a.status_aktif', 'Y');
        $this->db->where('a.id_murid', $id_murid);
        $this->db->order_by('a.id_murid DESC');
        return $this->db->get("tbl_murid a")
            ->result();
    }

    public function nilai_murid($id_murid)
    {
        $id_user = $this->session->userdata('id_user');
        $this->db->select('a.*,');
        // $this->db->where('a.guru', $id_user);
        $this->db->where('a.status_aktif', 'Y');
        $this->db->where('a.id_murid', $id_murid);
        $this->db->order_by('a.id_murid DESC');
        return $this->db->get("tbl_murid a")
            ->result();
    }



    public function penilaian()
    {
        $this->db->order_by('id_sub ASC');
        return $this->db->get("tbl_sub_kriteria a")
            ->result();
    }

    function save($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $insert;
    }

    function update_section($id, $data)
    {
        $this->db->where('id_ppa', $id);
        $this->db->update('tbl_ppa', $data);
    }

    function update_status_murid($id, $data)
    {
        $this->db->where('id_murid', $id);
        $this->db->update('tbl_murid', $data);
    }


    function detail_ppa($murid)
    {
        $this->db->where('id_siswa', $murid);
        return $this->db->order_by('id_ppa ASC')
            ->get('tbl_ppa')
            ->result();
    }

    function all_murid($murid_id)
    {
        $murids = $this->db->order_by('total DESC')
            ->get('tbl_ppa')
            ->result();
        $ranking = 0;
        foreach ($murids as $i => $value) {
            if ($murid_id == $value->id_siswa) {
                $ranking = $i + 1;
            }
        }
        return $ranking;
    }


    function perbandingan_bobot()
    {
        return $this->db->order_by('id_bobot ASC')
            ->get('tbl_bobot')
            ->result();
    }


    function total_bobot_1()
    {
        $query = $this->db->select_sum('nilai', 'nilai');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai;
    }

    function total_bobot_2()
    {
        $query = $this->db->select_sum('nilai2', 'nilai2');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai2;
    }

    function total_bobot_3()
    {
        $query = $this->db->select_sum('nilai3', 'nilai3');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai3;
    }

    function total_bobot_4()
    {
        $query = $this->db->select_sum('nilai4', 'nilai4');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai4;
    }
}
