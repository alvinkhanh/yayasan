<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_siswa extends CI_Model
{
    var $table = 'tbl_murid a';
    var $column_search = array('a.id_murid', 'a.nama_murid', 'a.nis', 'a.kelas', 'a.tanggal_lahir', 'a.jenis_kelamin', 'a.tahun_ajaran', 'a.status', 'a.guru', 'a.status_aktif', 'a.created_at', 'b.nama_guru');
    var $column_order = array('id_murid', 'nama_murid', 'nis', 'kelas', 'tanggal_lahir', 'jenis_kelamin', 'tahun_ajaran', 'status', 'guru', 'status_aktif', 'created_at', 'nama_guru', null);
    var $order = array('id_murid' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($term = '')
    {
        $this->db->select('a.*,b.nama_guru');
        $this->db->join('tbl_guru b', 'a.guru=b.id_guru');
        $this->db->from('tbl_murid a');

        $i = 0;

        foreach ($this->column_search as $murid) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($murid, $_POST['search']['value']);
                } else {
                    $this->db->or_like($murid, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_murid');
        return $this->db->count_all_results();
    }

    function cekUsername($username)
    {
        $this->db->where("nis", $username);
        return $this->db->get("tbl_murid");
    }


    function insert_murid($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $insert;
    }

    function update_murid($id, $data)
    {
        $this->db->where('id_murid', $id);
        $this->db->update('tbl_murid', $data);
    }

    function get_murid($id)
    {
        $this->db->where('id_murid', $id);
        return $this->db->get('tbl_murid')->row();
    }

    function getImage($id)
    {
        return $this->db
            ->select('image')
            ->from('tbl_murid')
            ->where('id_murid', $id)
            ->get();
    }


    function delete_murid($id, $table)
    {
        $this->db->where('id_murid', $id);
        $this->db->delete($table);
    }

    function guru()
    {
        $this->db->where('status', 'Y');
        return $this->db->order_by('nama_guru ASC')
            ->get('tbl_guru')
            ->result();
    }
}
