<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_input_pengeluaran extends CI_Model
{
    var $table = 'list_pengeluaran';
    var $column_search = array('id_pengeluaran', 'created_at', 'nama_keterangan', 'pengeluaran', 'bulan', NULL);
    var $column_order = array('id_pengeluaran', 'created_at', 'nama_keterangan', 'pengeluaran', 'bulan', NULL);
    var $order = array('id_pengeluaran' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('periode')) {
            $this->db->where('periode', $this->input->post('periode'));
        }

        if ($this->input->post('bulan')) {
            $this->db->where('bulan', $this->input->post('bulan'));
        }

        if ($this->input->post('full_name')) {
            $this->db->where('full_name', $this->input->post('full_name'));
        }

        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('list_pengeluaran');
        return $this->db->count_all_results();
    }

    function insert_pembayaran($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $insert;
    }

    function get_kriteria($id)
    {
        $this->db->where('id_pengeluaran', $id);
        return $this->db->get('list_pengeluaran')->row();
    }

    function delete_kriteria($id, $table)
    {
        $this->db->where('id_pengeluaran', $id);
        $this->db->delete($table);
    }

    function keterangan()
    {
        $this->db->select('id_keterangan, nama_keterangan');
        $this->db->group_by('id_keterangan, nama_keterangan');
        return $this->db->order_by('id_keterangan ASC')
            ->get('tbl_keterangan')
            ->result();
    }

    function periode()
    {
        $this->db->group_by('periode');
        return $this->db->order_by('periode ASC')
            ->get('list_pengeluaran')
            ->result();
    }

    function bulan()
    {
        $this->db->group_by('bulan');
        return $this->db->order_by('bulan ASC')
            ->get('list_pengeluaran')
            ->result();
    }

    function user()
    {
        $this->db->group_by('full_name');
        return $this->db->order_by('full_name ASC')
            ->get('list_pengeluaran')
            ->result();
    }

    function data_bulan($periode)
    {
        $this->db->select('bulan');
        $this->db->where('periode', $periode);
        $this->db->group_by('bulan');
        $this->db->order_by('bulan', 'ASC');
        return $this->db->from('list_pengeluaran')
            ->get()
            ->result();
    }
}
