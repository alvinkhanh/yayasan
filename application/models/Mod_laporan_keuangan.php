<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_laporan_keuangan extends CI_Model
{
    var $table = 'laporan_3';
    var $column_search = array('id_kegiatan', 'keterangan', 'pemasukan', 'pengeluaran', 'created_at', NULL);
    var $column_order =  array('id_kegiatan', 'keterangan', 'pemasukan', 'pengeluaran', 'created_at', NULL);
    var $order = array('id_kegiatan' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('periode')) {
            $this->db->where('periode', $this->input->post('periode'));
        }

        if ($this->input->post('bulan')) {
            $this->db->where('bulan', $this->input->post('bulan'));
        }

        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('laporan_3');
        return $this->db->count_all_results();
    }

    function insert_pembayaran($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $insert;
    }

    function get_kriteria($id)
    {
        $this->db->where('id_kegiatan', $id);
        return $this->db->get('laporan_3')->row();
    }

    function delete_kriteria($id, $table)
    {
        $this->db->where('id_kegiatan', $id);
        $this->db->delete($table);
    }

    function keterangan()
    {
        $this->db->select('id_keterangan, nama_keterangan');
        $this->db->group_by('id_keterangan, nama_keterangan');
        return $this->db->order_by('id_keterangan ASC')
            ->get('tbl_keterangan')
            ->result();
    }

    function periode()
    {
        $this->db->group_by('periode');
        return $this->db->order_by('periode ASC')
            ->get('laporan_3')
            ->result();
    }

    function bulan()
    {
        $this->db->group_by('bulan');
        return $this->db->order_by('bulan ASC')
            ->get('laporan_3')
            ->result();
    }


    function data_bulan($periode)
    {
        $this->db->select('bulan');
        $this->db->where('periode', $periode);
        $this->db->group_by('bulan');
        $this->db->order_by('bulan', 'ASC');
        return $this->db->from('laporan_3')
            ->get()
            ->result();
    }

    public function pemasukan()
    {
        $format = '%Y-%m';
        $yearmonth = date('Y-m');
        $this->db->select('SUM(pembayaran) as total_pemasukan');
        $this->db->where('DATE_FORMAT(created_at, "' . $format . '") =', $yearmonth);
        $this->db->from('tbl_pembayaran'); // TABLE NAME
        $result = $this->db->get()->row();

        if ($result) {
            return $result->total_pemasukan;
        } else {
            return 0; // If there are no records, return 0 as the total sum.
        }
    }

    public function pengeluaran()
    {
        $month = date('m');
        $this->db->select('SUM(pengeluaran) as pengeluaran');
        $this->db->where('bulan =', $month);
        $this->db->from('laporan_3'); // TABLE NAME
        $result = $this->db->get()->row();

        if ($result) {
            return $result->pengeluaran;
        } else {
            return 0; // If there are no records, return 0 as the total sum.
        }
    }


    public function past_saldo()
    {
        $format = '%Y-%m';
        $month = date('m') - 1;
        $this->db->select('saldo as past_saldo');
        $this->db->where('bulan =', $month);
        $this->db->from('laporan_3'); // TABLE NAME
        $this->db->order_by('id_kegiatan', 'desc'); // TABLE NAME
        $result = $this->db->get()->row();

        if ($result) {
            return $result->past_saldo;
        } else {
            return 0; // If there are no records, return 0 as the total sum.
        }
    }

    public function now_saldo()
    {
        $format = '%Y-%m';
        $month = date('m');
        $this->db->select('saldo as now_saldo');
        $this->db->where('bulan =', $month);
        $this->db->from('laporan_3'); // TABLE NAME
        $this->db->order_by('id_kegiatan', 'desc'); // TABLE NAME
        $result = $this->db->get()->row();

        if ($result) {
            return $result->now_saldo;
        } else {
            return 0; // If there are no records, return 0 as the total sum.
        }
    }
}
