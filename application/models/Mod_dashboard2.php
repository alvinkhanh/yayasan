<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_dashboard2 extends CI_Model
{
    var $table = 'tbl_murid a';
    var $column_search = array('a.id_murid', 'a.nama_murid', 'b.pembayaran');
    var $column_order = array('id_murid', 'nama_murid', 'pembayaran');
    var $order = array('a.id_murid');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $format = '%Y-%m';
        $year = date('Y');
        $yearmonth = date('Y-m');

        $this->db->select('a.id_murid, a.nama_murid, b.pembayaran');
        $this->db->from($this->table);
        $this->db->join('tbl_pembayaran b', 'a.id_murid = b.id_siswa');
        $this->db->where('a.status_aktif', 'Y');
        $this->db->where('DATE_FORMAT(b.created_at, "' . $format . '") =', $yearmonth);
        $this->db->where('a.id_murid IN (SELECT id_siswa FROM list_pembayaran WHERE periode = ' . $year . ')', NULL, false);
        $this->db->order_by('a.nama_murid', 'ASC');

        $i = 0;
        $searchValue = isset($_POST['search']['value']) ? $_POST['search']['value'] : '';

        foreach ($this->column_search as $item) // loop through columns
        {
            if ($searchValue) // if datatable sends POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. Query WHERE with OR clause is better with bracket, as it can be combined with other WHERE conditions using AND.
                    $this->db->like($item, $searchValue);
                } else {
                    $this->db->or_like($item, $searchValue);
                }

                if (count($this->column_search) - 1 == $i) // last loop
                    $this->db->group_end(); // close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // handle ordering
        {
            $columnIndex = $_POST['order']['0']['column'];
            $columnDir = $_POST['order']['0']['dir'];
            $column = $this->column_order[$columnIndex];

            $this->db->order_by($column, $columnDir);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_pembayaran');
        return $this->db->count_all_results();
    }

    public function jumlah_siswa()
    {
        $this->db->where('status =', '2'); // OTHER CONDITIONS IF ANY
        $this->db->from('tbl_murid'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function jumlah_semua_siswa()
    {
        $this->db->from('tbl_murid'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function jumlah_guru()
    {
        $this->db->where('status =', 'Y'); // OTHER CONDITIONS IF ANY
        $this->db->from('tbl_guru'); //TABLE NAME
        return $this->db->count_all_results();
    }
}
