<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_hasil extends CI_Model
{
    public function hasil()
    {
        $id_user = $this->session->userdata('id_user');
        $this->db->select('a.*, b.nama_status, c.total, c.SK1, c.SK2, c.SK3, c.SK4, c.SK5');
        $this->db->join('tbl_status b', 'a.status=b.id_status');
        $this->db->join('tbl_ppa c', 'a.id_murid=c.id_siswa');
        // $this->db->where('a.guru', $id_user);
        $this->db->where('a.status_aktif', 'Y');
        $this->db->where('a.status', 2);
        $this->db->order_by('c.total DESC');
        return $this->db->get("tbl_murid a")
            ->result();
    }
}
