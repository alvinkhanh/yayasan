<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_input_pembayaran extends CI_Model
{
    var $table = 'tbl_murid a';
    var $column_search = array('b.id_pembayaran', 'a.nama_murid', 'd.nama_guru', 'd.jam_mengajar', 'c.full_name', 'b.january', 'b.february', 'b.maret', 'b.april', 'b.mei', 'b.juni', 'b.july', 'b.agustus', 'b.september', 'b.oktober', 'b.november', 'b.desember');
    var $column_order = array('id_pembayaran', 'nama_murid', 'nama_guru', 'jam_mengajar', 'full_name', 'january', 'february', 'maret', 'april', 'mei', 'juni', 'july', 'agustus', 'september', 'oktober', 'november', 'desember', NULL);
    var $order = array('b.id_pembayaran' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('periode')) {
            $this->db->where('b.periode', $this->input->post('periode'));
        }

        if ($this->input->post('nama_siswa')) {
            $this->db->where('a.nama_murid', $this->input->post('nama_siswa'));
        }

        if ($this->input->post('nama_guru')) {
            $this->db->where('d.nama_guru', $this->input->post('nama_guru'));
        }


        $this->db->select('b.*,a.nama_murid, c.full_name, d.nama_guru, d.jam_mengajar');
        $this->db->join('list_pembayaran b', 'a.id_murid=b.id_siswa', 'left');
        $this->db->join('tbl_user c', 'b.id_user=c.id_user', 'left');
        $this->db->join('tbl_guru d', 'a.guru=d.id_guru', 'left');
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_pembayaran');
        return $this->db->count_all_results();
    }

    function insert_pembayaran($table, $data)
    {
        $insert = $this->db->insert($table, $data);
        return $insert;
    }

    function get_kriteria($id)
    {
        $this->db->where('id_pembayaran', $id);
        return $this->db->get('tbl_pembayaran')->row();
    }

    function delete_kriteria($id, $table)
    {
        $this->db->where('id_pembayaran', $id);
        $this->db->delete($table);
    }

    function murid()
    {
        $year = date('Y');
        $this->db->select('id_murid, nama_murid');
        $this->db->from('tbl_murid');
        $this->db->where('status_aktif', 'Y');
        $this->db->where('id_murid NOT IN (SELECT id_siswa FROM list_pembayaran WHERE periode = ' . $year . ')', NULL, false);
        $this->db->order_by('nama_murid', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function periode()
    {
        $this->db->group_by('periode');
        return $this->db->order_by('periode ASC')
            ->get('list_pembayaran')
            ->result();
    }

    function siswa()
    {
        $this->db->select('id_murid, nama_murid');
        $this->db->where('status_aktif', 'Y');
        $this->db->group_by('id_murid,nama_murid');
        return $this->db->order_by('id_murid ASC')
            ->get('tbl_murid')
            ->result();
    }

    function guru()
    {
        $this->db->select('id_guru, nama_guru');
        $this->db->where('status', 'Y');
        $this->db->group_by('id_guru, nama_guru');
        return $this->db->order_by('id_guru ASC')
            ->get('tbl_guru')
            ->result();
    }
}
