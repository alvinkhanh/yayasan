<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_nilai_perhitungan extends CI_Model
{
    var $table = 'tbl_bobot a';
    var $column_search = array('a.id_bobot', 'a.kode_bobot', 'a.nama_bobot', 'a.nilai');
    var $column_order = array('id_bobot', 'kode_bobot', 'nama_bobot', 'nilai', null);
    var $order = array('id_bobot' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);


        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getAll()
    {
        $this->db->order_by('a.id_bobot desc');
        return $this->db->get('tbl_bobot a');
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_bobot');
        return $this->db->count_all_results();
    }

    function update_section($id, $data)
    {
        $this->db->where('id_bobot', $id);
        $this->db->update('tbl_bobot', $data);
    }

    function get_section($id)
    {
        $this->db->where('id_bobot', $id);
        return $this->db->get('tbl_bobot')->row();
    }

    function delete_section($id, $table)
    {
        $this->db->where('id_bobot', $id);
        $this->db->delete($table);
    }

    function kriteria()
    {
        return $this->db->order_by('id_kriteria ASC')
            ->get('tbl_kriteria')
            ->result();
    }





    function perbandingan()
    {
        $sql = "SELECT 
        kode,
        SK1,
        SK2,
        SK3,
        SK4,
        SK5
        FROM tbl_sub_kriteria";
        return $this->db->query($sql)->result();
    }

    function perbandingan_bobot()
    {
        return $this->db->order_by('id_bobot ASC')
            ->get('tbl_bobot')
            ->result();
    }

    function total1()
    {
        $query = $this->db->select_sum('SK1', 'SK1');
        $query = $this->db->get('tbl_sub_kriteria');
        $result = $query->result();
        return  $result[0]->SK1;
    }

    function total2()
    {
        $query = $this->db->select_sum('SK2', 'SK2');
        $query = $this->db->get('tbl_sub_kriteria');
        $result = $query->result();
        return  $result[0]->SK2;
    }

    function total3()
    {
        $query = $this->db->select_sum('SK3', 'SK3');
        $query = $this->db->get('tbl_sub_kriteria');
        $result = $query->result();
        return  $result[0]->SK3;
    }

    function total4()
    {
        $query = $this->db->select_sum('SK4', 'SK4');
        $query = $this->db->get('tbl_sub_kriteria');
        $result = $query->result();
        return  $result[0]->SK4;
    }

    function total5()
    {
        $query = $this->db->select_sum('SK5', 'SK5');
        $query = $this->db->get('tbl_sub_kriteria');
        $result = $query->result();
        return  $result[0]->SK5;
    }

    function total_bobot_1()
    {
        $query = $this->db->select_sum('nilai', 'nilai');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai;
    }

    function total_bobot_2()
    {
        $query = $this->db->select_sum('nilai2', 'nilai2');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai2;
    }

    function total_bobot_3()
    {
        $query = $this->db->select_sum('nilai3', 'nilai3');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai3;
    }

    function total_bobot_4()
    {
        $query = $this->db->select_sum('nilai4', 'nilai4');
        $query = $this->db->get('tbl_bobot');
        $result = $query->result();
        return  $result[0]->nilai4;
    }
}
