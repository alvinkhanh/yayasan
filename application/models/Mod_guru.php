<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_guru extends CI_Model
{
    var $table = 'tbl_guru a';
    var $column_search = array('a.id_guru', 'a.nama_guru', 'a.ttl', 'jenkel', 'jam_mengajar', 'a.status',  'a.created_at');
    var $column_order = array('id_guru', 'nama_guru', 'ttl', 'jenkel', 'jam_mengajar', 'status', 'created_at', null);
    var $order = array('id_guru' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($term = '')
    {

        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $guru) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($guru, $_POST['search']['value']);
                } else {
                    $this->db->or_like($guru, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_guru');
        return $this->db->count_all_results();
    }

    function insert_guru($table, $data)
    {
        return $this->db->insert($table, $data);
    }
    function update_guru($id, $data)
    {
        $this->db->where('id_guru', $id);
        $this->db->update('tbl_guru', $data);
    }

    function get_guru($id)
    {
        $this->db->where('id_guru', $id);
        return $this->db->get('tbl_guru')->row();
    }

    function getImage($id)
    {
        $this->db->select('gambar');
        $this->db->from('tbl_guru');
        $this->db->where('id_guru', $id);
        return $this->db->get();
    }


    function delete_guru($id, $table)
    {
        $this->db->where('id_guru', $id);
        $this->db->delete($table);
    }

    function guru()
    {
        $this->db->where('id_level', '4');
        return $this->db->order_by('full_name ASC')
            ->get('tbl_user')
            ->result();
    }
}
