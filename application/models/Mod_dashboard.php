<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_dashboard extends CI_Model
{
    var $table = 'tbl_murid';
    var $column_search = array('id_murid', 'nama_murid');
    var $column_order = array('id_murid', 'nama_murid');
    var $order = array('id_murid');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        $year = date('Y');
        $month = date('m');
        $this->db->select('id_murid, nama_murid');
        $this->db->where('status_aktif', 'Y');
        $this->db->where('id_murid NOT IN (SELECT b.id_siswa FROM list_pembayaran b INNER JOIN tbl_pembayaran c ON b.id_pembayaran = c.id_pembayaran WHERE b.periode = ' . $year . ' AND c.bulan= ' . $month . ' )', NULL, false);
        $this->db->order_by('nama_murid', 'ASC');
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_pembayaran');
        return $this->db->count_all_results();
    }

    public function jumlah_siswa()
    {
        $this->db->where('status =', '2'); // OTHER CONDITIONS IF ANY
        $this->db->from('tbl_murid'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function jumlah_semua_siswa()
    {
        $this->db->from('tbl_murid'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function jumlah_guru()
    {
        $this->db->where('status =', 'Y'); // OTHER CONDITIONS IF ANY
        $this->db->from('tbl_guru'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function siswa_sudah_bayar()
    {
        $format = '%Y-%m';
        $yearmonth = date('Y-m');
        $this->db->where('DATE_FORMAT(created_at, "' . $format . '") =', $yearmonth);
        $this->db->from('tbl_pembayaran'); //TABLE NAME
        return $this->db->count_all_results();
    }

    public function pemasukan_bulan_ini()
    {
        $format = '%Y-%m';
        $yearmonth = date('Y-m');
        $this->db->select('SUM(pembayaran) as total_pemasukan');
        $this->db->where('DATE_FORMAT(created_at, "' . $format . '") =', $yearmonth);
        $this->db->from('tbl_pembayaran'); // TABLE NAME
        $result = $this->db->get()->row();

        if ($result) {
            return $result->total_pemasukan;
        } else {
            return 0; // If there are no records, return 0 as the total sum.
        }
    }
}
