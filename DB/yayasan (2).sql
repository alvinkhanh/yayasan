-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2023 at 03:43 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yayasan`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

CREATE TABLE `aplikasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_owner` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `tlp` varchar(50) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `nama_aplikasi` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `copy_right` varchar(50) DEFAULT NULL,
  `versi` varchar(20) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `nama_owner`, `alamat`, `tlp`, `title`, `nama_aplikasi`, `logo`, `copy_right`, `versi`, `tahun`) VALUES
(1, '-', 'Bekasi', '08970207386', 'Aplikasi Yayasan', 'Yayasan', 'AdminLTELogo1.png', 'Copy Right &copy;', '1.0.0.0', 2023);

-- --------------------------------------------------------

--
-- Stand-in structure for view `laporan_1`
-- (See below for the actual view)
--
CREATE TABLE `laporan_1` (
`id_kegiatan` int(11)
,`category` char(5)
,`pemasukan` double
,`pengeluaran` double
,`bulan` int(11)
,`keterangan` mediumtext
,`saldo` double
,`created_at` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `laporan_2`
-- (See below for the actual view)
--
CREATE TABLE `laporan_2` (
`id_kegiatan` int(11)
,`category` char(5)
,`pemasukan` double
,`pengeluaran` double
,`saldo` double
,`bulan` int(11)
,`keterangan` mediumtext
,`created_at` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `laporan_3`
-- (See below for the actual view)
--
CREATE TABLE `laporan_3` (
`id_kegiatan` int(11)
,`category` char(5)
,`keterangan` mediumtext
,`pengeluaran` double
,`pemasukan` double
,`saldo` double
,`bulan` int(11)
,`created_at` datetime
,`periode` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `list_pembayaran`
-- (See below for the actual view)
--
CREATE TABLE `list_pembayaran` (
`id_pembayaran` int(11)
,`id_siswa` int(11)
,`id_user` int(11)
,`january` varchar(200)
,`february` varchar(200)
,`maret` varchar(200)
,`april` varchar(200)
,`mei` varchar(200)
,`juni` varchar(200)
,`july` varchar(200)
,`agustus` varchar(200)
,`septmber` varchar(200)
,`oktober` varchar(200)
,`november` varchar(200)
,`desember` varchar(200)
,`keterangan` text
,`periode` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `list_pengeluaran`
-- (See below for the actual view)
--
CREATE TABLE `list_pengeluaran` (
`id_pengeluaran` int(11)
,`created_at` datetime
,`nama_keterangan` varchar(255)
,`pengeluaran` varchar(255)
,`full_name` varchar(50)
,`bulan` int(11)
,`periode` int(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pemasukan`
-- (See below for the actual view)
--
CREATE TABLE `pemasukan` (
`id_pembayaran` int(11)
,`pemasukan` double
,`bulan` int(11)
,`keterangan` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pengeluaran`
-- (See below for the actual view)
--
CREATE TABLE `pengeluaran` (
`id_pengeluaran` int(11)
,`pengeluaran` double
,`nama_keterangan` varchar(255)
,`bulan` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_menu`
--

CREATE TABLE `tbl_akses_menu` (
  `id` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_menu`
--

INSERT INTO `tbl_akses_menu` (`id`, `id_level`, `id_menu`, `view_level`) VALUES
(1, 1, 1, 'Y'),
(2, 1, 2, 'Y'),
(43, 4, 1, 'Y'),
(44, 4, 2, 'Y'),
(64, 1, 52, 'Y'),
(65, 4, 52, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_submenu`
--

CREATE TABLE `tbl_akses_submenu` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N',
  `add_level` enum('Y','N') DEFAULT 'N',
  `edit_level` enum('Y','N') DEFAULT 'N',
  `delete_level` enum('Y','N') DEFAULT 'N',
  `print_level` enum('Y','N') DEFAULT 'N',
  `upload_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_submenu`
--

INSERT INTO `tbl_akses_submenu` (`id`, `id_level`, `id_submenu`, `view_level`, `add_level`, `edit_level`, `delete_level`, `print_level`, `upload_level`) VALUES
(2, 1, 2, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(4, 1, 1, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(6, 1, 7, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(7, 1, 8, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(9, 1, 10, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(13, 1, 14, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(26, 1, 15, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(30, 1, 17, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(32, 1, 18, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(34, 1, 19, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(36, 1, 20, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(59, 4, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(60, 4, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(61, 4, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(62, 4, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(63, 4, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(64, 4, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(65, 4, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(66, 4, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(67, 4, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(68, 4, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(84, 1, 24, 'Y', 'N', 'N', 'N', 'N', 'N'),
(85, 4, 24, 'N', 'N', 'N', 'N', 'N', 'N'),
(86, 1, 25, 'Y', 'N', 'N', 'N', 'N', 'N'),
(87, 4, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(88, 1, 26, 'Y', 'N', 'N', 'N', 'N', 'N'),
(89, 4, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(90, 1, 27, 'Y', 'N', 'N', 'N', 'N', 'N'),
(91, 4, 27, 'N', 'N', 'N', 'N', 'N', 'N'),
(92, 1, 28, 'Y', 'N', 'N', 'N', 'N', 'N'),
(93, 4, 28, 'N', 'N', 'N', 'N', 'N', 'N'),
(94, 1, 29, 'Y', 'N', 'N', 'N', 'N', 'N'),
(95, 4, 29, 'N', 'N', 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_guru`
--

CREATE TABLE `tbl_guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(225) NOT NULL,
  `ttl` varchar(20) NOT NULL,
  `jenkel` varchar(25) NOT NULL,
  `jam_mengajar` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_guru`
--

INSERT INTO `tbl_guru` (`id_guru`, `nama_guru`, `ttl`, `jenkel`, `jam_mengajar`, `status`, `id_user`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'Epoy', 'Pandeglang, 30-06-20', 'Laki - Laki', '08:00 s.d 15:00', 0, 1, 'epoy2.png', '2023-06-03 21:27:40', '2023-06-03 21:30:04'),
(2, 'ica', 'Bekasi, 20-september', 'Perempuan', '16:00 s.d 20:00', 0, 1, '', '2023-06-03 21:29:55', '2023-06-03 21:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kegiatan`
--

CREATE TABLE `tbl_kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_peng_pem` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `category` char(5) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kegiatan`
--

INSERT INTO `tbl_kegiatan` (`id_kegiatan`, `id_peng_pem`, `id_user`, `category`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'A', '2023-06-03 21:37:25', '2023-06-03 21:40:00'),
(2, 2, 1, 'A', '2023-06-03 21:41:24', '2023-06-03 21:41:24'),
(3, 1, 1, 'B', '2023-06-03 23:07:47', '2023-06-03 23:07:47'),
(4, 2, 1, 'B', '2023-06-03 23:08:24', '2023-06-03 23:08:24'),
(5, 3, 1, 'B', '2023-06-04 09:35:19', '2023-06-04 09:35:19'),
(6, 3, 1, 'A', '2023-06-04 09:37:58', '2023-06-04 09:37:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keterangan`
--

CREATE TABLE `tbl_keterangan` (
  `id_keterangan` int(11) NOT NULL,
  `nama_keterangan` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_keterangan`
--

INSERT INTO `tbl_keterangan` (`id_keterangan`, `nama_keterangan`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'Lain - Lain', 1, '2023-06-03 21:46:36', '2023-06-03 21:46:36'),
(2, 'Apa gtuh', 1, '2023-06-03 23:08:15', '2023-06-03 23:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` bigint(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `parent` enum('Y') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `urutan`, `is_active`, `parent`) VALUES
(1, 'Dashboard', 'dashboard', 'fas fa-tachometer-alt', 1, 'Y', 'Y'),
(2, 'System', '#', 'fas fa-cogs', 2, 'Y', 'Y'),
(52, 'Keuangan', '#', 'fas fa-cash-register', 3, 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_murid`
--

CREATE TABLE `tbl_murid` (
  `id_murid` int(11) NOT NULL,
  `nama_murid` varchar(150) NOT NULL,
  `nis` int(30) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `tanggal_lahir` varchar(250) DEFAULT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `status` int(10) NOT NULL DEFAULT 1,
  `status_aktif` varchar(4) NOT NULL DEFAULT 'Y',
  `tahun_ajaran` varchar(20) DEFAULT NULL,
  `guru` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_murid`
--

INSERT INTO `tbl_murid` (`id_murid`, `nama_murid`, `nis`, `kelas`, `tanggal_lahir`, `jenis_kelamin`, `status`, `status_aktif`, `tahun_ajaran`, `guru`, `image`, `created_at`, `update_at`) VALUES
(1, 'Alvin Khanh rishad', 23211626, 'Kelas 1', 'Pandeglang, 30 June 1996', 'Perempuan', 1, 'Y', '2011 - 2013', 1, 'alvin-khanh-rishad5.png', '2022-07-31 13:28:02', '2023-06-03 21:22:59'),
(2, 'Setya Rahmawati', 21160703, 'Kelas 1', 'Pandeglang, 30 June 1996', 'Laki - Laki', 1, 'Y', '2011 - 2013', 1, '', '2022-07-31 13:28:11', '2023-06-03 21:23:06'),
(3, 'DIla Fadilah', 23211621, 'Kelas 3', 'Pandeglang, 30 June 1996', 'Laki - Laki', 2, 'Y', '2011 - 2013', 2, 'dila-fadilah1.png', '2022-07-31 13:28:30', '2023-06-03 21:23:08'),
(4, 'Sarah Fardayati', 21160712, 'Kelas 4', 'Pandeglang, 30 June 1996', 'Laki - Laki', 2, 'Y', '2011 - 2013', 2, '', '2022-07-31 13:28:53', '2023-06-03 21:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pembayaran`
--

CREATE TABLE `tbl_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pembayaran` varchar(200) NOT NULL,
  `bulan` int(11) NOT NULL,
  `keterangan` text NOT NULL DEFAULT '\'Iuran\'',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pembayaran`
--

INSERT INTO `tbl_pembayaran` (`id_pembayaran`, `id_siswa`, `id_user`, `pembayaran`, `bulan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '50000', 5, '\'Iuran\'', '2023-05-03 21:37:25', '2023-06-03 23:16:22'),
(2, 2, 1, '50000', 6, '\'Iuran\'', '2023-06-03 21:41:24', '2023-06-03 21:41:24'),
(3, 4, 1, '50000', 6, '\'Iuran\'', '2023-06-04 09:37:58', '2023-06-04 09:37:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengeluaran`
--

CREATE TABLE `tbl_pengeluaran` (
  `id_pengeluaran` int(11) NOT NULL,
  `pengeluaran` varchar(255) NOT NULL,
  `id_keterangan` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengeluaran`
--

INSERT INTO `tbl_pengeluaran` (`id_pengeluaran`, `pengeluaran`, `id_keterangan`, `bulan`, `id_user`, `created_at`, `updated_at`) VALUES
(1, '30000', 1, 6, 1, '2023-06-03 23:07:47', '2023-06-03 23:07:47'),
(2, '20000', 2, 6, 1, '2023-06-03 23:08:24', '2023-06-03 23:08:24'),
(3, '10000', 1, 6, 1, '2023-06-04 09:35:19', '2023-06-04 09:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id_status` int(11) NOT NULL,
  `nama_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id_status`, `nama_status`) VALUES
(1, 'Belum Mendapat Penilaian'),
(2, 'Sudah Mendapat Penilaian');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `id_submenu` int(11) UNSIGNED NOT NULL,
  `nama_submenu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`id_submenu`, `nama_submenu`, `link`, `icon`, `id_menu`, `is_active`) VALUES
(1, 'Menu', 'menu', 'far fa-circle', 2, 'Y'),
(2, 'SubMenu', 'submenu', 'far fa-circle', 2, 'Y'),
(7, 'Aplikasi', 'aplikasi', 'far fa-circle', 2, 'Y'),
(8, 'User', 'user', 'far fa-circle', 2, 'Y'),
(10, 'User Level', 'userlevel', 'far fa-circle', 2, 'Y'),
(24, 'List Siswa', 'siswa', 'fas fa-user-graduate', 2, 'Y'),
(25, 'Input Pembayaran', 'input_pembayaran', 'fas fa-money-bill-wave', 52, 'Y'),
(26, 'List Guru', 'guru', 'fas fa-chalkboard-teacher', 2, 'Y'),
(27, 'Input Pengeluaran', 'input_pengeluaran', 'fas fa-money-bill-alt', 52, 'Y'),
(28, 'Keterangan', 'keterangan_pengeluaran', 'fas fa-info-circle', 2, 'Y'),
(29, 'Report Keuangan', 'laporan_keuangan', 'fas fa-clipboard-list', 52, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) UNSIGNED NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `full_name`, `password`, `id_level`, `image`, `is_active`) VALUES
(1, 'admin', 'Administrator', '$2y$05$I0.hry0NCGaj/sAaGySYBuepR9dCEC9vWOakmadAHY9Ai32es/742', 1, 'admin1.jpg', 'Y'),
(6, 'kodil', 'Dila Fadillah', '$2y$05$e5dQyYJ3233j9UxK4OjLfebN2AT/bg9stbbSKEdHXSWzSuh.kFyIS', 4, 'kodil.PNG', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_userlevel`
--

CREATE TABLE `tbl_userlevel` (
  `id_level` int(11) UNSIGNED NOT NULL,
  `nama_level` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_userlevel`
--

INSERT INTO `tbl_userlevel` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(4, 'Guru');

-- --------------------------------------------------------

--
-- Structure for view `laporan_1`
--
DROP TABLE IF EXISTS `laporan_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `laporan_1`  AS SELECT `a`.`id_kegiatan` AS `id_kegiatan`, `a`.`category` AS `category`, `b`.`pemasukan` AS `pemasukan`, `c`.`pengeluaran` AS `pengeluaran`, CASE WHEN `b`.`bulan` is null THEN `c`.`bulan` ELSE `b`.`bulan` END AS `bulan`, CASE WHEN `b`.`keterangan` is null THEN `c`.`nama_keterangan` ELSE `b`.`keterangan` END AS `keterangan`, CASE WHEN `a`.`category` = 'A' THEN `b`.`pemasukan` ELSE `b`.`pemasukan`- `c`.`pengeluaran` END AS `saldo`, `a`.`created_at` AS `created_at` FROM ((`tbl_kegiatan` `a` left join `pemasukan` `b` on(`a`.`id_peng_pem` = `b`.`id_pembayaran` and `a`.`category` = 'A')) left join `pengeluaran` `c` on(`a`.`id_peng_pem` = `c`.`id_pengeluaran` and `a`.`category` = 'B')) ;

-- --------------------------------------------------------

--
-- Structure for view `laporan_2`
--
DROP TABLE IF EXISTS `laporan_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `laporan_2`  AS SELECT `laporan_1`.`id_kegiatan` AS `id_kegiatan`, `laporan_1`.`category` AS `category`, `laporan_1`.`pemasukan` AS `pemasukan`, `laporan_1`.`pengeluaran` AS `pengeluaran`, `laporan_1`.`saldo` AS `saldo`, `laporan_1`.`bulan` AS `bulan`, `laporan_1`.`keterangan` AS `keterangan`, `laporan_1`.`created_at` AS `created_at` FROM `laporan_1` WHERE `laporan_1`.`bulan` is not null GROUP BY `laporan_1`.`id_kegiatan`, `laporan_1`.`category`, `laporan_1`.`pemasukan`, `laporan_1`.`pengeluaran`, `laporan_1`.`saldo`, `laporan_1`.`bulan`, `laporan_1`.`keterangan`, `laporan_1`.`created_at` ;

-- --------------------------------------------------------

--
-- Structure for view `laporan_3`
--
DROP TABLE IF EXISTS `laporan_3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `laporan_3`  AS SELECT `t1`.`id_kegiatan` AS `id_kegiatan`, `t1`.`category` AS `category`, `t1`.`keterangan` AS `keterangan`, `t1`.`pengeluaran` AS `pengeluaran`, `t1`.`pemasukan` AS `pemasukan`, (select sum(ifnull(`t2`.`pemasukan`,0) - ifnull(`t2`.`pengeluaran`,0)) from `laporan_2` `t2` where `t2`.`created_at` <= `t1`.`created_at`) AS `saldo`, `t1`.`bulan` AS `bulan`, `t1`.`created_at` AS `created_at`, year(`t1`.`created_at`) AS `periode` FROM `laporan_2` AS `t1` ORDER BY `t1`.`created_at` ASC ;

-- --------------------------------------------------------

--
-- Structure for view `list_pembayaran`
--
DROP TABLE IF EXISTS `list_pembayaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `list_pembayaran`  AS SELECT `tbl_pembayaran`.`id_pembayaran` AS `id_pembayaran`, `tbl_pembayaran`.`id_siswa` AS `id_siswa`, `tbl_pembayaran`.`id_user` AS `id_user`, CASE WHEN `tbl_pembayaran`.`bulan` = 1 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `january`, CASE WHEN `tbl_pembayaran`.`bulan` = 2 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `february`, CASE WHEN `tbl_pembayaran`.`bulan` = 3 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `maret`, CASE WHEN `tbl_pembayaran`.`bulan` = 4 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `april`, CASE WHEN `tbl_pembayaran`.`bulan` = 5 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `mei`, CASE WHEN `tbl_pembayaran`.`bulan` = 6 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `juni`, CASE WHEN `tbl_pembayaran`.`bulan` = 7 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `july`, CASE WHEN `tbl_pembayaran`.`bulan` = 8 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `agustus`, CASE WHEN `tbl_pembayaran`.`bulan` = 9 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `septmber`, CASE WHEN `tbl_pembayaran`.`bulan` = 10 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `oktober`, CASE WHEN `tbl_pembayaran`.`bulan` = 11 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `november`, CASE WHEN `tbl_pembayaran`.`bulan` = 12 THEN `tbl_pembayaran`.`pembayaran` ELSE 0 END AS `desember`, `tbl_pembayaran`.`keterangan` AS `keterangan`, year(`tbl_pembayaran`.`created_at`) AS `periode` FROM `tbl_pembayaran` GROUP BY `tbl_pembayaran`.`id_pembayaran`, `tbl_pembayaran`.`id_siswa`, `tbl_pembayaran`.`id_user` ;

-- --------------------------------------------------------

--
-- Structure for view `list_pengeluaran`
--
DROP TABLE IF EXISTS `list_pengeluaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `list_pengeluaran`  AS SELECT `a`.`id_pengeluaran` AS `id_pengeluaran`, `a`.`created_at` AS `created_at`, `b`.`nama_keterangan` AS `nama_keterangan`, `a`.`pengeluaran` AS `pengeluaran`, `c`.`full_name` AS `full_name`, `a`.`bulan` AS `bulan`, year(`a`.`created_at`) AS `periode` FROM ((`tbl_pengeluaran` `a` join `tbl_keterangan` `b` on(`a`.`id_keterangan` = `b`.`id_keterangan`)) join `tbl_user` `c` on(`a`.`id_user` = `c`.`id_user`)) ;

-- --------------------------------------------------------

--
-- Structure for view `pemasukan`
--
DROP TABLE IF EXISTS `pemasukan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pemasukan`  AS SELECT `a`.`id_pembayaran` AS `id_pembayaran`, sum(`a`.`pembayaran`) AS `pemasukan`, `a`.`bulan` AS `bulan`, `a`.`keterangan` AS `keterangan` FROM `tbl_pembayaran` AS `a` GROUP BY `a`.`id_pembayaran`, `a`.`bulan`, `a`.`keterangan` ;

-- --------------------------------------------------------

--
-- Structure for view `pengeluaran`
--
DROP TABLE IF EXISTS `pengeluaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pengeluaran`  AS SELECT `a`.`id_pengeluaran` AS `id_pengeluaran`, sum(`a`.`pengeluaran`) AS `pengeluaran`, `b`.`nama_keterangan` AS `nama_keterangan`, `a`.`bulan` AS `bulan` FROM (`tbl_pengeluaran` `a` join `tbl_keterangan` `b` on(`a`.`id_keterangan` = `b`.`id_keterangan`)) GROUP BY `a`.`id_pengeluaran`, `b`.`nama_keterangan`, `a`.`bulan` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_guru`
--
ALTER TABLE `tbl_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `tbl_kegiatan`
--
ALTER TABLE `tbl_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `tbl_keterangan`
--
ALTER TABLE `tbl_keterangan`
  ADD PRIMARY KEY (`id_keterangan`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_murid`
--
ALTER TABLE `tbl_murid`
  ADD PRIMARY KEY (`id_murid`);

--
-- Indexes for table `tbl_pembayaran`
--
ALTER TABLE `tbl_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tbl_pengeluaran`
--
ALTER TABLE `tbl_pengeluaran`
  ADD PRIMARY KEY (`id_pengeluaran`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tbl_userlevel`
--
ALTER TABLE `tbl_userlevel`
  ADD PRIMARY KEY (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplikasi`
--
ALTER TABLE `aplikasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `tbl_guru`
--
ALTER TABLE `tbl_guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_kegiatan`
--
ALTER TABLE `tbl_kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_keterangan`
--
ALTER TABLE `tbl_keterangan`
  MODIFY `id_keterangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `tbl_murid`
--
ALTER TABLE `tbl_murid`
  MODIFY `id_murid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_pembayaran`
--
ALTER TABLE `tbl_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_pengeluaran`
--
ALTER TABLE `tbl_pengeluaran`
  MODIFY `id_pengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  MODIFY `id_submenu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_userlevel`
--
ALTER TABLE `tbl_userlevel`
  MODIFY `id_level` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
